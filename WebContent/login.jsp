<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>登录</title>
<meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
<meta name="description" content="Monster Dashboard - Multiskin Premium Admin Dashboard">
<meta name="author" content="MonsterDashboard">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Font CSS (Via CDN) -->
<!-- <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
 -->
<!-- Theme CSS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/private/main/css/theme.css" />
<!-- Admin Forms CSS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/private/main/css/admin-forms.css" />

<!-- Favicon -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/public/css/xcConfirm.css" />
<link rel="shortcut icon" href="${pageContext.request.contextPath}/static/image/favicon.ico">
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- CanvasBG Plugin(creates mousehover effect) -->
<script src="${pageContext.request.contextPath}/static/public/js/canvasbg.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/static/private/main/js/theme/utility.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/private/main/js/theme/demo.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/private/main/js/theme/main.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/xcConfirm.js"></script>

</head>

<body class="external-page sb-l-c sb-r-c" style="overflow: hidden;">

	<!-- Start: Main -->
	<div id="main" class="animated fadeIn">

		<!-- Start: Content-Wrapper -->
		<section id="content_wrapper">

			<!-- begin canvas animation bg -->
			<div id="canvas-wrapper">
				<canvas id="demo-canvas"></canvas>
			</div>

			<!-- Begin: Content -->
			<section id="content">

				<div class="admin-form theme-info mt10" id="login1" style="width: 500px;">

					<div class="row mb15 table-layout" style="margin-top:200px;">

						<div class="col-xs-6 va-m pln">
							<a href="#" title="Return to Dashboard">
								<img src="${pageContext.request.contextPath}/static/image/logo_white.png" title="Templatemonster Logo" class="img-responsive w250">
							</a>
						</div>

						<div class="col-xs-6 text-right va-b pr5">
							<div class="login-links">
								<a href="#" class="active" title="Sign In">Sign In</a>
							</div>

						</div>

					</div>

					<div class="panel panel-info mt10 br-n">

						<!-- end .form-header section -->
						<form method="post" action="" id="contact">
							<div class="panel-body bg-light p30">
								<div class="row">
									<div class="col-sm-12 pr30">
										<div class="section">
											<label for="username" class="field-label text-muted fs18 mb10">Username</label> <label for="username" class="field prepend-icon"> <label for="username" class="field-icon"> <i class="fa fa-user"></i>
											</label> <input type="text" name="username" id="username" class="gui-input" placeholder="Enter username">
											</label>
										</div>
										<!-- end section -->

										<div class="section">
											<label for="password" class="field-label text-muted fs18 mb10">Password</label> <label for="password" class="field prepend-icon"> <label for="password" class="field-icon"> <i class="fa fa-lock"></i>
											</label> <input type="password" name="password" id="password" class="gui-input" placeholder="Enter password">
											</label>
										</div>
										<!-- end section -->

									</div>
								</div>
							</div>
							<!-- end .form-body section -->
							<div class="panel-footer clearfix p10 ph15">
								<button id="loginBtn" type="button" class="button btn-primary mr10 pull-right">Sign In</button>
								<label class="switch ib switch-primary pull-left input-align mt10"> <input type="checkbox" name="remember" id="remember" checked> <label for="remember" data-on="YES" data-off="NO"></label> <span>Remember me</span>
								</label>
							</div>
						</form>
					</div>
				</div>
			</section>
		</section>
	</div>
<script type="text/javascript">
  $(document).ready(function() {

    Core.init();
    Demo.init();

    CanvasBG.init({
      Loc: {
        x: window.innerWidth / 2,
        y: window.innerHeight / 3.3
      },
    });

    $("#loginBtn").click(function(){
    	var usernName = $("#username").val().trim();
		var password = $("#password").val();
		if(usernName == '' || password == ''){
			window.wxc.xcConfirm("用户名或密码错误！","error");
			return;
		}
		var url = "${pageContext.request.contextPath}/login/loginAction.do?method=loginSystem";
		var params = {
				"userName":usernName,
				"passWord":password
		};
		$.ajax({
			url:url,
			type:"post",
			data:params,
			async:false,
			success:function(data){
				var json = JSON.parse(data);
				if(json.status == 1){
					window.location.href = "${pageContext.request.contextPath}/systemAction/systemAction.do?method=targetMain";
				}else{
					window.wxc.xcConfirm("登录失败","error");
				}
			}
		});
    });
  });
</script>
</body>

</html>
