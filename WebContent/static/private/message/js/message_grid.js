/**
 *每一行的数据放在tr上的，可以用table_id得到
 *每一个td中都有一个div,都有相同的name,不同的id,
 *第一个都由一个checkdbox,name都是有规则的
 *
 *
 */
(function($) { 
	//组件加载数据，只是单纯的加载数据
	function loadPageContent(msg,table_title,tbodyId,opts){ 
		var checkboxId = opts.checkboxId; 
		if(table_title&&table_title.length>0&&tbodyId&&tbodyId.length>0){   
			if(msg&&msg.list&&msg!="null" && msg!=null){
					
				      //----------------先解决cooking中的width设置问题，---以下--------------------
				      var cooktablewidth = getCookie(opts.tbodyId+"_width"); 
				      if(cooktablewidth == ''){
				    	  cooktablewidth = [];
					  }else{
						  //cooktablesort = cooktablesort.split(",");
						  cooktablewidth = eval(cooktablewidth); 
					  }
				      if(cooktablewidth.length>=0){
				    	  for(var i=0;i<cooktablewidth.length;i++){ 
				    		  var  sdft = cooktablewidth[i];
				    		  var thf1b = $("#"+table_title).find("th[fname="+sdft.th+"]");
				    		  if(thf1b.length>0){
				    			  thf1b.attr("width",sdft.width);
				    		  }
				    	  }
				      } 
				    //----------------先解决cooking中的width设置问题，---以上--------------------
				      //----------------先解决cooking中的排序设置问题，---以下--------------------
					  var tbody="";
					  //得到cook中的序列  解决cook的显示问题
					  var cooktablesort = getCookie(opts.tbodyId+"_sort"); 
					  if(cooktablesort == ''){
						  cooktablesort = [];
					  }else{
						  //cooktablesort = cooktablesort.split(",");
						  cooktablesort = eval(cooktablesort); 
					  }
					//console.log("排序为:"+cooktablesort); 
					var bodyth = $("#"+table_title).children().clone();
					var arrayth = [];
					arrayth.push(bodyth[0]);
					arrayth.push(bodyth[1]);
					for(var j=0;j<cooktablesort.length;j++){
						for(var i =2;i<bodyth.length;i++){
				 			var tthfname = $(bodyth[i]).attr("fname"); 
							 if(tthfname  == cooktablesort[j]){ 
								 arrayth.push(bodyth[i]);
							 }
						}
					}   
					if(arrayth.length<=2){
						tfile= $("#"+table_title).children(); //
					}else{
						tfile = arrayth;
					} 
					$("#"+table_title).html("");
					$("#"+table_title).append(tfile);
					//计算排序完成  解决cook的显示问题结束
					//----------------先解决cooking中的排序设置问题，---以下--------------------
					var skipTdNum = 0;
					if(tfile.length<=0){ 
						return;
					}
					$("#"+tbodyId).html(""); 
					var widthSum =0;
					var cwidth =$("#"+tbodyId).parent().parent().width(); 
					for(var i=0;i<tfile.length;i++){  
						$(tfile[i]).css("font-weight","bold");
						if(i<=1){
							continue;
						} 
						//第一次的事情没有图标
						if($(tfile[i]).children().length<=0){
							var sorticon = $('<span class="fa fa-sort-down" style="margin-left:7px;"></span>');
							$(tfile[i]).append(sorticon); 
							var fname1 = $(tfile[i]).attr("fname"); 
							sorticon.unbind().click({fname:fname1},function(event){
								var fbc1 = $(this).hasClass("fa fa-sort-down");
								var fname = event.data.fname;
								var orderbyfname ="";
								if(fbc1 == 1){
									$(this).removeClass("fa fa-sort-down").addClass("fa fa-sort-up");
									orderbyfname = ""+ fname + " desc ";
								}else{
									$(this).removeClass("fa fa-sort-up").addClass("fa fa-sort-down");
									orderbyfname = ""+ fname + " asc ";
								} 
								opts.data.desc = orderbyfname;
								opts.data.start=1;
								loadPageData(opts);
							});
						}else{
							//当点击分页和其他操作，thead当中的th被clone过来，已经有了图标，只需要事件
							var fname1 = $(tfile[i]).attr("fname");
							$(tfile[i]).children().click({fname:fname1},function(event){
								var fbc1 = $(this).hasClass("fa fa-sort-down");
								var fname = event.data.fname;
								var orderbyfname ="";
								if(fbc1 == 1){
									$(this).removeClass("fa fa-sort-down").addClass("fa fa-sort-up");
									orderbyfname = ""+ fname + " desc ";
								}else{
									$(this).removeClass("fa fa-sort-up").addClass("fa fa-sort-down");
									orderbyfname = ""+ fname + " asc ";
								} 
								opts.data.desc = orderbyfname;
								opts.data.start=1;
								loadPageData(opts);
							});
						}
						
					} 
					//给表格增加数据
					for(var i=0;i<msg.list.length;i++){ 
						var vb = msg.list[i]; 
						var tr;
						if(vb["table_id"]){
							tr= $('<tr name="'+tbodyId+'_tr" id="'+vb.table_id+'" trId='+vb.table_id+'></tr>');
						}else{
							tr= $('<tr name="'+tbodyId+'_tr" id="'+vb.id+'" trId='+vb.id+'></tr>');
						}
						var td =$('<td width="70" style="display:none;text-align:center;" name="'+tbodyId+"_tr_td_"+(i+1)+'"><div style="width:30px;">'+(i+1)+'</div></td>');
						if($(tfile[0]).css("display") =="none"){ 
							tr.append(td);
						}else{
							tr.append(td);
							td.show();
						} 
						 if(opts.checkbox==true){
							 var tdck =$('<td width="40" style="display:none;text-align:center;"><input value='+vb.table_id+' type="checkbox" name="'+tbodyId+'_checkbox" id="'+tbodyId+'_checkbox"></td>');
							 if($(tfile[1]).css("display") =="none"){ 
								 tr.append(tdck);
							 }else{
								 tr.append(tdck);
								 tdck.show();
							 }
						 } else{
							 skipTdNum = 1;
						 }
						 //如果选择了checkbox==false,一定要把th上面的chebox项目删除掉，不然就要出问题
						 for(var j=0;j<tfile.length;j++){
							  	if(j<= ( 1 - skipTdNum )){
							  		continue;//因为第一行和第二行是序号和checkecbox
							  	}
							  	var tfj =$(tfile[j]);
							    var b = false;
								for(var tc in vb){ 
									if(tfj.attr('fname')==tc){
										b =true;
										var tc =tfj.attr('fname');
										var width = tfj.attr("width"); 
										if(tc == 'fromUser' || tc == 'toUser'){
											var ctd = $('<td><div  id="'+tbodyId+"_"+tc+"_tr_td_"+(i+1)+'" name="'+tbodyId+"_"+tc+"_tr_td"+'" style="width:'+width+'px;margin:auto;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;" title="'+vb[tc].userName+'">'+vb[tc].userName+'</div></td>');
										}
										else if(tc == 'message_status'){
											if(vb[tc] == '1'){
												var ctd = $('<td><div  id="'+tbodyId+"_"+tc+"_tr_td_"+(i+1)+'" name="'+tbodyId+"_"+tc+"_tr_td"+'" style="width:'+width+'px;margin:auto;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;" title="'+vb[tc]+'">'+'已读'+'</div></td>');
											}else{
												var ctd = $('<td><div  id="'+tbodyId+"_"+tc+"_tr_td_"+(i+1)+'" name="'+tbodyId+"_"+tc+"_tr_td"+'" style="width:'+width+'px;margin:auto;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;" title="'+vb[tc]+'">'+'未读'+'</div></td>');
											}
										}
										else{
											var ctd = $('<td><div  id="'+tbodyId+"_"+tc+"_tr_td_"+(i+1)+'" name="'+tbodyId+"_"+tc+"_tr_td"+'" style="width:'+width+'px;margin:auto;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;" title="'+vb[tc]+'">'+vb[tc]+'</div></td>');
										}
										if(tfj.css("display")=="none"){
											ctd.hide();
										}else{ 
											if(opts.isEnter == true){  
												ctd.children().css({"white-space":"pre-wrap"});
												ctd.children().css({"word-wrap":"break-word"});
											} 
										} 
										tr.append(ctd);
										  
									}
								}
								if(b==false){//如果字段不存在，就加空字段 
									tr.append($("<td></td>"));
								}
						 }
						 
						//tbody+=tr; 
						
						$("#"+tbodyId).append(tr);
						$("tr[trId="+vb.table_id+"]").data(vb.table_id,vb); 
						
					}
					//表格的checkbox的代码
					if(opts.checkbox==true){
						$("tr[name="+tbodyId+"_tr]").click(function(){
							var mark = 0;//1:全选，2：全未选
							var obj = $(this).find("input[type=checkbox]").eq(0);
							var flag = false;
							if($(obj).prop("checked")){
								$(obj).prop("checked",false); 
							}else{
								$(obj).prop("checked",true);
								flag = true;
							}
							//判断当前是否全部被选中
							var $_obj = $("#"+tbodyId).find("input[type=checkbox]");
							var checkBoxSum = $_obj.length;
//							console.log(checkBoxSum);
								$_obj.each(function(){
									if($(this).prop("checked")){
										mark = parseInt(mark) + 1
									}else{
										mark = parseInt(mark) - 1
									}
								});
//							console.log("mark: "+Math.abs(parseInt(mark)));
								
							if(parseInt(checkBoxSum) == Math.abs(parseInt(mark))){
								if($(obj).prop("checked")){
									$("#"+checkboxId).prop("checked",true);
								}else{
									$("#"+checkboxId).prop("checked",false);
								}
							}else{
								$("#"+checkboxId).prop("checked",false);
							}
							opts.trFn(this);
						}); 
						$("#"+checkboxId).click(function(){
							var $_childrenCheckBox = $("#"+tbodyId).find("input[type=checkbox]");
							$_childrenCheckBox.each(function(){
								if($("#"+checkboxId).prop("checked")){
									$(this).prop("checked",true);
								}else{
									$(this).prop("checked",false);
								}
							});
							
						});
						$("tr[name="+tbodyId+"_tr]").find("input[type=checkbox]").eq(0).click(function(){
							if($(this).prop("checked")){
								$(this).prop("checked",false);
							}else{
								$(this).prop("checked",true);
							}
						});
					}
				}
			
			}
	};
	//点击事件 li点击事件
	function LiLister(event){
		if(!$(this).is(".currentPageMsg")){
			var msg =event.data[0];
			var pagination=event.data[1];
			var data=event.data[2];
			var opts=event.data[3];
			var tvalue=$(this).attr("page-num");/*
			$("#"+pagination).find(".active").toggleClass("active");
			$(this).toggleClass("active");*/
			opts.data.start=$(this).text();
			if(tvalue == '<' || tvalue == '<<' || tvalue == '>' || tvalue == '>>'){
				loadPage($("#"+opts.tbodyId).data("data"),pagination,tvalue,data,opts);
				return;
			}
			$("#showDetailsThId_"+opts.table_title).remove();
			loadPageData(opts);
			reloadImgColunm(opts);
			 reloadTableTdCss(opts);
			 showTrDetails(opts);
			loadPage($("#"+opts.tbodyId).data("data"),pagination,tvalue,data,opts);
		}
			
	};
	function loadPageData(opts){
		 $.ajax({
		   type: "POST",
		   url: opts.url,
		   async:false,
		   data:opts.data,
		   dataType:"json",
		   success: function(msg){ 
			     $("#"+opts.tbodyId).data("data",msg);
			     console.log("表格重新加载数据");
				 loadPageContent(msg,opts.table_title,opts.tbodyId,opts);
				 //执行成功的方法
				  opts.success(msg);
				 //findText(opts.data.start,opts.pagination);
				 //选中已经有page
				 //loadPage(msg,opts.pagination,"<<",opts.data,opts);
		   }
		});
	}
	//给丛新加事件的li加css选中效果
	function findText(flag,pagination){
					for(var i=0;i< $("#"+pagination).children().length;i++){
						var ab = $("#"+pagination).children()[i];
						if($(ab).text()==flag){
								$("#"+pagination).find(".active").toggleClass("active");
								$(ab).toggleClass("active");
								break;
						}
					}
	};

	//生成分页
	function loadPage(msg,pagination,flag,data,opts){
		//console.log(msg);
				 if(msg=="null" || msg=="null" || msg==null){
			    	 return;
			     }
				 
				var showIndex = 7;//显示的页码数
		//		var allPage =parseInt(msg.count%msg.sum)==0?parseInt(msg.count/msg.sum):parseInt(msg.count/msg.sum+1);
				var allPage = msg.count;
//				console.log("allPage: "+allPage);
				var mypage =parseInt($("#"+pagination).attr("cpage"));
			    if(!mypage){
			    	mypage=1;
			    }
				var ULContent="";
				var startPageNum = 0;
				var endPageNum = 0;
				var maxCpage = parseInt(allPage % showIndex == 0 ? 
						parseInt(allPage/showIndex) : parseInt(allPage/showIndex + 1));
				/**
				 * 计算显示页码数的开始与结束
				 * **/
				if(flag === "" || flag == "<<"){
					startPageNum = 1;
					$("#"+pagination).attr("cpage",1);
				}else if(flag == "<"){
					if(mypage == 1){
						startPageNum = 1;
						$("#"+pagination).attr("cpage",1);
					}else{
						startPageNum = (mypage - 2)  * showIndex + 1;
						$("#"+pagination).attr("cpage",parseInt(mypage) - 1);
					}
				}else if(flag == ">" || flag == "..."){
					if(allPage > (mypage * showIndex)){
						startPageNum = mypage  * showIndex + 1;
						$("#"+pagination).attr("cpage",parseInt(mypage) + 1);
					}else{
						startPageNum = (mypage - 1) * showIndex + 1;
						$("#"+pagination).attr("cpage",maxCpage);
					}
				}else if(flag == ">>"){
					if(allPage % showIndex){
						startPageNum = allPage - allPage % showIndex + 1;
					}else{
						startPageNum = allPage -  showIndex + 1; 
					}
					$("#"+pagination).attr("cpage",maxCpage);
				}else if(parseInt(flag)){
					startPageNum = (mypage - 1) * showIndex + 1;
					$("#"+pagination).attr("cpage",parseInt(mypage));
				}
				
				if(startPageNum + showIndex > allPage){
					endPageNum = allPage;
				}else{
					endPageNum = startPageNum + showIndex - 1;
				} 
				ULContent ='<li page-num="<<" class="page-item" f="1"><a  class="page-link" href="javascript:;"" value="1" id="'+pagination+'pali1"><<</a></li>';
				ULContent +='<li page-num="<" class="page-item"  f="1"><a  class="page-link" href="javascript:;"" value="up" id="'+pagination+'pali2"><</a></li>';
				
				for(var i = parseInt(startPageNum);i <= parseInt(endPageNum);i++){
					 ULContent+='<li page-num="'+i+'" class="page-item" ><a  class="page-link" href="javascript:;" name="'+pagination+'pali'+'" id="'+pagination+'pali3">'+i+'</a></li>';
				}
				/***计算后置省略号**/
				if(endPageNum < allPage){
					 ULContent+='<li class="page-item" page-num="..." ><a  class="page-link" href="javascript:;" name="'+pagination+'pali'+'" id="'+pagination+'pali4">...</a></li>'
				}
				
				ULContent+='<li page-num=">" f="1"  class="page-item" ><a class="page-link" href="javascript:;"" value="down">></a></li>';
				ULContent+='<li page-num=">>" f="1" class="active page-item"><a  class="page-link" href="javascript:;" value="1">>></a></li>';
				ULContent+='<li  f="1" class="disabled page-item currentPageMsg"><a  class="page-link" href="javascript:;">当前查询共【'+msg.sum+'】条数据</a></li>';
				
				$("#"+pagination).html(ULContent);//改入页面
				$("#"+pagination).children().click([msg,pagination,data,opts],LiLister);
				findText(flag,pagination);
				 
				//增加显示和隐藏列的功能
				var btnshowlist  = $('<button type="button" class="btn btn-default" style="margin-left:5px;"  title="设置表格列隐藏"><i class="fa fa-list-alt"></i></button>');
				btnshowlist.attr("opts",opts);
				$("#"+pagination).append(btnshowlist); 
				btnshowlist.click({opts:opts},function(event){
					//console.log(event.data.opts);
					isColunmShow(opts);
				});
				//增加列全显示和折叠显示
				var tdshowContent  = $('<button type="button" class="btn btn-default" style="margin-left:5px;"  title="设置表格列数据显示折行"><i class="fa fa-circle"></i></button>');
				$("#"+pagination).append(tdshowContent); 
				tdshowContent.click({opts:opts},function(event){
					//console.log(event.data.opts);
					 var body_id =event.data.opts.tbodyId;
					 var f = $(this).find("i").hasClass("fa fa-circle");
					  if(f==1){
						  $("#"+body_id).find("td").children().css({"white-space":"pre-wrap"}).css({"word-wrap":"break-word"});
						  $(this).find("i").removeClass("fa fa-circle").addClass("fa fa-circle-o");
					  }else{
						  $("#"+body_id).find("td").children().css({"white-space":null}).css({"word-wrap":null});
						  $(this).find("i").removeClass("fa fa-circle").addClass("fa fa-circle");
						  $("#"+body_id).find("td").children().css({"white-space":"nowrap"}).css({"word-wrap":null});
						   
					  }
				});
				//增加排序的button
				var sortbutton  = $('<button type="button" class="btn btn-default" style="margin-left:5px;"  title="设置表格列排序"><i class="fa fa-list-ul"></i></button>');
				$("#"+pagination).append(sortbutton); 
				
				sortbutton.click({opts:opts},function(event){ 
					sortTablelgrid(event.data.opts);
				});
				//增加设置列宽的button
				var colWidthButton  = $('<button type="button" class="btn btn-default" style="margin-left:5px;" title="设置表格列宽"><i class="fa fa-minus-square-o"></i></button>');
				$("#"+pagination).append(colWidthButton); 
				
				colWidthButton.click({opts:opts},function(event){ 
					setColWidth(event.data.opts);
				});
				
				
	};
	//初始化
	function init(_this,opts) {
		 $.ajax({
		   type: "POST",
		   url: opts.url,
		   data:opts.data,
		   dataType:"json",
		   success: function(msg){
			   	  $("#showDetailsThId_"+opts.table_title).remove(); 
			      $("#"+opts.tbodyId).data("data",msg);
//			      console.log($("#"+opts.tbodyId).data("data"));
//			      console.log(opts.tbodyId);
			   	 if(!msg.list || msg.list.length<=0){
			   		// alert(msg.msg);
			   		 $("#"+opts.tbodyId).html("");
			   		 $("#"+opts.pagination).html("");
			   		 opts.success(msg);
			   		 return;
			   	 }
			     if(msg=="null" || msg=="null"){
			    	 return;
			     }
				 loadPageContent(msg,opts.table_title,opts.tbodyId,opts);
				
				 if(opts.page == true){
					 loadPage(msg,opts.pagination,"<<",opts.data,opts);
					 findText(opts.data.start,opts.pagination);
				 } 
				 reloadImgColunm(opts);
				 reloadTableTdCss(opts); 
				 //设置详细信息
				 showTrDetails(opts);
				 // $("#"+$(_this).attr("id")).colResizable();
				 //调用隐藏列
				 setCookingShow(opts);  
				 
				 
				//以下代码为控制滚动条的，只是此项目独有的
				var pcouscrllbarid =$("#"+opts.tbodyId).parent().parent().attr("id");
				//检查组件是否被初始化过 ，开始加载计算过后的滚动条。滚动条完成过后，还可以继续控制滚动条的高度和宽度，以此在success调用的方法当中继续设定
				//高度和宽庋
				if(pcouscrllbarid == null || pcouscrllbarid.indexOf("mCSB")<=-1 || pcouscrllbarid =="undefined"){
					$("#"+opts.tbodyId).parent().parent().height($(window).height()-215);
					$("#"+opts.tbodyId).parent().parent().mCustomScrollbar({
						axis:"yx", 
						advanced:{
							autoExpandHorizontalScroll:true
						},
						autoHideScrollbar:true,
						theme:"rounded-dark"
					});
				} 
				//必须将最后一个宽度  
				 opts.success(msg);
		   }
		});
	};

	function setColWidth(opts){
		$("#table_mode_colWidth").remove();
		if($("#table_mode_colWidth").length==0){ 
			var modal ='<div id="table_mode_colWidth" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">';
				modal+='<div class="modal-dialog modal-sm" role="document">';
				modal+='<div class="modal-content" style="width:400px;"><div class="modal-header">设置表格列宽度</div><div class="admin-form modal-body" style="width:380px;height:500px;overflow:auto;" id="table_mode_colWidth_content" style="padding-left:10px;"></div>'; 
			    modal+='<div class="modal-footer"><button type="button" id="saveColWidth" class="btn btn-primary">保存设置</button></div></div>';
			    modal+='</div>';
			    modal+='</div>';
			$("body").append(modal);
			$('#table_mode_colWidth').modal();
			$('#table_mode_colWidth').modal({ keyboard: false }); 
		}  
		var bodyth = $("#"+opts.tbodyId).prev().clone(); 
		$("#table_mode_colWidth_content").html("");  
		var bodyth = $("#"+opts.tbodyId).prev().clone(); 
		function getosortCK(i,colName,fname,fwidth){  
			var divcol = '<div class="form-group"><label for="'+fname+'" class="col-sm-4 control-label"><p style="width:100%;text-overflow:ellipsis;overflow:hidden;"><nobr>'+colName+'</nobr></p></label><div class="col-sm-8"><input type="text" class="form-control" value="'+fwidth+'" style="width:150px;" fname="'+fname+'" placeholder="列宽度为数字"></div></div>';
			return $(divcol);
		}  
		var formdiv = $('<div class="form-horizontal" id="setTableModeColWidth"></div>');
		for(var i =0 ;i<bodyth.children().children().length;i++){
			  if(i<=1){
				  continue;
			  }
			  var th = bodyth.children().children()[i];
			  var colName  =$(th).text();
			  var fname =$(th).attr("fname");
			  var fwidth =$(th).attr("width");
			  formdiv.append(getosortCK(i,colName,fname,fwidth)); 
		} 
		 $("#table_mode_colWidth_content").append(formdiv);
		 $("#table_mode_colWidth_content").append("<br> <font color=red>温馨提示：当表格列太少，或者宽度低于显示屏幕宽度的时，设置表格列宽度无效，因为在宽度充裕的时，系统会自动运算最佳显示效果。</font>");
		 $("#saveColWidth").click({opts:opts},function(event){
			 var opts = event.data.opts;
			 var listText = $("#setTableModeColWidth").find("input");
			 var arrayWidth = new Array();
			 for(var i=0;i<listText.length;i++){ 
				 var fname = $(listText[i]).attr("fname");
				 var fwidth =  $(listText[i]).val();
				 var thc = $("#"+opts.tbodyId).prev().find("th[fname="+fname+"]"); 
				 thc.attr("width",fwidth);
				 var obj = new Object();
				 obj.th = fname;
				 obj.width = fwidth;
				 arrayWidth.push(obj);
			 }
			 setCookie(opts.tbodyId+"_width",obj2str(arrayWidth),365);  
			 $('#table_mode_colWidth').modal("hide");
		 });
	}
	//设置cooks中的列不显示
	function setCookingShow(opts){ 
		var cooktable = getCookie(opts.tbodyId); 
		  if(cooktable == ''){
			  cooktable={};
		  }else{
			  cooktable = eval("(" + cooktable + ")"); 
		  }
		 //console.log(cooktable);
		var bodyth = $("#"+opts.tbodyId).prev(); 
		//console.log(bodyth.children().children());
		for(var i =0 ;i<bodyth.children().children().length;i++){ 
			  var th = $("#"+opts.tbodyId).prev().find("tr").children()[i];
			  if(cooktable["ckcol"+i] == 0){ 
				  $(th).hide();
			  }
		}
		for(var i=0;i<$("#"+opts.tbodyId).find("tr").length;i++){
			  var temptr  = $("#"+opts.tbodyId).find("tr")[i]; 
			  for(var j=0;j<$(temptr).children().length;j++){
				  if(cooktable["ckcol"+j] == 0){ 
					  var td  = $(temptr).children()[j];
					  $(td).hide();
				  } 
			  } 
		}
	}
	//排序的功能
	function sortTablelgrid(opts){
		$("#table_mode_sortTable").remove();
		if($("#table_mode_sortTable").length==0){ 
			var modal ='<div id="table_mode_sortTable" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">';
				modal+='<div class="modal-dialog modal-sm" role="document">';
				modal+='<div class="modal-content"><div class="modal-header">设置表格列显示顺序</div><div class="admin-form modal-body" id="table_mode_sortTable_content" style="padding-left:10px;"></div>'; 
			    modal+='<div class="modal-footer"><button type="button" id="saveSort" class="btn btn-primary">保存顺序</button></div></div>';
			    modal+='</div>';
			    modal+='</div>';
			$("body").append(modal);
			$('#table_mode_sortTable').modal();
			$('#table_mode_sortTable').modal({ keyboard: false }); 
		}  
		var bodyth = $("#"+opts.tbodyId).prev().clone(); 
		$("#table_mode_sortTable_content").html("");  
		var bodyth = $("#"+opts.tbodyId).prev().clone(); 
		function getosortCK(i,colName,fname){ 
			//return $('<label class="block mt15 option option-primary"><input type="checkbox" name="checked" value="checked" checked=""><span class="checkbox"></span>Checked</label>');			
			return $('<li><button type="button" class="btn btn-dark" style="margin-top:4px;" fname= "'+fname+'">'+colName+'</button></li>');
		} 
		var ol = $("<ol id='sort_ol'></ol>"); 
		for(var i =0 ;i<bodyth.children().children().length;i++){
			  if(i<=1){
				  continue;
			  }
			  var th = bodyth.children().children()[i];
			  var colName  =$(th).text();
			  var fname =$(th).attr("fname");
			  ol.append(getosortCK(i,colName,fname));
		}
		$("#table_mode_sortTable_content").append(ol);
		$("#sort_ol").sortable(); 
		$("#saveSort").click({opts:opts},function(event){
			var opts =event.data.opts;
			var allli = $("#sort_ol").children();
			var obj = [];
			for(var i=0;i<allli.length;i++){
				var fname = $($(allli[i]).children()[0]).attr("fname");
				obj[i] = fname; 
			} 
			setCookie(opts.tbodyId+"_sort",obj2str(obj),365);  
			$('#table_mode_sortTable').modal("hide");
			loadPageData(opts);
		}); 
	}
	//是否启用显示字段
	function isColunmShow(opts){ 
		//如果有这个modal就显示，不然就加载一个新 
		$("#table_mode_colunm").remove();
		if($("#table_mode_colunm").length==0){ 
			var modal ='<div id="table_mode_colunm" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">';
				modal+='<div class="modal-dialog modal-sm" role="document">';
				modal+='<div class="modal-content"><div class="modal-header">设置表格显示列</div><div class="admin-form" id="table_mode_colunm_content" style="padding-left:10px;"></div>'; 
			    modal+='</div>';
			    modal+='</div>';
			    modal+='</div>';
			$("body").append(modal);
			$('#table_mode_colunm').modal();
			$('#table_mode_colunm').modal({ keyboard: false }); 
		}  
		var bodyth = $("#"+opts.tbodyId).prev().clone(); 
		$("#table_mode_colunm_content").html(""); 
		function getCK(i,colName){ 
			//return $('<label class="block mt15 option option-primary"><input type="checkbox" name="checked" value="checked" checked=""><span class="checkbox"></span>Checked</label>');			
			return $('<label class="block mt15 switch switch-primary"><input type="checkbox" name="toolsckcol" id="ckcol'+i+'"><label for="ckcol'+i+'" data-on="YES" data-off="NO"></label><span>'+colName+'</span></label>');
		} 
		var cooktable = getCookie(opts.tbodyId); 
		  if(cooktable == ''){
			  cooktable={};
		  }else{
			  cooktable = eval("(" + cooktable + ")"); 
		  }
		   
		for(var i =0 ;i<bodyth.children().children().length;i++){
			  var th = bodyth.children().children()[i];
			  var colName  =$(th).text();
			  if(i==1){
				  colName ="控件列";
			  }
			  //console.log(th+i);
			  var  isckf = cooktable["ckcol"+i];
			  if(typeof(isckf)=="undefined"){
				  isckf = 1;
			  }
			  var dck1 = getCK(i,colName); 
			  $("#table_mode_colunm_content").append(dck1);
			  if(isckf==1){
				  $("#ckcol"+i)[0].checked = true;
			  }else{
				  $("#ckcol"+i)[0].checked = false;
			  }
			  $("#ckcol"+i).unbind().click({cknumber:i,cooktable:cooktable},function(event){ 
				  var obj = event.data.cooktable; 
				  var flag = 1;
				  var td =$("#"+opts.tbodyId).find("tr").children()[event.data.cknumber];
				  var th = $("#"+opts.tbodyId).prev().find("tr").children()[event.data.cknumber];
				  if(this.checked==1){
					  for(var i=0;i<$("#"+opts.tbodyId).find("tr").length;i++){
						  var temptr  = $("#"+opts.tbodyId).find("tr")[i];
						  $($(temptr).children()[event.data.cknumber]).show();
						  obj["ckcol"+event.data.cknumber]=1;
					  }
					  $(th).show();
				  }else{ 
					  for(var i=0;i<$("#"+opts.tbodyId).find("tr").length;i++){
						  var temptr  = $("#"+opts.tbodyId).find("tr")[i];
						  $($(temptr).children()[event.data.cknumber]).hide();
						  obj["ckcol"+event.data.cknumber]=0;
					  }
					  $(th).hide();
				  }  
				  setCookie(opts.tbodyId,obj2str(obj),365);  
			  });
		} 
		//console.log(1);
		$('#table_mode_colunm').modal('show');
		//$('#table_mode_colunm').sortable();
	}
	function reloadTableTdCss(options){
		if(jQuery.isEmptyObject(options.tbodyTdCss)){
			return;
		}
		var colunmIndexJson = new Array();
		$("#"+options.table_title).find("th").each(function(index){
			for(var key in options.tbodyTdCss){
				if(key == $(this).attr("fname")){
					var j = {};
					j.code = index;
					j.valCss = options.tbodyTdCss[key];
					colunmIndexJson.push(j);
				}
			}
		});
		if(jQuery.isEmptyObject(colunmIndexJson)){
			return;
		}
		$("#"+options.tbodyId).find("tr").each(function(index){
			$(this).find("td").each(function(i,item){
				for(var n = 0;n < colunmIndexJson.length;n++){
					if(parseInt(colunmIndexJson[n].code) == parseInt(i)){
						$(item).addClass(colunmIndexJson[n].valCss);
					}
				}
			});
		});
	}
	function reloadImgColunm(options){
		var colunmIndex = -1;
		var maxHeight = 200;
		var maxWidth = 200;
		
		if(jQuery.isEmptyObject(options.imgColunm)){
			return;
		}
		$("#"+options.table_title).find("th").each(function(index){
			if(options.imgColunm == $(this).attr("fname")){
				colunmIndex = index;
			}
		});
		if(colunmIndex == -1){
			return;
		}
		$("body").append("<div id='reloadImgId'></div>");
		$("#"+options.tbodyId).find("tr").each(function(index){
			$(this).find("td").each(function(i){
				if(colunmIndex == i){
					var url = $(this).find("div").eq(0).attr("title");
					var _img = document.createElement("img");
					
					_img.setAttribute("style","height:30px;width:30px;");
					_img.setAttribute("src",url);
					$(this).html(_img);

					$(_img).on("mouseover",function(){
						var _div = document.createElement("div");
						var _url = $(this).attr("src");
						var _img = document.createElement("img");
							_img.setAttribute("src",url);
						var imgHeight =_img.height;
						var imgWidth = _img.width;
		
						var newHeight = 0;
						var newWidth = 0;
						if(imgWidth/imgHeight>= maxWidth/maxHeight){      
					         if(imgWidth>maxWidth){        
					        	 newWidth=maxWidth;      
					        	 newHeight=(imgHeight*maxWidth)/imgWidth;      
					         }else{      
					        	 newWidth=imgWidth;      
					        	 newHeight=imgHeight;      
					        }      
					     }else{      
					         if(imgHeight>maxHeight){        
					        	 newHeight=maxHeight;      
					        	 newWidth=(imgWidth*maxHeight)/imgHeight;              
					         }else{      
					        	 newWidth=imgWidth;      
					        	 newHeight=imgHeight;        
					        }      
					         
					     }   
						_img.setAttribute("style","height:"+newHeight+"px;width:"+newWidth+"px");
						_div.setAttribute("style","position: absolute;;z-index:10000;left:"+($(this).offset().left - newWidth)+"px;top:"+$(this).offset().top+"px;");
						_div.appendChild(_img);
						$("#reloadImgId").append(_div);
					});
					$(_img).on("mouseout",function(){
						$("#reloadImgId").html("");
					});
				}
			});
		});
	}
	//获取行数据
	function getTrData(obj,options){
		var data =  $("#"+options.tbodyId).data("data");
//		console.log("------------");
//		console.log(data);
		if(data != undefined && data.list != undefined){
			var arr = data.list;
			var $_obj = $("#"+options.tbodyId).find("input[type=checkbox]:checked");
//			console.log($_obj.length);
			var result = new Array()
			var j = 0;
			$_obj.each(function(){
				for(var i = 0;i < arr.length;i++){
					if($(this).val() == arr[i].table_id){
						result[j] = arr[i];
						j = parseInt(j) + 1;
						break;
					}
				}
			});
			
			return result;
		}
		return undefined;
	};
	function showTrDetails(opts){
		$("body").find("div").each(function(){
			var id = $(this).attr("id") == undefined ? "" : $(this).attr("id");
			if(id.indexOf("showDataDatailPage") > -1){
				$(this).remove();
			}
		});
		if(opts.isShowDail == false){
			return;
		}
		$("#"+opts.table_title).append("<th id='showDetailsThId_"+opts.table_title+"'>操作</td>");
		$("#"+opts.tbodyId).find("tr").each(function(index){
			$('#showTrDetails_td_'+opts.table_title+"_"+index).parent().remove();
			$(this).append("<td ><a id='showTrDetails_td_"+opts.table_title+"_"+index+"'>查看详情<a/></td>");
			
			var trData = $("#"+opts.tbodyId).data("data").list[index];
			$('#showTrDetails_td_'+opts.table_title+"_"+index).on("click",trData,function(){
				$("#showDataDatailPageId_"+opts.table_title).remove();
				$("#showDataDatailPageMainId_"+opts.table_title).remove();
				var showPageId = "";
				if(typeof(opts.trDetailsFn) == 'function'){
					if(opts.isUseDemoDetail == true){
						showPageId = "showDataDatailPageMainId_"+opts.table_title;
						initShowDetailsPage(trData,opts);
					}
					opts.trDetailsFn($(this),index,trData,showPageId);
					return;
				}else{
					initShowDetailsPage(trData,opts);
				}
			});
		});
	}
	function initShowDetailsPage(msg,opts){
		var maxHeight = 400;
		var _zIndexDiv = document.createElement("div");
		var _mainDiv = document.createElement("div");
		var _titleDiv = document.createElement("div");
		var _closeSpan = document.createElement("span");
		var _contentDiv = document.createElement("div");
		var _table = document.createElement("table");
		var _tableDiv = document.createElement("div");
		var _tbody = document.createElement("tbody");
		var _bottom = document.createElement("div");
		var _closeBtn = document.createElement("button");
		
		_zIndexDiv.setAttribute("id","showDataDatailPageId_"+opts.table_title);
		_zIndexDiv.setAttribute("style","width: "+ window.innerWidth +"px;height: "+window.innerHeight+"px;position: absolute;left: 0%;top: 0%;background-color:gray;opacity:0.5;;z-index:100000");
		
		_mainDiv.setAttribute("id","showDataDatailPageMainId_"+opts.table_title);
		_mainDiv.setAttribute("style","width: 700px;height: auto;border: 1px solid #ddd;background-color:white;position: absolute;left: 30%;top: 20%;z-index:10000000;border-radius:7px;");
		
		_titleDiv.setAttribute("style","font-size: 20px;height: 30px;width: 100%;border-bottom: 1px solid #ddd;line-height: 30px;padding-left:20px;");
		_titleDiv.textContent = "数据详情";
		
		_closeSpan.setAttribute("id","showDataDatailPageCloseId_"+opts.table_title);
		_closeSpan.setAttribute("style","margin-left:565px;cursor: pointer;");
		_closeSpan.textContent = "x";
		
		_table.setAttribute("class","table table-bordered");
		
		_tableDiv.setAttribute("style","padding:20px 20px 0px 20px;max-height:"+maxHeight+"px;");
		_tableDiv.setAttribute("class","tableDivClass");
		
		_bottom.setAttribute("style","height:60px;border-top:1px solid #ddd;");
		
		_closeBtn.setAttribute("class","btn btn-default");
		_closeBtn.textContent = "关闭";
		_closeBtn.setAttribute("style","float:right;margin-right:20px;margin-top:10px;");
		var skipTdNum = 0;
		if(opts.checkbox == true){
			skipTdNum = 1;
		}
		$("#"+opts.table_title).find("th").each(function(index){
			if(index > skipTdNum && index != ($("#"+opts.table_title).find("th").length - 1)){
			
				var _tr = document.createElement("tr");
				var _td_1 = document.createElement("td");
				var _td_2 = document.createElement("td");
				_td_1.textContent = $(this).text();
				$(_td_2).html(msg[$(this).attr("fname")]);
				_td_1.setAttribute("style","width:20%;WORD-WRAP: break-word;word-break:break-all");
				_td_2.setAttribute("style","WORD-WRAP: break-word;word-break:break-all;text-align:left;");
				_tr.appendChild(_td_1);
				_tr.appendChild(_td_2);
				_tbody.appendChild(_tr);
			}
		});

		_table.appendChild(_tbody);
		_titleDiv.appendChild(_closeSpan);
		_mainDiv.appendChild(_titleDiv);
		_tableDiv.appendChild(_table);
		_bottom.appendChild(_closeBtn);
		_mainDiv.appendChild(_tableDiv);
		_mainDiv.appendChild(_bottom);
		
		$("body").append(_zIndexDiv);
		$("body").append(_mainDiv);
		
		$(_closeSpan).on("click",opts,function(event){
			$("#showDataDatailPageId_"+event.data.table_title).remove();
			$("#showDataDatailPageMainId_"+event.data.table_title).remove();
		});
		$(_closeBtn).on("click",opts,function(event){
			$("#showDataDatailPageId_"+event.data.table_title).remove();
			$("#showDataDatailPageMainId_"+event.data.table_title).remove();
		});
		$("#showDataDatailPageMainId_"+opts.table_title).draggable();
		createScroll("showDataDatailPageMainId_"+opts.table_title,maxHeight);
	}
	function createScroll(id,maxHeight){
		var height = document.getElementById(id).offsetHeight;
		if(height > maxHeight){
			$("#"+id).find(".tableDivClass").eq(0).css("overflow-y","scroll");
		}
	}
	//组件初始化
	$.fn.message_grid = function(method, options) {
		if (typeof (method) == "string") {

			return $.fn.wgrid.methods[method](this, options);
		} else {
			var opts = $.extend({},$.fn.wgrid.defaults,method);
			init(this,opts);
		}
	};
	
	//组件默认参数
	$.fn.message_grid.defaults = {
		table_title:"",
		tbodyId:"",
		pagination:"",
		checkbox:false,
		page:true,
		isEnter:false,
		isColunmShow:false,//是否显示列隐藏的功能
		url:"",
		checkboxId: "",
		imgColunm:"",
		tbodyTdCss: {},
		data:{start:1,limit:20},
		isShowDail: false,
		isUseDemoDetail:false,
		trDetailsFn : "",
		success:function(){
			//alert("d");
		},
		trFn:function(obj){
			
		}
	};
	//组件方法注册
	$.fn.message_grid.methods = {
		init : function(_this, opts) {
			init(_this,opts);
		},
		getTrData: function(_this,options){
			return getTrData(_this,options);
		}
	};
	
})(jQuery);