/**
 *  将表格转化为表单
 */

function toform(id){
	var tables=$("#"+id).find("table");//找到div里面的table
	var object={};
	//一次请求处理单个表单
	var objarr=getTableKV($(tables.eq(0)));
	object.formData= JSON.stringify(objarr);
	return object;
//	for(var i=0;i<tables.length;i++){
//		getTableKV($(tables[i]));
//	}
}

/**
 * 处理table,把字段和名称对应起来
 * @param _table table的jQuery对象
 * @returns
 */
function getTableKV(_table){
	var objarr=[];
	var _trs =_table.find("tr");
	//行
	for(var i=0;i<_trs.length;i++){
		//到达最后一行
		if(i+1>=_trs.length){return objarr;}
		var _tds=$(_trs[i]).find("td");
		//列
		for(var j=0;j<_tds.length;j++){
			//到达最后一列
			if(j+1>=_tds.length){break;}
			var _td_text=$(_tds[j]).text();
			if(isNotNull(_td_text)){//如果当前单元格不为空,就判断右边和下边是否为空
				var obj={};
				var _Rtd=getRTD(_tds[j]);//获取右边的td
				var _Btd=getBTD(_tds[j],j);//获取下边的td
				if(isNotNull(_Rtd)){//判断右边和下边有格子存在,并且格子是里面有input或textarea
					$(_Rtd).find("input[type='text']").eq(0).attr("id","temp_"+i+"_"+j);
					$(_Rtd).find("textarea").eq(0).attr("id","temp_"+i+"_"+j);
					var _Rtd_text_id=$(_Rtd).find("input[type='text']").eq(0).attr("id");
					var _Rtd_textarea_id=$(_Rtd).find("textarea").eq(0).attr("id");
					if(isNotNull(_Rtd_text_id)){
						obj.column = _Rtd_text_id ;//字段英文名
					}else if(isNotNull(_Rtd_textarea_id)){
						obj.column = _Rtd_textarea_id;
					}
				}else if(isNotNull(_Btd)){
					$(_Btd).find("input[type='text']").eq(0).attr("id","temp_"+i+"_"+j);
					$(_Btd).find("textarea").eq(0).attr("id","temp_"+i+"_"+j);
					var _Btd_text_id=$(_Btd).find("input[type='text']").eq(0).attr("id");
					var _Btd_textarea_id=$(_Btd).find("textarea").eq(0).attr("id");
					if(isNotNull(_Btd_text_id)){
						obj.column = _Btd_text_id;
					}else if(isNotNull(_Btd_textarea_id)){
						obj.column = _Btd_textarea_id;
					}
				}else{
					//若右边和下边的格子都不符合条件,则跳过当前这个格子
					continue;
				}
				obj.name = _td_text;//字段中文名称
				obj.type = "text";//类型
				//判断右边或下边的类型
				if($(_Rtd).find("input[type='text']").length>0||$(_Btd).find("input[type='text']").length>0){
					obj.type = "text";
				}else if ($(_Rtd).find("textarea").length>0||$(_Btd).find("textarea").length>0) {
					obj.type = "textArea";
				}
				obj.val = "";//默认值
				
				objarr.push(obj);
			}
			
		}
	}
	return objarr;	
}
//获取当前格子右边符合条件的td
function getRTD(_td){
	var _Rtd=$(_td).next();
	var text=$(_Rtd).find("[type='text']");
	var textarea=$(_Rtd).find("textarea");
	if(text.length>0||textarea.length>0){
		return _Rtd;
	}else{
		return null;
	}
}
//获取当前格子下边符合条件的td
function getBTD(_td,index){
	var _Btd = $(_td).parent("tr").next().children("td").eq(index);
	var text=$(_Btd).find("[type='text']");
	var textarea=$(_Btd).find("textarea");
	if(text.length>0||textarea.length>0){
		return _Btd;
	}else{
		return null;
	}
}
/**
 * 判断值是否为空
 * @param str
 * @returns true:非空
 */
function isNotNull(str){
	return typeof (str) != 'undefined' && str != null&& str != ""
}