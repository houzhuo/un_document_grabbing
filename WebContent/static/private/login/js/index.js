$(function(){
	$("#loginSystemId").on("click",function(){
		login();
	});
	function login(){
		var usernName = $("#username").val().trim();
		var password = $("#password").val();
		
		if(usernName == '' || password == ''){
			alert("用户名或密码错误！");
			return;
		}
		var url = basePath + "/login/loginAction.do?method=loginSystem";
		var params = {
				"userName":usernName,
				"userPwd":password
		};
		$.post(url,params,function(data){
			var json = jQuery.parseJSON(data);
			if("success" in json){
				window.location.href = basePath + "/systemAction/systemAction.do?method=initMainPage";
			}else{
				alert("登陆失败！");
			}
		});
	}
	window.login={};
	window.login.login = login;
});