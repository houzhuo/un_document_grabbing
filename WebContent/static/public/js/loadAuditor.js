(function() {
	var pathName=window.document.location.pathname; 
	var projectName=pathName.substring(0,pathName.substr(1).indexOf('/')+1);
	var obj = {
			loadData:function(obj,option){
				var id = $(obj).attr("id");
				var inputId = "auditor";
				var dataListId = id+"s_01";
				var _input_string1 = $('<input type="hidden" id="string1" require="true" meth="notNull" placeholder="请选择正确的审核人"/>');
				var _input = $('<input type="text" id="'+inputId+'" list="'+dataListId+'" class="form-control">');
				var _dataList = $('<datalist id="'+dataListId+'"></datalist>');
				$(obj).append(_input_string1).append(_input).append(_dataList);
				
				$.ajax({
					url:projectName+"/compact/compactAction.do?method=getAuditorsData",
					type:"post",
					data:{"businessType":option.businessType},
					async:false,
					success:function(data){
						var json = JSON.parse(data);
						var user = json.auditor.toUser;
						if(user != null){
							$("#auditor").val(user.showName);
							$("#string1").val(user.table_id);
						}
						var list = json.userList;
						$("#"+dataListId).html("");
						for(var i=0;i<list.length;i++){
							var _option = $("<option></option>");
							_option.text(list[i].showName);
							_option.attr("table_id",list[i].table_id);
							_option.attr("employeeId",list[i].employeeId);
							$("#"+dataListId).append(_option);
						}
					}
				})
				
				$("#"+inputId).on("input",function(){
					var options = $("#"+dataListId).children();
					for(var i = 0;i<options.length;i++){
						if(options.eq(i).val().trim()==$("#"+inputId).val().trim()){
							$("#string1").val(options.eq(i).attr("table_id"));
						}
					}
				})
			}
	};
	
	$.fn.extend({
		loadAuditors:function(option){
			var options = $.extend({
				businessType:""
				
			}, option);	
			obj.loadData(this,options);
		}
	})
})()