/**
 * 初始化流程图
 */
(function(){
	var obj = {
		init:function(obj,option){
		    var $ = go.GraphObject.make;  // 定义模板
		    myDiagram =
			  $(go.Diagram, option.tagName,  // 名字必须引用HTML或DIV元素
			    {
			      initialContentAlignment: go.Spot.Center,
			      allowDrop: true, 
			      "animationManager.duration": 800, // 略微超过默认的动画(600毫秒)
			      "undoManager.isEnabled": true  ,/*
			      "initialAutoScale":go.Diagram.None,
			      "autoScale":go.Diagram.Uniform*/
			    });
		    // 文档被修改时，标题后面加个*，保存后取消
		    myDiagram.addDiagramListener("Modified", function(e) {
		      var button = document.getElementById("SaveButton");
		      if (button) button.disabled = !myDiagram.isModified;
		      var idx = document.title.indexOf("*");
		      if (myDiagram.isModified) {
		        if (idx < 0) document.title += "*";
		      } else {
		        if (idx >= 0) document.title = document.title.substr(0, idx);
		      }
		    });

		    // 辅助节点模板的定义
		    function nodeStyle() {
		      return [
		        // 节点的位置来自 "loc"属性中节点的数据
		        // 如果节点的位置改变，它的loc属性中的数据对应改变
		        new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
		        {
		          // 节点的位置是节点的中心坐标
		          locationSpot: go.Spot.Center,
		          // 处理鼠标进入/离开事件，显示/隐藏端点
		          mouseEnter: function (e, obj) { showPorts(obj.part, true); },
		          mouseLeave: function (e, obj) { showPorts(obj.part, false); }
		        }
		      ];
		    }

		    //创建端点
		    function makePort(name, spot, output, input) {
		      return $(go.Shape, "Circle",
		               {
		                  fill: "transparent",
		                  stroke: null,  // 在展示的时候改为白色
		                  desiredSize: new go.Size(8, 8),
		                  alignment: spot, alignmentFocus: spot,  
		                  portId: name,  
		                  fromSpot: spot, toSpot: spot,  
		                  fromLinkable: output, toLinkable: input,  
		                  cursor: "pointer"  
		               });
		    }

		    // 为普通节点定义节点模板
		    var lightText = 'whitesmoke';

		    //开始 
		    myDiagram.nodeTemplateMap.add("Start",
		    	$(go.Node, "Spot", nodeStyle(),
		    	     $(go.Panel, "Auto",
		    	          $(go.Shape, "Ellipse",
		    	            {fill:"#79C900",stroke: null },
		    	            new go.Binding("fill","color")),
		    	          $(go.TextBlock, "Start",
		    	            {
		    	        	  font: "bold 11pt Helvetica, Arial, sans-serif", 
		    	        	  stroke: lightText,
		    	        	  editable: true,
		    	        	  margin: 8,
		    	        	  maxSize:new go.Size(160,NaN),
		    	        	  wrap: go.TextBlock.WrapFit
		    	        	  },
		    	            new go.Binding("text"))
		    	       ),
		    	   makePort("T", go.Spot.Top, true, true),
		    	   makePort("L", go.Spot.Left, true, true),
		    	   makePort("R", go.Spot.Right, true, true),
		    	   makePort("B", go.Spot.Bottom, true, true),
		    	   {
		    		click:function(e,obj){
		    			if(option.isClick){
		    				clickNode(obj);
		    			}
		    	  	 }
		    	   },{toolTip:
		    	        $(go.Adornment, "Auto",
		    	            $(go.Shape, { fill: "lightyellow" }),
		   	                  $(go.TextBlock, { margin: 5 },
		   	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		   	    	     )//end adornment
		    	   }//end toolTip
		    ));
		    
		    
		    myDiagram.nodeTemplateMap.add("Step",  
		      $(go.Node, "Spot", nodeStyle(),
		        $(go.Panel, "Auto",
		          $(go.Shape, "Rectangle",
		            {width:70, height:50 , fill: "#00A9C9", stroke: null },
    	            new go.Binding("fill","color")
		            ),
		          $(go.TextBlock,
		            {
		              font: "bold 11pt Helvetica, Arial, sans-serif",
		              stroke: lightText,
		              margin: 8,
		              maxSize: new go.Size(80, NaN),
		              wrap: go.TextBlock.WrapFit,
		              editable: true
		            },
		            new go.Binding("text").makeTwoWay())
		        ),
		        // 四个端点，上下左右各一个
		        makePort("T", go.Spot.Top, true, true),
		        makePort("L", go.Spot.Left, true, true),
		        makePort("R", go.Spot.Right, true, true),
		        makePort("B", go.Spot.Bottom, true, true),
		        {
		    		click:function(e,obj){
		    			if(option.isClick){
		    				clickNode(obj);
		    			}
		    	  	 }
		   	  	},{toolTip:
		   	        $(go.Adornment, "Auto",
		   	            $(go.Shape, { fill: "lightyellow" }),
		  	                  $(go.TextBlock, { margin: 5 },
		  	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		  	    	     )//end adornment
		   	   }//end toolTip
		      ));
		    
		    myDiagram.nodeTemplateMap.add("Judge",  
		    	 $(go.Node, "Spot", nodeStyle(),
		    	      $(go.Panel, "Auto",
		    	          $(go.Shape, "Diamond",
		    	          { width:90, height:70 , fill: "#00A9C9", stroke: null },
		    	            new go.Binding("fill","color")
		    	       ),
		    	       $(go.TextBlock,
		    	            {
		    	              font: "bold 11pt Helvetica, Arial, sans-serif",
		    	              stroke: lightText,
		    	              margin: 8,
		    	              maxSize: new go.Size(90, NaN),
		    	              wrap: go.TextBlock.WrapFit,
		    	              editable: true
		    	            },
		    	        new go.Binding("text").makeTwoWay())
		    	        ),
		    	  // 四个端点，上下左右各一个
		    	  makePort("T", go.Spot.Top, true, true),
		    	  makePort("L", go.Spot.Left, true, true),
		    	  makePort("R", go.Spot.Right, true, true),
		    	  makePort("B", go.Spot.Bottom, true, true),
		    	  {
		      		click:function(e,obj){
		      			if(option.isClick){
		    				clickNode(obj);
		    			}
		      	  	 }
		      	   },{toolTip:
		    	        $(go.Adornment, "Auto",
		    	            $(go.Shape, { fill: "lightyellow" }),
		   	                  $(go.TextBlock, { margin: 5 },
		   	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		   	    	     )//end adornment
		    	   }//end toolTip
		    	));

		   	 myDiagram.nodeTemplateMap.add("Text",  
		        $(go.Node, "Spot", nodeStyle(),
		        	$(go.Panel, "Auto",
		        	   $(go.Shape, "OrGate",
		        	      { angle:-90,width:70,height:50,fill: "#00A9C9", stroke: null },
		    	            new go.Binding("fill","color")),
		        	   $(go.TextBlock,
		        	      {
		        	       font: "bold 11pt Helvetica, Arial, sans-serif",
		        	       stroke: lightText,
		        	       margin: 2,
		        	       maxSize: new go.Size(70, NaN),
		        	       wrap: go.TextBlock.WrapFit,
		        	       editable: true
		        	      },
		        	   new go.Binding("text").makeTwoWay())
		        	 ),
		      // 四个端点，上下左右各一个
		      makePort("T", go.Spot.Top, true, true),
		      makePort("L", go.Spot.Left, true, true),
		      makePort("R", go.Spot.Right, true, true),
		      makePort("B", go.Spot.Bottom, true, true),
		      {
		  		click:function(e,obj){
		  			if(option.isClick){
	    				clickNode(obj);
	    			}
		  	  	 }
		  	   },{toolTip:
		    	        $(go.Adornment, "Auto",
		    	            $(go.Shape, { fill: "lightyellow" }),
		   	                  $(go.TextBlock, { margin: 5 },
		   	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		   	    	     )//end adornment
		    	   }//end toolTip
		     ));
		   	 
		    myDiagram.nodeTemplateMap.add("Context",  
		       $(go.Node, "Spot", nodeStyle(),
		        	$(go.Panel, "Auto",
		        	   $(go.Shape, 
		        		  {geometry:go.Geometry.parse("F M0 90 L60 90 L60 30 L30 0 L0 30Z"),
		        	       width:70,height:60, fill: "#00A9C9", stroke: null },
		    	            new go.Binding("fill","color")),
		        	   $(go.TextBlock,
		        	       {
		        	         font: "bold 11pt Helvetica, Arial, sans-serif",
		        	         stroke: lightText,
		        	         margin: 8,
		        	         maxSize: new go.Size(70, NaN),
		        	         wrap: go.TextBlock.WrapFit,
		        	         editable: true
		        	       },
		        	    new go.Binding("text").makeTwoWay())
		        	 ),
		       // 四个端点，上下左右各一个
		       makePort("T", go.Spot.Top, true, true),
		       makePort("L", go.Spot.Left, true, true),
		       makePort("R", go.Spot.Right, true, true),
		       makePort("B", go.Spot.Bottom, true, true),
		       {
		   		click:function(e,obj){
		   			if(option.isClick){
	    				clickNode(obj);
	    			}
		   	  	 }
		   	   },{toolTip:
		    	        $(go.Adornment, "Auto",
		    	            $(go.Shape, { fill: "lightyellow" }),
		   	                  $(go.TextBlock, { margin: 5 },
		   	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		   	    	     )//end adornment
		    	   }//end toolTip
		      ));
		    
		    //基本事件
		    myDiagram.nodeTemplateMap.add("BasicEvent",  
		 	       $(go.Node, "Spot", nodeStyle(),
		 	        	$(go.Panel, "Auto",
		 	        	   $(go.Shape, 
		 	        		  {geometry:go.Geometry.parse("F M40 5 L40 15 A40 40 0 0 1 40 95 A40 40 0 0 1 40 15"),
		 	        	       width:70,height:60, fill: "#00A9C9", stroke: "#00A9C9" },
			    	            new go.Binding("fill","color")),
		 	        	   $(go.TextBlock,
		 	        	       {
		 	        	         font: "11pt Helvetica, Arial, sans-serif",
		 	        	         stroke: lightText,
		 	        	         margin: 8,
		 	        	         maxSize: new go.Size(90, NaN),
		 	        	         wrap: go.TextBlock.WrapFit,
		 	        	         editable: true
		 	        	       },
		 	        	    new go.Binding("text").makeTwoWay())
		 	        	 ),
		 	       // 四个端点，上下左右各一个
		 	       makePort("T", go.Spot.Top, true, true),
		 	       makePort("L", go.Spot.Left, true, true),
		 	       makePort("R", go.Spot.Right, true, true),
		 	       makePort("B", go.Spot.Bottom, true, true),
		 	      {
		    		click:function(e,obj){
		    			if(option.isClick){
		    				clickNode(obj);
		    			}
		    	  	 }
		    	   },{toolTip:
		    	        $(go.Adornment, "Auto",
		    	            $(go.Shape, { fill: "lightyellow" }),
		   	                  $(go.TextBlock, { margin: 5 },
		   	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		   	    	     )//end adornment
		    	   }//end toolTip
		      ));
		    
		    //未开展事件
		    myDiagram.nodeTemplateMap.add("NoStart",  
		    	       $(go.Node, "Spot", nodeStyle(),
		    	        	$(go.Panel, "Auto",
		    	        	   $(go.Shape, 
		    	        		  {geometry:go.Geometry.parse("F M30 0 L30 10 L0 50 L30 90 L60 50 L30 10 "),
		    	        	       width:70,height:60, fill: "#00A9C9", stroke: "#00A9C9" },
				    	            new go.Binding("fill","color")),
		    	        	   $(go.TextBlock,
		    	        	       {
		    	        	         font: "11pt Helvetica, Arial, sans-serif",
		    	        	         stroke: lightText,
		    	        	         margin: 8,
		    	        	         maxSize: new go.Size(90, NaN),
		    	        	         wrap: go.TextBlock.WrapFit,
		    	        	         editable: true
		    	        	       },
		    	        	    new go.Binding("text").makeTwoWay())
		    	        	 ),
		    	       // 四个端点，上下左右各一个
		    	       makePort("T", go.Spot.Top, true, true),
		    	       makePort("L", go.Spot.Left, true, true),
		    	       makePort("R", go.Spot.Right, true, true),
		    	       makePort("B", go.Spot.Bottom, true, true),
		    	       {
		   	    		click:function(e,obj){
		   	    			if(option.isClick){
			    				clickNode(obj);
			    			}
		   	    	  	 }
		   	    	   },{toolTip:
		       	       		 $(go.Adornment, "Auto",
		        	            $(go.Shape, { fill: "lightyellow" }),
		       	                  $(go.TextBlock, { margin: 5 },
		       	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		       	    	     )//end adornment
		        	   }//end toolTip
		   	      ));
		    
		    //禁止门
		    myDiagram.nodeTemplateMap.add("Ban",  
		 	       $(go.Node, "Spot", nodeStyle(),
		 	        	$(go.Panel, "Auto",
		 	        	   $(go.Shape, 
		 	        		  {geometry:go.Geometry.parse(
		 	        			"F M40 0 L40 10 L10 30 L10 70 L40 90 L70 70 L70 30 L40 10"
		 	        			+ "M70 50 L80 50 M40 90 L40 100"),
		 	        	       width:70,height:60, fill: "#00A9C9", stroke: "#00A9C9" },
			    	            new go.Binding("fill","color")),
		 	        	   $(go.TextBlock,
		 	        	       {
		 	        	         font: "11pt Helvetica, Arial, sans-serif",
		 	        	         stroke: lightText,
		 	        	         margin: 8,
		 	        	         maxSize: new go.Size(90, NaN),
		 	        	         wrap: go.TextBlock.WrapFit,
		 	        	         editable: true
		 	        	       },
		 	        	    new go.Binding("text").makeTwoWay())
		 	        	 ),
		 	       // 四个端点，上下左右各一个
		 	       makePort("T", go.Spot.Top, true, true),
		 	       makePort("L", go.Spot.Left, true, true),
		 	       makePort("R", go.Spot.Right, true, true),
		 	       makePort("B", go.Spot.Bottom, true, true),
		 	      {
		    		click:function(e,obj){
		    			if(option.isClick){
		    				clickNode(obj);
		    			}
		    	  	 }
		    	   },{toolTip:
		    	        $(go.Adornment, "Auto",
		    	            $(go.Shape, { fill: "lightyellow" }),
		   	                  $(go.TextBlock, { margin: 5 },
		   	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		   	    	     )//end adornment
		    	   }//end toolTip
		      ));
		   
		    //事件
		    myDiagram.nodeTemplateMap.add("Event",  
		  	       $(go.Node, "Spot", nodeStyle(),
		  	        	$(go.Panel, "Auto",
		  	        	   $(go.Shape, 
		  	        		  {geometry:go.Geometry.parse(
		  	        			"F M40 0 L40 10 L0 10 L0 50 L80 50 L80 10 L40 10 M40 50 L40 60"),
		  	        	       width:70,height:60, fill: "#00A9C9", stroke: "#00A9C9" },
			    	            new go.Binding("fill","color")),
		  	        	   $(go.TextBlock,
		  	        	       {
		  	        	         font: "11pt Helvetica, Arial, sans-serif",
		  	        	         stroke: lightText,
		  	        	         margin: 8,
		  	        	         maxSize: new go.Size(90, NaN),
		  	        	         wrap: go.TextBlock.WrapFit,
		  	        	         editable: true
		  	        	       },
		  	        	    new go.Binding("text").makeTwoWay())
		  	        	 ),
		  	       // 四个端点，上下左右各一个
		  	       makePort("T", go.Spot.Top, true, true),
		  	       makePort("L", go.Spot.Left, true, true),
		  	       makePort("R", go.Spot.Right, true, true),
		  	       makePort("B", go.Spot.Bottom, true, true),
		  	     {
		    		click:function(e,obj){
		    			if(option.isClick){
		    				clickNode(obj);
		    			}
		    	  	 }
		    	   },{toolTip:
		    	        $(go.Adornment, "Auto",
		    	            $(go.Shape, { fill: "lightyellow" }),
		   	                  $(go.TextBlock, { margin: 5 },
		   	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		   	    	     )//end adornment
		    	   }//end toolTip
		      ));
		    
		    //开关事件
		    myDiagram.nodeTemplateMap.add("SwitchEvent",  
		    	$(go.Node, "Spot", nodeStyle(),
		    	     $(go.Panel, "Auto",
		    	        $(go.Shape, 
		    	        	{geometry:go.Geometry.parse("F M40 0 L40 10 L0 30 L0 60 L80 60 L80 30 L40 10"),
		    	        	 width:70,height:60, fill: "#00A9C9", stroke: "#00A9C9" },
			    	            new go.Binding("fill","color")),
		    	        $(go.TextBlock,
		    	        	{
		    	        	 font: " 11pt Helvetica, Arial, sans-serif",
		    	        	 stroke: lightText,
		    	        	 margin: 8,
		    	        	 maxSize: new go.Size(70, NaN),
		    	        	 wrap: go.TextBlock.WrapFit,
		    	        	 editable: true
		    	        	 },
		    	        new go.Binding("text").makeTwoWay())
		    	     ),
		    	// 四个端点，上下左右各一个
		    	makePort("T", go.Spot.Top, true, true),
		    	makePort("L", go.Spot.Left, true, true),
		    	makePort("R", go.Spot.Right, true, true),
		    	makePort("B", go.Spot.Bottom, true, true),
		    	{
		   		  click:function(e,obj){
		   			if(option.isClick){
	    				clickNode(obj);
	    			}
		   	  	 	}
		   	   },{toolTip:
		    	        $(go.Adornment, "Auto",
		    	            $(go.Shape, { fill: "lightyellow" }),
		   	                  $(go.TextBlock, { margin: 5 },
		   	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		   	    	     )//end adornment
		    	   }//end toolTip
		   ));
		    
		    //转移符号
		    myDiagram.nodeTemplateMap.add("Symbol",  
		        	$(go.Node, "Spot", nodeStyle(),
		        	     $(go.Panel, "Auto",
		        	        $(go.Shape, 
		        	        	{geometry:go.Geometry.parse("F M40 0 L40 10 L0 60 L80 60 L40 10"),
		        	        	 width:70,height:60, fill: "#00A9C9", stroke: "#00A9C9" },
				    	            new go.Binding("fill","color")),
		        	        $(go.TextBlock,
		        	        	{
		        	        	 font: "11pt Helvetica, Arial, sans-serif",
		        	        	 stroke: lightText,
		        	        	 margin: 8,
		        	        	 maxSize: new go.Size(70, NaN),
		        	        	 wrap: go.TextBlock.WrapFit,
		        	        	 editable: true
		        	        	 },
		        	        new go.Binding("text").makeTwoWay())
		        	     ),
		        	// 四个端点，上下左右各一个
		        	makePort("T", go.Spot.Top, true, true),
		        	makePort("L", go.Spot.Left, true, true),
		        	makePort("R", go.Spot.Right, true, true),
		        	makePort("B", go.Spot.Bottom, true, true),
		        	{
		        		click:function(e,obj){
		        			if(option.isClick){
			    				clickNode(obj);
			    			}
		        	  	 }
		        	   },{toolTip:
		       	        $(go.Adornment, "Auto",
		        	            $(go.Shape, { fill: "lightyellow" }),
		       	                  $(go.TextBlock, { margin: 5 },
		       	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		       	    	     )//end adornment
		        	   }//end toolTip
		        ));
		  
		 	//and门
		    myDiagram.nodeTemplateMap.add("And",  
		        	$(go.Node, "Spot", nodeStyle(),
		        	     $(go.Panel, "Auto",
		        	        $(go.Shape, 
		        	        	{geometry:go.Geometry.parse("F M0 40 L0 80 L80 80 L80 40 Q40 -20 0 40 "+
		        	        			" M40 10 L40 -5 M10 80 L10 95 M40 80 L40 95 M70 80 L70 95"),
		        	        	 width:70,height:60, fill: "#00A9C9", stroke: "#00A9C9" },
				    	            new go.Binding("fill","color")),
		        	        $(go.TextBlock,
		        	        	{
		        	        	 font: "11pt Helvetica, Arial, sans-serif",
		        	        	 stroke: lightText,
		        	        	 margin: 8,
		        	        	 maxSize: new go.Size(70, NaN),
		        	        	 wrap: go.TextBlock.WrapFit,
		        	        	 editable: true
		        	        	 },
		        	        new go.Binding("text").makeTwoWay())
		        	     ),
		        	// 四个端点，上下左右各一个
		        	makePort("T", go.Spot.Top, true, true),
		        	makePort("L", go.Spot.Left, true, true),
		        	makePort("R", go.Spot.Right, true, true),
		        	makePort("B", go.Spot.Bottom, true, true),
		        	{
		        		click:function(e,obj){
		        			if(option.isClick){
			    				clickNode(obj);
			    			}
		        	  	 }
		        	   },{toolTip:
		       	        $(go.Adornment, "Auto",
		        	            $(go.Shape, { fill: "lightyellow" }),
		       	                  $(go.TextBlock, { margin: 5 },
		       	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		       	    	     )//end adornment
		        	   }//end toolTip
		        ));
		    
		  	//Or门
		    myDiagram.nodeTemplateMap.add("Or",  
		        	$(go.Node, "Spot", nodeStyle(),
		        	     $(go.Panel, "Auto",
		        	        $(go.Shape, 
		        	        	{geometry:go.Geometry.parse("F M0 80 L80 80 Q40 -60 0 80 "+
		        	        			" M40 10 L40 -5 M10 80 L10 95 M40 80 L40 95 M70 80 L70 95"),
		        	        	 width:70,height:60, fill: "#00A9C9", stroke: "#00A9C9" },
				    	            new go.Binding("fill","color")),
		        	        $(go.TextBlock,
		        	        	{
		        	        	 font: "11pt Helvetica, Arial, sans-serif",
		        	        	 stroke: lightText,
		        	        	 margin: 8,
		        	        	 maxSize: new go.Size(70, NaN),
		        	        	 wrap: go.TextBlock.WrapFit,
		        	        	 editable: true
		        	        	 },
		        	        new go.Binding("text").makeTwoWay())
		        	     ),
		        	// 四个端点，上下左右各一个
		        	makePort("T", go.Spot.Top, true, true),
		        	makePort("L", go.Spot.Left, true, true),
		        	makePort("R", go.Spot.Right, true, true),
		        	makePort("B", go.Spot.Bottom, true, true),
		        	{
		        		click:function(e,obj){
		        			if(option.isClick){
			    				clickNode(obj);
			    			}
		        	  	 }
		        	   },{toolTip:
		       	        $(go.Adornment, "Auto",
		        	            $(go.Shape, { fill: "lightyellow" }),
		       	                  $(go.TextBlock, { margin: 5 },
		       	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		       	    	     )//end adornment
		        	   }//end toolTip
		        ));
		  	
		  	//优先and门
		    myDiagram.nodeTemplateMap.add("PrecedenceAnd",  
		        	$(go.Node, "Spot", nodeStyle(),
		        	     $(go.Panel, "Auto",
		        	        $(go.Shape, 
		        	        	{geometry:go.Geometry.parse("F M0 40 L0 80 L80 80 L80 40 Q40 -20 0 40 "+
		        	        			"M0 90 L80 90 M40 10 L40 -5 M10 90 L10 100 M40 90 L40 100 M70 90 L70 100"),
		        	        	 width:70,height:60, fill: "#00A9C9", stroke: "#00A9C9" },
				    	            new go.Binding("fill","color")),
		        	        $(go.TextBlock,
		        	        	{
		        	        	 font: "11pt Helvetica, Arial, sans-serif",
		        	        	 stroke: lightText,
		        	        	 margin: 8,
		        	        	 maxSize: new go.Size(70, NaN),
		        	        	 wrap: go.TextBlock.WrapFit,
		        	        	 editable: true
		        	        	 },
		        	        new go.Binding("text").makeTwoWay())
		        	     ),
		        	// 四个端点，上下左右各一个
		        	makePort("T", go.Spot.Top, true, true),
		        	makePort("L", go.Spot.Left, true, true),
		        	makePort("R", go.Spot.Right, true, true),
		        	makePort("B", go.Spot.Bottom, true, true),
		        	{
		        		click:function(e,obj){
		        			if(option.isClick){
			    				clickNode(obj);
			    			}
		        	  	 }
		        	   },{toolTip:
		       	        $(go.Adornment, "Auto",
		        	            $(go.Shape, { fill: "lightyellow" }),
		       	                  $(go.TextBlock, { margin: 5 },
		       	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		       	    	     )//end adornment
		        	   }//end toolTip
		        ));
		  	
		 	 //异或门
		    myDiagram.nodeTemplateMap.add("Xor",  
		        	$(go.Node, "Spot", nodeStyle(),
		        	     $(go.Panel, "Auto",
		        	        $(go.Shape, 
		        	        	{geometry:go.Geometry.parse("F M0 80 L80 80 Q40 -60 0 80 "+
		        	        			"M0 90 L80 90 M40 10 L40 -5 M10 90 L10 100 M70 90 L70 100"),
		        	        	 width:70,height:60, fill: "#00A9C9", stroke: "#00A9C9" },
				    	            new go.Binding("fill","color")),
		        	        $(go.TextBlock,
		        	        	{
		        	        	 font: "11pt Helvetica, Arial, sans-serif",
		        	        	 stroke: lightText,
		        	        	 margin: 8,
		        	        	 maxSize: new go.Size(70, NaN),
		        	        	 wrap: go.TextBlock.WrapFit,
		        	        	 editable: true
		        	        	 },
		        	        new go.Binding("text").makeTwoWay())
		        	     ),
		        	// 四个端点，上下左右各一个
		        	makePort("T", go.Spot.Top, true, true),
		        	makePort("L", go.Spot.Left, true, true),
		        	makePort("R", go.Spot.Right, true, true),
		        	makePort("B", go.Spot.Bottom, true, true),
		        	{
		        		click:function(e,obj){
		        			if(option.isClick){
			    				clickNode(obj);
			    			}
		        	  	 }
		        	   },{toolTip:
		       	        $(go.Adornment, "Auto",
		        	            $(go.Shape, { fill: "lightyellow" }),
		       	                  $(go.TextBlock, { margin: 5 },
		       	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		       	    	     )//end adornment
		        	   }//end toolTip
		        ));
		 	
		    //条件事件
		    myDiagram.nodeTemplateMap.add("ConditionEvent",  
		        	$(go.Node, "Spot", nodeStyle(),
		        	     $(go.Panel, "Auto",
		        	        $(go.Shape, 
		        	        	{geometry:go.Geometry.parse("F M80 0 A40 25 0 0 1 80 80 A40 25 0 0 1 80 0 M0 40 L15 40"),
		        	        	 width:70,height:50, fill: "#00A9C9", stroke: "#00A9C9" },
				    	            new go.Binding("fill","color")),
		        	        $(go.TextBlock,
		        	        	{
		        	        	 font: "11pt Helvetica, Arial, sans-serif",
		        	        	 stroke: lightText,
		        	        	 margin: 8,
		        	        	 maxSize: new go.Size(70, NaN),
		        	        	 wrap: go.TextBlock.WrapFit,
		        	        	 editable: true
		        	        	 },
		        	        new go.Binding("text").makeTwoWay())
		        	     ),
		        	// 四个端点，上下左右各一个
		        	makePort("T", go.Spot.Top, true, true),
		        	makePort("L", go.Spot.Left, true, true),
		        	makePort("R", go.Spot.Right, true, true),
		        	makePort("B", go.Spot.Bottom, true, true),
		        	{
		        		click:function(e,obj){
		        			if(option.isClick){
			    				clickNode(obj);
			    			}
		        	  	 }
		        	   },{toolTip:
		       	        $(go.Adornment, "Auto",
		        	            $(go.Shape, { fill: "lightyellow" }),
		       	                  $(go.TextBlock, { margin: 5 },
		       	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		       	    	     )//end adornment
		        	   }//end toolTip
		        ));
		    
		    //表决门
		    myDiagram.nodeTemplateMap.add("Dicide",  
		    	       $(go.Node, "Spot", nodeStyle(),
		    	        	$(go.Panel, "Auto",
		    	        	   $(go.Shape,{geometry:go.Geometry.parse("F M0 80 L80 80 Q40 -60 0 80 "+
			        			" M40 10 L40 -5 M10 80 L10 95 M40 80 L40 95 M70 80 L70 95 M0 95 L80 95"),
		       	        	 	width:70,height:60, fill: "#00A9C9", stroke: "#00A9C9" },
			    	            new go.Binding("fill","color")),
		    	        	   $(go.TextBlock,
		    	        	       {
		    	        	         font: "11pt Helvetica, Arial, sans-serif",
		    	        	         stroke: lightText,
		    	        	         margin: 8,
		    	        	         maxSize: new go.Size(100, NaN),
		    	        	         wrap: go.TextBlock.WrapFit,
		    	        	         editable: true
		    	        	       },
		    	        	    new go.Binding("text").makeTwoWay())
		    	        	 ),
		    	       // 四个端点，上下左右各一个
		    	       makePort("T", go.Spot.Top, true, true),
		    	       makePort("L", go.Spot.Left, true, true),
		    	       makePort("R", go.Spot.Right, true, true),
		    	       makePort("B", go.Spot.Bottom, true, true),
		    	       {
		    	    		click:function(e,obj){
		    	    			if(option.isClick){
				    				clickNode(obj);
				    			}
		    	    	  	 }
	    	    	   },{toolTip:
	    	    	        $(go.Adornment, "Auto",
	    	        	            $(go.Shape, { fill: "lightyellow" }),
	    	       	                  $(go.TextBlock, { margin: 5 },
	    	       	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
	    	       	    	     )//end adornment
	    	        	   }//end toolTip
		    	      ));
		    
		    //comment
		    myDiagram.nodeTemplateMap.add("Comment",
		    	      $(go.Node, "Auto", nodeStyle(),
		    	        $(go.Shape, "File",
		    	          { fill: "#EEEEEE", stroke: null }),
		    	        $(go.TextBlock,
		    	          {
		    	            margin: 5,
		    	            maxSize: new go.Size(200, NaN),
		    	            wrap: go.TextBlock.WrapFit,
		    	            textAlign: "left",
		    	            editable: true,
		    	            font: "bold 12pt Helvetica, Arial, sans-serif",
		    	            stroke: '#454545'
		    	          },
		    	          new go.Binding("text").makeTwoWay())
		    	        // no ports, because no links are allowed to connect with a comment
		    	      ));
		 	 
		    //结束
		    myDiagram.nodeTemplateMap.add("End",
		      $(go.Node, "Spot", nodeStyle(),
		         $(go.Panel, "Auto",
		        	$(go.Shape, "Ellipse",
		        	   { width:70, height:50, fill: "#79C900", stroke: null },
	    	            new go.Binding("fill","color")),
		            $(go.TextBlock, "End",
		               { font: "bold 11pt Helvetica, Arial, sans-serif",
		            	stroke: lightText,
		            	editable: true },
		            new go.Binding("text"))
		         ),
		         makePort("T", go.Spot.Top, true, true),
		         makePort("L", go.Spot.Left, true, true),
		         makePort("R", go.Spot.Right, true, true),
		         makePort("B", go.Spot.Bottom, true, true),
		         {
		     		click:function(e,obj){
		     			if(option.isClick){
		    				clickNode(obj);
		    			}
		     	  	 }
		     	   },{toolTip:
		    	        $(go.Adornment, "Auto",
		    	            $(go.Shape, { fill: "lightyellow" }),
		   	                  $(go.TextBlock, { margin: 5 },
		   	                	  new go.Binding("text","", function(s) { return loadToolTipData(s); }))
		   	    	     )//end adornment
		    	   }//end toolTip
		      ));
		    
		   
			
		    //连接线条模板
		    myDiagram.linkTemplate =
		        $(go.Link, 
		          {
		            routing: go.Link.AvoidsNodes,
		            curve: go.Link.JumpOver,
		            corner: 5, toShortLength: 4,
		            relinkableFrom: true,
		            relinkableTo: true,
		            reshapable: true,
		            resegmentable: true,
		            mouseEnter: function(e, link) { link.findObject("HIGHLIGHT").stroke = "rgba(30,144,255,0.2)"; },
		            mouseLeave: function(e, link) { link.findObject("HIGHLIGHT").stroke = "transparent"; }
		          },
		          new go.Binding("points").makeTwoWay(),
		          $(go.Shape,  // 连接时的图形
		            { isPanelMain: true, strokeWidth: 8, stroke: "transparent", name: "HIGHLIGHT" }),
		          $(go.Shape,  // 连接路径图形
		            { isPanelMain: true, stroke: "gray", strokeWidth: 2 },
		            new go.Binding("stroke","color")),
		          $(go.Shape,  // 箭头
		            { toArrow: "standard", stroke: null, fill: "gray"})
		        );

		    myDiagram.toolManager.linkingTool.temporaryLink.routing = go.Link.Orthogonal;
		    myDiagram.toolManager.relinkingTool.temporaryLink.routing = go.Link.Orthogonal;

		    // 初始化面板左边的图形
		     myPalette =
		    	 $(go.Palette, "myPaletteDiv" ,  // 名字必须引用HTML或DIV元素
		        {
		          "animationManager.duration": 800,  
		           nodeTemplateMap: myDiagram.nodeTemplateMap,    
		           model: new go.GraphLinksModel([  // 指定的内容面板
		            { category: "Step",text: "Step" },      
		            { category: "NoStart",text: "未开展" },             
		            { category: "Judge",text: "Judge" },
		            { category: "Context",text: "Context" },
		            { category: "Text",text: "Text" },            
		            { category: "Event",text: "事件" },
		            { category: "BasicEvent",text: "基本事件" },
		            { category: "ConditionEvent",text: "条件事件 " },
		            { category: "SwitchEvent",text: "\n开关事件" },
		            { category: "Start", text: "Start" },
		            { category: "And",text: "and门 " },
		            { category: "Or",text: "Or门 " },
		            { category: "Ban",text: "禁止门" },
		            { category: "Dicide",text: "表决门 " },
		            { category: "PrecedenceAnd",text: "优先And门 " },
		            { category: "Xor",text: "异或门 " },
		            { category: "Symbol",text: "\n符号" },
		            { category: "Comment",text: "Comment"},
		            { category: "End", text: "End" }
		          ])  
		        } ); 

		    
			//点击和拖动
		    function customFocus() {
		      var x = window.scrollX || window.pageXOffset;
		      var y = window.scrollY || window.pageYOffset;
		      go.Diagram.prototype.doFocus.call(this);
		      window.scrollTo(x, y);
		    }

		    myDiagram.doFocus = customFocus;
		    myPalette.doFocus = customFocus;

			//load(document.getElementById("mySavedModel").value); 
		  } // init结束
	};
	
	// 当鼠标在该节点上时使一个节点上的所有端口可见
	  function showPorts(node, show) {
	    var diagram = node.diagram;
	    if (!diagram || diagram.isReadOnly || !diagram.allowLink) return;
	    node.ports.each(function(port) {
	        port.stroke = (show ? "white" : null);
	      });
	  }
	
	//加载JSON数据转换的图形
	function load(json) {
	  myDiagram.model = go.Model.fromJson(json);
	}
	  
	  
	$.extend({
		initFlowChart:function(option){//初始化
			var options = $.extend({
				tagName:"",
				isClick:false //是否添加单击事件
			}, option);	
			obj.init(this,options);
		},
		loadFromJson:function(data){//加载JSON数据转换的图形
			load(data);
		}
	})
})();