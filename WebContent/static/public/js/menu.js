(function($){
	var menu = {
		init: function(obj,params){
			return (function(){
				menu.getData(params);
				menu.createMenu(params,obj);
				menu.checkBtnPower(params);
			})();
		},
		getData: function(params){
			return (function(){
				if(params.data != null && params.data != undefined && !jQuery.isEmptyObject(params.data)){
					return ;
				}
				$.ajax({
					type: "POST",
					data: {},
					url: params.url,
					async: false,
					success: function(data){
						if(menu.checkObj(data) == false){
							return;
						}
						var json = jQuery.parseJSON(data);
						$("body").data("powBtn",json.powBtn);
						$("body").data("iniutMenuData",json.initMenu);
					}
				});
			})();
		},
		createMenu: function(params,obj){
			var json =  $("body").data("iniutMenuData");
			if(menu.checkObj(json) == false){
				return;
			}
			var pId = params.dataType.rootId;
			var _ul = document.createElement("ul");
			var baseLeft = 10;
			menu.createMenuItem(pId,json,params,_ul,baseLeft);
			$(_ul).addClass("mtree");
			$(_ul).attr("id","ulMtreeId");
			$(obj).html(_ul);
		},
		createMenuItem:function(pId,json,params,parentHtmlObj,baseLeft){
			var _ul = document.createElement("ul");
			baseLeft = parseInt(baseLeft) + 10;
			for(var i =0;i < json.length ;i++){
				if(json[i].pId == pId && json[i].type == 'menu'){
					var _li = document.createElement("li");
					var _a = document.createElement("a");
					var _i_before =  document.createElement("i");
					var _i_after =  document.createElement("i");
					var _span = document.createElement("span");
					json[i].url = params.basePath + json[i].url;
					$(_a).attr("a-href",json[i].url);
					$(_a).css("padding-left",baseLeft+"px");
					$(_i_after).attr("class","menu-arrow");
					$(_i_before).attr("class",json[i].icon+" menu-icon");
					$(_span).append(json[i].text);
					
					$(_a).append(_i_before);
					$(_a).append(_span);
					$(_a).append(_i_after);
					$(_li).append(_a);
					$(_li).addClass("top_item");
					$(_li).attr("id",json[i].id);
					
					$(_ul).append(_li);
					$(parentHtmlObj).append(_ul);
					
					$(_li).find("a:first").eq(0).on("mouseover",function(event){
						$(this).addClass("menu-item-over");
					});
					$(_li).find("a:first").eq(0).on("mouseout",function(event){
						$(this).removeClass("menu-item-over");
					});
					
					var isChildren = menu.checkChildren(json[i].id,json);
					if(isChildren){
						$(_li).addClass("has-children-close");
						$(_li).on("click",function(event){
								event.stopPropagation();
								menu.itemClick(this,params);
						});
						menu.createMenuItem(json[i].id,json,params,_li,baseLeft);
						$(_li).find("ul").css({display:"none"});
					}else{
						$(_li).on("click",function(event){
							event.stopPropagation();
							var itemData = new Array();
							menu.getItemData(json,$(this).attr('id'),itemData);
							menu.openItemUrl(this,params,itemData);
						});
					}
				}
			}
		},
		checkChildren:function(pId,data){
			for(var i =0;i < data.length ;i++){
				if(data[i].pId == pId &&  data[i].type == 'menu'){
					return true;
				}
			}
			return false;
		},
		getItemData:function(data,id,itemData){
			for(var i = 0;i < data.length;i++){
				if(data[i].id == id){
					itemData.push(data[i]);
				}
			}
		},
		openItemUrl:function(obj,params,itemData){
			menu.updateClickItemBackColor(obj);
			
			if(typeof(params.backFunction)  == "function" ){
				$("#"+params.contentWrapper).html("").load(itemData[0].url,{},function(){
					params.backFunction(obj,itemData);
				});
			}
			
		},
		checkBtnPower:function(params){
			var powBtn = $("body").data("powBtn");
			var initMenuData = $("body").data("iniutMenuData");
			if(menu.checkObj(powBtn) == false ||  menu.checkObj(initMenuData) == false){
				return;
			}
			$('#'+params.contentWrapper).on('DOMNodeInserted',function(){	
				$("#"+params.contentWrapper+" *").each(function() {
					if(!menu.isPowerBtn($(this).attr("id"),powBtn)){
						return true;
					}
					var mark = false;
					for (var i = 0; i < initMenuData.length; i++) {
						if (initMenuData[i].type != 'button') {
							continue;
						}
						if (initMenuData[i].id == $(this).attr("id")
								|| initMenuData[i].id == $(this).attr("power") ) {
							mark = true;
							break;
						}
					}
					if (mark == false) {
						$(this).remove();
					}
				});
			 });
		},
		checkObj:function(obj){
			if(obj == undefined || obj == '' || obj == null || obj == 'null'){
				return false;
			}
			return true;
		},
		isPowerBtn:function(id,powerBtn){
			if(id == '' || id == undefined){
				return false;
			}
			if(powerBtn == undefined){
				return false;
			}
			for(var i = 0;i < powerBtn.length;i++){
				if(powerBtn[i].id == id){
					return true;
				}
			}
			return false;
		},
		updateClickItemBackColor:function(obj){
			//修改菜单颜色
			$("#ulMtreeId").find("li").each(function(){
				$(this).find("a:first").eq(0).removeClass("menu-item-click menu-item-over");
			});
			
			if($(obj).is(".has-children-open")){
				//修改选中颜色
				$(obj).find("a:first").eq(0).addClass("menu-item-click");
			}
			if(!$(obj).is(".has-children-open") && !$(obj).is(".has-children-close")){
				//修改选中颜色
				$(obj).find("a:first").eq(0).addClass("menu-item-click");
			}
		},
		itemClick: function(obj,params){
			var _ul = $(obj).find("ul");
			
			//关闭其他菜单项
			$(obj).parent().find("li").each(function(index){
				if($(obj).attr("id") != $(this).attr("id")){
					var _li_ul = $(this).find("ul:first");
					_li_ul.eq(0).css("display","none");
					if(_li_ul.length > 0){
						$(this).addClass("has-children-close");
						$(this).removeClass("has-children-open");
					}
				}
			});
			
			//打开当前菜单
			if(_ul.length > 0){
				if(_ul.eq(0).css("display") == "none"){
					$(obj).removeClass("has-children-close");
					$(obj).addClass("has-children-open");
					_ul.eq(0).slideDown(200);
				}else{
					$(obj).addClass("has-children-close");
					$(obj).removeClass("has-children-open");
					_ul.eq(0).slideUp(200);
				}
			}
			
			menu.updateClickItemBackColor(obj);
		}
	}
	
	$.fn.extend({
		initMenu: function(options){
			var params = $.extend({
				menuId: "",
				url: "",
				data: {},
				backFunction: {}
			//	pageId: ""
			},options);
			menu.init(this,params);
		}
	});
	
})(jQuery);
