/**
 * 使用说明:
 * width:"300px",
 * height:"300px",
 * url: "",// 后台访问地址
 * dataParams:{},// 只有url时的参数配置
 * treeData:{},//isTree为true时且使用sql语句查询数据时，对应id,name,pId三个字段名称
 * sql: "",// 数据库操作HQL语句
 * showColnums:[],// 表格中要使用的在页面显示的字段名，必填
 * inputColnums:[],// 表格中要使用的在实体里数据库查询的字段名，必填
 * paramsValue:[],// sql语句中的查询条件的值，如果HQL语句中有?时需填的值
 * jsonIgnore:[],// 返回的json数据里要忽略的字段，sql不为空的时候，用来忽略json里一对多的字段或多对多的字段
 * searchColnum:"",// 要搜索的字段名，必填
 * searchPrompt:"",// 搜索框提示语，必填
 * useColnum:"",// 要使用并展示到文本框里的字段名，必填
 * keyField:"table_id",// 主键
 * orderBy:"table_id",// 要排序的字段
 * sort:"desc",// desc倒序，asc升序
 * dataPosition:"T",// 选中数据后显示的位置（T:top,R:right）
 * maxSelectLimit:3,// 限制最多选中三个项目
 * maxDataLimit:3,// 限制显示最多条数
 * isMultiple:true,// 是否多选
 * isInput:true,// 控件是否允许输入
 * isNumber:true,// 表格是否显示编号
 * isTree:false,// 是否显示树
 * treeSetting : {},// 树参数
 * showInputText:true,//是否显示搜索框
 * isCascade:true,//是否选择级联项，isTree:true时有效
 * getGridData: function(data) // 回调函数，选中数据之后点击确认按钮之后的操作
 * setDownGridDatas // 设置多个值
 * setDownGridData  // 设置单个值
 * getDownGridDatas // 得到值
 * getAllDatas      // 得到激活組件時的所有值
 */
(function($) {
	// 记录展示的数据在第几列
	var colnumIndex = 0;
	// 记录展示的数据字段名称
	var useColnum = '';
	// 记录所有数据条数用来判断是否全选
	var checkBoxSum = 0;
	// 记录限制最多选中多少个项目
	var maxSelectLimit = 0;
	
	// 单选
	function tbodyTrClick(_tdObj,options,obj){
		var isMultiple = options.isMultiple;
		if(isMultiple){// 多选情况下的点击事件
			var $_obj = _tdObj.find("input[type=checkbox]");
			var tableId = $_obj.val();
			var name = _tdObj.find("td:eq("+colnumIndex+")").find("div").html();
			if($_obj.prop("checked")){// 取消所选中数据
				$_obj.prop("checked",false);
				var _btn = $("#dataChecked").find("button[id='"+tableId+"']");
				_btn.next().remove();
				_btn.remove();
				if($("#dataChecked").find("button").length==0){
					$("#dataChecked").html("");
					$("#showData").hide();
				}
			}else{// 选中数据
				var btnNum = $("#dataChecked").find("button").length;
				if(parseInt(maxSelectLimit) > Math.abs(parseInt(btnNum))){
					$_obj.prop("checked",true);
					// 展示所选数据
					showCheckData(name,tableId,obj);
				}else{
					alert("只能选择"+maxSelectLimit+"个条目");
				}
			}
			checkedIsAll();
		}else{// 单选情况下的点击事件，直接赋值
			var keyField = options.keyField;
			var datas = $(obj).data($(obj).attr("id"));
			var gridList = new Array();
			var keyData = _tdObj.attr("id");
			var strData = _tdObj.find("td:eq("+colnumIndex+")").find("div").html();
			for (var i = 0; i < datas.length; i++){
				var vb = datas[i];
				var dataId = vb[keyField];
				if(keyData==dataId){
					gridList.push(vb);
					break;
				}
			}
			$(obj).parent().find("input[name='getKeyValue']").val(keyData);
			$(obj).val(strData);
			$("#demo_div").remove();
			options.getGridData(gridList);
		}
	}
	// 全选
	function checkBoxAllClick(obj){
		$("#demoCheckBox").click(function(){
			if($("#demoCheckBox").prop("checked")){
				var btnNum = $("#dataChecked").find("button").length;
				var checkedNum = $("#demo_tbody tr").find("input[type=checkbox]:checked").length;
				if((parseInt(checkBoxSum)-parseInt(checkedNum)+parseInt(btnNum)) > parseInt(maxSelectLimit)){
					$("#demoCheckBox").prop("checked",false);
					alert("只能选择"+maxSelectLimit+"个条目");
				}else{
					$("#demo_tbody tr").each(function(){
						var $_obj = $(this).find("input[type=checkbox]");
						$_obj.prop("checked",true);
						var tableId = $_obj.val();
						var name = $(this).find("td:eq("+colnumIndex+")").find("div").html();
						var btnLenth = $("#dataChecked").find("button[id='"+tableId+"']").length;
						if(btnLenth==0){
							showCheckData(name,tableId,obj);
						}
					});
				}
			}else{
				$("#demo_tbody tr").each(function(){
					var $_obj = $(this).find("input[type=checkbox]");
					$_obj.prop("checked",false);
				});
				$("#dataChecked").html("");
				$("#showData").hide();
				// 清空文本框里的数据
				$(obj).val('');
				$(obj).parent().find("input[name='getKeyValue']").val('');
			}
		});
	}
	// 确认按钮
	function confirmClick(obj,options){
		var keyField = options.keyField;
		var inputColArr = options.inputColnums;
		var gridList = new Array();
		var strData = "", keyData = "";
		var datas = $(obj).data($(obj).attr("id"));
		$("#dataChecked button").each(function(){
			var id = $(this).attr("id");
			var title = $(this).attr("title");
			for (var i = 0; i < datas.length; i++){
				var vb = datas[i];
				var dataId = vb[keyField];
				if(id==dataId){
					gridList.push(vb);
					break;
				}
			}
			keyData += id + ",";
			strData += title + ",";
		});
		options.getGridData(gridList);
		$(obj).val(strData.substring(0,strData.length-1));
		$(obj).parent().find("input[name='getKeyValue']").val(keyData.substring(0,keyData.length-1));
		$("#demo_div").remove();
	}
	// 展示所选数据
	function showCheckData(name,tableId,obj){
		$("#showData").show();
		// 叠加数据信息
		var width = $("#demo_div").css("max-width");
		$("#demo_div").css("max-width",parseInt(width)+300);
		var _btnNum = $("#dataChecked").find("button[id='"+tableId+"']").length;
		if(_btnNum==0){
			var _button_div = $("<div style='float:left;width:180px;'></div>");
			var _button_css = "float:left;width:150px;height:30px;margin-left: 5px;margin-top: 3px;border: 1px solid #ccc;color: #666;border-top-left-radius: 3px;border-bottom-left-radius: 3px;background: -webkit-linear-gradient(#DDE8ED, #FAFCFC);background: -o-linear-gradient(#DDE8ED, #FAFCFC);background: -moz-linear-gradient(#DDE8ED, #FAFCFC);background: linear-gradient(#DDE8ED, #FAFCFC);";
			var _button = $("<button style='"+_button_css+"'></button>");
			
			var str_name = name.length>8?(name.substring(0,8)+"..."):name;
			_button.attr("title",name);
			_button.attr("name",useColnum);
			_button.attr("id",tableId);
			_button.text(str_name);
			var _span_css = "float:left;margin-top: 3px;cursor: pointer;line-height:30px;display: inline-block;position: relative;width:23px;height:30px;border: 1px solid #B14600;border-top-right-radius: 3px;border-bottom-right-radius: 3px;background: #FF6600;";
			var _span = $("<div style='"+_span_css+"'></div>");
			var _svg = $('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin-top:3px;" x="0px" y="0px" width="20px" height="20px" viewBox="10 10 1000 1000" enable-background="new 10 10 1000 1000" xml:space="preserve">'+
						'<g><path fill="white" d="M843.9,80.3H636.7V10H361.1v70.3H153.9v69.3h690.1L843.9,80.3z"/>'+
						'<path fill="white" d="M845.7,203.6H85.5v101.8h69V990h691.1V305.4h68.8V203.6H845.7L845.7,203.6z M353.4,890.8h-93.2V304.4h93.2V890.8z M546.3,890.8h-93.2V304.4h93.2V890.8z M742.5,890.8h-93.2V304.4h93.2V890.8z"/></g>'+
						'</svg>');
			_span.append(_svg);
			// 删除所选项
			_span.click(function(){
				var tId = $(this).prev().attr("id");
				$("#demo_tbody").find("tr[id='"+tId+"']").find("input[type=checkbox]").attr('checked',false);
				$("#demoCheckBox").attr('checked',false);
				var _zTree = $.fn.zTree.getZTreeObj("downGridTree");
				if(_zTree!=null){
					var options = $("#downGridTree").data("opts");
					var keyField = options.keyField;
					var node = _zTree.getNodeByParam(keyField,tId);
					_zTree.checkNode(node,false,false);
				}
				$(this).prev().remove();
				$(this).remove();
				if($("#dataChecked").find("button").length==0){
					$("#dataChecked").html("");
					$("#showData").hide();
					// 清空文本框里的数据
					$(obj).val('');
					$(obj).parent().find("input[name='getKeyValue']").val('');
				}
			});
			_button_div.append(_button).append(_span);
			$("#dataChecked").append(_button_div);
		}
	}
	// 传递数据
	function loadPageData(options,inputVal,obj){
		// 拼接sql语句
		var sql = options.sql;
		var searchColnum = options.searchColnum;
		var jsonIgnore = options.jsonIgnore;
		var maxDataLimit = options.maxDataLimit;
		var dataParams = {};
		var paramsValue = new Array();
		// sql不为空时
		if(sql!=null && sql!=''){
			paramsValue = options.paramsValue;
		}else{
			dataParams = options.dataParams;
		}
		var paramsArr = paramsValue;
		if(inputVal!=null && inputVal!=""){
			sql += " and "+searchColnum+" like ? ";
			paramsArr = paramsValue.concat("%"+inputVal+"%");
		}
		// 排序
		var orderBy = options.orderBy;
		var sort = options.sort;
		var orderBySql = " order by "+orderBy+" "+sort;
		dataParams.sql=sql;
		dataParams.orderBySql=orderBySql;
		dataParams.paramsValue=paramsArr.join(",");
		dataParams.jsonIgnore=jsonIgnore.join(",");
		dataParams[searchColnum]=paramsArr.join(",");
		var isTree = options.isTree;
		if(isTree){// 树对应id(树节点id),name(树节点名称),pId(树节点父级id)三个固定字段名称，这三个必须存在，也可自定义添加字段名称
			var treeData = options.treeData;
			dataParams.treeData = JSON.stringify(treeData);
		}else{// 表格限制显示最多条数，树不限制
			dataParams.limit = maxDataLimit;
		}
		$.ajax({
			type: "POST",
			url: options.url,
			async:false,
			data:dataParams,
			dataType:"json",
			success: function(msg){
				var isTree = options.isTree;
				if(isTree){// 显示树
					$(obj).data($(obj).attr("id"),msg.list);
					loadTreeContent(msg.list,options,obj);
				}else{// 显示表格
					$(obj).data($(obj).attr("id"),msg.list);
					loadPageContent(msg,options,obj);
				}
			}
		});
	}
	// 树加载数据
	function loadTreeContent(data,options,obj){
		var isMultiple = options.isMultiple;
		var setting = {
			data: {
				simpleData: {
					enable: true
				}
			},
			check: {
				enable: isMultiple
			}
		};
		var treeSet = options.treeSetting;
		treeSet.callback = {
			onCheck:treeOnCheck,
			onClick:treeOnClick
		};
		if(treeSet.check!=null){
			treeSet.check.enable = isMultiple;
		}
		var treeSetting = $.extend({}, setting, treeSet);
	    var _zTree = $.fn.zTree.init($("#downGridTree"), treeSetting, data);
	    $("#downGridTree").data("opts",options);
	    $("#downGridTree").data("inputObj",obj);
	    // 设置值被选中
	    var keyValue = $(obj).parent().find("input[name='getKeyValue']").val();
	    if(keyValue!=null && keyValue!=''){
    		var keyField = options.keyField;
	    	if(isMultiple){
		    	var keyArr = keyValue.split(",");
		    	for (var i = 0; i < keyArr.length; i++) {
		    		var node = _zTree.getNodeByParam(keyField,keyArr[i]);
		    		_zTree.checkNode(node, true, true);
		    		node.checked = true;
		    		_zTree.updateNode(node);
		    		showCheckData(node.name,node[keyField],obj);
				}
	    	}
//	    	else{
//	    		var node = _zTree.getNodeByParam(keyField,keyValue);
//	    		_zTree.selectNode(node,true);
//	    	}
	    }
	}
	// 树单击选择事件
	function treeOnClick(event, treeId, treeNode, clickFlag){
		var options = $("#downGridTree").data("opts");
		var isMultiple = options.isMultiple;
		if(!isMultiple){
			var obj = $("#downGridTree").data("inputObj");
			var datas = $(obj).data($(obj).attr("id"));
			var keyField = options.keyField;
			var useColnum = options.useColnum;
			var datas = $(obj).data($(obj).attr("id"));
			var gridList = new Array();
			var keyData = treeNode[keyField];
			var strData = treeNode[useColnum];
			for (var i = 0; i < datas.length; i++){
				var vb = datas[i];
				var dataId = vb[keyField];
				if(keyData==dataId){
					gridList.push(vb);
					break;
				}
			}
			$(obj).parent().find("input[name='getKeyValue']").val(keyData);
			$(obj).val(strData);
			$("#demo_div").remove();
			options.getGridData(gridList);
		}
	}
	// 树多选框选择事件
	function treeOnCheck(event, treeId, treeNode){
		// 得到控件对象
		var _inputObj = $("#downGridTree").data("inputObj");
		// 得到控件参数
		var _options = $("#downGridTree").data("opts");
		var keyField = _options.keyField;
		// 得到树对象
		var _zTree = $.fn.zTree.getZTreeObj("downGridTree");
		// 得到勾选的对象
		var nodes_yes = _zTree.getCheckedNodes(true);
		for (var i = 0; i < nodes_yes.length; i++) {
			var n = nodes_yes[i];
			showCheckData(n.name,n[keyField],_inputObj);
		}
		// 得到未勾选的节点的对象
		var nodes_no = _zTree.getCheckedNodes(false);
		for (var i = 0; i < nodes_no.length; i++) {
			var n = nodes_no[i];
			var _btn = $("#dataChecked").find("button[id='"+n[keyField]+"']");
    		_btn.next().remove();
    		_btn.remove();
    		if($("#dataChecked").find("button").length==0){
				$("#dataChecked").html("");
				$("#showData").hide();
				// 清空文本框里的数据
				$(_inputObj).val('');
				$(_inputObj).parent().find("input[name='getKeyValue']").val('');
			}
		}
	}
	// 判断选中数据后是否是全选
	function checkedIsAll(){
		var flag = false;
		$("#demo_tbody tr").each(function(){
			var $_obj = $(this).find("input[type=checkbox]");
			if($_obj.prop("checked")){
				flag = true;
			}else{
				flag = false;
				return flag;
			}
		});
		if(flag)
			$("#demoCheckBox").prop('checked',true);
		else
			$("#demoCheckBox").prop("checked",false);
	}
	// 表格加载数据
	function loadPageContent(data,options,obj){
		var isMultiple = options.isMultiple;
		var isNumber = options.isNumber;
		$("#demo_tbody").html("");
		var inputColArr = options.inputColnums;
		if(!data.list || data.list.length<=0){
			var _num = inputColArr.length+1;
			if(isNumber)
				_num += 1;
			$("#demo_tbody").html('<tr><td colspan="'+_num+'" align="center"><b>查询结果无数据</b></td></tr>');
			return;
		}
		checkBoxSum = data.list.length;
		var keyField = options.keyField;
		for(var i=0;i<data.list.length;i++){
			var _td = "";
			var vb = data.list[i];
			var _id = vb[keyField];
			var _tr = $("<tr id='"+_id+"' style='cursor: pointer;'></tr>");
			_tr.click(function(){
				tbodyTrClick($(this),options,obj);
			});
			// 多选框
			if(isMultiple){
				_td = '<td width="20" style="text-align:center;"><input value='+_id+' type="checkbox"></td>';
				var keyValue = $(obj).parent().find("input[name='getKeyValue']").val();
				if(keyValue!=null && keyValue!='' && keyValue.indexOf(_id)>-1){
					_td = '<td width="20" style="text-align:center;"><input value='+_id+' type="checkbox" checked></td>';
					showCheckData(vb[useColnum],_id,obj);
				}else{
					var _btnNum = $("#dataChecked").find("button[id='"+_id+"']").length;
					if(_btnNum==1)
						_td = '<td width="20" style="text-align:center;"><input value='+_id+' type="checkbox" checked></td>';
				}
			}
			// 是否显示编号
			if(isNumber){
				_td += '<td width="20" style="text-align:center;">'+(i+1)+'</td>';
			}
			for (var j = 1; j <= inputColArr.length; j++) {
				var _col = inputColArr[j-1];
				_td += '<td name="'+_col+'" style="text-align:center;"><div style="margin:auto;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;" title="'+vb[_col]+'">'+vb[_col]+'</div></td>';
			}
			_tr.append(_td);
			$("#demo_tbody").append(_tr);
		}
		checkedIsAll();
		options.success(data.list);
	}
	// 搜索框输入事件
	function searchInputKeyup(options,obj){
		$("#searchData").on('input propertychange',function(e){
			$("#searchData").parent().find("span").hide();
			var val = $(this).val();
			loadPageData(options,val,obj);
		});
	}
	// 初始化表格
	function initDiv(obj,options){
		var width = options.width;
		var height = options.height;
		var inputColArr = options.inputColnums;
		var showColArr = options.showColnums;
		var searchPrompt = options.searchPrompt;
		var isMultiple = options.isMultiple;
		var isNumber = options.isNumber;
		var isTree = options.isTree;
		var isCascade = options.isCascade;
		// 得到悬浮层绝对位置
		var _top = $(obj).offset().top;
		var _left = $(obj).offset().left;
		var _height = $(obj).css("height").replace("px","");
		// 得到要使用并展示到文本框里的字段名所处第几列
		useColnum = options.useColnum;
		if(isNumber && isMultiple)
			colnumIndex = 2;
		if((!isNumber && isMultiple) || (isNumber && !isMultiple))
			colnumIndex = 1;
		for ( var i = 0; i < inputColArr.length; i++) {
			var inputCol = inputColArr[i];
			if(inputCol==useColnum){
				colnumIndex += i;
				break;
			}
		}
		var zIndex = parseInt(getMaxZIndex())+100;
		var _div = $("<div id='demo_div' style=' left:"+_left+"px;top:"+(parseInt(_top)+parseInt(_height)+2)+"px;max-width:"+width+";max-height:"+height+";position: absolute;z-index: "+zIndex+";-webkit-transition: .5s ease-in-out;-moz-transition: .5s ease-in-out;-o-transition: .5s ease-in-out;background-color:white;'></div>");
		// 鼠标移入移出事件
		_div.on({
            mouseover : function(){  
            	$(obj).unbind("blur");
            	$(window).unbind("click");
            },
            mouseout : function(){
            	$(obj).on("blur",function() {
        			$("#demo_div").remove();
        		});
            	$(obj).focus();
            }
        });
		var _tableDiv = $("<div id='demo_tableDiv' style='width:"+width+";height:"+height+";float:left;background-color: white;border: 1px solid #bcbdbd;overflow: auto;'></div>");
		// 搜索框
		var _searchDiv = $("<div width='100%' style='margin: 5px;'></div>");
		var showInputText = options.showInputText;
		if(showInputText){
			var _searchText = $("<input type='text' id='searchData' class='form-control' style='height: 30px;' placeholder='"+searchPrompt+"' />");
			_searchDiv.append(_searchText);
		}
		// 为树时显示选择级联项选项
		if(isTree && isCascade){
			var _cascade_div = $("<div style='padding-left:23px;'><lebel>选择级联项</label></div>");
			var _cascade_input = $("<input id='cascade_check' type='checkbox' checked />");
			_cascade_div.prepend(_cascade_input);
			_tableDiv.append(_cascade_div);
			_cascade_input.on("click",function(){
				var _c = $(this).is(':checked');
				var _zTree = $.fn.zTree.getZTreeObj("downGridTree");
				var datas = $(obj).data($(obj).attr("id"));
				if(!_c){
					_zTree.setting.check.chkboxType = {
						"Y": "", "N": ""
					};
				}else{
					_zTree.setting.check.chkboxType = {
						"Y": "ps", "N": "ps"
					};
				}
				$.fn.zTree.init($("#downGridTree"), _zTree.setting, datas);
			});
		}
		// 是否以树结构展示出来
		if(isTree){// 树
			var _ul = "<ul id='downGridTree' class='ztree'></ul>";
			_tableDiv.append(_searchDiv).append(_ul);
		}else{// 表格
			var _table_css = "width:100%;border-top: 1px solid #ccc;border-bottom: 1px solid #ccc;color: #999;background: #fff;background: #fcfcfc;font: normal 12px/35px arial,sans-serif;text-align:center; table-layout:fixed;";
			var _table = $("<table id='demo_table' style='"+_table_css+"'></table>");
			_tableDiv.append(_searchDiv).append(_table);
			var _thead = $("<thead></thead>");
			var _thead_tr = $("<tr></tr>");
			_thead.append(_thead_tr);
			var _thead_tr_th = "";
			if(isMultiple)
				_thead_tr_th = "<th style='text-align: center;' width='20'><input id='demoCheckBox' type='checkbox'></th>";
			// 得到表格里的title
			if(inputColArr.length != showColArr.length){
				return;
			}
			// 是否显示编号
			if(isNumber){
				_thead_tr_th += '<td width="20" style="text-align:center;"></td>';
			}
			for ( var i = 0; i < inputColArr.length; i++) {
				var inputCol = inputColArr[i];
				var showCol = showColArr[i];
				_thead_tr_th += "<th style='text-align: center'>"+showCol+"</th>";
			}
			_thead_tr.append(_thead_tr_th);
			var _tbody = $("<tbody id='demo_tbody'></tbody>");
			_table.append(_thead).append(_tbody);
		}
		// 选中数据后的展示
		var dataPosition = options.dataPosition;
		if(dataPosition=='R'){
			_div.append(_tableDiv).append(positionRight(height,obj,options));
		}else if(dataPosition=='T'){
			_div.append(positionTop(width,obj,options)).append(_tableDiv);
			var _span = $("#searchData").parent().find("span");
			var _top = _span.css("top");
			_span.css("top",_top+200);
		}
		return _div;
	}
	// 选中的数据居右显示
	function positionRight(h,obj,options){
		var _showDiv = $("<div id='showData' style='display: none;float:left;height:"+parseInt(h)+"px;width:200px;background-color: white;border-right: 1px solid #bcbdbd;'></div>");
		var _show_span = $("<div style='height:"+(parseInt(h)-50)+"px;overflow-y: auto;' id='dataChecked'></div>");
		var _show_button_div = $("<div style='margin-top:5px;height:45px;text-align: right;border-top: 1px solid #e5e5e5;background: #f2f2f2;border-bottom: 1px solid #bcbdbd;'></div>");
		var _show_button_css = "margin: 8px;width:66px;height:33px;box-shadow:1px 1px 0px #fff inset;background: #00A1E5;color: #fff;border: 1px solid #0078CB;border-radius: 5px;";
		var _show_button = $("<button id='submitDataBtn' style='"+_show_button_css+"' type='button'>确认</button>").click(function(event){
			confirmClick(obj,options);
		});
		_show_button_div.append(_show_button);
		_showDiv.append(_show_span).append(_show_button_div);
		return _showDiv;
	}
	// 选中的数据居上显示
	function positionTop(w,obj,options){
		var _showDiv = $("<div id='showData' style='display: none;width:"+parseInt(w)+"px;background-color: white;border-right: 1px solid #bcbdbd;border-left: 1px solid #bcbdbd;'></div>");
		var _show_span = $("<div style='max-height:300px;overflow-y: auto;' id='dataChecked'></div>");
		var _show_button_div = $("<div style='margin-top:5px;height:50px;text-align: right;border-top: 1px solid #e5e5e5;'></div>");
		var _show_button_css = "margin: 8px;width:66px;height:33px;box-shadow:1px 1px 0px #fff inset;background: #00A1E5;color: #fff;border: 1px solid #0078CB;border-radius: 5px;";
		var _show_button = $("<button id='submitDataBtn' style='"+_show_button_css+"' type='button'>确认</button>").click(function(){
			confirmClick(obj,options);
		});
		_show_button_div.append(_show_button);
		_showDiv.append(_show_span).append(_show_button_div);
		return _showDiv;
	}
	// 初始化
	function init(obj,options){
		maxSelectLimit = options.maxSelectLimit;
		// 文本框点击事件
		$(obj).click(function(e){
			e.stopPropagation();
			$("#demo_div").remove();
			var _keyObj = $(obj).parent().find("input[name='getKeyValue']");
			if(_keyObj.length==0){
				$(obj).parent().append("<input type='hidden' name='getKeyValue' />");
			}
			$("body").append(initDiv(obj,options));
			loadPageData(options,"",obj);
			searchInputKeyup(options,obj);
	        checkBoxAllClick(obj);
			// 文本框失去焦点事件
			$(obj).blur(function() { 
				$("#demo_div").remove();
			});
		});
		var isInput = options.isInput;
		if(!isInput){
			// 文本框禁止输入
			$(obj).attr("readonly","readonly");
			$(obj).css({
				"cursor":"auto",
				"background-color":"white"
			});
		}
	}
	// 组件初始化
	$.fn.downGrid = function(method, options) {
		if (typeof (method) == "string") {
			if(options==null){
				var opts = $.extend({},$.fn.downGrid.defaults,method);
				return $.fn.downGrid.methods[method](this, opts);
			}else
				return $.fn.downGrid.methods[method](this, options);
		} else {
			var opts = $.extend({},$.fn.downGrid.defaults,method);
			init(this,opts);
		}
	};
	// 悬浮层始终在最外层
	function getMaxZIndex() {
		var maxZ = Math.max.apply(null, 
	    　　	$.map($('body *'), function(e,n) {
	    　　		if ($(e).css('position') != 'static')
	        　			return parseInt($(e).css('z-index')) || -1;
	    }));
	    return maxZ;
	}
	// 设置数据
	function setDownGridData(obj, datas) {
		var _keyObj = $(obj).parent().find("input[name='getKeyValue']");
		if(_keyObj.length==0){
			$(obj).parent().append("<input type='hidden' name='getKeyValue' value='"+datas.join(",")+"' />");
		}
	}
	// 得到数据
	function getDownGridDatas(obj, options){
		var _keyObj = $(obj).parent().find("input[name='getKeyValue']").val();
		var gridList = new Array();
		if(_keyObj!=null && _keyObj!=''){
			var _keyArr = _keyObj.split(",");
			var keyField = options.keyField;
			var datas = $(obj).data($(obj).attr("id"));
			for(var j = 0; j < _keyArr.length; j++) {
				for (var i = 0; i < datas.length; i++){
					var vb = datas[i];
					var dataId = vb[keyField];
					if(_keyArr[j]==dataId){
						gridList.push(vb);
						break;
					}
				}
			} 
		}
		return gridList;
	}
	// 得到激活组建时的所有值，组建自定义输入文字时判断是否和数据库中的数据相吻合
	function getAllDatas(obj, options){
		var datas = $(obj).data($(obj).attr("id"));
		return datas;
	}
	// 组件默认参数
	$.fn.downGrid.defaults = {
		width:"300px",
		height:"300px",
		url: "",// 后台访问地址
		dataParams:{},// 只有url时的参数配置
		treeData:{},//isTree为true时且使用sql语句查询数据时，对应id,name,pId三个字段名称
		sql: "",// 数据库操作HQL语句
		showColnums:[],// 表格中要使用的在页面显示的字段名
		inputColnums:[],// 表格中要使用的在实体里数据库查询的字段名
		paramsValue:[],// sql语句中的查询条件的值，如果HQL语句中有?时需填的值
		jsonIgnore:[],// 返回的json数据里要忽略的字段，sql不为空的时候，用来忽略json里一对多的字段或多对多的字段
		searchColnum:"",// 要搜索的字段名
		searchPrompt:"",// 搜索框提示语
		useColnum:"",// 要使用并展示到文本框里的字段名
		keyField:"table_id",// 主键
		orderBy:"table_id",// 要排序的字段
		sort:"desc",// desc倒序，asc升序
		dataPosition:"T",// 选中数据后显示的位置（T:top,R:right）
	    maxSelectLimit:3,// 限制最多选中三个项目
	    maxDataLimit:200,// 限制显示最多条数
	    isMultiple:true,// 是否多选
	    isInput:true,// 控件是否允许输入
	    isNumber:true,// 表格是否显示编号
	    isTree:false,// 是否显示树
	    treeSetting : {},// 树参数
	    showInputText:true,//是否显示搜索框
	    isCascade:true,//是否选择级联项，isTree:true时有效
		getGridData: function(data) {// 回调函数，选中数据之后点击确认按钮之后的操作
		},
		success: function(data) {
		}
	};
	
	// 组件方法注册
	$.fn.downGrid.methods = {
		init : function(_this, opts) {
			init(_this,opts);
		},
		setDownGridDatas: function(_this,datas){
			return setDownGridData(_this,datas);
		},
		setDownGridData: function(_this,data){
			var dataArr = new Array();
			dataArr.push(data);
			return setDownGridData(_this,dataArr);
		},
		getDownGridDatas: function(_this,opts){
			return getDownGridDatas(_this,opts);
		},
		getAllDatas: function(_this,opts) {
			return getAllDatas(_this,opts);
		}
	};
})(jQuery);