<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div style="margin-top: 5px; width: 100%; height: 100%;">
	<div>
		<form class="form-inline" style="margin: 0px; padding: 0px;background-color: white;padding: 4px">
			<select id="systemLogLevel" class="form-control" style="width: 100px;">
				<option value=""></option>
				<option value="INFO">INFO</option>
				<option value="DEBUG">DEBUG</option>
				<option value="ERROR">ERROR</option>
			</select>
			<input id="systemLogStartDate" type="text" class="form-control" readonly="readonly" style="width: 150px;margin: 0px 20px 0px 20px;" />
			<input id="systemLogEndDate" type="text" class="form-control" readonly="readonly" style="width: 150px;"/>
			<button id="searchSystemLogBtnId" type="button" class="btn btn-info ml5" data-effect="mfp-rotateLeft">搜索</button>
			<button id="clearDateDataBtnId" type="button" class="btn btn-info ml5" data-effect="mfp-rotateLeft">清空时间</button>
		</form>
	</div>
	<div style="background-color: white;"> 
		<table id="system_log" class="table table-bordered mt10">
			<thead>
				<tr id="system_log_title">
					<th fname="number" width="50" style="text-align: center">编号</th>
					<!-- <th fname="systemLogCheckBox" width="40" style="text-align: center"><input id="roleCheckBox" type="checkbox"></th> -->
					<th fname="level" width="80" style="text-align: center">日志级别</th>
					<th fname=message width="1000"  style="text-align: center">日志信息</th>
					<th fname="showTime"  style="text-align: center">创建时间</th>
				</tr>
			</thead>
			<tbody id="system_log_table"">

			</tbody>
		</table>
	</div> 
	<nav aria-label="Page navigation example">
		<ul class="pagination" id="system_log_pagination">

		</ul>
	</nav>

</div>
<script>
(function(){
	function loadSystemLog(data){
		if(typeof(data) == 'undefined'){
			data = {};
		}
		data.start = 1;
		$('#system_log').wgrid({
			table_title: "system_log_title",
			tbodyId: "system_log_table",
			pagination:"system_log_pagination",
			url: basePath + "/systemAction/systemAction.do?method=getSystemLog",
			checkbox: false,
			data: data,
			success:function(data){
			}
		});
	}
	$("#searchSystemLogBtnId").on("click",function(){
		var data = {
				"level":$("#systemLogLevel").val(),
				"string1":$("#systemLogStartDate").val(),
				"string2":$("#systemLogEndDate").val()
		};
		loadSystemLog(data);
	});
	$("#clearDateDataBtnId").on("click",function(){
		$("#systemLogStartDate").val("");
		$("#systemLogEndDate").val("");
	});
	$(function(){
		loadSystemLog();
		$('#systemLogStartDate').datetimepicker({
			lang:"ch",  //中文
			format:"Y-m-d H:i", //日期格式化
			yearStart:2018     //设置最小年份
		}
		);
		$('#systemLogEndDate').datetimepicker({
			lang:"ch",  //中文
			format:"Y-m-d H:i", //日期格式化
			yearStart:2018     //设置最小年份
		}
		);
	});
})();
</script>