<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="modal fade" id="addOrganizationModal" tabindex="1000" role="dialog" aria-labelledby="addOrganizationModalLabel" aria-hidden="true" style="overflow: hidden;">
	<div class="modal-dialog" style="width:80%;">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title mt5">
					数据展示<a style="float: right" id="organizationPanelDivCloseId"><span style="font-size: 16px;" class="glyphicon glyphicon-remove"></span></a>
				</h4>

			</div>
			<div class="modal-body" style="overflow: auto;">
				<table id="data_file_table_modal" class="table table-bordered">
					<thead>
						<tr id="data_file_title_modal">
							<th fname="fileName" width="250" style="text-align: center">文件名</th>
							<th fname="fileDocID" width="200" style="text-align: center">文号</th>
							<th fname="fileLanguage" width="80" style="text-align: center">语言</th>
							<th fname="fileRange" width="70" style="text-align: center">分类</th>
							<th fname="fileType" width="40" style="text-align: center">后缀</th>
							<th fname="" width="40" style="text-align: center">操作</th>
						</tr>
					</thead>
					<tbody id="data_file_tbody_modal">

					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button id="cancel" type="button" class="btn btn-info ml5 ">取消</button>
			</div>
		</div>
	</div>
</div>
<script>
	(function() {
		$(function() {

		});
		//加载table
		function loadGridModal(id){
			var url = "${pageContext.request.contextPath}/file/fileAction.do?method=getFilesByDocId";
			$.post(url,{"docId":id},function(data){
				var jsonArr = jQuery.parseJSON(data);
				for(var i = 0 ;i < jsonArr.length;i++){
					var json = jsonArr[i];
					var _tr = $("<tr></tr>");
					var _td_fileName = $("<td>"+json.fileName+"</td>");
					var _td_fileDocID= $("<td>"+json.fileDocID+"</td>");
					var _td_fileLanguage = $("<td>"+json.fileLanguage+"</td>");
					var _td_fileRange = $("<td>"+json.fileRange+"</td>");
					var _td_fileType = $("<td>"+json.fileType+"</td>");
					var _td_deal = $("<td tId = '"+json.table_id+"' ></td>");
					var _td_deal_btn = $('<button type="button" class="btn btn-info ml5"  >预览</button>');
					
					_td_deal.append(_td_deal_btn);
					_tr.append(_td_fileName);
					_tr.append(_td_fileDocID);
					_tr.append(_td_fileLanguage);
					_tr.append(_td_fileRange);
					_tr.append(_td_fileType);
					_tr.append(_td_deal);
					
					$("#data_file_tbody_modal").append(_tr);
					
					_td_deal_btn.on("click",function(){
						var table_id = $(this).parent().attr("tId");
						if(table_id == '' || table_id == undefined){
							return;
						}
						var tableid = window.Base.encode("table_id="+table_id);
						
						window.open("${pageContext.request.contextPath}/static/pdfjs/web/viewer.jsp?"+tableid);
					});
				}
			});
		}
		//关闭
		$("#organizationPanelDivCloseId").click(function() {
			$("#addOrganizationModal").remove();
		});
		//取消
		$("#cancel").on("click", function() {
			$("#addOrganizationModal").remove();
		});

		$.fn.extend({
			loadGridModal: function(docId){
				loadGridModal(docId);
			}
		});
	})();
</script>
