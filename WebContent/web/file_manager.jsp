<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div style="margin-top: 5px; width: 100%; height: 100%;background-color: white;">
	<!-- <div class="bg-light lighter mb5 pb5"> -->
	<div>
		<form class="form-inline" style="margin: 0px; padding: 0px; background-color: white; padding: 4px">

			<!-- <button id="delete_file_Btn" type="button" class="btn btn-danger ml5 mt5" data-toggle="modal" data-target="deleteFileModal">删除文件</button> -->
			<div class="input-group ml10">
				<input type="text" readonly="readonly" class="form-control ml5" meth="isMinLength-30" placeholder="开始时间" id="reptileStartTimeModel">
			</div>
			<div class="input-group ml10">
				<input type="text" readonly="readonly" class="form-control" meth="isMinLength-30" placeholder="结束时间" id="reptileEndTimeEModel">
			</div>
			<div class="input-group ml10">
				<button type="button" class="btn btn-info ml5"  id="reptileRemoteHtmlData">爬取数据</button>
			</div>
			<div class="input-group ml10">
				<input type="text" id="searchFileModel" class="form-control" placeholder="请输入文件名">
			</div>
			<div class="input-group ml10">
				<input type="text" id="searchFileDocModel" class="form-control" placeholder="请输入文号">
			</div>
			<div class="input-group ml10">
				<button type="button" class="btn btn-info ml5"  id="searcheFile_btn">搜索</button>
			</div>
			<div class="input-group ml10">
				<button type="button" class="btn btn-info ml5"  id="downloadFiles">下载</button>
			</div>
			<div class="input-group ml10">
				<button type="button" class="btn btn-info ml5"  id="previewRemoteHtmlData">预览</button>
			</div>
		</form>
	</div>
	<div class="sctrcontent mCustomScrollbar">
		<table id="data_file_table" class="table table-bordered">
			<thead>
				<tr id="data_file_title">
					<th fname="number" width="70" style="text-align: center">编号</th>
					<th fname="file_ckbox" width="50" style="text-align: center"><input id="client_ckbox" type="checkbox"></th>
					<th fname="fileName" width="250" style="text-align: center">文件名</th>
					<th fname="fileDocID" width="200" style="text-align: center">文号</th>
					<th fname="fileLanguage" width="80" style="text-align: center">语言</th>
					<th fname="fileSize" width="70" style="text-align: center">文件大小</th>
					<th fname="fileRange" width="70" style="text-align: center">分类</th>
					<th fname="fileDomain" width="70" style="text-align: center">领域 </th>
					<th fname="fileAnnual" width="100" style="text-align: center">届会/年份 </th>
					<th fname="fileAgenda" width="70" style="text-align: center">议程项目 </th>
					<th fname="fileJobID" width="80" style="text-align: center">工号</th>
					<th fname="filePublishDate" width="80" style="text-align: center">发布日期</th>
					<th fname="fileType" width="40" style="text-align: center">后缀</th>
					<th fname="path" width="150" style="text-align: center">文件路径</th>
				</tr>
			</thead>
			<tbody id="data_file_tbody">

			</tbody>
		</table>
	</div>
	<nav aria-label="Page navigation example">
		<ul class="pagination" id="file_pagination">

		</ul>
	</nav>

</div>
<div id="relevancyModal"  ></div>

<script>

(function(){
	//加载table
	function loadGrid(event,data){
		if(typeof(data) == 'undefined'){
			data = {};
		}
		data.start = 1;
		$('#data_file_table').wgrid({
			table_title: "data_file_title",
			tbodyId: "data_file_tbody",
			pagination:"file_pagination",
			url: "${pageContext.request.contextPath}/file/fileAction.do?method=getFilesList",
			checkbox: true,
			checkboxId: "file_ckbox",
			data: data,
			tbodyTdCss:{
				"number":"text-center",
				"file_ckbox":"text-center"
			},
			success:function(data){
				console.log(data);
			}
		});
	}
	$(function(){
		String.prototype.replaceAll = function (FindText, RepText) {
		    regExp = new RegExp(FindText, "g");
		    return this.replace(regExp, RepText);
		};
		loadGrid();
		$("#reptileRemoteHtmlData").on("click",function(){
			debugger;
			var url = "${pageContext.request.contextPath}/file/fileAction.do?method=reptileRemoteData";
			var startDate = $("#reptileStartTimeModel").val().trim();
			var endDate = $("#reptileEndTimeEModel").val().trim();
			if(startDate == '' || endDate == ''){
				alert("开始时间与结束时间不能为空!");
				return;
			}
			var s = parseInt(startDate.replaceAll("-",""));
			var e = parseInt(endDate.replaceAll("-",""));
			
			if( s >  e ){
				alert("结束时间不能小于开始时间!");
				return;
			}
			
			/* if(e - s > 1){
				alert("最多同时只能爬取两天数据!");
				return;
			} */
			 $.post(url,{"startDate":startDate,"endDate":endDate},function(data){
				
				var json = jQuery.parseJSON(data);
				alert(json.msg);
			}); 
		});
		
		$('#reptileStartTimeModel').datetimepicker({
			lang : "ch", //中文
			format : "Y-m-d", //日期格式化
			timepicker:false,
			allowBlank:true
		}
		);
		
		$('#reptileEndTimeEModel').datetimepicker({
			lang : "ch", //中文
			format : "Y-m-d", //日期格式化
			timepicker:false,
			allowBlank:true
		});
		$("#searcheFile_btn").on("click",function(e){
			var params = {
					"fileName":$("#searchFileModel").val(),
					"fileDocID": $("#searchFileDocModel").val()
			};
			loadGrid(e,params);
		});
		$("#downloadFiles").on("click",function(e){
			var params = $("#data_file_table").wgrid("getTrData",{
				tbodyId: "data_file_tbody"
			});
			if(params.length <= 0){
				window.wxc.xcConfirm("至少选择一条数据","warning");
				return;
			}
			var ids = "";
			for(var i = 0;i < params.length;i++){
				ids = ids + params[i].fileDocID + ",";
			}
			var url = "${pageContext.request.contextPath}/file/fileAction.do?method=zipFiles&ids="+ids;
			window.open(url);
		});
		$("#previewRemoteHtmlData").on("click",function(){
			var params = $("#data_file_table").wgrid("getTrData",{
				tbodyId: "data_file_tbody"
			});
			if(params.length != 1){
				window.wxc.xcConfirm("只能选择一条数据","warning");
				return;
			}
			
			$("#relevancyModal").html("").load("${pageContext.request.contextPath}/web/show_files.jsp",{},function(data){
				$("#addOrganizationModal").modal({
					show:true,
					keyboard:false,
					backdrop:'static'
				});
				
				$(this).loadGridModal(params[0].fileDocID);
			});
			
		
		});
	});
})();
</script>