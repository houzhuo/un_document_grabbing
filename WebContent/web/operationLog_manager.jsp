<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 

<div style="margin-top:5px;width:100%;height:100%">
	<!-- <div class="bg-light lighter mb5 pb5"> -->
	<div>
		 <form class="form-inline" style="margin: 0px; padding: 0px;background-color: white;padding: 4px">
		 	<div class="bs-component">
               <div class="row form-group">
                   <div class="col-xs-1 ml10" style="width:9%">
		              <div class="bs-component ">
		                <button type="button" class="btn btn-danger" id="delete_operationLog_Btn" data-target="deleteOperationLogModal">删除日志</button>
		              </div>
		            </div>
					<div class="col-xs-1 ml10" style="width:9%">
						<div class="bs-component">
							<button type="button" class="btn btn-danger" id="importExcelDatas">导出excel</button>
						</div>
					</div>
					<div class="col-xs-2 ml10">
	                   <input type="text" class="form-control ml5" meth="isMinLength-30" placeholder="操作时间起" id="searchCreateTimeSModel">
	               </div>
                   <div class="col-xs-2 ml10">
                       <input type="text" class="form-control" meth="isMinLength-30" placeholder="操作时间止" id="searchcreateTimeEModel">
                   </div>
                   <div class="col-xs-2 ml10 ">
                       <input type="text" class="form-control" id="searchRoleNameModel" placeholder="请输入角色名称">
          		   </div>
                   <div class="col-xs-3 ml10">
                   	   <div class="input-group" id="search_operationLog_btnDivId">
						   <input type="text"  class="form-control" id="searchUserNameModel" placeholder="请输入操作人名称"> 
						   <span id="search_operationLog_btn" class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
					   </div>
                    </div>
              </div>
          </div>
		 
	 	</form>
	</div>
	<div class="sctrcontent mCustomScrollbar">
		<table id="data_operationLog_table" class="table table-bordered" >
			<thead>
				<tr id="data_operationLog_title">
					<th fname="number" width="70" style="text-align: center">编号</th>
					<th fname="operationLog_ckbox" width="50" style="text-align: center"><input id="operationLog_ckbox" type="checkbox"></th>
					<th fname="userName" width="120" style="text-align: center">操作人名称</th>
					<th fname="userEmployeeId" width="120" style="text-align: center">操作人工号</th>
					<th fname="roleName" width="120" style="text-align: center">操作角色</th>
					<th fname="operationType" width="80" style="text-align: center">操作类型</th>
					<th fname="tableName" width="120" style="text-align: center">操作表</th>
					<th fname="operationContent" width="300" style="text-align: center">操作内容</th>
					<th fname="operationResult" width="100" style="text-align: center">操作结果</th>
					<th fname="showTime" width="150" style="text-align: center">操作时间</th>
					<th fname="ip" width="110" style="text-align: center">IP地址</th>
				</tr>
			</thead>
			<tbody id="data_operationLog_tbody">
				
			</tbody>
		</table>
	</div>
	<nav aria-label="Page navigation example">
		<ul class="pagination" id="oLog_pagination">
		  
		</ul>
	</nav>
</div>
	<div id="modal-panel" style="max-width:100%">
		
	</div>
	<div id="relevancyModal" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width:65%">
	
	</div>

<script>

	(function(){
		var roleTag="${param.roleTag}";
		
		//DOM加载完成后立即加载
		$(function(){
			loadGrid();
			
		});
		
		$('#searchCreateTimeSModel').datetimepicker({
			lang:"ch",  //中文
			format:"Y-m-d", //日期格式化
			yearStart:2018, //设置最小年份
		}
		);
		
		$('#searchcreateTimeEModel').datetimepicker({
			lang:"ch",  //中文
			format:"Y-m-d", //日期格式化
			yearStart:2018     //设置最小年份
		});
	/**
		$('#searchCreateTimeSModel').datetimepicker({
		      lang:"ch", //语言选择中文 注：旧版本 新版方法：$.datetimepicker.setLocale('ch');
		      format:"Y-m-d",      //格式化日期
		      timepicker:false,    //关闭时间选项
		      yearStart：2000,     //设置最小年份
		      yearEnd:2050,        //设置最大年份
		      todayButton:false    //关闭选择今天按钮
		});
		*/
		//加载table
		function loadGrid(event,data){
			if(typeof(data) == 'undefined'){
				data = {};
			}
			data.start = 1;
			$('#data_operationLog_table').wgrid({
				table_title: "data_operationLog_title",
				tbodyId: "data_operationLog_tbody",
				pagination:"oLog_pagination",
				url: "${pageContext.request.contextPath}/operationLog/operationLogAction.do?method=getOperationLogList&roleTag=" + roleTag,
				checkbox: true,
				checkboxId: "operationLog_ckbox",
				data: data,
				tbodyTdCss:{
					"number":"text-center",
					"operationLog_ckbox":"text-center"
				},
				success:function(data){
					console.log(data);
				}
			});
		}
		
		//删除
		$("#delete_operationLog_Btn").click(function(){
			
			var params = $("#data_operationLog_table").wgrid("getTrData",{
				tbodyId: "data_operationLog_tbody"
			});
			if(params.length <= 0){
				alert("请选择数据");
				return;
			} 
			
			window.wxc.xcConfirm("是否确认删除？","confirm",{onOk:function(){
				$.ajax({
					type:"post",
					url:"${pageContext.request.contextPath}/operationLog/operationLogAction.do?method=deleteOperationLog",
					data:{"data":JSON.stringify(params)},
					success:function(data){
						var json = JSON.parse(data);
						if(json.msg == 1){
							alert("删除成功");
							loadGrid();
						}else{
							alert("删除失败");
						}
					}
				});
			}})
			
		});
		
		//搜索框回车事件
		$("#searchUserNameModel").keyup(function(e){
			if (e.keyCode == "13") {
				  e.preventDefault(); //阻止默认动作
			　　　　//回车执行查询
			　　　　$('#search_operationLog_btn').click();
			　　}
		});
		$("#searchRoleNameModel").keyup(function(e){
			if (e.keyCode == "13") {
				  e.preventDefault(); //阻止默认动作
			　　　　//回车执行查询
			　　　　$('#search_operationLog_btn').click();
			　　}
		});
		
		//查询
		$("#search_operationLog_btn").click(function(){
			var userName = $("#searchUserNameModel").val()== undefined ? '':$("#searchUserNameModel").val().trim();
			var roleName = $("#searchRoleNameModel").val()== undefined ? '':$("#searchRoleNameModel").val().trim();
			var startTime = $("#searchCreateTimeSModel").val()== undefined ? '':$("#searchCreateTimeSModel").val().trim();
			var endTime = $("#searchcreateTimeEModel").val()== undefined ? '':$("#searchcreateTimeEModel").val().trim();
			var data = {
						"userName":userName,
						"roleName" :roleName,
						"startTime" : startTime,
						"endTime" : endTime
			};
			loadGrid(this, data);
		});
		
		//导出Excel
		$("#importExcelDatas").click(function(){
			var userName = $("#searchUserNameModel").val()== undefined ? '':$("#searchUserNameModel").val().trim();
			var roleName = $("#searchRoleNameModel").val()== undefined ? '':$("#searchRoleNameModel").val().trim();
			var startTime = $("#searchCreateTimeSModel").val()== undefined ? '':$("#searchCreateTimeSModel").val().trim();
			var endTime = $("#searchcreateTimeEModel").val()== undefined ? '':$("#searchcreateTimeEModel").val().trim();
			window.open("${pageContext.request.contextPath}/operationLog/operationLogAction.do?method=importExcelDatas&userName="+
					userName+"&roleName="+roleName+"&startTime="+startTime+"&endTime="+endTime);
		})
		
		window.operationLog = {};
		window.operationLog.loadGrid = loadGrid;
		
	})();
</script>