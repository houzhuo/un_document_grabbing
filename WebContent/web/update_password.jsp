<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<div class="modal fade" id="updatePasswordModal" tabindex="1000" user="dialog" aria-labelledby="updatePasswordModalLabel" aria-hidden="true" style="overflow: hidden;">
		<div class="modal-dialog" style="width:600px;" > 
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title mt5" >修改密码<a style="float:right" id="updatePasswordModalCloseId"><span style="font-size:16px;" class="glyphicon glyphicon-remove"></span></a></h4>
				
			</div>
			<div class="modal-body" style="max-height:500px;overflow: auto;">
				<div id="update_password_div" class="form-horizontal">
					<input type="hidden" id="table_id">
					<div class="form-group">
						<label for="userName" class="col-lg-3 control-label">用户名称:</label>
						<div class="col-lg-8">
							<div class="bs-component">
								<input type="text" id="userName" name="userName" readonly="readonly" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group" id="showPassWord">
						<label for="passWord" class="col-lg-3 control-label">原密码:</label>
						<div class="col-lg-8">
							<div class="bs-component">
								<input type="password" id="passWord" name="passWord" require='true' meth="notNull,isMinLength-50" class="form-control" placeholder="请输入原密码(不能为空，长度小于50)">
							</div>
						</div>
					</div>
					<div class="form-group" id="showPassWord">
						<label for="temp1" class="col-lg-3 control-label">新密码:</label>
						<div class="col-lg-8">
							<div class="bs-component">
								<input type="password" id="temp1" name="passWord" require='true' meth="notNull,isMinLength-50" class="form-control" placeholder="请输入新密码(不能为空，长度小于50)">
							</div>
						</div>
					</div>
					<div class="form-group" id="showPassWord">
						<label for="temp2" class="col-lg-3 control-label">确认密码:</label>
						<div class="col-lg-8">
							<div class="bs-component">
								<input type="password" id="temp2" name="passWord" require='true' meth="notNull,isMinLength-50" class="form-control" placeholder="请确认新密码(不能为空，长度小于50)">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="saveUpdateBtn" type="button" class="btn btn-info ml5 ">保存</button>
				<button id="cancelUpdateModal" type="button" class="btn btn-info ml5 ">取消</button>
			</div>
		</div>
	</div>
		
	
	</div>
<script>
(function(){
	$(function(){
		
	});
	
	
	//保存
	function addOrUpdateUser(){
		$("#saveUpdateBtn").attr("disabled","disabled");
		var s = $("#update_password_div").hzValidate("init");
		if(!s){
			$("#saveUpdateBtn").removeAttr("disabled");
			return false;
		}
		 var params = OAajaxSubmit("update_password_div");
		 if(params.temp1 != params.temp2){
			 alert("两次密码不一样!");
			 $("#saveUpdateBtn").removeAttr("disabled");
				return false;
		 }
		 var url = "${pageContext.request.contextPath}/user/userAction.do?method=updatePassword";
		$.ajax({
			url:url,
			type:"post",
			data:params,
			async:false,
			success:function(data){
				var json = JSON.parse(data);
				if(json.status == 2){
					alert("原密码错误!");
					$("#saveUpdateBtn").removeAttr("disabled");
				}else if(json.status == 1){
					var f = confirm("修改成功，返回登录页面");
					location.href = "${pageContext.request.contextPath}/login.jsp";
				}else{
					alert("修改失败!");
					$("#saveUpdateBtn").removeAttr("disabled");
				}
			}
		});
		
	}
	
	//保存
	$("#saveUpdateBtn").on("click",function(){
		addOrUpdateUser();
	});
	//关闭
	$("#updatePasswordModalCloseId").click(function(){
		$("#updatePasswordModal").remove();
	});
	//取消
	$("#cancelUpdateModal").on("click",function(){
		$("#updatePasswordModal").remove();
	});
	
})();	
</script>
