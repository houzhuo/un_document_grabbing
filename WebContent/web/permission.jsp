<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 

<div style="margin-top:5px;width:100%;height:100%">
	<div class="col-adjust-8 mb10 bg-light pb10 lighter ">
		<div class="row mt4 ml5">
			<div class="form-inline mt5 text-left">
				<button id="updatePermissionBtn" type="button" class="btn btn-info ml5 mt5">修改权限</button>
				<div id="searchPermissionBtnDiv" class="input-group ml10 mt5">
					<input type="text" id="searchPermission" class="form-control" placeholder="请输入权限名称" style="height: 37px"> <span class="input-group-addon"><i id="searchPermissionBtn" style="cursor: pointer;" class="glyphicon glyphicon-search"></i></span>
				</div>
			</div>
		</div>
	</div>
	<div class="sctrcontent mCustomScrollbar">
		<table id="data_permission" class="table table-bordered bg-light" >
			<thead>
				<tr id="data_permission_title">
					<th fname="number" width="50" style="text-align: center">编号</th>
					<th fname="dataPermissionCheckBox" width="40" style="text-align: center"><input id="permissionCheckBox" type="checkbox"></th>
					<th fname="permissionName" width="120" style="text-align: center">权限名称</th>
					<th fname="icon" width="120" style="text-align: center">图标</th>
					<th fname="cid" width="120" style="text-align: center">cid</th>
					<th fname="pid" width="120" style="text-align: center">pid</th>
					<th fname="url" width="120" style="text-align: center">url</th>
					<th fname="startUsing" width="120" style="text-align: center">启用/禁用</th>
				</tr>
			</thead>
			<tbody id="data_permission_table" style="text-align: center">
				
			</tbody>
		</table>
	</div>
	<nav aria-label="Page navigation example">
		<ul class="pagination" id="permission_pagination">
		  
		</ul>
	</nav>
	
</div>
	<div id="permissionModalPanel" style="max-width:100%">
	</div>
	<script>
	(function(){
		//全局加载
		$(function(){
			$("body").append($("#permissionModalPanel"));
			loadGrid();
		});
		
		//加载table
		function loadGrid(event,data){
			if(typeof(data) == 'undefined'){
				data = {};
			}
			data.start = 1;
			$('#data_permission').wgrid({
				table_title: "data_permission_title",
				tbodyId: "data_permission_table",
				pagination:"permission_pagination",
				url: "${pageContext.request.contextPath}/permission/permissionAction.do?method=getAllPermission",
				checkbox: true,
				checkboxId: "permissionCheckBox",
				data: data,
				tbodyTdCss:{
					"number":"text-center",
					"dataRoleCheckBox":"text-center"
				},
				success:function(data){
					var divs = $("div[name=data_permission_table_startUsing_tr_td]");
					for(var i=0;i<divs.length;i++){
						var startUsing = $(divs[i]).text();
						if(startUsing == "1"){
							$(divs[i]).text("启用");
						}else{
							$(divs[i]).text("禁用");
						}
					}
				}
			});
		}
		
		//修改
		$("#updatePermissionBtn").click(function(){
			var params = $("#data_permission").wgrid("getTrData",{
				tbodyId: "data_permission_table"
			});
			if(params.length != 1){
				window.wxc.xcConfirm("请选择一条数据","warning");
				return;
			}
			$("#permissionModalPanel").html("").load("${pageContext.request.contextPath}/web/permission_add.jsp",{},function(data){
				$("#addPermissionModal").modal({
					show:true,
					keyboard:false,
					backdrop:'static'
				});
				setFormData("addPermissionModal", params[0]);
				//$("#startUsing").val(params[0].startUsing);
			})
			
		});
		//查询
		$("#searchPermissionBtn").click(function(){
			var permissionName = $("#searchPermission").val();
			var data = {"permissionName":permissionName};
			loadGrid(this,data);
		});

		
		window.permission = {};
		window.permission.loadGrid = loadGrid;
	})();
</script>