<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div style="margin-top: 5px; width: 100%; height: 100%;background-color: white;">
	<!-- <div class="bg-light lighter mb5 pb5"> -->
	<div>
		
	</div>
	<div class="sctrcontent mCustomScrollbar">
		<table id="data_file_table" class="table table-bordered">
			<thead>
				<tr id="data_file_title">
					<th fname="number" width="70" style="text-align: center">编号</th>
					<th fname="rr_ckbox" width="50" style="text-align: center"><input id="client_ckbox" type="checkbox"></th>
					<th fname="startDate" width="150" style="text-align: center">开始时间</th>
					<th fname="endDate" width="150" style="text-align: center">结束时间</th>
					<th fname="allNum" width="80" style="text-align: center">文件数</th>
					<th fname="reptlieNum" width="80" style="text-align: center">下载数</th>
					<th fname="showTime" width="150" style="text-align: center">创建时间</th>
				</tr>
			</thead>
			<tbody id="data_file_tbody">

			</tbody>
		</table>
	</div>
	<nav aria-label="Page navigation example">
		<ul class="pagination" id="file_pagination">

		</ul>
	</nav>

</div>
<script>
(function(){
	function loadGrid(event,data){
		if(typeof(data) == 'undefined'){
			data = {};
		}
		data.start = 1;
		$('#data_file_table').wgrid({
			table_title: "data_file_title",
			tbodyId: "data_file_tbody",
			pagination:"file_pagination",
			url: "${pageContext.request.contextPath}/file/fileAction.do?method=getRRList",
			checkbox: true,
			checkboxId: "client_ckbox",
			data: data,
			tbodyTdCss:{
				"number":"text-center",
				"file_ckbox":"text-center"
			},
			success:function(data){
				console.log(data);
			}
		});
	}
	$(function(){
		loadGrid();
	})
})();
</script>