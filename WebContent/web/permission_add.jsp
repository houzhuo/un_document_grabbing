<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<div class="modal fade" id="addPermissionModal" tabindex="1000" permission="dialog" aria-labelledby="addPermissionModalLabel" aria-hidden="true" style="overflow: hidden;">
		<div class="modal-dialog" style="width:600px;" > 
		<div class="modal-content" style="max-height:500px;overflow: auto;">
			<div class="modal-header">
				<h4 class="modal-title mt5" >编辑权限<a style="float:right" id="permissionPanelDivCloseId"><span style="font-size:16px;" class="glyphicon glyphicon-remove"></span></a></h4>
				
			</div>
			<div class="modal-body" style="overflow: auto;">
				<form id="addPermissionInfoId" class="form-horizontal" permission="form">
				<input type="hidden" id="table_id" >
				<div class="form-group">
					<label for="permissionName" class="col-lg-3 control-label">权限名称:</label>
					<div class="col-lg-8">
						<div class="bs-component">
							<input type="text" id="permissionName" name="permissionName"  require='true' meth="notNull,isMinLength-30" class="form-control" placeholder="请输入用户名称(不能为空，长度小于30)">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="startUsing" class="col-lg-3 control-label">启用/禁用:</label>
					<div class="col-lg-8">
						<div class="bs-component">
							<select id="startUsing" class="form-control">
								<option value="1">启用</option>
								<option value="0">禁用</option>
							</select>
						</div>
					</div>
				</div>
				</form>
			</div>
			<div class="modal-footer">
				<button id="save" type="button" class="btn btn-info ml5 ">保存</button>
				<button id="cancel" type="button" class="btn btn-info ml5 ">取消</button>
			</div>
		</div>
	</div>
	</div>
<script>
(function(){
	$(function(){
		
	});
	
	
	//保存
	function addOrUpdatePermission(){
		var s = $("#addPermissionInfoId").hzValidate("init");
		if(s == true){
			 var params = OAajaxSubmit("addPermissionInfoId");
			 var url = "${pageContext.request.contextPath}/permission/permissionAction.do?method=addOrUpdatePermission";		
			  $.ajax({
				url:url,
				type:"post",
				data:params,
				async:false,
				success:function(data){
					var json = JSON.parse(data);
					if(json.status == 1){
						window.wxc.xcConfirm("操作成功","success");
						$("#permissionPanelDivCloseId").trigger("click");//关闭modal
						window.permission.loadGrid();//刷新页面
					}
				}
			});
		}
	}
	
	//保存
	$("#save").on("click",function(){
		addOrUpdatePermission();
	});
	//关闭
	$("#permissionPanelDivCloseId").click(function(){
		$("#addPermissionModal").remove();
	});
	//取消
	$("#cancel").on("click",function(){
		$("#addPermissionModal").remove();
	});
	
})();	
</script>
