<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 

<div style="margin-top:5px;width:100%;height:100%">
	<div class="col-adjust-8 mb10 bg-light pb10 lighter ">
		<div class="row mt4 ml5">
			<div class="form-inline mt5 text-left">
				<button id="addOrganizationBtn" type="button" class="btn btn-info ml5 mt5" data-toggle="modal" data-target="#addOrganizationModal">添加机构</button>
				<button id="updateOrganizationBtn" type="button" class="btn btn-info ml5 mt5">修改机构</button>
				<button id="deleteOrganizationBtn" type="button" class="btn btn-danger ml5 mt5">删除机构</button>
				<div id="searchOrganizationBtnDiv" class="input-group ml10 mt5">
					<input type="text" id="searchOrganization" class="form-control" placeholder="请输入机构名称" style="height: 37px"> <span class="input-group-addon"><i style="cursor: pointer;" id="searchOrganizationBtn" class="glyphicon glyphicon-search"></i></span>
				</div>
			</div>
		</div>
	</div>
	<div class="sctrcontent mCustomScrollbar">
		<table id="data_organization" class="table table-bordered bg-light" >
			<thead>
				<tr id="data_organization_title">
					<th fname="number" width="50" style="text-align: center">编号</th>
					<th fname="dataOrganizationCheckBox" width="40" style="text-align: center"><input id="organizationCheckBox" type="checkbox"></th>
					<th fname="organizationName" width="120" style="text-align: center">机构名称</th>
					<th fname="organizationType" width="120" style="text-align: center">机构类型</th>
					<th fname="cid" width="120" style="text-align: center">cid</th>
					<th fname="pid" width="120" style="text-align: center">pid</th>
					<th fname="discription" width="120" style="text-align: center">机构描述</th>
				</tr>
			</thead>
			<tbody id="data_organization_table" style="text-align: center">
				
			</tbody>
		</table>
	</div>
	<nav aria-label="Page navigation example">
		<ul class="pagination" id="organization_pagination">
		  
		</ul>
	</nav>
	
</div>
	<div id="organizationModalPanel" style="max-width:100%;">
	</div>
	<script>
	(function(){
		
		
		//加载table
		function loadGrid(event,data){
			if(typeof(data) == 'undefined'){
				data = {};
			}
			data.start = 1;
			$('#data_organization').wgrid({
				table_title: "data_organization_title",
				tbodyId: "data_organization_table",
				pagination:"organization_pagination",
				url: "${pageContext.request.contextPath}/organization/organizationAction.do?method=getAllOrganization",
				checkbox: true,
				checkboxId: "organizationCheckBox",
				data: data,
				success:function(data){
					//console.log(data);
				}
			});
		}
		
		//添加
		$("#addOrganizationBtn").click(function(){
			 $("#organizationModalPanel").html("").load("${pageContext.request.contextPath}/web/organization_add.jsp",{"type":"add"},function(data){
				$("#addOrganizationModal").modal({
					show:true,
					keyboard:false,
					backdrop:'static'
				});
				var url1 = "${pageContext.request.contextPath}/permission/permissionAction.do?method=getMenuJsonData";
				var url2 = "${pageContext.request.contextPath}/organization/organizationAction.do?method=getOrganizationJsonData";
				loadMenuTree($("#menuTree"),url1);
				loadMenuTree($("#organizationTree"),url2);
			})
		});
		
		//修改
		$("#updateOrganizationBtn").click(function(){
			var params = $("#data_organization").wgrid("getTrData",{
				tbodyId: "data_organization_table"
			});
			if(params.length != 1){
				window.wxc.xcConfirm("请选择一条数据","warning");
				return;
			}
			$.ajax({
				url:"${pageContext.request.contextPath}/organization/organizationAction.do?method=getOrgaPrincipal",
				type:"post",
				data:{"table_id":params[0].table_id},
				async:false,
				dataType:"json",
				success:function(json){
					$("#organizationModalPanel").html("").load("${pageContext.request.contextPath}/web/organization_add.jsp",{},function(data){
						$("#addOrganizationModal").modal({
							show:true,
							keyboard:false,
							backdrop:'static'
						});
						//显示基本数据
						setFormData("addOrganizationModal", params[0]);
						if(json.table_id != null && json.table_id != "null"){
							$("#temp2").val(json.table_id);
							$("#principal").val(json.userName);
						}
						//显示对应权限
						var url = "${pageContext.request.contextPath}/permission/permissionAction.do?method=getMenuJsonData&type=orga&table_id="+params[0].table_id;
						loadMenuTree($("#menuTree"),url);
					});
				}
			});
		
			
		});
		
		//删除
		$("#deleteOrganizationBtn").click(function(){
			var params = $("#data_organization").wgrid("getTrData",{
				tbodyId: "data_organization_table"
			});
			if(params.length != 1){
				window.wxc.xcConfirm("请选择一条数据","warning");
				return;
			}
			window.wxc.xcConfirm("是否确认删除？","confirm",{onOk:function(){
				$.ajax({
					url:"${pageContext.request.contextPath}/organization/organizationAction.do?method=deleteOrganization",
					type:"post",
					async:false,
					data:{"table_id":params[0].table_id},
					success:function(data){
						var json = jQuery.parseJSON(data);
						if(json.status == 1){
							window.wxc.xcConfirm("删除成功","success");
							loadGrid();
						}else{
							window.wxc.xcConfirm("删除失败","error");
						}
					}
				});
			}});
			
		
		});
		
		//查询
		$("#searchOrganizationBtn").click(function(){
			var organizationName = $("#searchOrganization").val();
			var data = {"organizationName":organizationName};
			loadGrid(this,data);
		});
		
		function loadMenuTree(tag,url){
			var setting = {
					data:{
						simpleData:{
							enable:true,
							idKey:"cid",
							pIdKey:"pid",
							rootPid:"0"
						}
					},
					check:{
						enable: true,
						chkStyle: "checkbox",
						chkboxType: { "Y": "", "N": "s" }
					},
					callback:{
						onClick:null,
						onCheck:null
					},
					view: {
						showIcon: true
					}
				};
			$.ajax({
				url:url,
				type:"post",
				async:false,
				success:function(data){
					var json = jQuery.parseJSON(data);
					var zTree_Menu = $.fn.zTree.init(tag, setting, json);
					zTree_Menu.expandAll(true);//展开整棵树
				}
			});
			
		}
		
		//全局加载
		$(function(){
			$("body").append($("#organizationModalPanel"));
			loadGrid();
		});
	
		window.oranization = {};
		window.oranization.loadGrid = loadGrid;
		window.oranization.loadMenuTree = loadMenuTree;
	})();
</script>