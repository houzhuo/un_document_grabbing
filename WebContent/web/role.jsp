<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<div style="margin-top:5px;width:100%;height:100%">
	<div class="col-adjust-8 mb10 bg-light pb10 lighter ">
		<div class="row mt4 ml5">
			<div class="form-inline mt5 text-left">
				<button id="addRoleBtn" type="button" class="btn btn-info ml5 mt5" data-toggle="modal" data-target="#addRoleModal">添加角色</button>
				<button id="updateRoleBtn" type="button" class="btn btn-info ml5 mt5">修改角色</button>
				<button id="deleteRoleBtn" type="button" class="btn btn-danger ml5 mt5">删除角色</button>
				<div id="searchRoleBtnDiv" class="input-group ml10 mt5">
					<input type="text" id="searchRole" class="form-control" placeholder="请输入角色名称" style="height: 37px"> <span class="input-group-addon"><i id="searchRoleBtn" style="cursor: pointer;" class="glyphicon glyphicon-search"></i></span>
				</div>
			</div>
		</div>
	</div>
	<div class="sctrcontent mCustomScrollbar">
		<table id="data_role" class="table table-bordered bg-light" >
			<thead>
				<tr id="data_role_title">
					<th fname="number" width="50" style="text-align: center">编号</th>
					<th fname="dataRoleCheckBox" width="40" style="text-align: center"><input id="roleCheckBox" type="checkbox"></th>
					<th fname="roleName" width="120" style="text-align: center">角色名称</th>
					<th fname="description" width="120" style="text-align: center">描述</th>
				</tr>
			</thead>
			<tbody id="data_role_table" style="text-align: center">
				
			</tbody>
		</table>
	</div>
	<nav aria-label="Page navigation example">
		<ul class="pagination" id="role_pagination">
		  
		</ul>
	</nav>
	
</div>
	<div id="roleModalPanel" style="max-width:100%;">
	
	</div>
<script>
(function(){
	
	$(function(){
		$("body").append($("#roleModalPanel"));
		loadGrid();
	});
	function loadGrid(event,data){
		if(typeof(data) == 'undefined'){
			data = {};
		}
		data.start = 1;
		$('#data_role').wgrid({
			table_title: "data_role_title",
			tbodyId: "data_role_table",
			pagination:"role_pagination",
			url: "${pageContext.request.contextPath}/role/roleAction.do?method=getAllRole",
			checkbox: true,
			checkboxId: "roleCheckBox",
			data: data,
			tbodyTdCss:{
				"number":"text-center",
				"dataRoleCheckBox":"text-center"
			},
			success:function(data){
				//console.log(data);
			}
		});
	}
	
	//添加
	$("#addRoleBtn").click(function(){
		 $("#roleModalPanel").html("").load("${pageContext.request.contextPath}/web/role_add.jsp",{},function(data){
			$("#addRoleModal").modal({
				show:true,
				keyboard:false,
				backdrop:'static'
			});
			var url1 = "${pageContext.request.contextPath}/permission/permissionAction.do?method=getMenuJsonData";
			loadMenuTree($("#permissionTree"),url1);
		})
	});
	
	//修改
	$("#updateRoleBtn").click(function(){
		var params = $("#data_role").wgrid("getTrData",{
			tbodyId: "data_role_table"
		});
		if(params.length != 1){
			window.wxc.xcConfirm("请选择一条数据","warning");
			return;
		}
		$("#roleModalPanel").html("").load("${pageContext.request.contextPath}/web/role_add.jsp",{},function(data){
			$("#addRoleModal").modal({
				show:true,
				keyboard:false,
				backdrop:'static'
			});
			//显示基本数据
			setFormData("addRoleModal", params[0]);
			var url1 = "${pageContext.request.contextPath}/permission/permissionAction.do?method=getMenuJsonData&type=role&table_id="+params[0].table_id;
			loadMenuTree($("#permissionTree"),url1);
		})
	});
	
	//删除
	$("#deleteRoleBtn").click(function(){
		var params = $("#data_role").wgrid("getTrData",{
			tbodyId: "data_role_table"
		});
		if(params.length != 1){
			window.wxc.xcConfirm("请选择一条数据","warning");
			return;
		}
		window.wxc.xcConfirm("是否确认删除？","confirm",{onOk:function(){
			$.ajax({
				url:"${pageContext.request.contextPath}/role/roleAction.do?method=deleteRole",
				type:"post",
				async:false,
				data:params[0],
				success:function(data){
					var json = jQuery.parseJSON(data);
					if(json.status == 1){
						window.wxc.xcConfirm("删除成功","success");
						loadGrid();
					}else{
						window.wxc.xcConfirm("删除失败","error");
					}
				}
			});
		}
		});
	
	});
	
	//查询
	$("#searchRoleBtn").click(function(){
		var roleName = $("#searchRole").val();
		var data = {"roleName":roleName};
		loadGrid(this,data);
	});
	
	
	function loadMenuTree(tag,url){
		var setting = {
				data:{
					simpleData:{
						enable:true,
						idKey:"cid",
						pIdKey:"pid",
						rootPid:"0"
					}
				},
				check:{
					enable: true,
					chkStyle: "checkbox",
					chkboxType: { "Y": "p", "N": "s" }
				},
				callback:{
					onClick:null,
					onCheck:null
				},
				view: {
					showIcon: true
				}
			};
		$.ajax({
			url:url,
			type:"post",
			async:false,
			success:function(data){
				var json = jQuery.parseJSON(data);
				var zTree_Menu = $.fn.zTree.init(tag, setting, json);
				zTree_Menu.expandAll(true);//展开整棵树
			}
		});
	}
	
	window.role ={};
	window.role.loadGrid = loadGrid;
})();
</script>