<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title>首页</title>
<meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
<meta name="description" content="Monster Dashboard - Multiskin Premium Admin Dashboard">
<meta name="author" content="MonsterDashboard">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/private/main/css/fullcalendar.min.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/private/main/css/admin-forms.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/private/main/css/theme.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/private/main/css/magnific-popup.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/fonts/iconsweets/iconsweets.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/public/css/jquery.datetimepicker.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/public/css/zTreeStyle.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/public/css/xcConfirm.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/public/css/menu.styles.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/public/js/mCustomScrollbar/jquery.mCustomScrollbar.min.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/public/js/jquery/jquery_ui/jquery-ui.min.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/public/css/menu.styles.css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/bootstrap/bootstrap-tabdrop.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/bootstrap/bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/jquery/jquery_ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/jquery/jquery.datetimepicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/grid.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/myValidate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/xcConfirm.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/upload.bigfile.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/ueditor/lang/zh-cn/zh-cn.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/jquery-sortable.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/loadAuditor.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/private/message/js/message_grid.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/echarts.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/release/go.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/initFlowChart.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/jsBase64.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/sockjs-1.0.0.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/menu.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/public/js/multipleDownList.js"></script>
<style>
</style>
</head>
<body class="dashboard-page">
	<div id="main">
		<header class="navbar navbar-fixed-top" style="background-color:#2a3b4c">
		<div class="navbar-branding" style="background-color: #2E4254;">
			<a class="navbar-brand" href="#" style="font-weight: bold; font-family: 'Oswald', sans-serif"> MonsterDashboard </a> <span id="toggle_sidemenu_l" class="fa fa-bars"></span>
		</div>
		<ul class="nav navbar-nav navbar-right">
			<li class="dropdown menu-merge">
				<div class="navbar-btn btn-group">
					<button data-toggle="dropdown" class="btn btn-sm dropdown-toggle">
						<span class="fa fa-bell fs16 va-m"></span> <span class="badge">5</span>
					</button>
				</div>
			</li>
			<li class="menu-divider hidden-xs"><i class="fa fa-circle"></i></li>
			<li class="dropdown menu-merge"><a href="#" class="dropdown-toggle fw600 p10 pr20" data-toggle="dropdown"> <img src="${pageContext.request.contextPath}/static/image/1.jpg" alt="avatar" class="mw40 br64"> <span class="hidden-xs pl15"><c:out value="${loginUser.userName }"></c:out></span> <span class="caret caret-tp hidden-xs"></span>
			</a>
				<ul class="dropdown-menu list-group dropdown-persist w280" role="menu">
					<li class="list-group-item"><a href="#" class="animated animated-short fadeInUp"> <span class="fa fa-envelope fs18"></span> Messages <span class="label label-warning">2</span>
					</a></li>
					<li class="list-group-item"><a href="#" class="animated animated-short fadeInUp"> <span class="fa fa-user fs18"></span> Friends <span class="label label-warning">6</span>
					</a></li>
					<li class="list-group-item"><a href="#" class="animated animated-short fadeInUp"> <span class="fa fa-bell fs18"></span> Notifications
					</a></li>
					<li class="list-group-item"><a id="update_user_password" href="#" class="animated animated-short fadeInUp"> <span class="fa fa-edit fs22"></span> Change Psw
					</a></li>
					<li class="list-group-item"><a href="#" class="animated animated-short fadeInUp"> <span class="fa fa-gear fs22"></span> Settings
					</a></li>
					<li class="dropdown-footer"><a id="logout_sys" href="#" class=""> <span class="fs18"></span> Logout
					</a></li>
				</ul></li>
		</ul>
		</header>
		<aside id="sidebar_left" class="nano nano-light affix">
			<div id="mainLeftMenuDivId" class="sidebar-left-content nano-content" style="padding: 0; margin: 0; background-color: #33485c"></div>
		</aside>
		<section id="content_wrapper"> </section>
	</div>
	<div id="update_password_modal"></div>
<script type="text/javascript">
var basePath = "${pageContext.request.contextPath}";
(function(){
	$(function(){
		$.ajaxSetup({
			type : 'POST',
			complete : function(xhr, status) {
				var sessionStatus = xhr.getResponseHeader('ajaxSessionTimeOutStatus');
				if (sessionStatus == '0') {
					var top = getTopWinow();
					var yes = confirm('由于您长时间没有操作, session已过期, 请重新登录.');
					if (yes) {
						top.location.href = '${pageContext.request.contextPath}/login.jsp';
					}
				}
			}
		});
		loadMainMenu();
	});
	function getTopWinow() {
		var p = window;
		while (p != p.parent) {
			p = p.parent;
		}
		return p;
	}
	function loadMainMenu(){
		$("#mainLeftMenuDivId").initMenu({
			url: basePath + "/user/userAction.do?method=getUserMenuJsonArr",
			contentWrapper:"content_wrapper",
			basePath: basePath,
			dataType:{
				rootId: "0"
			},
			backFunction: function(obj,itemData){
				
			} 
		});
	}
	$('.dropdown-toggle').dropdown(); 
	
	//Update Password
	$("#update_user_password").click(function(){
		if('${loginUser.userName }' == 'admin'){
			alert("admin不能修改密码！");
			return false;
		}
		$("#update_password_modal").html("").load("${pageContext.request.contextPath}/web/update_password.jsp",{},function(data){
			$("#updatePasswordModal").modal({
				show:true,
				keyboard:false,
				backdrop:'static'
			});
			$("#table_id").val('${loginUser.table_id }');
			$("#userName").val('${loginUser.userName }');
		})
	});
	
	//logout
	$("#logout_sys").click(function(){
		var url = "${pageContext.request.contextPath}/login/loginAction.do?method=logoutSys";
		$.ajax({
			url:url,
			type:"post",
			dataType:"json",
			success:function(data){
				if(data.status == 1){
					location.href = "${pageContext.request.contextPath}/login.jsp";
				}else{
					alert("失败")
				}
			}
		});
	});
	
	
})();
window.alert=function(value){
	window.wxc.xcConfirm(value, "info");
}   
</script>
</body>
</html>
