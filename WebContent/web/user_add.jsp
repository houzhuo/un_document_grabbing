<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<div class="modal fade" id="addUserModal" tabindex="1000" user="dialog" aria-labelledby="addUserModalLabel" aria-hidden="true" style="overflow: hidden;">
		<div class="modal-dialog" style="width:600px;" > 
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title mt5" >编辑用户<a style="float:right" id="userPanelDivCloseId"><span style="font-size:16px;" class="glyphicon glyphicon-remove"></span></a></h4>
				
			</div>
			<div class="modal-body" style="max-height:500px;overflow: auto;">
				<div id="addUserInfoId" class="form-horizontal">
					<input type="hidden" id="table_id">
					<div class="form-group">
						<label for="userName" class="col-lg-3 control-label">角色名称:</label>
						<div class="col-lg-8">
							<div class="bs-component">
								<input type="text" id="userName" name="userName" require='true' meth="notNull,isMinLength-30" class="form-control" placeholder="请输入用户名称(不能为空，长度小于30)">
							</div>
						</div>
					</div>
					<div class="form-group" id="showPassWord">
						<label for="passWord" class="col-lg-3 control-label">密码:</label>
						<div class="col-lg-8">
							<div class="bs-component">
								<input type="password" id="passWord" name="passWord" require='true' meth="notNull,isMinLength-50" class="form-control" placeholder="请输入密码(不能为空，长度小于50)">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-lg-3 control-label">用户权限:</label>
						<div class="col-lg-9">
							<ul id="permissionTree" class="ztree"></ul>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-lg-3 control-label">用户机构:</label>
						<div class="col-lg-9">
							<ul id="organizationTree" class="ztree"></ul>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-lg-3 control-label">用户角色:</label>
						<div class="col-lg-9">
							<ul id="roleTree" class="ztree"></ul>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button id="saveUserBtn" type="button" class="btn btn-info ml5 ">保存</button>
				<button id="cancel" type="button" class="btn btn-info ml5 ">取消</button>
			</div>
		</div>
	</div>
		
	
	</div>
<script>
(function(){
	$(function(){
		
	});
	
	
	//保存
	function addOrUpdateUser(){
		var s = $("#addUserInfoId").hzValidate("init");
		if(s == true){
			var treeObj=$.fn.zTree.getZTreeObj("permissionTree");
			var permNodes = treeObj.getCheckedNodes(true);
			var treeObj=$.fn.zTree.getZTreeObj("organizationTree");
			var orgaNodes = treeObj.getCheckedNodes(true);
			
			var treeObj=$.fn.zTree.getZTreeObj("roleTree");
			var roleNodes = treeObj.getCheckedNodes(true);
			var ids1 = new Array();
			var ids2 = new Array();
			var ids3 = new Array();
			for(var i = 0;i < permNodes.length;i++){
				ids1.push({"table_id":permNodes[i].table_id});
			}
			for(var i = 0;i < orgaNodes.length;i++){
				ids2.push({"table_id":orgaNodes[i].table_id});
			}
			for(var i = 0;i < roleNodes.length;i++){
				ids3.push({"table_id":roleNodes[i].table_id});
			}
			 var params = OAajaxSubmit("addUserInfoId");
			 params["temp1"] = JSON.stringify(ids1);//权限
			 params["temp2"] = JSON.stringify(ids2);//机构
			 params["temp3"] = JSON.stringify(ids3);//角色
			 var url = "${pageContext.request.contextPath}/user/userAction.do?method=addOrUpdateUser";
			$.ajax({
				url:url,
				type:"post",
				data:params,
				async:false,
				success:function(data){
					var json = JSON.parse(data);
					if(json.status == 1){
						window.wxc.xcConfirm("操作成功","success");
						$("#userPanelDivCloseId").trigger("click");//关闭modal
						window.user.loadGrid();//刷新页面
					}else{
						window.wxc.xcConfirm("操作失败","error");
					}
				}
			});
		}else{
			alert("error")
		}
	}
	
	//保存
	$("#saveUserBtn").on("click",function(){
		addOrUpdateUser();
	});
	//关闭
	$("#userPanelDivCloseId").click(function(){
		$("#addUserModal").remove();
	});
	//取消
	$("#cancel").on("click",function(){
		$("#addUserModal").remove();
	});
	
})();	
</script>
