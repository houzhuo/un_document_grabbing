<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<div class="modal fade" id="addRoleModal" tabindex="1000" role="dialog" aria-labelledby="addRoleModalLabel" aria-hidden="true">
		<div class="modal-dialog" style="width:600px;"> 
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title mt5" >编辑角色<a style="float:right" id="rolePanelDivCloseId"><span style="font-size:16px;" class="glyphicon glyphicon-remove"></span></a></h4>
				
			</div>
			<div class="modal-body" style="overflow: auto;">
				<form id="addRoleInfoId" class="form-horizontal" role="form">
				<input type="hidden" id="table_id" >
				<div class="form-group">
					<label for="roleName" class="col-lg-3 control-label">角色名称:</label>
					<div class="col-lg-9">
						<div class="bs-component">
							<input type="text" id="roleName" name="roleName"  require='true' meth="notNull,isMinLength-30" class="form-control" placeholder="请输入角色名称(不能为空，长度小于30)">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="description" class="col-lg-3 control-label">描述:</label>
					<div class="col-lg-9">
						<div class="bs-component">
							<input type="text" id="description" name="description"  require='true' meth="notNull,isMinLength-30" class="form-control" placeholder="请输入描述(不能为空，长度小于30)">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-lg-3 control-label">角色权限:</label>
					<div class="col-lg-9">
						<ul id="permissionTree" class="ztree" style="max-height:150px;overflow: auto;"></ul>
					</div>
				</div>
			</form>
			</div>
			<div class="modal-footer">
				<button id="save" type="button" class="btn btn-info ml5 ">保存</button>
				<button id="cancel" type="button" class="btn btn-info ml5 ">取消</button>
			</div>
		</div>
	</div>
		
	
	</div>
<script>
(function(){
	$(function(){
		
	});
	
	
	//保存
	function addOrUpdateRole(){
		var s = $("#addRoleInfoId").hzValidate("init");
		if(s == true){
			var treeObj=$.fn.zTree.getZTreeObj("permissionTree");
			var permNodes = treeObj.getCheckedNodes(true);
			
			var ids = new Array();
			for(var i = 0;i < permNodes.length;i++){
				ids.push({"table_id":permNodes[i].table_id});
			}
			 var params = OAajaxSubmit("addRoleInfoId");
			 params["temp1"] = JSON.stringify(ids);//权限
			 var url = "${pageContext.request.contextPath}/role/roleAction.do?method=addOrUpdateRole";
			$.ajax({
				url:url,
				type:"post",
				data:params,
				async:false,
				success:function(data){
					var json = JSON.parse(data);
					if(json.status == 1){
						window.wxc.xcConfirm("操作成功","success");
						$("#rolePanelDivCloseId").trigger("click");//关闭modal
						window.role.loadGrid();//刷新页面
					}
				}
			});
		}
	}
	
	//保存
	$("#save").on("click",function(){
		addOrUpdateRole();
	});
	//关闭
	$("#rolePanelDivCloseId").click(function(){
		$("#addRoleModal").remove();
	});
	//取消
	$("#cancel").on("click",function(){
		$("#addRoleModal").remove();
	});
	
})();	
</script>
