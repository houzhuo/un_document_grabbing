<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<div class="modal fade" id="addOrganizationModal" tabindex="1000" role="dialog" aria-labelledby="addOrganizationModalLabel" aria-hidden="true" style="overflow: hidden;">
		<div class="modal-dialog" style="width:600px;"> 
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title mt5" >编辑机构<a style="float:right" id="organizationPanelDivCloseId"><span style="font-size:16px;" class="glyphicon glyphicon-remove"></span></a></h4>
				
			</div>
			<div class="modal-body" style="overflow: auto;">
				<form id="addOrganizationInfoId" class="form-horizontal" role="form">
				<input type="hidden" id="table_id" >
				<div class="form-group">
					<label for="organizationName" class="col-lg-3 control-label">机构名称:</label>
					<div class="col-lg-9">
						<div class="bs-component">
							<input type="text" id="organizationName" name="organizationName"  require='true' meth="notNull,isMinLength-30" class="form-control" placeholder="请输入机构名称(不能为空，长度小于30)">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="organizationType" class="col-lg-3 control-label">机构类型:</label>
					<div class="col-lg-9">
						<div class="bs-component">
							<input type="text" id="organizationType" name="organizationType"  require='true' meth="notNull,isMinLength-30" class="form-control" placeholder="请输入机构类型(不能为空，长度小于30)">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="temp2" class="col-lg-3 control-label">机构负责人:</label>
					<div class="col-lg-9">
						<div class="bs-component">
							<input type="hidden" id="temp2" />
							<input type="text" id="principal" list="orgaPrincipal"  require='true' meth="notNull" class="form-control" placeholder="请输入机构责任人">
							<datalist id="orgaPrincipal"></datalist>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="" class="col-lg-3 control-label">机构权限:</label>
					<div class="col-lg-9">
						<ul id="menuTree" class="ztree" style="max-height:150px;overflow: auto;"></ul>
					</div>
				</div>
				<c:if test="${param.type eq 'add'}">
				<div class="form-group">
					<label for="" class="col-lg-3 control-label">选择父节点:</label>
					<div class="col-lg-9">
						<ul id="organizationTree" class="ztree" style="max-height:150px;overflow: auto;"></ul>
					</div>
				</div>
				</c:if>
			</form>
			</div>
			<div class="modal-footer">
				<button id="save" type="button" class="btn btn-info ml5 ">保存</button>
				<button id="cancel" type="button" class="btn btn-info ml5 ">取消</button>
			</div>
		</div>
	</div>
		
	
	</div>
<script>
(function(){
	$(function(){
		$.ajax({
			url:"${pageContext.request.contextPath}/user/userAction.do?method=getAllUsers",
			type:"post",
			async:false,
			success:function(data){
				var json = JSON.parse(data);
				$("#orgaPrincipal").html("");
				for(var i = 0;i<json.length;i++){
					var _option = $("<option></option>");
					_option.attr("table_id",json[i].table_id);
					_option.text(json[i].userName);
					$("#orgaPrincipal").append(_option);
				}
				$("#principal").on("input",function(){
					var options = $("#orgaPrincipal").children();
					for(var i = 0;i<options.length;i++){
						if(options.eq(i).val().trim()==$("#principal").val().trim()){
							$("#temp2").val(options.eq(i).attr("table_id"));
						}
					}
				})
				
			}
		})
	});
	
	
	function addOrUpdateOrganization(){
		var s = $("#addOrganizationInfoId").hzValidate("init");
		
		if(s == true){
			var treeObj=$.fn.zTree.getZTreeObj("menuTree");
			var nodes = treeObj.getCheckedNodes(true);
			
			if(nodes.length == 0){
				window.wxc.xcConfirm("必须选择权限项!","warning")
				return;
			}
			var ids = new Array();
			for(var i = 0;i < nodes.length;i++){
				ids.push({"table_id":nodes[i].table_id});
			}
			 var params = OAajaxSubmit("addOrganizationInfoId");
			 params["temp1"] = JSON.stringify(ids);
			
			
			var treeObj2=$.fn.zTree.getZTreeObj("organizationTree");
			if(treeObj2 != null && typeof(treeObj2) !="undefined" ){
				var pNode = treeObj2.getCheckedNodes(true);
				if(pNode.length>1){
					window.wxc.xcConfirm("最多选择一个父节点","warning");
					return ;
				}
				//父节点
				 if(pNode.length == 1){
					 params["pid"] = pNode[0].cid;
				 }else{
					 params["pid"] = 0;
				 }
			}
			 
			 var url = "${pageContext.request.contextPath}/organization/organizationAction.do?method=addOrUpdateOrganization";
			$.ajax({
				url:url,
				type:"post",
				data:params,
				async:false,
				success:function(data){
					var json = JSON.parse(data);
					if(json.status == 1){
						window.wxc.xcConfirm("操作成功","success");
						$("#organizationPanelDivCloseId").trigger("click");//关闭modal
						window.oranization.loadGrid();
					}
				}
			});
		}
	}
	
	//保存
	$("#save").on("click",function(){
		addOrUpdateOrganization();
	});
	//关闭
	$("#organizationPanelDivCloseId").click(function(){
		$("#addOrganizationModal").remove();
	});
	//取消
	$("#cancel").on("click",function(){
		$("#addOrganizationModal").remove();
	});
	
})();	
</script>
