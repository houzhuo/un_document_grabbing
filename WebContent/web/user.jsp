<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%> 
<div style="margin-top:5px;width:100%;height:100%;background-color: white;">
	<div class="col-adjust-8 mb10 bg-light pb10 lighter ">
		<div class="row mt4 ml5">
			<div class="form-inline mt5 text-left">
				<button id="addUserBtn" type="button" class="btn btn-info ml5 mt5" data-toggle="modal" data-target="#addUserModal">添加用户</button>
				<button id="updateUserBtn" type="button" class="btn btn-info ml5 mt5">修改用户</button>
				<button id="deleteUserBtn" type="button" class="btn btn-danger ml5 mt5">删除用户</button>
				<div id="searchUserBtnDiv" class="input-group ml10 mt5">
					<input type="text" id="searchUser" class="form-control" placeholder="请输入用户名称" style="height: 37px"> <span class="input-group-addon"><i id="searchUserBtn" style="cursor: pointer;" class="glyphicon glyphicon-search"></i></span>
				</div>
			</div>
		</div>
	</div>
	<div class="sctrcontent mCustomScrollbar">
		<table id="data_user" class="table table-bordered bg-light" >
			<thead>
				<tr id="data_user_title">
					<th fname="number" width="50" style="text-align: center">编号</th>
					<th fname="dataUserCheckBox" width="40" style="text-align: center"><input id="userCheckBox" type="checkbox"></th>
					<th fname="userName" width="120" style="text-align: center">用户名称</th>
				</tr>
			</thead>
			<tbody id="data_user_table" style="text-align: center">
				
			</tbody>
		</table>
	</div>
	<nav aria-label="Page navigation example">
		<ul class="pagination" id="user_pagination">
		  
		</ul>
	</nav>
	
</div>
	<div id="userModalPanel" style="max-width:100%">
	
	</div>
<script>
(function(){
	$(function(){
		$("body").append($("#userModalPanel"));
		loadGrid();
	});
	
	function loadGrid(event,data){
		if(typeof(data) == 'undefined'){
			data = {};
		}
		data.start = 1;
		$('#data_user').wgrid({
			table_title: "data_user_title",
			tbodyId: "data_user_table",
			pagination:"user_pagination",
			url: "${pageContext.request.contextPath}/user/userAction.do?method=getAllUser",
			checkbox: true,
			checkboxId: "userCheckBox",
			data: data,
			tbodyTdCss:{
				"number":"text-center",
				"dataUserCheckBox":"text-center"
			},
			success:function(data){
				//console.log(data);
			}
		});
	}
	
	//添加
	$("#addUserBtn").click(function(){
		 $("#userModalPanel").html("").load("${pageContext.request.contextPath}/web/user_add.jsp",{},function(data){
			$("#addUserModal").modal({
				show:true,
				keyboard:false,
				backdrop:'static'
			});
			var url1 = "${pageContext.request.contextPath}/permission/permissionAction.do?method=getMenuJsonData";
			var url2 = "${pageContext.request.contextPath}/organization/organizationAction.do?method=getOrganizationJsonData";
			var url3 = "${pageContext.request.contextPath}/role/roleAction.do?method=getRoleJsonData";
			loadMenuTree($("#permissionTree"),url1);
			loadMenuTree($("#organizationTree"),url2);
			loadMenuTree($("#roleTree"),url3);
		})
	});
	
	//修改
	$("#updateUserBtn").click(function(){
		var params = $("#data_user").wgrid("getTrData",{
			tbodyId: "data_user_table"
		});
		if(params.length != 1){
			window.wxc.xcConfirm("请选择一条数据","warning");
			return;
		}
		$("#userModalPanel").html("").load("${pageContext.request.contextPath}/web/user_add.jsp",{},function(data){
			$("#addUserModal").modal({
				show:true,
				keyboard:false,
				backdrop:'static'
			});
			//显示基本数据
			setFormData("addUserModal", params[0]);
			var url1 = "${pageContext.request.contextPath}/permission/permissionAction.do?method=getMenuJsonData&type=user&table_id="+params[0].table_id;
			var url2 = "${pageContext.request.contextPath}/organization/organizationAction.do?method=getOrganizationJsonData&table_id="+params[0].table_id;
			var url3 = "${pageContext.request.contextPath}/role/roleAction.do?method=getRoleJsonData&table_id="+params[0].table_id;
			loadMenuTree($("#permissionTree"),url1);
			loadMenuTree($("#organizationTree"),url2);
			loadMenuTree($("#roleTree"),url3);
		})
	});
	
	//删除
	$("#deleteUserBtn").click(function(){
		var params = $("#data_user").wgrid("getTrData",{
			tbodyId: "data_user_table"
		});
		if(params.length != 1){
			window.wxc.xcConfirm("请选择一条数据","warning");
			return;
		}
		window.wxc.xcConfirm("是否确认删除？","confirm",{onOk:function(){
			$.ajax({
				url:"${pageContext.request.contextPath}/user/userAction.do?method=deleteUser",
				type:"post",
				async:false,
				data:params[0],
				success:function(data){
					var json = jQuery.parseJSON(data);
					if(json.status == 1){
						alert("删除成功","success");
						loadGrid();
					}else{
						alert("删除失败","error");
					}
				}
			});
		}
		
		});
	});
	
	//查询
	$("#searchUserBtn").click(function(){
		var userName = $("#searchUser").val();
		var data = {"userName":userName};
		loadGrid(this,data);
	});
	
	function loadMenuTree(tag,url){
		var setting = {
				data:{
					simpleData:{
						enable:true,
						idKey:"cid",
						pIdKey:"pid",
						rootPid:"0"
					}
				},
				check:{
					enable: true,
					chkStyle: "checkbox",
					chkboxType: { "Y": "p", "N": "s" }
				},
				callback:{
					onClick:null,
					onCheck:null
				},
				view: {
					showIcon: true
				}
			};
		$.ajax({
			url:url,
			type:"post",
			async:false,
			success:function(data){
				var json = jQuery.parseJSON(data);
				var zTree_Menu = $.fn.zTree.init(tag, setting, json);
				zTree_Menu.expandAll(true);//展开整棵树
			}
		});
		
	}
	
	window.user = {};
	window.user.loadGrid = loadGrid;
})();
</script>