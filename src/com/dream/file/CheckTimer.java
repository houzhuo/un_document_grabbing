package com.dream.file;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.persistence.criteria.CriteriaBuilder.In;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.dream.model.Reptile_Record;
import com.dream.system.service.FileService;

@Component
@SuppressWarnings("all")
public class CheckTimer {

	@Autowired
	private FileService fileService;

	private int maxNum = 20;
	
	@Scheduled(cron = "0 0/5 * * * ?")
	public void excute() {
		
		System.out.println("============开始校验数据库===============");
		List<Reptile_Record> pullLit = fileService.getPullDate();
		PullMain.searchDateQueue.clear();
		List<Reptile_Record> temp =  new ArrayList<>();
		for(Reptile_Record rr:pullLit){
			if(StringUtils.isEmpty(rr.getAllNum())){
				PullMain.searchDateQueue.add(rr.getStartDate());
				temp.add(rr);
			}
		}
		pullLit.removeAll(temp);
		Random random = new Random();
		while(pullLit != null && pullLit.size() > 0){
			int index = random.nextInt(pullLit.size());
			Reptile_Record rr = pullLit.get(index);
			if(StringUtils.isNotEmpty(rr.getStartDate())){
				PullMain.searchDateQueue.add(rr.getStartDate());
			}
			pullLit.remove(index);
		}
		if(JxBrower.num == maxNum){
			System.out.println("============重新加载浏览器核心===============");
			JxBrower.num = 0;
			JxBrower.getJxBrower().dispose();
			JxBrower.setBrower();
			InitBrower.init();
			InitBrower.setLoadListener();
		}
		if(PullMain.searchDateQueue.size() > 0){
			InitBrower.initBrowerUrl();
		}
		JxBrower.num += 1;
	}
}
