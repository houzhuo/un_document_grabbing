package com.dream.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

import javax.mail.Flags.Flag;

import org.apache.http.client.CookieStore;

import com.dream.model.File_Entity;

@SuppressWarnings("all")
public class HandOutFileDownLoad extends Thread {

	private int maxNum = 2000;

	private int num = 0;

	
	public void run() {
		CookieStore cookieStore = getCookie();
		System.out.println(cookieStore);
		InitBrower.initBrowerUrl();
		while (true) {
			try {
				File_Entity file = PullMain.downloadQueue.take();
				if(PullMain.isContains(file.getFileHttpUrl())){
					continue;
				}
				
				PullMain.pool.submit(new FileDownloadThread(file, cookieStore));
				num++;
				if(num >= maxNum){
					num = 0;
					cookieStore = getCookie();
				}
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				cookieStore = getCookie();
			} 
		}
	}

	public CookieStore getCookie() {
		CookieStore cookieStore = null;
		while (cookieStore == null) {
			
			try {
				System.out.println("==========正在链接=======================");
				cookieStore = PullRemoteHtml.getDownloadCookie();
			} catch (Exception e) {
				// TODO: handle exception
				cookieStore = null;
				try {
					Thread.sleep(1000 * 5);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
				}
			}
			
		}

		return cookieStore;
	}
}
