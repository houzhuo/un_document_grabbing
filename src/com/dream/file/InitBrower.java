package com.dream.file;

import java.awt.BorderLayout;
import java.nio.file.Files;

import javax.swing.JFrame;

import com.dream.system.service.FileService;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;

@SuppressWarnings("all")
public class InitBrower {

	public static void init(){
		Browser browser = JxBrower.getJxBrower();
		browser.getCacheStorage().clearCache();
		browser.setUserAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36");
		//初始化加载
		String url = "https://documents.un.org/prod/ods.nsf/home.xsp";
		browser.loadURL(url);
		/*BrowserView view = new BrowserView(browser);
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frame.setLocationByPlatform(true); frame.setVisible(true);
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frame.setSize(400, 600); frame.add(view, BorderLayout.CENTER);*/
		
	}
	
	public static void initBrowerUrl(){
		Browser browser = JxBrower.getJxBrower();
		String url = "https://documents.un.org/prod/ods.nsf/home.xsp";
		browser.loadURL(url);
	}
	public static void setLoadListener(){
		Browser browser = JxBrower.getJxBrower();
		browser.addLoadListener(new ParseInitPageBrowerListener(0));
	}
}
