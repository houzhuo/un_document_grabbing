package com.dream.file;

import com.dream.model.File_Entity;
import com.dream.model.Reptile_Record;
import com.dream.system.service.FileService;

@SuppressWarnings("all")
public class InsertFileInfoThread extends Thread{

	private FileService fileService;

	private int num = 0;
	
	private int maxNum = 200;
	
	public InsertFileInfoThread(FileService fileService) {
		// TODO Auto-generated constructor stub
		this.fileService = fileService;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true){
			try{
				Object object = PullMain.insertQueue.take();
				if(object.getClass().getName().equals(File_Entity.class.getName())){
					File_Entity file = (File_Entity)object;
					fileService.insertFile(file);
				}else{
					Reptile_Record rr = (Reptile_Record)object;
					fileService.insertObject(rr);
				}
				/*num++;
				if(num > maxNum){
					num = 0;
					Thread.sleep(1000 * 30);
				}*/
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
