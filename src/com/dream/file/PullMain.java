package com.dream.file;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import com.dream.model.File_Entity;

@SuppressWarnings("all")
public class PullMain {

	public static ExecutorService pool = Executors.newFixedThreadPool(20);
	
	public static LinkedBlockingQueue<File_Entity> downloadQueue = new LinkedBlockingQueue<>();
	
	public static LinkedBlockingQueue<Object> insertQueue = new LinkedBlockingQueue<>();
	
	public static LinkedBlockingQueue<String> searchDateQueue = new LinkedBlockingQueue<>();
	
	//public static Set<String> fileHttpUrlHashSet = Collections.synchronizedSet(new HashSet<>());
	
	private static Set<String> fileHttpUrlList = new HashSet<>();
	
	
	public static boolean isContains(String s){
		synchronized (fileHttpUrlList) {
			boolean mark = fileHttpUrlList.contains(s);
			if(!mark){
				fileHttpUrlList.add(s);
			}
			return mark;
		}
	}
	
	public static void addAll(List<String> list){
		synchronized (fileHttpUrlList) {
			fileHttpUrlList.addAll(list);
		}
	}
	
	public static void add(String s){
		synchronized (fileHttpUrlList) {
			fileHttpUrlList.add(s);
		}
	}
	
}
