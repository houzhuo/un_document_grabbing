package com.dream.file;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.dream.common.upload.BigFileUploadUtil;
import com.dream.system.service.FileService;

/*@Component*/
@SuppressWarnings("all")
public class ClearFiles {

	@Autowired
	private FileService fileService;
	
	@Scheduled(cron = "0 0/2 * * * ?")
	public void excute(){
		try{
			List<Object[]> list = fileService.getFileIds();
			for(Object[] ob:list){
				String id = String.valueOf(ob[0]);
				String path = BigFileUploadUtil.getBasePath() + String.valueOf(ob[1]);
				File f = new File(path);
				if(f.exists()){
					f.delete();
				}
				fileService.deleteFiles(id);
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
}
