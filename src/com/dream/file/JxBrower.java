package com.dream.file;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigInteger;

import com.teamdev.jxbrowser.chromium.az;
import com.hxtt.b.br;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserContext;

@SuppressWarnings("all")
public class JxBrower {
	
	private static Browser browser;
	
	public static int num = 0;
	
	static {
		try {
			Field e = az.class.getDeclaredField("e");
			e.setAccessible(true);
			Field f = az.class.getDeclaredField("f");
			f.setAccessible(true);
			Field modifersField = Field.class.getDeclaredField("modifiers");
			modifersField.setAccessible(true);
			modifersField.setInt(e, e.getModifiers() & ~Modifier.FINAL);
			modifersField.setInt(f, f.getModifiers() & ~Modifier.FINAL);
			e.set(null, new BigInteger("1"));
			f.set(null, new BigInteger("1"));
			modifersField.setAccessible(false);
			browser = new Browser();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	
	public static Browser getJxBrower(BrowserContext browserContext){
		return new Browser(browserContext);
	}
	
	public static Browser getJxBrower(){
		return browser;
	}
	
	public static void setBrower(){
		browser = new Browser();
	}
}
