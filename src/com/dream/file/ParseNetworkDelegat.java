package com.dream.file;

import java.nio.charset.Charset;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.scheduling.commonj.DelegatingWork;

import com.dream.model.Reptile_Record;
import com.teamdev.jxbrowser.chromium.AuthRequiredParams;
import com.teamdev.jxbrowser.chromium.BeforeRedirectParams;
import com.teamdev.jxbrowser.chromium.BeforeSendHeadersParams;
import com.teamdev.jxbrowser.chromium.BeforeSendProxyHeadersParams;
import com.teamdev.jxbrowser.chromium.BeforeURLRequestParams;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.Cookie;
import com.teamdev.jxbrowser.chromium.DataReceivedParams;
import com.teamdev.jxbrowser.chromium.HeadersReceivedParams;
import com.teamdev.jxbrowser.chromium.NetworkDelegate;
import com.teamdev.jxbrowser.chromium.PACScriptErrorParams;
import com.teamdev.jxbrowser.chromium.RequestCompletedParams;
import com.teamdev.jxbrowser.chromium.RequestParams;
import com.teamdev.jxbrowser.chromium.ResponseStartedParams;
import com.teamdev.jxbrowser.chromium.SendHeadersParams;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.swing.DefaultNetworkDelegate;

/**
 * 拦截返回数据数据
 *
 **/
@SuppressWarnings("all")
public class ParseNetworkDelegat extends DefaultNetworkDelegate {

	private String receiveData = "";

	private int pageNum = 0;

	private int parsePageNum = -1;

	private Browser browser;

	private List<String> pageNumList = new ArrayList<>();
	
	private String linkMark = "view:_id1:_id2:cbMain:_id135:pager1__Group__lnk__";
	
	private DOMDocument document;
	
	private String startDate;
	
	private String endDate;
	
	public ParseNetworkDelegat(Browser browser,String startDate, String endDate) {
		// TODO Auto-generated constructor stub
		this.browser = browser;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	@Override
	public void onCompleted(RequestCompletedParams params) {
		// TODO Auto-generated method stub
		int code = params.getResponseCode();
		String codeStr = String.valueOf(code);
		if(codeStr.startsWith("4") || codeStr.startsWith("5") || codeStr.startsWith("6")){
			System.out.println("===================爬取数据异常=======================");
			//browser.dispose();
		}
	}

	@Override
	public void onDataReceived(DataReceivedParams params) {
		try{
			String url = params.getURL();
			if (url.contains("xpSearchResultsM.xsp")) {
				String tempHtmlStr = new String(params.getData(), Charset.forName("UTF-8"));
				receiveData += tempHtmlStr;
				if (tempHtmlStr.contains("</html>") && !tempHtmlStr.contains("XSP_UPDATE_SCRIPT_END")) {
					parsePageNum = ParseHtmlPageData.getPageNum(receiveData);
					pageNumList = ParseHtmlPageData.parsePageNum(receiveData);
					if(pageNumList == null || pageNumList.size() <= 0){
						System.out.println("===================爬取数据完成1=======================");
						//browser.dispose();
					}else{
						pageNumList.remove(0);
					}
					System.out.println("=========================数据总数==============================="+parsePageNum);
					String dataNum =  ParseHtmlPageData.getDataNum(receiveData);
					Reptile_Record rr = new Reptile_Record();
					rr.setAllNum(dataNum);
					rr.setStartDate(startDate);
					rr.setEndDate(endDate);
					rr.setReptlieNum("0");
					
					PullMain.insertQueue.add(rr);
				}
				if (tempHtmlStr.contains("</html>") || tempHtmlStr.contains("XSP_UPDATE_SCRIPT_END")) {
					ParseHtmlPageData.parse(receiveData,startDate,endDate);
					receiveData = "";
					pageNum+=1;
					String nextId = linkMark + pageNum;
					System.out.println("==================="+nextId+"=======================");
					
					if(pageNumList == null || pageNumList.size() <= 0){
						System.out.println("===================爬取数据完成2=======================");
						browser.loadURL("https://documents.un.org/prod/ods.nsf/home.xsp");
						//browser.dispose();
						return;
					}
					if(pageNumList.contains(nextId)){
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						pageNumList.remove(nextId);
						document = browser.getDocument();
						DOMElement elemnt = document.findElement(By.id(nextId));
						elemnt.click();
					}
				}
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	
}
