package com.dream.file;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.hxtt.a.a;
import com.hxtt.b.br;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.DOMNode;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEvent;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEventListener;
import com.teamdev.jxbrowser.chromium.dom.events.DOMEventType;
import com.teamdev.jxbrowser.chromium.events.FailLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.FrameLoadEvent;
import com.teamdev.jxbrowser.chromium.events.LoadEvent;
import com.teamdev.jxbrowser.chromium.events.LoadListener;
import com.teamdev.jxbrowser.chromium.events.ProvisionalLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.StartLoadingEvent;

/**
 * 触发翻页代码
 *
 **/
@SuppressWarnings("all")
public class ParsePageBrowerListener implements LoadListener {

	private int pagetNum = 0;

	private List<String> pageNumList = new ArrayList<>();

	@Override
	public void onDocumentLoadedInFrame(FrameLoadEvent arg0) {
		// TODO Auto-generated method stub
		Browser browser = arg0.getBrowser();
		try {
			String htmlContents = browser.getHTML();
			DOMDocument document = browser.getDocument();
			if (StringUtils.isEmpty(htmlContents)) {

				return;
			}
			if (pageNumList.size() <= 0) {
				pageNumList = ParseHtmlPageData.parsePageNum(htmlContents);

				if (pageNumList == null || pageNumList.size() <= 0) {
					browser.removeLoadListener(this);
					return;
				}

				String linkMark = "view:_id1:_id2:cbMain:_id135:pager1__Group__lnk__";
				
				for(String id:pageNumList){
					DOMElement elemnt = document.findElement(By.id(id));
					elemnt.click();
					document = browser.getDocument();
				}
				
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}

	}

	@Override
	public void onDocumentLoadedInMainFrame(LoadEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onFailLoadingFrame(FailLoadingEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onFinishLoadingFrame(FinishLoadingEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onProvisionalLoadingFrame(ProvisionalLoadingEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStartLoadingFrame(StartLoadingEvent arg0) {
		// TODO Auto-generated method stub
	}

}
