package com.dream.file;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dream.model.File_Entity;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;

@SuppressWarnings("all")
public class ParseHtmlPageData {

	private static SimpleDateFormat formatddMMyyyy = new SimpleDateFormat("dd/MM/yyyy");

	private static SimpleDateFormat formatyyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");

	public static String parsePageNum(String htmlContents, int pageNum) {
		try {
			Document doc = Jsoup.parse(htmlContents);
			Element page = doc.getElementById("view:_id1:_id2:cbMain:_id135:pager1");
			if (page == null || !page.hasText()) {
				return null;
			}
			Elements pageNums = page.getElementsByTag("a");
			String id = pageNums.get(pageNum).attr("id");

			return id;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public static List<String> parsePageNum(String htmlContents) {
		try {
			Document doc = Jsoup.parse(htmlContents);
			Element page = doc.getElementById("view:_id1:_id2:cbMain:_id135:pager1");
			if (page == null || !page.hasText()) {
				return null;
			}
			Elements pageNums = page.getElementsByTag("a");
			List<String> pageNumList = new ArrayList<>();

			for (int i = 1; i < pageNums.size() - 1; i++) {
				String id = pageNums.get(i).attr("id");
				if (StringUtils.isNotEmpty(id)) {
					pageNumList.add(id);
				}
			}
			return pageNumList;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public static int getPageNum(String htmlContents) {
		try {
			Document doc = Jsoup.parse(htmlContents);
			Element page = doc.getElementById("view:_id1:_id2:cbMain:_id135:pager1");
			if (page == null || !page.hasText()) {
				return 0;
			}
			Elements pageNums = page.getElementsByTag("a");

			return pageNums.size() - 2;

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	public static String getDataNum(String contents){
		Document doc = Jsoup.parse(contents);
		Element page = doc.getElementById("view:_id1:_id2:cbMain:_id135:cfPageTitle");
		String dataNumHtmlStr = page.html();
		String dataNum = dataNumHtmlStr.substring(dataNumHtmlStr.indexOf("显示总共<b>")+"显示总共<b>".length(), dataNumHtmlStr.indexOf("</b>个检索结果"));
		return dataNum;
	}
	public static void parse(String htmlContents, String startDate, String endDate) {
		Document doc = Jsoup.parse(htmlContents);
		Elements es = doc.getElementsByClass("viewHover");
		System.out.println("====================parse========================");
		for (int i = 0; i < es.size(); i++) {
			try {
				Element e = es.get(i);
				Element childE = e.child(1).child(0).child(0);
				Elements childRowThree = e.child(2).getElementsByTag("div");
				Elements childRowThree2 = e.child(3).getElementsByTag("div");
				Elements spans0 = childRowThree.get(0).getElementsByTag("span");
				Elements spans2 = childRowThree.get(4).getElementsByTag("span");
				String docId = spans0.get(0).text() == null ? "" : spans0.get(0).text();
				String range = spans2.get(0).text() == null ? "" : spans2.get(0).text();
				String domain = spans2.get(1).text() == null ? "" : spans2.get(1).text();
				String annual = spans0.get(1).text() == null ? "" : spans0.get(1).text();
				String agenda = spans0.get(2).text() == null ? "" : spans0.get(2).text();
				String publishDate = childRowThree2.get(0).getElementsByClass("odsText").get(0).text();
				String fileName = childE.html();
				System.out.println((i + 1) + " " + fileName);

				Elements details = e.getElementsByClass("details");
				if (details == null || details.size() <= 0) {
					continue;
				}

				Elements files = details.get(0).getElementsByClass("row").get(0).children();
				for (int j = 0; j < files.size(); j++) {
					try {

						Element file = files.get(j);
						String language = file.getElementsByClass("odsTitle").html();
						String jobID = file.getElementsByClass("addLines").get(0).html();
						Elements as = file.getElementsByTag("a");
						if (as == null || as.size() <= 0) {
							continue;
						}
						for (int n = 0; n < as.size(); n++) {
							try {
								Element a = as.get(n);
								String httpUrl = a.attr("href");
								if (StringUtils.isEmpty(httpUrl)) {
									continue;
								}
								File_Entity fileEntity = new File_Entity();

								fileEntity.setFileName(fileName);
								fileEntity.setUuidFileName(UUID.randomUUID().toString().replaceAll("-", "")
										+ httpUrl.substring(httpUrl.lastIndexOf("."), httpUrl.lastIndexOf("?")));
								String fileType = "";
								if (httpUrl.contains(".")) {
									fileType = httpUrl.substring(httpUrl.lastIndexOf(".") + 1,
											httpUrl.lastIndexOf("?"));
								}
								fileEntity.setFileHttpUrl(httpUrl);
								fileEntity.setFileLanguage(language);
								fileEntity.setFileDocID(docId);
								fileEntity.setFileRange(range);
								fileEntity.setFileDomain(domain);
								fileEntity.setFileAnnual(annual);
								fileEntity.setFileAgenda(agenda);
								fileEntity.setFileJobID(jobID);
								fileEntity.setFilePublishDate(publishDate);
								fileEntity.setFileType(fileType);
								fileEntity.setString1(startDate);
								fileEntity.setString2(endDate);
								fileEntity.setString3(getDataNum(htmlContents));
								PullMain.downloadQueue.add(fileEntity);
							} catch (Exception exx) {
								// TODO: handle exception
								exx.printStackTrace();
							}
						}
					} catch (Exception ex) {
						// TODO: handle exception
						ex.printStackTrace();
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	public static void clickSearchBtn(Browser browser, String startDate, String endDate) throws ParseException {
		System.out.println("================="+startDate+"===================");
		DOMDocument document = browser.getDocument();
		DOMElement docE = document.findElement(By.id("view:_id1:_id2:btnRefine"));
		System.out.println(docE.getInnerHTML());
		DOMElement docStartDate = document.findElement(By.id("view:_id1:_id2:dtPubDateFrom"));
		docStartDate.setAttribute("value", formatddMMyyyy.format(formatyyyyMMdd.parse(startDate)));

		DOMElement docHiddenStartDom = document.findElement(By.name("view:_id1:_id2:dtPubDateFrom"));
		docHiddenStartDom.setAttribute("value", startDate);

		DOMElement docEndDate = document.findElement(By.id("view:_id1:_id2:dtPubDateTo"));
		docEndDate.setAttribute("value", formatddMMyyyy.format(formatyyyyMMdd.parse(endDate)));
		DOMElement docHiddenEndDom = document.findElement(By.name("view:_id1:_id2:dtPubDateTo"));
		docHiddenEndDom.setAttribute("value", endDate);

		docE.click();

	}
}
