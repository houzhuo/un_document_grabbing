package com.dream.file;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

@SuppressWarnings("all")
public class PullRemoteHtml {

	private static String view_id1__VUID = "";

	private static String last_view_id1__VUID = "";

	public static CookieStore getDownloadCookie() throws ClientProtocolException, IOException {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpGet get = new HttpGet("https://documents.un.org");
		HttpClientContext context = HttpClientContext.create();
		HttpClientContext context2 = HttpClientContext.create();
		CloseableHttpResponse response = httpClient.execute(get, context);
		String fileContent = EntityUtils.toString(response.getEntity(), "UTF-8");
		CloseableHttpClient client = HttpClients.custom().setDefaultCookieStore(context.getCookieStore()).build();
		HttpPost post = new HttpPost("https://documents.un.org/prod/ods.nsf/home.xsp");
		post.setHeader("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36");
		CloseableHttpResponse response2 = client.execute(post, context2);

		CookieStore cookieStore = context2.getCookieStore();
		List<Cookie> list = cookieStore.getCookies();
		StringBuffer tmpcookies = new StringBuffer();

		return context.getCookieStore();
	}

	public static CookieStore getCookie() {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpGet get = new HttpGet("https://documents.un.org");
		HttpClientContext context = HttpClientContext.create();
		try {
			CloseableHttpResponse response = httpClient.execute(get, context);
			try {
				String fileContent = EntityUtils.toString(response.getEntity(), "UTF-8");
				Document doc = Jsoup.parse(fileContent);
				Element element = doc.getElementById("view:_id1__VUID");
				view_id1__VUID = element.attr("value");
				return context.getCookieStore();
			} finally {
				response.close();
			}
		} catch (IOException e) {
		} finally {
			try {
				httpClient.close();
			} catch (IOException e) {
			}
		}
		return null;
	}

	public static String getRemoteHtml(CookieStore cookieStore, String startDate, String endDate)
			throws ClientProtocolException, IOException {
		List<Cookie> list = cookieStore.getCookies();
		StringBuffer tmpcookies = new StringBuffer();
		System.out.println("getRemoteHtml1: " + view_id1__VUID);
		if (StringUtils.isEmpty(last_view_id1__VUID)) {
			last_view_id1__VUID = view_id1__VUID;
		}
		for (Cookie c : list) {
			tmpcookies.append(c.getName() + "=" + c.getValue() + ";");
		}
		String tmpcookieStr = tmpcookies.toString();
		if (tmpcookieStr.contains(";")) {
			tmpcookieStr = tmpcookieStr.substring(0, tmpcookieStr.lastIndexOf(";"));
		}
		CloseableHttpClient client = HttpClients.custom().setDefaultCookieStore(cookieStore).build();

		HttpPost httpPost = new HttpPost("https://documents.un.org/prod/ods.nsf/xpSearchResultsM.xsp");

		MultipartEntityBuilder partBuild = MultipartEntityBuilder.create();
		partBuild.addTextBody("view:_id1:_id2:dtPubDateFrom", startDate);
		partBuild.addTextBody("view:_id1:_id2:dtPubDateTo", endDate);
		partBuild.addTextBody("view:_id1:_id2:rgTrunc", "R");
		partBuild.addTextBody("view:_id1:_id2:cbType", "FP");
		partBuild.addTextBody("view:_id1:_id2:cbSort", "R");
		partBuild.addTextBody("$$viewid", view_id1__VUID);
		partBuild.addTextBody("$$xspsubmitid", "view:_id1:_id2:_id130");
		partBuild.addTextBody("$$xspsubmitscroll", "0|322");
		partBuild.addTextBody("view:_id1", "view:_id1");

		httpPost.setEntity(partBuild.build());

		httpPost.setHeader("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36");

		httpPost.setHeader("Cookie", tmpcookieStr);

		HttpResponse response = client.execute(httpPost);

		String fileContent = EntityUtils.toString(response.getEntity(), "UTF-8");
		client.close();

		CloseableHttpClient client2 = HttpClients.custom().setDefaultCookieStore(cookieStore).build();

		HttpResponse response2 = client2.execute(httpPost);

		String fileContent2 = EntityUtils.toString(response2.getEntity(), "UTF-8");

		return fileContent2;

	}

	public static String getRemoteHtml(CookieStore cookieStore, String startDate, String endDate, String xspsubmitid,
			String $$viewid) throws ClientProtocolException, IOException {
		List<Cookie> list = cookieStore.getCookies();
		StringBuffer tmpcookies = new StringBuffer();
		System.out.println("$$viewid: " + view_id1__VUID);
		for (Cookie c : list) {
			tmpcookies.append(c.getName() + "=" + c.getValue() + ";");
		}
		String tmpcookieStr = tmpcookies.toString();
		if (tmpcookieStr.contains(";")) {
			tmpcookieStr = tmpcookieStr.substring(0, tmpcookieStr.lastIndexOf(";"));
		}
		// CloseableHttpClient client =
		// HttpClients.custom().setDefaultCookieStore(cookieStore).build();
		CloseableHttpClient client = HttpClients.custom().build();
		HttpPost httpPost = new HttpPost(
				"https://documents.un.org/prod/ods.nsf/xpSearchResultsM.xsp?$$ajaxid=view:_id1:_id2:cbMain:mainPanel");

		MultipartEntityBuilder partBuild = MultipartEntityBuilder.create();
		partBuild.addTextBody("view:_id1:_id2:dtPubDateFrom", startDate);
		partBuild.addTextBody("view:_id1:_id2:dtPubDateTo", endDate);
		partBuild.addTextBody("view:_id1:_id2:rgTrunc", "R");
		partBuild.addTextBody("view:_id1:_id2:cbType", "FP");
		partBuild.addTextBody("view:_id1:_id2:cbSort", "R");
		partBuild.addTextBody("$$viewid", view_id1__VUID);
		partBuild.addTextBody("$$xspsubmitid", xspsubmitid);
		partBuild.addTextBody("$$xspsubmitscroll", "0|400");
		partBuild.addTextBody("$$xspexecid", "view:_id1:_id2:cbMain:_id135:pager2");
		partBuild.addTextBody("view:_id1", "view_id1");
		System.out.println(xspsubmitid);

		httpPost.setEntity(partBuild.build());

		httpPost.setHeader("User-Agent",
				"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36");

		httpPost.setHeader("Cookie", tmpcookieStr);
		httpPost.setHeader("X-Requested-With", "XMLHttpRequest");
		httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
		httpPost.setHeader("connection", "keep-alive");
		httpPost.setHeader("Accept", "*/*");
		httpPost.setHeader("Accept-Encoding", "gzip, deflate");
		HttpResponse response = client.execute(httpPost);

		String fileContent = EntityUtils.toString(response.getEntity(), "UTF-8");
		System.out.println("============fileContent==================");

		client.close();

		/*
		 * CloseableHttpClient client2 =
		 * HttpClients.custom().setDefaultCookieStore(cookieStore).build();
		 * 
		 * HttpResponse response2 = client2.execute(httpPost);
		 * 
		 * String fileContent2 = EntityUtils.toString(response2.getEntity(),
		 * "UTF-8");
		 * 
		 * client2.close();
		 */

		return fileContent;

	}
}
