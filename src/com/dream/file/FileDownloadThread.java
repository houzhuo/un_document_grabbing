package com.dream.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.dream.common.upload.BigFileUploadUtil;
import com.dream.model.File_Entity;
import com.hxtt.sql.fi;

@SuppressWarnings("all")
public class FileDownloadThread extends Thread{

	private File_Entity file;
	private CookieStore cookieStore;

	public FileDownloadThread(File_Entity file, CookieStore cookieStore) {
		// TODO Auto-generated constructor stub
		this.file = file;
		this.cookieStore = cookieStore;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		HttpEntity entity = null;
		try {
			List<Cookie> list = cookieStore.getCookies();
			StringBuffer tmpcookies = new StringBuffer();
			for (Cookie c : list) {
				if ("SessionID".equals(c.getName())) {
					continue;
				}
				tmpcookies.append(c.getName() + "=" + c.getValue() + ";");
			}
			String tmpcookieStr = tmpcookies.toString();

			CloseableHttpClient client = HttpClients.custom().setDefaultCookieStore(cookieStore).build();
			HttpGet post = new HttpGet(file.getFileHttpUrl());
			post.setHeader("Cookie", tmpcookieStr);

			HttpResponse response = client.execute(post);
			entity = response.getEntity();
			
			if(entity == null){
				PullMain.pool.submit(new FileDownloadThread(file, cookieStore));
				return;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			PullMain.pool.submit(new FileDownloadThread(file, cookieStore));
			return;
		}
		
		try {

			InputStream is = entity.getContent();
			String publishDate = file.getFilePublishDate();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			if (StringUtils.isEmpty(publishDate)) {
				publishDate = dateFormat.format(new Date());
			} else {
				SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd/MM/yyy");
				publishDate = dateFormat.format(dateFormat2.parse(publishDate));
			}
			String downLoadPath = File.separator + publishDate + File.separator + file.getUuidFileName();
			String filepath = BigFileUploadUtil.getBasePath() + downLoadPath;

			File localFile = new File(filepath);
			if (localFile.exists()) {
				localFile.delete();
			}
			localFile.getParentFile().mkdirs();
			localFile.createNewFile();
			
			FileOutputStream fileout = new FileOutputStream(localFile);

			byte[] buffer = new byte[1024];
			int ch = 0;
			while ((ch = is.read(buffer)) != -1) {
				fileout.write(buffer, 0, ch);
			}

			is.close();
			fileout.flush();
			fileout.close();

			file.setPath(downLoadPath);
			file.setFilePublishDate(publishDate);
			file.setFileSize(BigFileUploadUtil.reckonFileSize(localFile.length()));
			PullMain.insertQueue.add(file);
			
			
		} catch (Exception e) {
			// TODO: handle exception
			// 再次下载
			PullMain.pool.submit(new FileDownloadThread(file, cookieStore));
		}
	}
}
