package com.dream.file;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.dream.system.service.FileService;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.BrowserContext;
import com.teamdev.jxbrowser.chromium.NetworkService;
import com.teamdev.jxbrowser.chromium.dom.By;
import com.teamdev.jxbrowser.chromium.dom.DOMDocument;
import com.teamdev.jxbrowser.chromium.dom.DOMElement;
import com.teamdev.jxbrowser.chromium.dom.DOMNode;
import com.teamdev.jxbrowser.chromium.events.FailLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.FrameLoadEvent;
import com.teamdev.jxbrowser.chromium.events.LoadEvent;
import com.teamdev.jxbrowser.chromium.events.LoadListener;
import com.teamdev.jxbrowser.chromium.events.ProvisionalLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.StartLoadingEvent;

/**
 * 初始化浏览器url路径
 * **/
@SuppressWarnings("all")
public class ParseInitPageBrowerListener implements LoadListener {

	
	private SimpleDateFormat formatddMMyyyy = new SimpleDateFormat("dd/MM/yyyy");

	private SimpleDateFormat formatyyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");

	private ParseNetworkDelegat parseNetworkDelegat;
	
	private String mark = "view:_id1:_id2:cbMain:_id135:pager1__Group__lnk__";
	
	private int initStatus = 0;
	
	
	public ParseInitPageBrowerListener (int initStatus) {
		// TODO Auto-generated constructor stub
		this.initStatus = initStatus;
	}

	@Override
	public void onDocumentLoadedInFrame(FrameLoadEvent arg0) {
		// TODO Auto-generated method stub
		Browser browser = arg0.getBrowser();
		System.out.println("================onDocumentLoadedInFrame==============");
		try {
			
			DOMElement doe =  browser.getDocument().getDocumentElement();
			List<String> pageList = ParseHtmlPageData.parsePageNum(doe.getInnerHTML());
			List<DOMElement> domList =  doe.findElements(By.className("active"));
			
			String currentActivePageNum = "";
			
			//初始化加载数据判断
			if(initStatus == 1){
				if(domList != null && domList.size() > 0){
					//查询后是否有分页数据
					for(DOMElement de:domList){
						List<DOMElement> nodeList = de.findElements(By.tagName("a"));
						if(nodeList == null || nodeList.size() <= 0){
							continue;
						}
						String id = nodeList.get(0).getAttribute("id");
						if(id.contains(mark)){
							currentActivePageNum = id.replaceAll(mark, "");
							break;
						}
					}
					
					if(Integer.valueOf(currentActivePageNum) + 1 < pageList.size()){
						return;
					}
				}
				
			}
			
			String tempDate = PullMain.searchDateQueue.take();
			
			BrowserContext browserContext = BrowserContext.defaultContext();
			NetworkService networkService = browserContext.getNetworkService();
					
			parseNetworkDelegat = new ParseNetworkDelegat(browser, tempDate, tempDate);
			networkService.setNetworkDelegate(parseNetworkDelegat);
			
			try{
				ParseHtmlPageData.clickSearchBtn(arg0.getBrowser(), tempDate, tempDate);
			}catch (Exception e) {
				// TODO: handle exception
				InitBrower.initBrowerUrl();
			}
			
			initStatus = 1;
		
		} catch (Exception e) {
			// TODO: handle exception
			browser.removeLoadListener(this);
		}

	}

	@Override
	public void onDocumentLoadedInMainFrame(LoadEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("================onDocumentLoadedInMainFrame==============");
		
	}

	@Override
	public void onFailLoadingFrame(FailLoadingEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("================onFailLoadingFrame=============");
	}

	@Override
	public void onFinishLoadingFrame(FinishLoadingEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("================onFinishLoadingFrame==============");
	}

	@Override
	public void onProvisionalLoadingFrame(ProvisionalLoadingEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("================onProvisionalLoadingFrame==============");
	}

	@Override
	public void onStartLoadingFrame(StartLoadingEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("================onStartLoadingFrame==============");

	}

}
