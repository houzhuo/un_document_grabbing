package com.dream.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import javax.persistence.criteria.CriteriaBuilder.In;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.dream.common.upload.BigFileUploadUtil;
import com.dream.model.File_Entity;
import com.dream.system.service.FileService;

/*@Component*/
@SuppressWarnings("all")
public class DeleteFiles {

	@Autowired
	private FileService fileService;
	
	@Scheduled(fixedDelay = 10000,initialDelay = 60000)
	public void excute(){
		try{
			System.out.println("=============================DeleteFiles=============================");
			List<String> list = fileService.getFileUUidName();
			List<File> files = new ArrayList<>();
			getFiles(BigFileUploadUtil.getBasePath(), files);
			System.out.println("dir files: "+files.size()+" database:"+list.size());
			
			List<File> temp = null;
			int size = 1000;
			int num = (int) Math.ceil((Double.valueOf(files.size()))/(Double.valueOf(size)));
			List<FutureTask<Integer>> tasks = new ArrayList<>();
			/*for(int i = 0;i < num;i++){
				temp = new ArrayList<>();
				int start = i * size;
				int end = (i + 1) * size > list.size() ? list.size() : (i + 1) * size;
				temp.addAll(list.subList(start, end));
				FutureTask<Integer> task = new FutureTask<>(new exce(files, temp,(i+1)));
				PullMain.pool.execute(new Thread(task));
				tasks.add(task);
			}*/
			
			for(int i = 0;i < num;i++){
				temp = new ArrayList<>();
				int start = i * size;
				int end = (i + 1) * size > files.size() ? files.size() : (i + 1) * size;
				temp.addAll(files.subList(start, end));
				FutureTask<Integer> task = new FutureTask<>(new checkFiles(temp, list, num));
				PullMain.pool.execute(new Thread(task));
				tasks.add(task);
			}
			
			
			for(FutureTask<Integer> task:tasks){
				System.out.println(task.get());
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	
	public void getFiles(String path,List<File> files){
		File f = new File(path);
		if(!f.exists()){
			return;
		}
		if(f.isFile()){
			System.out.println(f.getAbsolutePath());
			files.add(f);
		}
		if(f.isDirectory()){
			File[] fs = f.listFiles();
			for(File e:fs){
				if(e.isFile()){
					files.add(e);
				}else if(f.isDirectory()){
					getFiles(e.getAbsolutePath(), files);
				}
			}
		}
	}
	
	class checkFiles implements Callable<Integer>{
		
		private List<File> files;
		
		private List<String> dataBases;
		
		private int num;
		
		public checkFiles(List<File> files,List<String> dataBases,int num) {
			// TODO Auto-generated constructor stub
			this.files = files;
			this.dataBases = dataBases;
			this.num = num;
		}
		@Override
		public Integer call() throws Exception {
			// TODO Auto-generated method stub
			for(File e:files){
				String fileName = e.getName();
				boolean mark = false;
				for(String data:dataBases){
					if (data.equals(fileName)) {
						mark = true;
						break;
					}
				}
				if(!mark){
					System.out.println(e.getAbsolutePath());
					e.delete();
				}
			}
			System.out.println("==============完成=========================="+num);
			return 1;
		}
	}
	
	class exce implements Callable<Integer>{
		private List<File> files;
		
		private List<String> dataBases;
		
		private int num;
		
		public exce(List<File> files,List<String> dataBases,int num) {
			// TODO Auto-generated constructor stub
			this.files = files;
			this.dataBases = dataBases;
			this.num = num;
		}
		@Override
		public Integer call() {
			// TODO Auto-generated method stub
			for(String data:dataBases){
				boolean mark = false;
				for(File file:files){
					String fileName = file.getName();
					if(data.equals(fileName)){
						mark = true;
					}
					
				}
				if(!mark){
					System.out.println(data);
					fileService.deleteFilesByUUIDName(data);
				}
			}
			System.out.println("==============完成=========================="+num);
			return 1;
		}
	}
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		for(int i = 0;i < 56;i++){
			list.add(i+"");
		}
		List<String> temp = null;
		int size = 10;
		int num = (int) Math.ceil((Double.valueOf(list.size()))/(Double.valueOf(size)));
		for(int i = 0;i < num;i++){
			temp = new ArrayList<>();
			int start = i * size;
			int end = (i + 1) * size > list.size() ? list.size() : (i + 1) * size;
			temp.addAll(list.subList(start, end));
			System.out.println(start+" "+end+" "+temp.size());
		}
	}
}
