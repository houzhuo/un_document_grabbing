package com.dream.common.upload;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.springframework.web.multipart.MultipartFile;

import com.dream.common.rsa.RSAUtil;
import com.hxtt.b.by;
import com.hxtt.sql.fi;
import com.hxtt.sql.s;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@SuppressWarnings("all")
public class BigFileUploadUtil {

	private static String basePath = "";

	private static String downloadbaseUrl = "";

	private static String encrypt = "";

	private static String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKGOsKqfWm/3DIqa3iZCPYJPwrMDp9MMDhdv7+A9U4ldqhLDyXevJVixcT4O4XmK7N6OJrWx+deYmwrQyud1U4cCAwEAAQ==";
	private static String privateKey = "MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAoY6wqp9ab/cMipreJkI9gk/CswOn0wwOF2/v4D1TiV2qEsPJd68lWLFxPg7heYrs3o4mtbH515ibCtDK53VThwIDAQABAkArkZmfSf5Pt6ZQMte6JZ8E55GwSUoXJmcedveP17oRjxxdqkplM918lO7s97/pd+KN9cbxmFUBdjHQaPyUt0xBAiEA5t8wLjB7V6wjQbPfaBj0si5Y+Pt6XO9zLwEhcAa8XnECIQCzJC86TAHom9z1uAikjXaB+8iICICT0EIcp1or5Oy9dwIhALE81jgfcBE2TTq7Y97+iZvgiN1b8ew2xy5mp8zg3uuBAiBco5K0wAegpbGPdFsr1Wf6ch1Sk6sCRYlazgRofJscowIgXH0SPK1ABiHdAisZKXUClP3+ACA5redJ25gcpUFTQDI=";

//	private static String confuseStr = "qwertyuiopasdfghjklzxcvbnm123456789";
	
	private static int confuseInt = 1024;
	
	private static int confuseFileLength = 500;//-1：全文件
	/************ 密钥差长度512 ***********/

	// 加密数据长度
	private static int encrySize = 53;
	// 解密数据长度
	private static int decryptSize = 64;

	static {
		basePath = System.getProperty("user.dir") + File.separator + "..";
		downloadbaseUrl = "http://";
		encrypt = "0";
	}

	public static String getUploadFileSavePath() {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String dirName = dateFormat.format(new Date());
		String resultDirPath = basePath + File.separator + "uploadFiles" + File.separator + dirName;
		File f = new File(resultDirPath);
		if (!f.exists()) {
			f.mkdirs();
		}
		return f.getAbsolutePath();
	}

	public static int getEncrySize() {
		return encrySize;
	}

	public static void setEncrySize(int encrySize) {
		BigFileUploadUtil.encrySize = encrySize;
	}

	public static int getDecryptSize() {
		return decryptSize;
	}

	public static void setDecryptSize(int decryptSize) {
		BigFileUploadUtil.decryptSize = decryptSize;
	}

	public static String getBasePath() {
		return basePath;
	}

	public static void setBasePath(String basePath) {
		BigFileUploadUtil.basePath = basePath;
	}

	public static String getDownloadbaseUrl() {
		return downloadbaseUrl;
	}

	public static void setDownloadbaseUrl(String host, String port, String projectName) {
		BigFileUploadUtil.downloadbaseUrl = BigFileUploadUtil.downloadbaseUrl + host + ":" + port + "/" + projectName;
	}

	public static String getEncrypt() {
		return encrypt;
	}

	public static void setEncrypt(String encrypt) {
		BigFileUploadUtil.encrypt = encrypt;
	}

	public static void deleteTempFiles(String path) {
		File f = new File(path);
		if (!f.exists()) {
			return;
		}
		File[] fs = f.listFiles();
		for (File e : fs) {
			if (e.isDirectory()) {
				deleteTempFiles(e.getAbsolutePath());
			} else if (e.isFile()) {
				boolean mark = e.delete();
				if (!mark) {
					System.gc();
					e.delete();
				}
			}
		}
		boolean mark = f.delete();
		if (!mark) {
			System.gc();
			f.delete();
		}
	}

	public static String reckonFileSize(long size) {
		DecimalFormat format = new DecimalFormat("0.00");
		String[] s = { "B", "KB", "M", "G" };
		boolean mark = true;
		String res = "";
		int i = 0;
		Double douSize = Double.valueOf(size);
		while (mark) {
			Double temp = douSize / Double.valueOf(1024);
			i += 1;
			if (temp < 1 || i > 3) {
				res = String.valueOf(format.format(temp * Double.valueOf(1024))) + s[i - 1];
				mark = false;
			}
			douSize = temp;
		}

		return res;
	}

	/***
	 * 出现跨域问题添加<br />
	 * response.setHeader("Access-Control-Allow-Origin", "*");
	 * 
	 */
	public static void uploadPartFile(HttpServletRequest request, MultipartFile file)
			throws IOException, ServletException {
		String path = BigFileUploadUtil.getBasePath();
		String name = request.getParameter("name");
		String index = request.getParameter("index");
		String tempFileName = name.substring(0, name.lastIndexOf(".")) + "_" + index
				+ name.substring(name.lastIndexOf("."), name.length());

		BufferedInputStream buff = null;
		FileOutputStream out = null;
		try {
			buff = new BufferedInputStream(file.getInputStream());
			File f = new File(path + File.separator + name + "_temp" + File.separator + tempFileName);
			if (!f.getParentFile().exists()) {
				f.getParentFile().mkdirs();
			}
			if (f.exists()) {
				f.delete();
			}
			out = new FileOutputStream(f);
			byte[] read = new byte[1024];
			int mark = 0;
			while ((mark = buff.read(read)) != -1) {
				out.write(read, 0, mark);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (buff != null) {
				try {
					buff.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	/***
	 * 出现跨域问题添加<br />
	 * response.setHeader("Access-Control-Allow-Origin", "*");
	 * 
	 * @throws Exception
	 * 
	 */
	public static List<JSONObject> meragePartFile(HttpServletRequest request, boolean isEncrypt) throws Exception {
		String jsonStr = request.getParameter("jsonStr");
		String path = BigFileUploadUtil.getBasePath();
		JSONArray ja = JSONArray.fromObject(jsonStr);
		List<JSONObject> resultList = new ArrayList<JSONObject>();
		List<File> encryptFileList = new ArrayList<>();

		for (int j = 0; j < ja.size(); j++) {
			String message = "";
			String name = ja.getJSONObject(j).getString("name");
			String total = ja.getJSONObject(j).getString("total");
			/*String fileSecurity = ja.getJSONObject(j).getString("fileSecurity");*/
			String uuid = UUID.randomUUID().toString().replaceAll("-", "");

			String resultFilePath = BigFileUploadUtil.getUploadFileSavePath() + File.separator + uuid
					+ name.substring(name.lastIndexOf("."), name.length());
			File resultFile = new File(resultFilePath);
			FileChannel fc = new FileOutputStream(resultFile, true).getChannel();
			String tempDir = path + File.separator + name + "_temp";

			for (int i = 1; i <= Integer.parseInt(total); i++) {
				String tempFileName = name.substring(0, name.lastIndexOf(".")) + "_" + i
						+ name.substring(name.lastIndexOf("."), name.length());
				File e = new File(tempDir + File.separator + tempFileName);
				if (!e.exists()) {
					message = "临时文件不存在!";
					System.out.println(message);
					break;
				}
				FileChannel tempFc = new FileInputStream(e).getChannel();
				fc.transferFrom(tempFc, fc.size(), tempFc.size());
				tempFc.close();
			}

			fc.close();

			encryptFileList.add(resultFile);

			JSONObject resultObject = new JSONObject();

			resultObject.put("path",
					resultFile.getAbsolutePath()
							.substring(BigFileUploadUtil.getBasePath().length(), resultFile.getAbsolutePath().length())
							.replaceAll("\\\\", "/"));
			resultObject.put("uuidFileName", resultFile.getName());
			resultObject.put("fileSize", BigFileUploadUtil.reckonFileSize(resultFile.length()));
			resultObject.put("fileType", resultFile.getName().subSequence(resultFile.getName().lastIndexOf(".") + 1,
					resultFile.getName().length()));
			resultObject.put("temp1", BigFileUploadUtil.getDownloadbaseUrl() + File.separator);
			/*resultObject.put("fileSecurity", fileSecurity);*/
			// resultObject.put("fileMD5",
			// BigFileUploadUtil.getFileMd5(resultFile));

			resultList.add(resultObject);

			BigFileUploadUtil.deleteTempFiles(tempDir);

		}

		// 加密文件

		if (isEncrypt) {
			for (File e : encryptFileList) {
				confuseFile(e);
			}
		}

		return resultList;
	}

	/**
	 * 
	 * rsa 加密
	 * 
	 **/
	public static File encryptRandomFile(File file) throws Exception {
		String fileName = file.getName();
		String path = file.getAbsolutePath();
		String encryptFileName = fileName.substring(0, fileName.lastIndexOf(".")) + "_encrypt_temp_"
				+ fileName.substring(fileName.lastIndexOf("."), fileName.length());
		File tempFile = new File(path.substring(0, path.lastIndexOf(File.separator)) + File.separator + File.separator
				+ encryptFileName);
		if (tempFile.exists()) {
			tempFile.delete();
		}
		tempFile.createNewFile();
		System.out.println(tempFile);
		RandomAccessFile randomFile = new RandomAccessFile(file, "rw");
		RandomAccessFile randomTempFile = new RandomAccessFile(tempFile, "rw");

		byte[] data = new byte[encrySize];
		int mark = 0;

		try {
			while ((mark = randomFile.read(data)) != -1) {
				if (mark < encrySize) {
					byte[] temp = RSAUtil.copyByteArray(data, mark);
					byte[] encryptData = RSAUtil.encryptByPublicKey(temp, publicKey);
					randomTempFile.seek(randomTempFile.length());
					randomTempFile.write(encryptData);
				} else {
					byte[] encryptData = RSAUtil.encryptByPublicKey(data, publicKey);
					randomTempFile.seek(randomTempFile.length());
					randomTempFile.write(encryptData);
				}
			}
			file.delete();
			tempFile.renameTo(file);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (randomFile != null) {
				randomFile.close();
			}
			if (randomTempFile != null) {
				randomTempFile.close();
			}
		}
		return tempFile;
	}

	/**
	 * TODO rsa 加密
	 * 
	 * @param file
	 * @return
	 * @throws Exception
	 */
	public static File encryptFile(File file) throws Exception {
		String fileName = file.getName();
		String path = file.getAbsolutePath();
		String encryptFileName = fileName.substring(0, fileName.lastIndexOf(".")) + "_encrypt_temp_"
				+ fileName.substring(fileName.lastIndexOf("."), fileName.length());
		File tempFile = new File(
				path.substring(0, path.lastIndexOf(File.separator)) + File.separator + encryptFileName);
		if (tempFile.exists()) {
			tempFile.delete();
		}
		tempFile.createNewFile();
		System.out.println(tempFile);

		byte[] data = new byte[encrySize];
		int mark = 0;
		BufferedInputStream buffer = null;
		FileInputStream fileIn = null;
		FileOutputStream fileOut = null;
		try {
			fileIn = new FileInputStream(file);
			buffer = new BufferedInputStream(fileIn);
			fileOut = new FileOutputStream(tempFile);

			while ((mark = buffer.read(data)) != -1) {
				if (mark < encrySize) {
					byte[] temp = RSAUtil.copyByteArray(data, mark);
					byte[] encryptData = RSAUtil.encryptByPublicKey(temp, publicKey);
					fileOut.write(encryptData);
				} else {
					byte[] encryptData = RSAUtil.encryptByPublicKey(data, publicKey);
					fileOut.write(encryptData);
				}

			}
			fileOut.flush();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (fileOut != null) {
				fileOut.close();
			}
			if (fileIn != null) {
				fileIn.close();
			}
		}

		file.delete();
		tempFile.renameTo(file);

		return tempFile;
	}
	
	/**
	 * TODO rsa 加密
	 * 
	 * @param String
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptString(String resource) throws Exception {
		byte[] data = new byte[encrySize];
		int mark = 0;
		List<byte[]> list = new ArrayList<>();
		byte[] resourceByte = resource.getBytes();
		System.out.println(resourceByte.length);
		int count = (int) (Math.ceil(Double.valueOf(resourceByte.length) / Double.valueOf(encrySize)));
		for (int i = 0; i < count; i++) {
			if ((i + 1) == count) {
				byte[] b = new byte[resourceByte.length - (i * encrySize)];
				System.arraycopy(resourceByte, i * encrySize, b, 0, resourceByte.length - (i * encrySize));
				byte[] encryptData = RSAUtil.encryptByPublicKey(b, publicKey);
				list.add(encryptData);
			} else {
				byte[] b = new byte[encrySize];
				System.arraycopy(resourceByte, i * encrySize, b, 0, encrySize);
				byte[] encryptData = RSAUtil.encryptByPublicKey(b, publicKey);
				list.add(encryptData);
			}

		}
		return byteMergerAll(list.toArray(new byte[0][0]));
	}

	private static byte[] byteMergerAll(byte[]... values) {
		int length_byte = 0;
		for (int i = 0; i < values.length; i++) {
			length_byte += values[i].length;
		}
		byte[] all_byte = new byte[length_byte];
		int countLength = 0;
		for (int i = 0; i < values.length; i++) {
			byte[] b = values[i];
			System.arraycopy(b, 0, all_byte, countLength, b.length);
			countLength += b.length;
		}
		return all_byte;
	}

	/**
	 * TODO rsa 解密
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static File decryptFile(File file) throws IOException {
		String fileName = file.getName();
		String path = file.getAbsolutePath();
		String encryptFileName = fileName.substring(0, fileName.lastIndexOf(".")) + "_decrypt_temp_"
				+ fileName.substring(fileName.lastIndexOf("."), fileName.length());
		File tempFile = new File(
				path.substring(0, path.lastIndexOf(File.separator)) + File.separator + encryptFileName);
		if (tempFile.exists()) {
			tempFile.delete();
		}
		tempFile.createNewFile();
		System.out.println(tempFile);

		byte[] data = new byte[decryptSize];
		int mark = 0;
		BufferedInputStream buffer = null;
		FileInputStream fileIn = null;
		FileOutputStream fileOut = null;
		try {
			fileIn = new FileInputStream(file);
			buffer = new BufferedInputStream(fileIn);
			fileOut = new FileOutputStream(tempFile);

			while ((mark = buffer.read(data)) != -1) {
				if (mark < decryptSize) {
					byte[] temp = RSAUtil.copyByteArray(data, mark);
					byte[] encryptData = RSAUtil.decryptByPrivateKey(temp, privateKey);
					fileOut.write(encryptData);
				} else {
					byte[] encryptData = RSAUtil.decryptByPrivateKey(data, privateKey);
					fileOut.write(encryptData);
				}

			}
			fileOut.flush();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (fileOut != null) {
				fileOut.close();
			}
			if (fileIn != null) {
				fileIn.close();
			}
		}
		return tempFile;
	}

	/**
	 * TODO rsa 解密
	 * 
	 * @param string 最大 64
	 * @return
	 * @throws IOException
	 */
	public static byte[] decryptByteArray(byte[] resource) throws Exception {
		return RSAUtil.decryptByPrivateKey(resource, privateKey);
	}

	/**
	 * TODO 获取文件MD5
	 * 
	 * @param string 
	 * @return
	 * @throws IOException
	 */
	public static String getFileMd5(File file) throws FileNotFoundException {
		String value = null;
		FileInputStream in = new FileInputStream(file);
		try {
			MappedByteBuffer byteBuffer = in.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, file.length());
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(byteBuffer);
			BigInteger bi = new BigInteger(1, md5.digest());
			value = bi.toString(16);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != in) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return value;
	}
	
	/**
	 * TODO 异域 加密
	 * 
	 * @param file 
	 * @return
	 * @throws IOException
	 */
	public static File confuseFile(File file) throws IOException {
		String path = file.getAbsolutePath();
		String fileName = file.getName();
		String encryptFileName = fileName.substring(0, fileName.lastIndexOf(".")) + "_confuse_temp_"
				+ fileName.substring(fileName.lastIndexOf("."), fileName.length());
		File tempFile = new File(
				path.substring(0, path.lastIndexOf(File.separator)) + File.separator + encryptFileName);
		if (tempFile.exists()) {
			tempFile.delete();
		}
		int numOfEncAndDec = 0x99;
		tempFile.createNewFile();
		InputStream fileIn = new FileInputStream(file);
		OutputStream fileOut = new FileOutputStream(tempFile);
		try {
			int b = -1;
			while((b = fileIn.read()) != -1){
				fileOut.write(b^numOfEncAndDec);
			}
			fileOut.flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			fileIn.close();
			fileOut.close();
		}
		file.delete();
		tempFile.renameTo(file);
		return tempFile;
	}
	/**
	 * TODO 异域 解密
	 * 
	 * @param file 
	 * @return
	 * @throws IOException
	 */
	public static String correctFile(File file) throws IOException {
		File tempFile = correctFileData(file);
		return tempFile.getAbsolutePath();
	}
	
	/**
	 * TODO 异域 解密
	 * 
	 * @param file 
	 * @return
	 * @throws IOException
	 */
	public static File correctFile2(File file) throws IOException {
		File tempFile = correctFileData(file);
		return tempFile;
	}
	
	private static File correctFileData(File file) throws IOException{
		String path = file.getAbsolutePath();
		String fileName = file.getName();
		String encryptFileName = fileName.substring(0, fileName.lastIndexOf("."))+  "_correctFile_temp_"
				+ fileName.substring(fileName.lastIndexOf("."), fileName.length());
		File tempFile = new File(
				path.substring(0, path.lastIndexOf(File.separator)) + File.separator + encryptFileName);
		if (tempFile.exists()) {
			tempFile.delete();
		}
		int numOfEncAndDec = 0x99;
		tempFile.createNewFile();
		InputStream fileIn = new FileInputStream(file);
		OutputStream fileOut = new FileOutputStream(tempFile);
		try {
			int b = -1;
			while((b = fileIn.read()) != -1){
				fileOut.write(b^numOfEncAndDec);
			}
			fileOut.flush();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			fileIn.close();
			fileOut.close();
		}
		return tempFile;
	}
	public static void main(String[] args) throws Exception {
		/*
		 * String path = "G:\\电影\\[电影天堂www.dy2018.com]奇门遁甲HD高清中英双字.mp4"; File f = new
		 * File(path); System.out.println(BigFileUploadUtil.getFileMd5(f));
		 */
		/*
		 * System.out.println("================1=========================");
		 * System.out.println(System.currentTimeMillis()); String path =
		 * "D:\\uploadImg\\uploadFiles\\2018-06-20\\公开★信息安全检测知识服务平台软件需求规格说明书V1.1.docx";
		 * File f = new File(path); BigFileUploadUtil.encryptFile(f);
		 * System.out.println(System.currentTimeMillis());
		 * System.out.println("================2=========================");
		 */
		/*System.out.println("================1=========================");
		System.out.println(System.currentTimeMillis());
		String path = "C:\\Users\\Luhj\\Desktop\\synchronization_1529562096652.zip";
		File f = new File(path);
		BigFileUploadUtil.decryptFile(f);
		System.out.println(System.currentTimeMillis());
		System.out.println("================2=========================");*/
		String path = "C:\\Users\\Luhj\\Desktop\\2.docx";
		File f = new File(path);
		System.out.println("开始转换："+System.currentTimeMillis());
		BigFileUploadUtil.confuseFile(f);
		System.out.println("开始转换："+System.currentTimeMillis());
		BigFileUploadUtil.correctFile(f);
	//	System.out.println("开始转换："+System.currentTimeMillis());
		/*int num = 2035;
		int res = 1 ^ num;
		System.out.println(res);
		System.out.println(res ^ num);*/
	}
}
