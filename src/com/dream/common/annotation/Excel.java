package com.dream.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Documented
public @interface Excel {

	String desc() default "";//对字段进行描述
	
	boolean isExport() default true;//是否导出当前字段数据
	
	boolean isModel() default true;//生成模板时当前字段是否时模板
	
	String type() default ObjType.NORMAL;//外键类型 
	
	String path() default "";//处理外键是文件时
	
	String val() default "";//外键非文件需要处理的属性逗号分开 与desc对应匹配
	
}
