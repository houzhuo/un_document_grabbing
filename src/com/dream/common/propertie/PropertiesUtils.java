package com.dream.common.propertie;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@SuppressWarnings("all")
public class PropertiesUtils {

	private static Map<String, Properties> propMap = new HashMap<>();
	
	static{
		initProperties();
	}
	
	public static Properties getProperties(String propFileName){
		return propMap.get(propFileName);
	}
	
	public static String getValueByKey(Properties prop,String key){
		return prop.getProperty(key);
	}
	
	private static void initProperties(){
		String rootPath = PropertiesUtils.class.getResource("/").getPath();
		rootPath = rootPath.substring(0, rootPath.indexOf("WEB-INF"));//从路径字符串中取出工程路径
		String path = rootPath+ File.separator + "config";
		File f = new File(path);
		if(!f.exists()){
			return;
		}
		File[] fs = f.listFiles();
		if(fs == null || fs.length <= 0){
			return;
		}
		Properties properties = null;
		InputStream in = null;
		FileInputStream fileInputStream = null;
		for(File e:fs){
			if(e.getName().endsWith(".properties") && !e.isDirectory()){
				try{
					properties = new Properties();
					fileInputStream = new FileInputStream(e.getAbsolutePath());
					in = new BufferedInputStream(fileInputStream);
					properties.load(in);
					propMap.put(e.getName(), properties);
				}catch (Exception ex) {
					// TODO: handle exception
					ex.printStackTrace();
				}finally {
					try{
						if(in != null){
							in.close();
						}
						if(fileInputStream != null){
							fileInputStream.close();
						}
					}catch (Exception eee) {
						// TODO: handle exception
						eee.printStackTrace();
					}
					
				}
				
			}
		}
	}
	
}
