package com.dream.common.export;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.tools.ant.types.resources.selectors.Compare;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.FloatType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.ShortType;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;

import com.dream.common.annotation.Excel;
import com.dream.common.annotation.ObjType;
import com.dream.common.upload.BigFileUploadUtil;
import com.dream.model.File_Entity;
import com.hxtt.b.cl;
import com.hxtt.b.m;
import com.hxtt.c.o;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 数据导出为Excel
 * 
 * 导入excel文件model 的field数据
 * 类型：String,int,Integer,double,Double,Float,float,short,Short,char,char[]
 * 
 */
@SuppressWarnings("all")
public class ExportExcel {

	/**
	 * 
	 * TODO 导出数据和文件
	 * 
	 * @param titles
	 *            列标题
	 * @param fields
	 *            列字段
	 * @param datas
	 *            列数据
	 * @param files
	 *            文件
	 * @param response
	 */
	public static void exportFile(List<String> titles, List<String> fields, List<?> datas, List<File_Entity> files,
			HttpServletResponse response) throws IOException {
		ZipOutputStream zipops = null;
		try {
			String uuid = UUID.randomUUID().toString().replaceAll("-", "");
			response.reset(); // 清除缓存
			// 设置请求头
			response.setHeader("Content-disposition",
					"attachment; filename=" + URLEncoder.encode(uuid + ".zip", "UTF-8"));
			response.setContentType("application/octet-stream; charset=UTF-8");// 设置下载文件的类型
			OutputStream ops = response.getOutputStream();
			zipops = new ZipOutputStream(ops);// zip输出流
			BufferedOutputStream bos = new BufferedOutputStream(zipops);

			byte[] buffer = new byte[1024];

			// ===导出Excel文件===
			// 创建Excel文档对象
			HSSFWorkbook hwb = new HSSFWorkbook();
			// 创建Excel字体对象
			HSSFFont fontStyle = hwb.createFont();
			// 创建样式
			HSSFCellStyle cellStyle = hwb.createCellStyle();
			// 粗体
			// fontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			// 创建sheet
			HSSFSheet sheet = hwb.createSheet("sheet1");

			// 创建第一行
			HSSFRow row0 = sheet.createRow(0);

			// 创建标题列
			for (int i = 0; i < titles.size(); i++) {
				Cell cell = row0.createCell(i);
				// 将字体应用于样式
				cellStyle.setFont(fontStyle);
				// 将样式应用于单元格
				cell.setCellStyle(cellStyle);
				HSSFRichTextString text = new HSSFRichTextString(titles.get(i));
				cell.setCellValue(text);
			}
			if (null != datas && datas.size() > 0) {
				// 写入数据
				for (int i = 0; i < datas.size(); i++) {
					Object objClass = datas.get(i);
					// 创建行
					HSSFRow rowi = sheet.createRow(i + 1);
					// 创建每一行的列
					for (int j = 0; j < fields.size(); j++) {
						Cell cell = rowi.createCell(j);
						Object obj = invokeGet(objClass, fields.get(j));
						String value = obj == null ? "" : obj + "";
						HSSFRichTextString text = new HSSFRichTextString(value);
						cell.setCellValue(text);
					}
				}
			}
			// 创建zip实体
			ZipEntry zipEnt = new ZipEntry(uuid + ".xls");
			zipops.putNextEntry(zipEnt);
			hwb.write(zipops);

			// =====下载文件=====

			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (zipops != null) {
				zipops.closeEntry();
				zipops.close();
			}
		}
	}

	/**
	 * 导出Excel
	 * 
	 * @param titles
	 *            列标题
	 * @param fields
	 *            列字段
	 * @param datas
	 *            列数据
	 * @param response
	 */
	public static void exportFile(List<String> titles, List<String> fields, List<?> datas, HttpServletResponse response)
			throws IOException {
		try {
			// 创建Excel文档对象
			HSSFWorkbook hwb = new HSSFWorkbook();
			// 创建Excel字体对象
			HSSFFont fontStyle = hwb.createFont();
			// 创建样式
			HSSFCellStyle cellStyle = hwb.createCellStyle();
			// 粗体
			// fontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			// 创建sheet
			HSSFSheet sheet = hwb.createSheet("sheet1");

			// 创建第一行
			HSSFRow row0 = sheet.createRow(0);

			// 创建标题列
			for (int i = 0; i < titles.size(); i++) {
				Cell cell = row0.createCell(i);
				// 将字体应用于样式
				cellStyle.setFont(fontStyle);
				// 将样式应用于单元格
				cell.setCellStyle(cellStyle);
				HSSFRichTextString text = new HSSFRichTextString(titles.get(i));
				cell.setCellValue(text);
			}
			if (null != datas && datas.size() > 0) {
				// 写入数据
				for (int i = 0; i < datas.size(); i++) {
					Object objClass = datas.get(i);
					// 创建行
					HSSFRow rowi = sheet.createRow(i + 1);
					// 创建每一行的列
					for (int j = 0; j < fields.size(); j++) {
						Cell cell = rowi.createCell(j);
						Object obj = invokeGet(objClass, fields.get(j));
						String value = obj == null ? "" : obj + "";
						HSSFRichTextString text = new HSSFRichTextString(value);
						cell.setCellValue(text);
					}
				}
			}

			String uuid = UUID.randomUUID().toString().replaceAll("-", "");
			OutputStream ops = response.getOutputStream();
			response.reset(); // 清除缓存
			response.setHeader("Content-disposition",
					"attachment; filename=" + URLEncoder.encode(uuid + ".xls", "UTF-8"));
			response.setContentType("application/octet-stream; charset=UTF-8");// 设置下载文件的类型
			hwb.write(ops);
			ops.close();

		} catch (IOException e) {
			e.printStackTrace();

		}
	}
	public static void exportFile(String[] titles,List<List<String>> datas,String basePathFile, HttpServletResponse response)
			throws IOException {
		try {
			// 创建Excel文档对象
			HSSFWorkbook hwb = new HSSFWorkbook();
			// 创建Excel字体对象
			HSSFFont fontStyle = hwb.createFont();
			// 创建样式
			HSSFCellStyle cellStyle = hwb.createCellStyle();
			// 粗体
			// fontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			// 创建sheet
			HSSFSheet sheet = hwb.createSheet("sheet1");

			// 创建第一行
			HSSFRow row0 = sheet.createRow(0);

			// 创建标题列
			for (int i = 0; i < titles.length; i++) {
				Cell cell = row0.createCell(i);
				// 将字体应用于样式
				cellStyle.setFont(fontStyle);
				// 将样式应用于单元格
				cell.setCellStyle(cellStyle);
				HSSFRichTextString text = new HSSFRichTextString(titles[i]);
				cell.setCellValue(text);
			}
			if (null != datas && datas.size() > 0) {
				// 写入数据
				for (int i = 0; i < datas.size(); i++) {
					List<String>  fieldDatas= datas.get(i);
					// 创建行
					HSSFRow rowi = sheet.createRow(i + 1);
					// 创建每一行的列
					for (int j = 0; j < fieldDatas.size(); j++) {
						Cell cell = rowi.createCell(j);
						HSSFRichTextString text = new HSSFRichTextString(fieldDatas.get(j));
						cell.setCellValue(text);
					}
				}
			}
			
			File f = new File(basePathFile);
			if(f.exists()){
				f.delete();
			}
			f.createNewFile();
			FileOutputStream out = new FileOutputStream(f);
			
			hwb.write(out);
			
			out.close();
			hwb.close();

		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	/**
	 * 执行get方法
	 * 
	 * @param o
	 * @param fieldName
	 *            字段名
	 * @return
	 */
	private static Object invokeGet(Object o, String fieldName) {
		Method method = getGetMethod(o.getClass(), fieldName);
		try {
			return method.invoke(o, new Object[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Object invokeGet(Object o, Method method) {
		try {
			return method.invoke(o, new Object[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取类的get方法
	 * 
	 * @param objectClass
	 *            类
	 * @param fieldName
	 *            字段名
	 * @return
	 */
	private static Method getGetMethod(Class objectClass, String fieldName) {
		StringBuffer sb = new StringBuffer();
		sb.append("get");
		sb.append(fieldName.substring(0, 1).toUpperCase());
		sb.append(fieldName.substring(1));
		try {
			return objectClass.getMethod(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Method getSetMethod(Class<?> claz, String fieldName) {
		StringBuffer sb = new StringBuffer();
		sb.append("set");
		sb.append(fieldName.substring(0, 1).toUpperCase());
		sb.append(fieldName.substring(1));
		try {

			return claz.getMethod(sb.toString(), new Class<?>[] { String.class });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void exportFile(List<?> datas, Class<?> claz, HttpServletResponse response,String fileName) {
		try {
			List<exportSort> sortField = ExportExcel.getExportClassSortFiled(claz);

			// 创建Excel文档对象
			HSSFWorkbook hwb = new HSSFWorkbook();
			// 创建Excel字体对象
			HSSFFont fontStyle = hwb.createFont();
			// 创建样式
			HSSFCellStyle cellStyle = hwb.createCellStyle();
			// 粗体
			// fontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			// 创建sheet
			HSSFSheet sheet = hwb.createSheet("sheet1");

			// 创建第一行
			HSSFRow row0 = sheet.createRow(0);

			// 创建标题列
			for (int i = 0; i < sortField.size(); i++) {
				Cell cell = row0.createCell(i);
				// 将字体应用于样式
				cellStyle.setFont(fontStyle);
				// 将样式应用于单元格
				cell.setCellStyle(cellStyle);
				HSSFRichTextString text = new HSSFRichTextString(sortField.get(i).getDesc());
				cell.setCellValue(text);
			}
			if (null != datas && datas.size() > 0) {
				// 写入数据
				for (int i = 0; i < datas.size(); i++) {
					Object objClass = datas.get(i);
					// 创建行
					HSSFRow rowi = sheet.createRow(i + 1);
					// 创建每一行的列
					for (int j = 0; j < sortField.size(); j++) {
						Cell cell = rowi.createCell(j);
						String value = "";
						exportSort sort = sortField.get(j);
						String type = sort.getType();
						if (ObjType.NORMAL.equals(type)) {
							Object obj = invokeGet(objClass, sort.getMethod());
							value = obj == null ? "" : obj + "";
						} else if (ObjType.OUT_KEY.equals(type)) {
							value = exportFileDealOutKey(sort, objClass);
						}

						HSSFRichTextString text = new HSSFRichTextString(value);
						cell.setCellValue(text);

					}
				}
			}

			String uuid = UUID.randomUUID().toString().replaceAll("-", "");
			OutputStream ops = response.getOutputStream();
			response.reset(); // 清除缓存
			response.setHeader("Content-disposition",
					"attachment; filename=" + URLEncoder.encode(fileName + ".xls", "UTF-8"));
			response.setContentType("application/octet-stream; charset=UTF-8");// 设置下载文件的类型
			hwb.write(ops);
			ops.close();

		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	public static void exportFile(JSONArray data, String fileName, HttpServletResponse response) {
		try {

			// 创建Excel文档对象
			HSSFWorkbook hwb = new HSSFWorkbook();
			// 创建Excel字体对象
			HSSFFont fontStyle = hwb.createFont();
			// 创建样式
			HSSFCellStyle cellStyle = hwb.createCellStyle();
			// 粗体
			// fontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			// 创建sheet
			HSSFSheet sheet = hwb.createSheet("sheet1");

			// 创建标题列

			for (int i = 0; i < data.size(); i++) {
				JSONObject json = data.getJSONObject(i);
				String cellName = json.getString("name");
				String firsRow = json.getString("firsRow");
				String lastRow = json.getString("firsRow");
				String fisrtCol = json.getString("fisrtCol");
				String lasrtCol = json.getString("fisrtCol");

				if (!StringUtils.isEmpty(firsRow) && !StringUtils.isEmpty(lastRow) && !StringUtils.isEmpty(fisrtCol)
						&& !StringUtils.isEmpty(lasrtCol)) {
					sheet.addMergedRegion(new CellRangeAddress(Integer.valueOf(firsRow), Integer.valueOf(lastRow),
							Integer.valueOf(fisrtCol), Integer.valueOf(lasrtCol)));
				}

				HSSFRow row0 = sheet.createRow(i);
				Cell cell = row0.createCell(i);
				// 将字体应用于样式
				cellStyle.setFont(fontStyle);
				// 将样式应用于单元格
				cell.setCellStyle(cellStyle);
				HSSFRichTextString text = new HSSFRichTextString(cellName);
				cell.setCellValue(text);

			}

			OutputStream ops = response.getOutputStream();
			response.reset(); // 清除缓存
			response.setHeader("Content-disposition",
					"attachment; filename=" + URLEncoder.encode(fileName + ".xls", "UTF-8"));
			response.setContentType("application/octet-stream; charset=UTF-8");// 设置下载文件的类型
			hwb.write(ops);
			ops.close();

		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	/***
	 * 该方法会导出附件信息<br />
	 * 打包下载附件和对应的excel文件
	 */
	public static void exportFileAndAccessory(List<?> datas, Class<?> claz, HttpServletResponse response)
			throws IOException {
		ZipOutputStream zipops = null;
		try {
			List<String[]> fileList = new ArrayList<>();
			List<exportSort> sortField = ExportExcel.getExportClassSortFiled(claz);
			String uuid = UUID.randomUUID().toString().replaceAll("-", "");
			response.reset(); // 清除缓存
			// 设置请求头
			response.setHeader("Content-disposition",
					"attachment; filename=" + URLEncoder.encode(uuid + ".zip", "UTF-8"));
			response.setContentType("application/octet-stream; charset=UTF-8");// 设置下载文件的类型
			OutputStream ops = response.getOutputStream();
			zipops = new ZipOutputStream(ops);// zip输出流

			byte[] buffer = new byte[1024];

			// ===导出Excel文件===
			// 创建Excel文档对象
			HSSFWorkbook hwb = new HSSFWorkbook();
			// 创建Excel字体对象
			HSSFFont fontStyle = hwb.createFont();
			// 创建样式
			HSSFCellStyle cellStyle = hwb.createCellStyle();
			// 粗体
			// fontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			// 创建sheet
			HSSFSheet sheet = hwb.createSheet("sheet1");

			// 创建第一行
			HSSFRow row0 = sheet.createRow(0);
			// 创建标题列
			for (int i = 0; i < sortField.size(); i++) {
				Cell cell = row0.createCell(i);
				// 将字体应用于样式
				cellStyle.setFont(fontStyle);
				// 将样式应用于单元格
				cell.setCellStyle(cellStyle);
				HSSFRichTextString text = new HSSFRichTextString(sortField.get(i).getDesc());
				cell.setCellValue(text);
			}
			if (null != datas && datas.size() > 0) {
				// 写入数据
				for (int i = 0; i < datas.size(); i++) {
					Object objClass = datas.get(i);
					// 创建行
					HSSFRow rowi = sheet.createRow(i + 1);

					// 创建每一行的列
					for (int j = 0; j < sortField.size(); j++) {
						Cell cell = rowi.createCell(j);
						String value = "";
						exportSort sort = sortField.get(j);
						String type = sort.getType();
						if (ObjType.NORMAL.equals(type)) {
							Object obj = invokeGet(objClass, sort.getMethod());
							value = obj == null ? "" : obj + "";
						} else if (ObjType.FILE.equals(type)) {
							value = exportFileDealAccessory(sort, objClass, fileList);
						} else if (ObjType.OUT_KEY.equals(type)) {
							value = exportFileDealOutKey(sort, objClass);
						}

						HSSFRichTextString text = new HSSFRichTextString(value);
						cell.setCellValue(text);

					}
				}
			}
			// 创建zip实体
			ZipEntry zipEnt = new ZipEntry(uuid + ".xls");
			zipops.putNextEntry(zipEnt);
			hwb.write(zipops);

			// =====下载文件=====

			for (String[] obj : fileList) {
				String filePath = BigFileUploadUtil.getBasePath() + obj[1];
				File f = new File(filePath);
				if (!f.exists()) {
					continue;
				}
				FileInputStream fis = new FileInputStream(f);
				// 创建ZIP实体，并添加进压缩包
				ZipEntry zipEntry = new ZipEntry(obj[0]);
				zipops.putNextEntry(zipEntry);
				int len = 0;
				while ((len = fis.read(buffer)) != -1) {
					zipops.write(buffer, 0, len);
				}
				zipops.flush();
				zipops.closeEntry();
				fis.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (zipops != null) {
				zipops.closeEntry();
				zipops.close();
			}
		}
	}

	public static String exportFileDealOutKey(exportSort sort, Object objClass) {
		Object obj = invokeGet(objClass, sort.getMethod());

		if (obj == null) {
			return "";
		}

		List<String> vals = new ArrayList<>();
		String val = sort.getVal();
		if (val.contains(",")) {
			vals = Arrays.asList(val.split(","));
		} else {
			vals.add(val);
		}
		for (int i = 0; i < vals.size(); i++) {
			obj = invokeGet(obj, vals.get(i));
		}
		return obj + "";
	}

	private static String dealOutKey(exportSort sort, Object resObj, List<String> vals) {
		String result = "";
		List<String> columns;
		for (int i = 0; i < vals.size(); i++) {
			String val = vals.get(i);
			columns = new ArrayList<>();
			Object res = resObj;
			if (val.contains(".")) {
				columns = Arrays.asList(val.split("."));
			} else {
				columns.add(val);
			}
			for (String colunm : columns) {
				res = invokeGet(res, colunm);
			}
			result += res + " ";
		}
		return result;
	}

	private static String exportFileDealAccessory(exportSort sort, Object objClass, List<String[]> fileList) {
		String result = "";
		Object obj = invokeGet(objClass, sort.getMethod());

		if (obj == null) {
			return "";
		}

		List<String> vals = new ArrayList<>();

		String path = sort.getPath();
		String val = sort.getVal();

		if (val.contains(",")) {
			vals = Arrays.asList(val.split(","));
		} else {
			vals.add(val);
		}

		if (obj instanceof List || obj instanceof Set) {
			List list;
			if (obj instanceof Set) {
				list = new ArrayList<>((Set) obj);
			} else {
				list = new ArrayList<>((List) obj);
			}
			for (Object resObj : list) {
				result += dealAccessory(vals, resObj, fileList, path);
			}
		} else {
			result = dealAccessory(vals, obj, fileList, path);
		}
		return result;
	}

	private static String dealAccessory(List<String> vals, Object resObj, List<String[]> fileList, String path) {
		String result = "";
		String[] arr = new String[2];
		String fileName = "";
		for (int i = 0; i < vals.size(); i++) {
			Object name = invokeGet(resObj, vals.get(i));
			fileName += name + "_";
		}
		if (fileName.contains("_")) {
			fileName = fileName.substring(0, fileName.lastIndexOf("_"));
		}
		Object res = invokeGet(resObj, path);
		arr[0] = fileName;
		arr[1] = res == null ? "" : res + "";
		fileList.add(arr);
		result = fileName + "\n";
		return result;
	}

	public static void createExcelModel(Class<?> claz, String fileName, HttpServletResponse response) {
		try {
			List<String> fields = ExportExcel.getModelExcelClassFieldDesc(claz);

			// 创建Excel文档对象
			HSSFWorkbook hwb = new HSSFWorkbook();
			// 创建Excel字体对象
			HSSFFont fontStyle = hwb.createFont();
			// 创建样式
			HSSFCellStyle cellStyle = hwb.createCellStyle();
			// 粗体
			// fontStyle.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			// 创建sheet
			HSSFSheet sheet = hwb.createSheet("sheet1");

			// 创建第一行
			HSSFRow row0 = sheet.createRow(0);

			// 创建标题列
			for (int i = 0; i < fields.size(); i++) {
				Cell cell = row0.createCell(i);
				// 将字体应用于样式
				cellStyle.setFont(fontStyle);
				// 将样式应用于单元格
				cell.setCellStyle(cellStyle);
				HSSFRichTextString text = new HSSFRichTextString(fields.get(i));
				cell.setCellValue(text);
			}

			String uuid = UUID.randomUUID().toString().replaceAll("-", "");
			OutputStream ops = response.getOutputStream();
			response.reset(); // 清除缓存
			response.setHeader("Content-disposition",
					"attachment; filename=" + URLEncoder.encode(fileName + ".xls", "UTF-8"));
			response.setContentType("application/octet-stream; charset=UTF-8");// 设置下载文件的类型
			hwb.write(ops);
			ops.close();

		} catch (IOException e) {
			e.printStackTrace();

		}
	}

	public static List<?> parseExcel(File f, Class<?> claz)
			throws EncryptedDocumentException, InvalidFormatException, IOException, ClassNotFoundException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException, InstantiationException {
		Workbook workbook = WorkbookFactory.create(f);
		List<importExcel> methods = null;
		List<Object> result = new ArrayList<>();
		boolean mark = true;
		for (Sheet sheet : workbook) {
			if (!mark) {
				break;
			}
			for (int j = 0; j < sheet.getLastRowNum() + 1; j++) {
				Row row = sheet.getRow(j);
				if (j == 0) {
					methods = sortExcelSheetTitle(row, claz);
					if (methods == null) {
						mark = false;
						break;
					}
					continue;
				}
				Object ob = claz.newInstance();

				for (int i = 0; i < row.getLastCellNum(); i++) {
					String cellStr = getCellStr(row.getCell(i));
					invokeObject(cellStr, methods.get(i), ob);
				}
				result.add(ob);
			}
		}
		workbook.close();
		if (!mark) {
			return null;
		}
		return result;
	}

	public static List<?> parseCsv(File f, Class<?> claz) {
		List<Object> result = new ArrayList<>();

		BufferedReader bufferedReader = null;
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(f);
			bufferedReader = new BufferedReader(fileReader);
			List<importExcel> methods = getCSVObject(claz, bufferedReader.readLine());
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				String item[] = line.split(",");// CSV格式文件为逗号分隔符文件，这里根据逗号切分
				Object ob = claz.newInstance();
				for (int i = 0;i < item.length;i++) {
					invokeObject(item[i], methods.get(i), ob);
				}
				result.add(ob);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return result;
	}

	private static List<importExcel> getCSVObject(Class<?> claz, String titles) {
		List<importExcel> list = ExportExcel.getImportClassField(claz);
		List<String> titleList = Arrays.asList(titles.split(","));
		List<importExcel> result = new ArrayList<>();
		for (String title : titleList) {
			for (importExcel importCsv : list) {
				if (title.trim().toLowerCase().equals(importCsv.getDesc().trim().toLowerCase())) {
					result.add(importCsv);
				}
			}
		}
		return result;
	}

	private static void invokeObject(String cellStr, importExcel importExcel, Object ob)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Class<?>[] claz = importExcel.getClaz();
		List<Object> params = new ArrayList<>();

		for (Class<?> cl : claz) {
			paramType(cl.getName(), cellStr, params);
		}

		Method method = importExcel.getMethod();
		method.invoke(ob, params.toArray());

	}

	private static void paramType(String claz, String cellStr, List<Object> params) {
		if (claz.equals(String.class.getName())) {
			params.add(cellStr);
		} else if (claz.equals(Integer.class.getName())) {
			params.add(Integer.valueOf(cellStr));
		} else if (claz.equals(int.class.getName())) {
			params.add((int) Integer.parseInt(cellStr));
		} else if (claz.equals(Float.class.getName())) {
			params.add(Float.valueOf(cellStr));
		} else if (claz.equals(float.class.getName())) {
			params.add((float) Float.parseFloat(cellStr));
		} else if (claz.equals(Double.class.getName())) {
			params.add(Double.valueOf(cellStr));
		} else if (claz.equals(double.class.getName())) {
			params.add((double) Double.parseDouble(cellStr));
		} else if (claz.equals(long.class.getName())) {
			params.add((long) Long.parseLong(cellStr));
		} else if (claz.equals(Long.class.getName())) {
			params.add(Long.valueOf(cellStr));
		} else if (claz.equals(char.class.getName())) {
			params.add(cellStr.toCharArray()[0]);
		} else if (claz.equals(char[].class.getName())) {
			params.add(cellStr.toCharArray());
		} else if (claz.equals(Short.class.getName())) {
			params.add(Short.valueOf(cellStr));
		} else if (claz.equals(short.class.getName())) {
			params.add((short) Short.parseShort(cellStr));
		}
	}

	private static String getSetMethodName(String fieldName) {
		StringBuffer sb = new StringBuffer();
		sb.append("set");
		char[] charr = fieldName.toCharArray();
		if (charr.length <= 1) {
			return fieldName.toUpperCase();
		}
		if (Character.isLowerCase(charr[1])) {
			sb.append(fieldName.substring(0, 1).toUpperCase());
			sb.append(fieldName.substring(1));
		} else {
			sb.append(fieldName.substring(0, 1));
			sb.append(fieldName.substring(1));
		}
		return sb.toString();
	}

	private static List<importExcel> sortExcelSheetTitle(Row row, Class<?> claz) {
		List<importExcel> list = ExportExcel.getImportClassField(claz);
		List<importExcel> result = new ArrayList<>();

		if (row == null || list == null || list.size() <= 0) {
			return null;
		}

		String excelTitles = "";
		String modelTitles = "";

		for (int i = 0; i < row.getLastCellNum(); i++) {
			String cellStr = getCellStr(row.getCell(i));
			excelTitles += cellStr;
			for (importExcel model : list) {
				if (i == 0) {
					modelTitles += model.getDesc();
				}
				if (model.getDesc().equals(cellStr)) {
					result.add(model);
				}
			}
		}

		if ("".equals(modelTitles) || "".equals(excelTitles) || !excelTitles.equals(modelTitles)) {
			return null;
		}

		return result;
	}

	private static String getCellStr(Cell cell) {
		String result = "";
		DataFormatter dataFormatter = new DataFormatter();
		result = dataFormatter.formatCellValue(cell);
		return result;
	}

	private static List<importExcel> getImportClassField(Class<?> claz) {
		List<Field> list = new ArrayList<>();

		list.addAll(Arrays.asList(claz.getDeclaredFields()));
		list.addAll(Arrays.asList(claz.getFields()));
		List<importExcel> sortList = new ArrayList<>();
		Map<String, importExcel> map = getClassSetMethod(claz);

		for (Field field : list) {
			Excel exportFile = field.getAnnotation(Excel.class);
			if (exportFile != null && exportFile.isModel()) {
				String methodName = getSetMethodName(field.getName());
				if (map.keySet().contains(methodName)) {
					importExcel importExcel = map.get(methodName);
					importExcel.setDesc(exportFile.desc());
					sortList.add(importExcel);
				}
			}
		}
		return sortList;
	}

	private static List<exportSort> getExportClassSortFiled(Class<?> claz) {

		List<Field> list = new ArrayList<>();
		exportSort exportSort = null;
		List<String> vaList = null;
		List<String> desList = null;
		String val = "";
		String desc = "";
		list.addAll(Arrays.asList(claz.getDeclaredFields()));
		list.addAll(Arrays.asList(claz.getFields()));
		List<exportSort> sortList = new ArrayList<>();

		for (Field field : list) {
			Excel exportFile = field.getAnnotation(Excel.class);
			if (exportFile != null && exportFile.isExport()) {
				if (ObjType.OUT_KEY.equals(exportFile.type())) {
					vaList = new ArrayList<>();
					desList = new ArrayList<>();
					val = exportFile.val();
					if (val.contains(",")) {
						vaList = Arrays.asList(val.split(","));
					} else {
						vaList.add(val);
					}
					desc = exportFile.desc();
					if (desc.contains(",")) {
						desList = Arrays.asList(desc.split(","));
					} else {
						desList.add(desc);
					}
					for (int j = 0; j < vaList.size(); j++) {
						exportSort = new exportSort();
						exportSort.setDesc(desList.get(j));
						exportSort.setMethod(getGetMethod(claz, field.getName()));
						exportSort.setType(exportFile.type());
						exportSort.setVal(vaList.get(j));
						sortList.add(exportSort);
					}
				} else {
					exportSort = new exportSort();
					exportSort.setDesc(exportFile.desc());
					exportSort.setMethod(getGetMethod(claz, field.getName()));
					exportSort.setPath(exportFile.path());
					exportSort.setType(exportFile.type());
					exportSort.setVal(exportFile.val());
					sortList.add(exportSort);
				}
			}
		}
		return sortList;

	}

	private static List<String> getModelExcelClassFieldDesc(Class<?> claz) {

		List<Field> list = new ArrayList<>();

		list.addAll(Arrays.asList(claz.getDeclaredFields()));
		list.addAll(Arrays.asList(claz.getFields()));
		List<String> result = new ArrayList<>();

		for (Field field : list) {
			Excel exportFile = field.getAnnotation(Excel.class);
			if (exportFile != null && exportFile.isModel()) {
				result.add(exportFile.desc());
			}
		}
		return result;

	}

	/*
	 * private static List<exportSort> sortExport(List<exportSort> sortList,
	 * Class<?> claz) { ExportFileSort fileSort =
	 * claz.getAnnotation(ExportFileSort.class); boolean sort = true; if
	 * (fileSort != null) { sort = fileSort.sort(); } exportSort[] es =
	 * sortList.toArray(new exportSort[0]);
	 * 
	 * if (sort) { Arrays.sort(es, new Comparator<exportSort>() {
	 * 
	 * @Override public int compare(exportSort o1, exportSort o2) { // TODO
	 * Auto-generated method stub if (o1.getSort() > o2.getSort()) { return 1; }
	 * else if (o1.getSort() == o2.getSort()) { return 0; } return -1; }
	 * 
	 * }); } else { Arrays.sort(es, new Comparator<exportSort>() {
	 * 
	 * @Override public int compare(exportSort o1, exportSort o2) { // TODO
	 * Auto-generated method stub if (o1.getSort() > o2.getSort()) { return -1;
	 * } else if (o1.getSort() == o2.getSort()) { return 0; } return 1; }
	 * 
	 * }); }
	 * 
	 * return Arrays.asList(es); }
	 */
	/**
	 * @param claz
	 */
	private static Map<String, importExcel> getClassSetMethod(Class<?> claz) {
		Method[] ms = claz.getMethods();
		Map<String, importExcel> map = new HashMap();
		importExcel importExcel = null;
		for (Method method : ms) {
			importExcel = new importExcel();
			importExcel.setMethod(method);
			importExcel.setClaz(method.getParameterTypes());
			map.put(method.getName(), importExcel);
		}
		return map;
	}

	public static void main(String[] args) {

	}
}

class importExcel {
	private Method method;

	private String desc;

	private Class<?>[] claz;

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Class<?>[] getClaz() {
		return claz;
	}

	public void setClaz(Class<?>[] claz) {
		this.claz = claz;
	}

}

class exportSort {
	private Method method;

	private String desc;

	private int sort;

	private String type;

	private String path;

	private String val;

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

	public static void main(String[] args) throws IOException {
		String zipFile = "C:\\uploadImg\\uploadFiles\\2018-09-15\\a3731d6f240645e7982bcd90127657de.zip";
		String sourceFile = "C:\\uploadImg\\uploadFiles\\2018-09-15\\a3731d6f240645e7982bcd90127657de.txt";
		File zipFiles = new File(zipFile);
		if (zipFiles.exists()) {
			zipFiles.delete();
		}
		ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(zipFiles));
		zipOut.putNextEntry(new ZipEntry("a3731d6f240645e7982bcd90127657de.txt"));
		FileInputStream fileIn = new FileInputStream(new File(sourceFile));
		int len;
		byte[] b = new byte[1024];
		while ((len = fileIn.read(b)) > 0) {
			System.out.println("-------------------------------");
			System.out.println(len);
			zipOut.write(b, 0, len);
		}
		zipOut.closeEntry();
		zipOut.close();
		fileIn.close();
	}

}
