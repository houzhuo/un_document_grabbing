package com.dream.common.init;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import com.dream.file.PullMain;
import com.dream.model.Reptile_Record;
import com.dream.model.System_Permission;
import com.dream.system.service.FileService;
import com.dream.system.service.PermissionService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@SuppressWarnings("all")
@Service
public class StartUpListener implements ApplicationListener<ContextRefreshedEvent>{

	@Autowired
	private PermissionService permissionServiceImpl;
	
	@Autowired
	private FileService fileService;
	@Override
	public void onApplicationEvent(ContextRefreshedEvent evt) {
		if (evt.getApplicationContext().getParent() == null) {
			try {
				System.out.println("====================获取文档docId========================");
				fileService.getFileDocIds();
				System.out.println("================初始化数据库菜单表====================");
			    ApplicationContext applicationContext = evt.getApplicationContext();  
		        WebApplicationContext webApplicationContext = (WebApplicationContext)applicationContext;  
		        ServletContext servletContext = webApplicationContext.getServletContext();  
				String basicPath = servletContext.getRealPath("/");
		        JSONArray ja = getMenu(basicPath + "config" + File.separator + "menu.xml");
				List<System_Permission> list = JSONArray.toList(ja, System_Permission.class);
				permissionServiceImpl.initMenuData(list);
			//	DataInit.tabelClass = getTableClass();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private static JSONArray getMenu(String path) throws Exception {
		File f = new File(path);
		SAXReader reader = new SAXReader();

		JSONArray json = new JSONArray();

		Document doc = reader.read(f);
		Element root = doc.getRootElement();
		for (Iterator i = root.elementIterator("menu"); i.hasNext();) {
			
			Element foo = (Element) i.next();
			JSONObject js = new JSONObject();
			js.put("cid", foo.elementText("id"));
			js.put("permissionName", foo.elementText("name"));
			js.put("url", foo.elementText("url"));
			js.put("icon", foo.elementText("icon"));
			js.put("type", "menu");
			js.put("orderNum", foo.elementText("order") == null ? "" : foo.elementText("order"));
			String pid = foo.elementText("pId");
			js.put("pid", pid);
			js.put("sort", foo.elementText("sort") == null ? 0 : foo.elementText("sort"));
			json.add(js);
			
			Element btn = foo.element("buttons");
			if(btn != null){

				for(Iterator bi = btn.elementIterator("button");bi.hasNext();){
					Element bfoo = (Element) bi.next();
					JSONObject j = new JSONObject();
					j.put("cid", bfoo.elementText("id"));
					j.put("permissionName", bfoo.elementText("name"));
					j.put("pid", foo.elementText("id"));
					j.put("type", "button");
					json.add(j);
				}
			}
			
		}
		return json;
	}

}
