package com.dream.common.init;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.persistence.Table;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.index.IndexWriter;
import org.apache.tomcat.util.security.MD5Encoder;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.dream.common.init.DataInit;
import com.dream.common.propertie.PropertiesUtils;
import com.dream.common.upload.BigFileUploadUtil;
import com.dream.file.HandOutFileDownLoad;
import com.dream.file.InitBrower;
import com.dream.file.InsertFileInfoThread;
import com.dream.model.System_User;
import com.dream.system.service.FileService;
import com.hxtt.a.c;
  

@Component("springInit")
@SuppressWarnings("all")
public class SpringInit extends HttpServlet {

	private FileService fileService;
	
	private static long serialVersionUID = 1L;
	public static String ImageUrl = "http://127.0.0.1:80/dreamOA/";
	public static String IP = "";
	public static String DK = ":80/dreamOA/";

	public void init(ServletConfig config) throws ServletException {
		super.init(config);

		ServletContext servletContext = config.getServletContext();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		AutowireCapableBeanFactory autowireCapableBeanFactory = webApplicationContext.getAutowireCapableBeanFactory();
		DataInit.WEB_APP_REAL_PATH = config.getServletContext().getRealPath(File.separator);
		ServletContext application = getServletContext();
		DataInit.SYSTEM_CONTENT_PATH = config.getServletContext().getContextPath();
		
		fileService = autowireCapableBeanFactory.getBean(FileService.class);
		//开启分发线程
		new HandOutFileDownLoad().start();
		//开启数据库操作线程
		new InsertFileInfoThread(fileService).start();
		//初始化浏览器核心
		InitBrower.init();
		InitBrower.setLoadListener();
		try {
			//getLucencePath(DataInit.WEB_APP_REAL_PATH + File.separator + "config" + File.separator + "setting.xml");
			/*getLucencePath("setting.properties");*/
			System.out.println("上传文件路径为:" + DataInit.UPLOADURL);
			/*initLuceneFilterTables(DataInit.WEB_APP_REAL_PATH + File.separator + "config" + File.separator + "luceneFilterTables.txt");
			System.out.println("加载索引过滤 数据库表单: "+DataInit.luceneFilterTables);
		    initLuceneFileTables(DataInit.WEB_APP_REAL_PATH + File.separator + "config" + File.separator + "luceneFileTables.txt");
			System.out.println("加载索引要处理文件数据库表单: "+DataInit.luceneFileTables);
			*/
		
			System.out.println("----------------------初始化创建索引基础文件------------------------------");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("------------加载上传配置文件失败!-------------------");
		}
		application.setAttribute("imageUrl", ImageUrl);

		System.out.println("------------------加载配置全局类型菜单成功------------------ ");
		JSONArray json;
		try {
			//DataInit.listAdmin = getListTerm(DataInit.WEB_APP_REAL_PATH + File.separator + "config" + File.separator + "setting.xml");
			DataInit.listAdmin = getListTerm("setting.properties");
			System.out.println("超级管理员路径 为:" + DataInit.UPLOADURL);
			DataInit.menuJSONArray = getMenu(DataInit.WEB_APP_REAL_PATH + File.separator + "config" + File.separator + "menu.xml");
			System.out.println("加载全局菜单: " + DataInit.WEB_APP_REAL_PATH + File.separator + "config" + File.separator + "menu.xml");
			iniUploadFileParams();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * 加载上传的路径
	 * 
	 * @param path
	 * @throws DocumentException
	 */
	public static void getListUploadUrl(String path) throws DocumentException {
		File f = new File(path);
		SAXReader reader = new SAXReader();
		Document doc = reader.read(f);
		Element root = doc.getRootElement();
		String str = System.getProperties().getProperty("os.name");
		boolean b = StringUtils.containsIgnoreCase(str, "windows");

		for (Iterator i = root.elementIterator("system"); i.hasNext();) {
			Element foo = (Element) i.next();

			if (b) {
				DataInit.UPLOADURL = foo.elementText("windows");
				// SpringInit.ImageUrl = "http://127.0.0.1:8080/dreamOA/";
			} else {
				DataInit.UPLOADURL = foo.elementText("linux");
				// SpringInit.ImageUrl = "http://125.65.42.126:8080/dreamOA/";
			}
		}
	}
	
	/**
	 * 加载lucence路径
	 * 
	 * @param path
	 * @throws DocumentException
	 */
	/*public static void getLucencePath(String path) throws DocumentException {
		File f = new File(path);
		SAXReader reader = new SAXReader();
		Document doc = reader.read(f); 
		Element root = doc.getRootElement();
		String str = System.getProperties().getProperty("os.name");
		boolean b = StringUtils.containsIgnoreCase(str, "windows");

		for (Iterator i = root.elementIterator("system"); i.hasNext();) {
			Element foo = (Element) i.next();

			if (b) {
				LucenceSystemContants.LUCENCE_INDEX_PATH = foo.elementText("windows");
			} else {
				LucenceSystemContants.LUCENCE_INDEX_PATH = foo.elementText("linux");
			}
		}
		File fPath = new File(LucenceSystemContants.LUCENCE_INDEX_PATH);
		if(!fPath.exists()) {
			fPath.mkdirs();
		}
	}*/
	
	
	
	/**
	 * 加载lucence路径
	 * 
	 * @param path
	 * @throws DocumentException
	 */
	public static String getProjectVersion(String path) throws DocumentException {
		File f = new File(path);
		SAXReader reader = new SAXReader();
		Document doc = reader.read(f);
		Element root = doc.getRootElement();
		String projectVersion = null;
		for (Iterator i = root.elementIterator("project"); i.hasNext();) {
			Element foo = (Element) i.next();
			projectVersion = foo.elementText("version");
			break;
		}
		if("".equals(projectVersion) || projectVersion == null) {
			projectVersion = "0";
		}
		return projectVersion;
	}
	/**
	 * 加载语言定义
	 * 
	 * @param path
	 * @throws DocumentException
	 */
	public static JSONArray getLanguage(String path) throws DocumentException {
		JSONArray json = new JSONArray();
		File f = new File(path);
		SAXReader reader = new SAXReader();
		Document doc;
		doc = reader.read(f);
		Element root = doc.getRootElement();
		root.getDocument();
		for (@SuppressWarnings("rawtypes")
		Iterator i = root.elementIterator("language"); i.hasNext();) {
			Element e = (Element) i.next();
			json.add(e.elementText("value"));
		}
		return json;
	}

	/**
	 * 加载学历定位
	 * 
	 * @param path
	 * @throws DocumentException
	 */
	public static JSONArray getEduAction(String path) throws DocumentException {
		JSONArray json = new JSONArray();
		File f = new File(path);
		SAXReader reader = new SAXReader();
		Document doc;
		doc = reader.read(f);
		Element root = doc.getRootElement();
		Element foo;
		for (@SuppressWarnings("rawtypes")
		Iterator i = root.elementIterator("edu"); i.hasNext();) {
			Element e = (Element) i.next();
			json.add(e.elementText("value"));
		}
		return json;
	}

	/**
	 * 加载超级管理员
	 * 
	 * @param path
	 * @return
	 * @throws DocumentException
	 */
	/*public static List<System_User> getListTerm(String path) throws DocumentException {
		List listAdmin = new ArrayList();
		File f = new File(path);
		SAXReader reader = new SAXReader();

		Document doc = reader.read(f);
		Element root = doc.getRootElement();
		for (Iterator i = root.elementIterator("user"); i.hasNext();) {
			Element foo = (Element) i.next();
			System_User sys_user = new System_User();
			sys_user.setTable_id(foo.elementText("table_id"));
			sys_user.setUserName(foo.elementText("userName"));
			sys_user.setPassWord(foo.elementText("userPwd"));
			//sys_user.setUserShowName(foo.elementText("userShowName"));
			listAdmin.add(sys_user);
		}
		return listAdmin;
	}*/
	
	/**
	 * 加载超级管理员
	 * 
	 * @param path
	 * @return
	 * @throws DocumentException
	 */
	public static List<System_User> getListTerm(String path) throws DocumentException {
		List listAdmin = new ArrayList();
		Properties prop = PropertiesUtils.getProperties(path);
		String str = prop.getProperty("user.info");
		JSONArray array = JSONArray.fromObject(str);
		for (Object o : array) {
			JSONObject obj = JSONObject.fromObject(o);
			System_User sys_user = new System_User();
			sys_user.setTable_id(obj.get("table_id").toString());
			sys_user.setUserName(obj.get("userName").toString());
			sys_user.setPassWord(obj.get("userPwd").toString());
			listAdmin.add(sys_user);
		}
		
		return listAdmin;
	}


	/**
	 * 加载教材类型
	 * 
	 * @param path
	 * @return
	 * @throws DocumentException
	 */
	public static JSONArray getJiaoyuType(String path) throws DocumentException {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		File f = new File(path);
		SAXReader reader = new SAXReader();

		JSONArray json = new JSONArray();

		Document doc = reader.read(f);
		Element root = doc.getRootElement();

		for (Iterator i = root.elementIterator("power"); i.hasNext();) {
			Element foo = (Element) i.next();
			JSONObject js = new JSONObject();
			js.put("id", foo.elementText("id"));
			js.put("name", foo.elementText("name"));
			js.put("pid", foo.elementText("pid"));
			json.add(js);
		}
		return json;
	}
	/**
	 * 加载菜单
	 * 
	 * **/
	public static JSONArray getMenu(String path) throws Exception {
		File f = new File(path);
		SAXReader reader = new SAXReader();

		JSONArray json = new JSONArray();

		Document doc = reader.read(f);
		Element root = doc.getRootElement();
		for (Iterator i = root.elementIterator("menu"); i.hasNext();) {
			
			Element foo = (Element) i.next();
			JSONObject js = new JSONObject();
			js.put("id", foo.elementText("id"));
			js.put("name", foo.elementText("name"));
			js.put("url", foo.elementText("url"));
			String pid = foo.elementText("pId");
			js.put("pId", pid);
			js.put("sort", foo.elementText("sort"));
			
			if("0".equals(pid)){
				js.put("open", " false");
			}
			
			Element btn = foo.element("buttons");
			if(btn != null){
				js.put("open", "false");
				for(Iterator bi = btn.elementIterator("button");bi.hasNext();){
					Element bfoo = (Element) bi.next();
					JSONObject j = new JSONObject();
					j.put("id", bfoo.elementText("id"));
					j.put("name", bfoo.elementText("name"));
					j.put("pId", foo.elementText("id"));
					j.put("open", "true");
					json.add(j);
				}
			}

			json.add(js);
			
		}
		return json;
	}
	
	/**
	 * 读取XML配置文件，判断是否启用操作日志，开启TRUE;否则为FALSE。
	 * @return 
	 * 		0  不启用
	 * 		1 启用
	 * @throws Exception 
	 */
	/*public static boolean IsLogOn(String path) throws Exception {

		File file = new File(path);
		SAXReader reader = new SAXReader();
		Document doc = reader.read(file);
		Element root = doc.getRootElement();
		for (Iterator i = root.elementIterator("operationlog"); i.hasNext();) {
			Element ele = (Element) i.next();
			String on = ele.elementText("on");
			if("1".equals(on)) return true;
		}
		return false;
	}*/
	
	/**
	 * 读取properties配置文件，判断是否启用操作日志，开启TRUE;否则为FALSE。
	 * @return 
	 * 		0  不启用
	 * 		1 启用
	 * @throws Exception 
	 */
	public static boolean IsLogOn(String path) throws Exception {
		Properties prop = PropertiesUtils.getProperties("setting.properties");
		
		String on = prop.getProperty("operationlog.on");
		if("1".equals(on)) return true;
		
		return false;
	}
	
	private static void initLuceneFilterTables(String path) throws IOException {
		File f = new File(path);
		if(!f.exists()) {
			return;
		}
		BufferedReader reader = null;
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(f);
			reader = new BufferedReader(fileReader);
			String str = null;
			while( (str = reader.readLine()) != null ) {
				if("".equals(str.trim())) {
					continue;
				}
				DataInit.luceneFilterTables.add(str.trim());
			}
		}catch (Exception e) {
			// TODO: handle exception
		}finally {
			if(reader != null) {
				reader.close();
			}
			if(fileReader != null) {
				fileReader.close();
			}
		}
		
	}
	private static void initLuceneFileTables(String path) throws IOException {
		File f = new File(path);
		if(!f.exists()) {
			return;
		}
		BufferedReader reader = null;
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(f);
			reader = new BufferedReader(fileReader);
			String str = null;
			while( (str = reader.readLine()) != null ) {
				if("".equals(str.trim())) {
					continue;
				}
				DataInit.luceneFileTables.add(str.trim());
			}
		}catch (Exception e) {
			// TODO: handle exception
		}finally {
			if(reader != null) {
				reader.close();
			}
			if(fileReader != null) {
				fileReader.close();
			}
		}
		
	}
	/**
	 * 加载上传文件基本信息
	 * 
	 **/
	public static void iniUploadFileParams() {
		Properties prop = PropertiesUtils.getProperties("setting.properties");
		
		String uploadBasePath = prop.getProperty("upload.uploadBasePath");
		String downHost =  prop.getProperty("upload.downHost");
		String downPort = prop.getProperty("upload.downPort");
		String downProject = prop.getProperty("upload.downProject");
		System.out.println("上传路径:"+uploadBasePath);
		BigFileUploadUtil.setBasePath(uploadBasePath);
		BigFileUploadUtil.setDownloadbaseUrl(downHost, downPort, downProject);

	}
	
}
