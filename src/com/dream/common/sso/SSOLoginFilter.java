package com.dream.common.sso;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;

import com.dream.model.System_User;
/***
 * 
 * 此方法具体实现，需要更具实际情况修改用户对象
 * **/
public class SSOLoginFilter implements Filter{

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		HttpServletRequest request = (HttpServletRequest)req;
		System_User user = (System_User) request.getAttribute("userLoginInfo");
		if(user == null){
			String userName = null;
			Assertion assertion = (Assertion) request.getSession().getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION);
			if(assertion != null){
				userName = assertion.getPrincipal().getName();
			}
			if(userName == null){
				RequestDispatcher disp = request.getRequestDispatcher("/login.jsp");
				disp.forward(req, res);
			}else{
				user = new System_User();
				if("admin".equals(userName)){
					user.setTable_id("1");
				}
				user.setUserName(userName);
				request.getSession().setAttribute("userLoginInfo", user);
				chain.doFilter(req, res);
			}
		}else{
			chain.doFilter(req, res);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
