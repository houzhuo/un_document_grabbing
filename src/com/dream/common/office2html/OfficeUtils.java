package com.dream.common.office2html;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.poi.hssf.converter.ExcelToHtmlConverter;
import org.apache.poi.hssf.usermodel.HSSFPictureData;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.converter.PicturesManager;
import org.apache.poi.hwpf.converter.WordToHtmlConverter;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.hwpf.usermodel.PictureType;
import org.apache.poi.sl.usermodel.Slide;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.converter.core.BasicURIResolver;
import org.apache.poi.xwpf.converter.core.FileImageExtractor;
import org.apache.poi.xwpf.converter.core.FileURIResolver;
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.w3c.dom.Document;

import com.dream.common.upload.BigFileUploadUtil;

@SuppressWarnings("all")
public class OfficeUtils {

	public static void wordToHtml(File f, String path, String fileName) throws IOException {
		String name = f.getName();
		if (name.endsWith("docx")) {
			WordDocxToHtml(f, path, fileName);
		} else {
			WordDocToHtml(f, path, fileName);
		}
	}

	private static void WordDocxToHtml(File f, String path, String fileName) throws IOException {
		InputStream in = new FileInputStream(f);
		XWPFDocument document = new XWPFDocument(in);

		// 2) 解析 XHTML配置 (这里设置IURIResolver来设置图片存放的目录)
		File imageFolderFile = new File(path);
		XHTMLOptions options = XHTMLOptions.create().indent(4);
		options.URIResolver(new BasicURIResolver(BigFileUploadUtil.getDownloadbaseUrl() + "/" + "officeToHtml" + "/" + fileName.subSequence(0, fileName.lastIndexOf(".")) ));
		// >> 文件的保存路径 之后自动会添加 word\media子路径
		FileImageExtractor extractor = new FileImageExtractor(new File(path));

		options.setExtractor(extractor);
		/*
		 * options.setIgnoreStylesIfUnused(false); options.setFragment(true);
		 */

		// 3) 将 XWPFDocument转换成XHTML
		OutputStream out = new FileOutputStream(new File(path + fileName));
		XHTMLConverter.getInstance().convert(document, out, options);
	}

	private static void WordDocToHtml(File f, String path, String fileName) {
		InputStream in = null;
		String contents = null;
		HWPFDocument doc = null;
		try {
			in = new FileInputStream(f);
			doc = new HWPFDocument(in);
			WordToHtmlConverter wordToHtmlConverter = new WordToHtmlConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
			wordToHtmlConverter.setPicturesManager(new PicturesManager() {
				public String savePicture(byte[] content, PictureType pictureType, String suggestedName, float widthInches, float heightInches) {
					return suggestedName;
				}
			});
			wordToHtmlConverter.processDocument(doc);
			List pics = doc.getPicturesTable().getAllPictures();
			if (pics != null) {
				for (int i = 0; i < pics.size(); i++) {
					Picture pic = (Picture) pics.get(i);
					pic.writeImageContent(new FileOutputStream(path + pic.suggestFullFileName()));
				}
			}
			Document htmlDocument = wordToHtmlConverter.getDocument();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			DOMSource domSource = new DOMSource(htmlDocument);
			StreamResult streamResult = new StreamResult(outStream);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer serializer = tf.newTransformer();
			serializer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
			serializer.setOutputProperty(OutputKeys.INDENT, "yes");
			serializer.setOutputProperty(OutputKeys.METHOD, "html");
			serializer.transform(domSource, streamResult);
			outStream.close();

			contents = new String(outStream.toByteArray());

			writeDataToFile(path + File.separator + fileName, contents.getBytes());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (doc != null) {
				try {
					doc.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static void excelToHtml(File f, String path, String fileName) {
		boolean isExcel2003 = f.getName().toLowerCase().endsWith("xls") ? true : false;
		if (isExcel2003) {
			convertExcel2003(f, path, fileName);
		} else {
			convertExcel2007(f, path, fileName);
		}
	}

	private static void convertExcel2007(File f, String path, String fileName) {
		XLSXToHTML toHtml;
		try {
			toHtml = XLSXToHTML.create(f.getAbsolutePath(), new PrintWriter(new File(path + File.separator + fileName), "utf-8"));
			toHtml.setCompleteHTML(true);
			toHtml.printPage();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void convertExcel2003(File f, String path, String fileName) {
		InputStream input = null;
		try {
			input = new FileInputStream(f);
			HSSFWorkbook workbook = new HSSFWorkbook(input);
			ExcelToHtmlConverter excelToHtmlConverter = new ExcelToHtmlConverter(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
			excelToHtmlConverter.processWorkbook(workbook);
			Document htmlDocument = excelToHtmlConverter.getDocument();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			DOMSource domSource = new DOMSource(htmlDocument);
			StreamResult streamResult = new StreamResult(outStream);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer serializer = tf.newTransformer();
			serializer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
			serializer.setOutputProperty(OutputKeys.INDENT, "yes");
			serializer.setOutputProperty(OutputKeys.METHOD, "html");
			serializer.transform(domSource, streamResult);
			outStream.close();

			String content = new String(outStream.toByteArray());

			writeDataToFile(path + File.separator + fileName, content.getBytes());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} finally {
			try {
				if (input != null) {
					input.close();
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}

	public static void pdfToHtml(File pdfFile, String path, String fileName,String imgBasePath) {
		StringBuffer buffer = new StringBuffer();
		FileOutputStream fos;
		PDDocument document;
		int size;
		BufferedImage image;
		FileOutputStream out;
		try {
			buffer.append("<style>\r\n");
			buffer.append(".pdf_to_html_img {background-color:#fff; text-align:center; width:100%; max-width:100%;margin-top:6px;}\r\n");
			buffer.append("</style>\r\n");
			document = new PDDocument();
			document = PDDocument.load(pdfFile, (String) null);
			size = document.getNumberOfPages();
			Long start = System.currentTimeMillis(), end = null;
			System.out.println("===>pdf : " + pdfFile.getName() + " , size : " + size);
			PDFRenderer reader = new PDFRenderer(document);
			for (int i = 0; i < size; i++) {
				image = reader.renderImage(i, 1.5f);
				// 生成图片,保存位置
				out = new FileOutputStream(path + "/" + "image" + "_" + i + ".jpg");
				ImageIO.write(image, "png", out); // 使用png的清晰度
				// 将图片路径追加到网页文件里
				buffer.append("<img class=\"pdf_to_html_img\" src=\"" + imgBasePath  + "image" + "_" + i + ".jpg\"/>\r\n");
				image = null;
				out.flush();
				out.close();
			}
			reader = null;
			document.close();
			start = end = null;
			// 生成网页文件
			fos = new FileOutputStream(path + fileName);
			fos.write(buffer.toString().getBytes());
			fos.flush();
			fos.close();
			buffer.setLength(0);

		} catch (Exception e) {
			System.out.println("===>Reader parse pdf to jpg error : " + e.getMessage());
			e.printStackTrace();
		}
	}

	private static void writeDataToFile(String path, byte[] content) throws Exception {
		File f = new File(path);
		if (!f.exists()) {
			f.createNewFile();
		}
		FileOutputStream out = new FileOutputStream(f);
		out.write(content);
		out.close();
	}
	
	public static void main(String[] args) throws IOException {

		/*
		 * String path = "F:\\新建 Microsoft Excel 工作表.xls"; String resP = "F:\\";
		 * OfficeUtils.excelToHtml(new File(path), resP, "xxsddddsxx.html");
		 */

		/*
		 * String path = "F:\\新建文件夹\\公开★信息安全检测知识服务平台软件需求规格说明书D0.3.docx"; String
		 * resP = "F:\\"; //OfficeUtils.WordToHtml(new File(path), resP,
		 * "test1.html"); OfficeUtils.Word2007ToHtml(new File(path), resP,
		 * "test3.html");
		 */

		/*
		 * XLSXToHTML toHtml =
		 * XLSXToHTML.create("F:\\\\新建 Microsoft Excel 工作表.xlsx", new
		 * PrintWriter(new File("F:\\test4.html"),"gbk"));
		 * toHtml.setCompleteHTML(true); toHtml.printPage();
		 */
	/*	String path = "F:\\文档\\[www.java1234.com]Three.js 入门指南.pdf";
		OfficeUtils.pdfToHtml(new File(path), "F:\\", "test_pdf.html");*/
	}
}
