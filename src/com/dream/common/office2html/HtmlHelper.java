package com.dream.common.office2html;

import java.util.Formatter;

import org.apache.poi.ss.usermodel.CellStyle;

public interface HtmlHelper {
	
	void colorStyles(CellStyle style, Formatter out);  
}
