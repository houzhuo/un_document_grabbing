package com.dream.common.rsa;

import org.apache.commons.codec.binary.Base64;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

@SuppressWarnings("all")
public class RSAUtil {

	public static final String KEY_ALGORITHM = "RSA";
	public static final String SIGNATURE_ALGORITHM = "MD5withRSA";

	private static final String PUBLIC_KEY = "RSAPublicKey";
	private static final String PRIVATE_KEY = "RSAPrivateKey";

	public static byte[] decryptBASE64(String key) {
		return Base64.decodeBase64(key);
	}

	public static String encryptBASE64(byte[] bytes) {
		return Base64.encodeBase64String(bytes);
	}
	/**
	 * 用私钥对信息生成数字签名
	 *
	 * @param data
	 *            加密数据
	 * @param privateKey
	 *            私钥
	 * @return
	 * @throws Exception
	 */
	public static String sign(byte[] data, String privateKey) throws Exception {
		// 解密由base64编码的私钥
		byte[] keyBytes = decryptBASE64(privateKey);
		// 构造PKCS8EncodedKeySpec对象
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		// KEY_ALGORITHM 指定的加密算法
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		// 取私钥匙对象
		PrivateKey priKey = keyFactory.generatePrivate(pkcs8KeySpec);
		// 用私钥对信息生成数字签名
		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initSign(priKey);
		signature.update(data);
		return encryptBASE64(signature.sign());
	}

	/**
	 * 校验数字签名
	 *
	 * @param data
	 *            加密数据
	 * @param publicKey
	 *            公钥
	 * @param sign
	 *            数字签名
	 * @return 校验成功返回true 失败返回false
	 * @throws Exception
	 */
	public static boolean verify(byte[] data, String publicKey, String sign) throws Exception {
		// 解密由base64编码的公钥
		byte[] keyBytes = decryptBASE64(publicKey);
		// 构造X509EncodedKeySpec对象
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
		// KEY_ALGORITHM 指定的加密算法
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		// 取公钥匙对象
		PublicKey pubKey = keyFactory.generatePublic(keySpec);
		Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM);
		signature.initVerify(pubKey);
		signature.update(data);
		// 验证签名是否正常
		return signature.verify(decryptBASE64(sign));
	}

	public static byte[] decryptByPrivateKey(byte[] data, String key) throws Exception {
		// 对密钥解密
		byte[] keyBytes = decryptBASE64(key);
		// 取得私钥
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
		// 对数据解密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		return cipher.doFinal(data);
	}

	/**
	 * 解密<br>
	 * 用私钥解密
	 *
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptByPrivateKey(String data, String key) throws Exception {
		return decryptByPrivateKey(decryptBASE64(data), key);
	}

	/**
	 * 解密<br>
	 * 用公钥解密
	 *
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static byte[] decryptByPublicKey(byte[] data, String key) throws Exception {
		// 对密钥解密
		byte[] keyBytes = decryptBASE64(key);
		// 取得公钥
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicKey = keyFactory.generatePublic(x509KeySpec);
		// 对数据解密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.DECRYPT_MODE, publicKey);
		return cipher.doFinal(data);
	}

	/**
	 * 加密<br>
	 * 用公钥加密
	 *
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptByPublicKey(byte[] data, String key) throws Exception {
		// 对公钥解密
		byte[] keyBytes = decryptBASE64(key);
		// 取得公钥
		X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key publicKey = keyFactory.generatePublic(x509KeySpec);
		// 对数据加密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		return cipher.doFinal(data);
	}

	/**
	 * 加密<br>
	 * 用私钥加密
	 *
	 * @param data
	 * @param key
	 * @return
	 * @throws Exception
	 */
	public static byte[] encryptByPrivateKey(byte[] data, String key) throws Exception {
		// 对密钥解密
		byte[] keyBytes = decryptBASE64(key);
		// 取得私钥
		PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
		Key privateKey = keyFactory.generatePrivate(pkcs8KeySpec);
		// 对数据加密
		Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
		cipher.init(Cipher.ENCRYPT_MODE, privateKey);
		return cipher.doFinal(data);
	}

	/**
	 * 取得私钥
	 *
	 * @param keyMap
	 * @return
	 * @throws Exception
	 */
	public static String getPrivateKey(Map<String, Key> keyMap) throws Exception {
		Key key = (Key) keyMap.get(PRIVATE_KEY);
		return encryptBASE64(key.getEncoded());
	}

	/**
	 * 取得公钥
	 *
	 * @param keyMap
	 * @return
	 * @throws Exception
	 */
	public static String getPublicKey(Map<String, Key> keyMap) throws Exception {
		Key key = keyMap.get(PUBLIC_KEY);
		return encryptBASE64(key.getEncoded());
	}

	/**
	 * 初始化密钥
	 *
	 * @return
	 * @throws Exception
	 */
	public static Map<String, Key> initKey() throws Exception {
		KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
		keyPairGen.initialize(512);
		KeyPair keyPair = keyPairGen.generateKeyPair();
		Map<String, Key> keyMap = new HashMap(2);
		keyMap.put(PUBLIC_KEY, keyPair.getPublic());// 公钥
		keyMap.put(PRIVATE_KEY, keyPair.getPrivate());// 私钥
		return keyMap;
	}

	public static byte[] copyByteArray(byte[] data,int size) {
		byte[] result = new byte[size];
		for(int i = 0;i < size;i++) {
			result[i] = data[i];
		}
		return result;
	}
	public static void main(String[] args) throws Exception {
		/*Map<String, Key> keyMap = initKey();
		String publicKey = getPublicKey(keyMap);
		String privateKey = getPrivateKey(keyMap);*/

		String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAKGOsKqfWm/3DIqa3iZCPYJPwrMDp9MMDhdv7+A9U4ldqhLDyXevJVixcT4O4XmK7N6OJrWx+deYmwrQyud1U4cCAwEAAQ==";
		String privateKey = "MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAoY6wqp9ab/cMipreJkI9gk/CswOn0wwOF2/v4D1TiV2qEsPJd68lWLFxPg7heYrs3o4mtbH515ibCtDK53VThwIDAQABAkArkZmfSf5Pt6ZQMte6JZ8E55GwSUoXJmcedveP17oRjxxdqkplM918lO7s97/pd+KN9cbxmFUBdjHQaPyUt0xBAiEA5t8wLjB7V6wjQbPfaBj0si5Y+Pt6XO9zLwEhcAa8XnECIQCzJC86TAHom9z1uAikjXaB+8iICICT0EIcp1or5Oy9dwIhALE81jgfcBE2TTq7Y97+iZvgiN1b8ew2xy5mp8zg3uuBAiBco5K0wAegpbGPdFsr1Wf6ch1Sk6sCRYlazgRofJscowIgXH0SPK1ABiHdAisZKXUClP3+ACA5redJ25gcpUFTQDI=";
		System.out.println("---------------1--------------------");
		System.out.println(publicKey);
		System.out.println("---------------2--------------------");
		System.out.println(privateKey);
	
		//byte[] encryptByPrivateKey = encryptByPrivateKey("中国".getBytes(), privateKey);
		byte[] encryptByPublicKey = encryptByPublicKey("1234222256".getBytes(), publicKey);
		//System.out.println(new String(encryptByPrivateKey));
		System.out.println("---------------4--------------------");
		System.out.println(new String(encryptByPublicKey));
		System.out.println("-----------------5------------------");
	//	String sign = sign(encryptByPrivateKey, privateKey);
	//	System.out.println(sign);
		System.out.println("-----------------6------------------");
	//	boolean verify = verify(encryptByPrivateKey, publicKey, sign);
	//	System.out.println(verify);
		System.out.println("-----------------7------------------");
	//	byte[] decryptByPublicKey = decryptByPublicKey(encryptByPrivateKey, publicKey);
		byte[] decryptByPrivateKey = decryptByPrivateKey(encryptByPublicKey, privateKey);
		//System.out.println(new String(decryptByPublicKey));
		System.out.println("------------------8-----------------");
		System.out.println(new String(decryptByPrivateKey));
		
		
	/*	Map<String, Key> keyMap = initKey();
		String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjmf4L/vCDZ5eYNfjYANI/bZ5Hj6QC/H4/s+QAXn9ly2TK/tg/YBWM1MrnGKtxfQYI/cdxf+dBKfyWClHD6Flfcyq+v9vtmyf/+wa5qoIRtYozbe1PO+TLNro9wyZIHHfUh6aVM76GN6CVO9fCEJNXSwAyvNd2EZBHpHFR92T9mmUXuHnDa/+tfoOXPFWyPd0UAOUeTbHuCCs89gD3uYKS6SSzja3lM+CbVJB7iHhaLLDLsYl6mnNL7D+epLWY2lHKDop7NZw1RsgvEWvI5nwGxybFOLBTWES4FQMEyg4/nkH1uSC8dBrlgud6b9vWBaCrKiu21iyZycW3c4vhm3v2QIDAQAB";
		String privateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCOZ/gv+8INnl5g1+NgA0j9tnkePpAL8fj+z5ABef2XLZMr+2D9gFYzUyucYq3F9Bgj9x3F/50Ep/JYKUcPoWV9zKr6/2+2bJ//7BrmqghG1ijNt7U875Ms2uj3DJkgcd9SHppUzvoY3oJU718IQk1dLADK813YRkEekcVH3ZP2aZRe4ecNr/61+g5c8VbI93RQA5R5Nse4IKzz2APe5gpLpJLONreUz4JtUkHuIeFossMuxiXqac0vsP56ktZjaUcoOins1nDVGyC8Ra8jmfAbHJsU4sFNYRLgVAwTKDj+eQfW5ILx0GuWC53pv29YFoKsqK7bWLJnJxbdzi+Gbe/ZAgMBAAECggEARzrwJKgsJ324vX4fKG5cOzPjn2Ix0RfGsUYHRQXM96p/O/ewSvy/oUqPvNiLDUIMNSktRlWWJ7cN/FUhS0sypdsTBxtvuHLbETr7NbeFDlGe9U9Xo1Q+bKhO8QvnKBhtWPkfvx4McRYTYQKeqVXotpsFBn72yPBTMEQ/xrOpiA4E4bGVY8cMNiFfSOoiGqbmvmEfpH+BbMinscex5WSO08hA8QUIHrQJOteOo0zaGLgpNVH9+Np0eEaIKG7YvAVEHkRugYwPj8sgdW2ei5qaiTiA7I1kdNCo+biy27/PMB7y7fe85ksw9gFyECyP5rIwY9n00Yo9ABOlttVoLxsnkQKBgQDZTO2tMpAbDENtxGUJpENXDwHSRGI8Oj/DANq40KYqoHx/E/VTahnmYbuZfzmfck3CKeOPFQaY1gglKQwbYl2KZGNZrh/XdNoXx6mGYOQjA2xqsoAPfqXyxMFySTEoDFpvAduZ+33DEz9u6C2qt3vTWVKf43ULqY6KrEO6+9PE3QKBgQCnxH0pyAj0A7JSM++vAzGBuj22EtGz7zE+1X+CenH6I8mp3nXEkXLc9T1aixpqEy2Qy1II7hvW868zzmgJyU9lnZ+YTm/eV4qK/Es5CJxnZF5SaNFoi8Gx3AG1WEVHIMGWLXiX4n59jMRbqctuntKvdz24GbgaFUBZkbhx1dnZLQKBgDIwi4ECebhVl8lloDSe5RLZm4EWXF4u0gVXUC15u7eCw96SHIq8qejFUBA/C28hdoZPKIkmwRRR8yWqGmrEyHgrgar4BdC845a0fhpUjLztkIAAZIWvqeKtwUSFVT47xDN5iWBCbyS8cF1McSfKI2aIbgYSAjnEWvD6otYwThRdAoGAZSbJOFnNXCKmb32089Z/EPKTpqWuHkkHbhUXrjnZYrIVMwBYDU6M7MBIX8EjCfXKMVUFfzDhRDbk2d2HSO9BmcjxoskYFfy+IZKAYEDa3FPdst5T9C7feDdoKEiybHJJhhygayM6lMQ81++GIopMCVNhuRB4ufE6PUbo6GnB0XkCgYBN6y+QLUgYDz5DiipuHehNDKMRolx5xfuSXRIoYzP5gYQENhAvQsBzr3Kn7H+OONu2kcN4rw3GYgp61xbMPfm6DDoVX3Itto9wXxVG+XI+sLxZV+ZcbWqreg4gzxU6LWFeIiqHRkCosRoKQYGzcqVRN9156yRJ/skDHNvy+oxGZw==";
		System.out.println("---------------1--------------------");
		System.out.println(publicKey);
		System.out.println("---------------2--------------------");
		System.out.println(privateKey);*/
/*		BufferedInputStream buffer = new BufferedInputStream(new FileInputStream(new File("C:\\Users\\Luhj\\Desktop\\askquestion.sql")));
		File f = new File("C:\\\\Users\\\\Luhj\\\\Desktop\\\\test.sql");
		if(f.exists()) {
			f.delete();
		}
		f.createNewFile();
		FileOutputStream fileOutputStream = new FileOutputStream(f);
		
		int encrySize = 53;
		byte[] data = new byte[encrySize];
		int mark = 0;
		while((mark = buffer.read(data)) != -1) {
			if(mark < encrySize) {
				byte[] temp = RSAUtil.copyByteArray(data, mark);
				byte[] encryptData = encryptByPublicKey(temp, publicKey);
				fileOutputStream.write(encryptData);
			}else {
				byte[] encryptData = encryptByPublicKey(data, publicKey);
				fileOutputStream.write(encryptData);
			}
			
		}
		fileOutputStream.flush();
		fileOutputStream.close();*/
		System.out.println(System.currentTimeMillis());
		BufferedInputStream buffer2 = new BufferedInputStream(new FileInputStream(new File("C:\\Users\\Luhj\\Desktop\\test.sql")));
		File f2 = new File("C:\\\\Users\\\\Luhj\\\\Desktop\\\\test2.sql");
		if(f2.exists()) {
			f2.delete();
		}
		f2.createNewFile();
		FileOutputStream fileOutputStream2 = new FileOutputStream(f2);
		int decrySize = 64;
		int mark = 0;
		byte[] data2 = new byte[decrySize];
		while((mark = buffer2.read(data2)) != -1) {
			if(mark < decrySize) {
				byte[] temp =  RSAUtil.copyByteArray(data2, mark);
				byte[] encryptData2 = decryptByPrivateKey(temp, privateKey);
				fileOutputStream2.write(encryptData2);
			}else {
				byte[] encryptData2 = decryptByPrivateKey(data2, privateKey);
				fileOutputStream2.write(encryptData2);
			}
		}
		fileOutputStream2.flush();
		fileOutputStream2.close();
		System.out.println(System.currentTimeMillis());
	}
}
