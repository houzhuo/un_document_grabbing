package com.dream.common.webservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.rpc.client.RPCServiceClient;

@SuppressWarnings("all")
public class WebServiceUtils {

	/**
	 * url: 访问路径  http://localhost:9000/HelloWorld 
	 * namespace: 命名空间  http://common.dream.com/
	 * method: sayHello
	 * params:参数
	 * classes： 参数类型
	 **/
	public static Object getData(String url, String namespace, String method, Object[] params, Class<?>[] classes)
			throws AxisFault {
		RPCServiceClient client = new RPCServiceClient();
		Options options = client.getOptions();
		options.setTo(new EndpointReference(url));
		QName qname = new QName(namespace, method);
		return client.invokeBlocking(qname, params, classes)[0];
	}

	public static void main(String[] args) throws AxisFault {
		
		WebServiceUtils utils = new WebServiceUtils();
		String url = "http://localhost:9000/HelloWorld";
		String namespace = "http://common.dream.com/";
		String method = "sayHello";
		Object[] params = new Object[] { "超人" };
		Class<?>[] classes = new Class[] { String.class };
		String res = (String)utils.getData(url, namespace, method, params, classes);
		System.out.println(res);
	}
}
