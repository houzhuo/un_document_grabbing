package com.dream.common.validate;

import java.lang.reflect.Field;

import org.apache.commons.lang3.StringUtils;

import com.dream.common.annotation.Regex;

@SuppressWarnings("all")
public class ValidateUtils {

	public static String checkParams(Object object){
		Class<?> claz = object.getClass();
		Field[] fields =  claz.getDeclaredFields();
		
		if(fields == null || fields.length <= 0){
			return "";
		}
		for(Field field:fields){
			Regex check = field.getAnnotation(Regex.class);
			if(check == null){
				continue;
			}
			field.setAccessible(true);
			try {
				Object fObjectRes = field.get(object);
				if(check.isNotNull() && fObjectRes == null){
					return check.errorMsg();
				}
				Class<?> fClaszType = field.getType();
				if(fClaszType.getName().equals(String.class.getName()) && StringUtils.isEmpty(String.valueOf(fObjectRes))){
					return check.errorMsg();
				}
				
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return check.errorMsg();
			}
		}
		return "";
	}
}
