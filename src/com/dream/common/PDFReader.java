package com.dream.common;

import java.awt.image.RenderedImage;
import java.io.File;
import java.util.Iterator;

import javax.imageio.ImageIO;

import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.text.PDFTextStripper;

/**
 * 读取PDF中的内容
 * @author zlj
 *
 */
@SuppressWarnings("all")
public class PDFReader {
	
	public static String readText(File pdfFile){
	        PDDocument document = null;
	        try
	        {
	            // 方式一：
	            /**
	            InputStream input = null;
	            input = new FileInputStream( pdfFile );
	            //加载 pdf 文档
	            PDFParser parser = new PDFParser(new RandomAccessBuffer(input));
	            parser.parse();
	            document = parser.getPDDocument();
	            **/

	            // 方式二：
	            document=PDDocument.load(pdfFile);

	            // 获取页码
	            int pages = document.getNumberOfPages();

	            // 读文本内容
	            PDFTextStripper stripper=new PDFTextStripper();
	            // 设置按顺序输出
	            stripper.setSortByPosition(true);
	            stripper.setStartPage(1);
	            stripper.setEndPage(pages);
	            String content = stripper.getText(document);
	            return content;
	        }
	        catch(Exception e)
	        {
	            System.out.println(e);
	            return null;
	        }
	}

	/**
	 * 读取pdf中的图片
	 * @param pdfFile
	 * @param pdfFile_out
	 */
	public static void readImage(File pdfFile,File pdfFile_out){
		PDDocument document = null;
		PDDocument document_out = null;
		try {
			document = PDDocument.load(pdfFile);
			//document_out = PDDocument.load(pdfFile_out);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		int page_size = document.getNumberOfPages();
		System.out.println("getAllPages============"+page_size);
		
		int j = 0;
		for(int i=0;i<page_size;i++){
			PDPage page = document.getPage(i);
			//PDPage page1 = document_out.getPage(0);
			PDResources resources = page.getResources();
			Iterable xobjects = resources.getXObjectNames();
			
			if (xobjects != null) { 
				Iterator imageIter = xobjects.iterator();
				while(imageIter.hasNext()){
					COSName key = (COSName) imageIter.next();
					if(resources.isImageXObject(key)){
						try{
							 PDImageXObject imageobj = (PDImageXObject) resources.getXObject(key);
							 RenderedImage rendImage = imageobj.getImage();
							 ImageIO.write(rendImage,"png",new File("F://pdf//123"+j+".jpg"));
	                         
							 //将PDF文档中的图片 分别存到一个空白PDF中。
	                        /* PDPageContentStream contentStream = new PDPageContentStream(document_out,page1,AppendMode.APPEND,true);

	                         float scale = 1f;
	                         contentStream.drawImage(image, 20,20,image.getWidth()*scale,image.getHeight()*scale);
	                         contentStream.close();
	                         document_out.save("F://pdf//123"+j+".pdf");

	                         System.out.println(image.getSuffix() + ","+image.getHeight() +"," + image.getWidth());
						*/
						}catch (Exception e) {
							e.getMessage();
						}		
						j++;
					}
				}
			}
		}
		System.out.println(j);
	}
	 public static void main(String args[]){
	        String file="C:\\Users\\ThinkPad\\Desktop\\模型设计技术规格书.pdf";
	        String file_out = "F://pdf/blank.pdf";
	        long startTime=System.currentTimeMillis();
	       // getODFOutline(file);
	       // getPDFInformation(file);
	       // String context = readText(file);
	       // System.out.println(context);
	        readImage(new File(file), new File(file_out));
	        long endTime=System.currentTimeMillis();
	        System.out.println("读写所用时间为："+(endTime-startTime)+"ms");
	    }
}
