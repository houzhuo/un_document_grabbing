package com.dream.common.officetable2html;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.converter.PicturesManager;
import org.apache.poi.hwpf.converter.WordToHtmlConverter;
import org.apache.poi.hwpf.usermodel.BorderCode;
import org.apache.poi.hwpf.usermodel.Picture;
import org.apache.poi.hwpf.usermodel.PictureType;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.hwpf.usermodel.Table;
import org.apache.poi.hwpf.usermodel.TableCell;
import org.apache.poi.hwpf.usermodel.TableIterator;
import org.apache.poi.hwpf.usermodel.TableRow;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xwpf.converter.core.BasicURIResolver;
import org.apache.poi.xwpf.converter.core.FileImageExtractor;
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;

import com.microsoft.schemas.office.word.CTBorder;
/**
 * word转html table
 * @author w.g
 * @date 2018-09-26
 * 
 */
@SuppressWarnings("all")
public class WordToHtmlTable {
	
	
	public static void main(String[] args) {
		String filePath="E:/testFile/单元格合并测试07.docx";
		String htmlPath="E:/testFile/单元格合并测试07.html";
//		String savePath="C:\\Users\\ThinkPad\\Desktop";
//		String pdfPath="C:\\Users\\ThinkPad\\Desktop\\测试.pdf";
		readWordToHtml(filePath,htmlPath,true);

//		try {
//			txtToPdf(filePath, pdfPath);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		//jdk 11 test
		/*List<String> slist=new ArrayList<>();
		for(int i=0;i<100;i++) {
			slist.add((new Random(100)).toString());
		}
		long t1=System.currentTimeMillis();
		List<String> slist1 =slist.stream().map(s -> s.length()+"").collect(Collectors.toList()) ;
		System.out.println("Lambda方式:"+(System.currentTimeMillis()-t1)+"ms");
		
		long t2=System.currentTimeMillis();
		List<String> slist2 = new ArrayList<>();
		for(String s:slist) {
			slist2.add(s.length()+"");
		}
		System.out.println("Foreach方式:"+(System.currentTimeMillis()-t2)+"ms");*/
	}
	
	/**
	 * TODO word转html 
     * @param filePath 源文件文件的路径
     * @param htmlPositon 生成的html文件的路径
     * @param isStyle 是否需要表格样式 包含 字体 颜色 边框 对齐方式 
     */
    public static String readWordToHtml(String filePath ,String htmlPositon, boolean isStyle){
        
        InputStream is = null;
        String htmlExcel = null;
        try {
        	String suf = filePath.substring(filePath.lastIndexOf(".") + 1);//文件后缀
            File sourcefile = new File(filePath);
            is = new FileInputStream(sourcefile);
            if (suf.equals("doc")) {
            	System.out.println("doc文件");
            	//创建文档对象
            	POIFSFileSystem pfs=new POIFSFileSystem(is);
            	HWPFDocument doc=new HWPFDocument(pfs);//创建文档对象  poi-3.15会报错  3.17正常
            	htmlExcel=getWord03Info(doc);
    			//word2Html(doc);	
    		}
    		if (suf.equals("docx")) {
    			System.out.println("docx文件");
    			//创建文档对象
    			XWPFDocument docx = new XWPFDocument(is);
    			htmlExcel=getWord07Info(docx);
    			//word2Html(docx);	3.17报错,3.15正常
    		}
    		
            //写入文件
            writeFile(htmlExcel,htmlPositon);
        } catch (Exception e) {
        	System.out.println("Err:"+e.getMessage());
            e.printStackTrace();  
        }finally{
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return htmlExcel;
    }
    
    /**
     * TODO 读取word03的表格
     * @param doc
     * @return
     */
    public static String getWord03Info(HWPFDocument doc) {
    	System.out.println("getWord03Info()....");
    	StringBuffer sb = new StringBuffer();
    	Range range=doc.getRange();//获取文档的读取范围
    	TableIterator it=new TableIterator(range);//创建表格构造器
    	//迭代文档中的表格
    	while(it.hasNext()) {
    		Table tb=(Table)it.next();
    		TableRow row = null;
    		TableCell cell = null;
    		sb.append("<table style='border-collapse:collapse;' width='100%'>");//
    		//行数不为空
    		for(int rowNum=0;rowNum<tb.numRows();rowNum++) {
    			row=tb.getRow(rowNum);
    			if(null==row) {
    				sb.append("<tr><td ><nobr> </nobr></td></tr>");
                    continue;
    			}
    			sb.append("<tr>");
    			for(int colNum=0;colNum<row.numCells();colNum++) {
    				cell=row.getCell(colNum);
    				if (cell == null) {    //特殊情况 空白的单元格会返回null
                        sb.append("<td> </td>");
                        continue;
                    }
    				String value=cell.text();
    				sb.append("<td ");
    				dealWord03CellStyle(cell);
    				sb.append("><nobr>");
                    if (value == null || "".equals(value.trim())) {
                        sb.append("   ");
                    } else {
                        // 将ascii码为160的空格转换为html下的空格（ ）
                        sb.append(value.replace(String.valueOf((char) 160)," "));
                    }
                    sb.append("</nobr></td>");
    			}//  colNum end
    			sb.append("</tr>");
    		}//  rowNum end
    		 sb.append("</table>");
    	}
    	
		return sb.toString();
    	
    }
    /**
     * TODO word07转html,原样转换包括图片和表格
     * @param docx
     */
    public static void word2Html(XWPFDocument docx) {
    	ByteArrayOutputStream out = new ByteArrayOutputStream();
    	OutputStreamWriter osw=null;
    	String imgpath="E:/testFile/image/";
    	String file="E:/testFile/07_图文测试.html";
    	try {
    		XHTMLOptions options=XHTMLOptions.create();
    		
        	//存放图片的文件夹
        	options.setExtractor(new FileImageExtractor(new File(imgpath)));
        	//html中的图片路径
        	options.URIResolver(new BasicURIResolver("image"));
//        	osw = new OutputStreamWriter(new FileOutputStream(file), "utf-8"); 
//          XHTMLConverter xhtmlConverter =(XHTMLConverter) XHTMLConverter.getInstance(); 
//          xhtmlConverter.convert(docx, osw, options);
            XHTMLConverter.getInstance().convert(docx, out, options);//poi-3.17会报错  3.15正常
		} catch (Exception e) {
			e.printStackTrace();
		}finally { 
			try {
				if (out != null) out.close();
				if (osw != null) osw.close();
			} catch (IOException e) {
				e.printStackTrace();
			} 
	    }
    	String content=new String(out.toByteArray());
    	System.out.println(content);
    	try {
			FileUtils.writeStringToFile(new File(file), content, "utf-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    /**
     * TODO word03转html,原样转换包括图片和表格
     * @param doc
     */
    public static void word2Html(HWPFDocument doc) {
    	String path="E:/testFile/image/";
    	String file="E:/testFile/单元格合并03_图文测试.html";
    	try {
    		//生成针对Dom对象的转化器
			WordToHtmlConverter whtmlConverter=new WordToHtmlConverter(
					//通过反射构建dom创建者工厂
					DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument());
			//转化器重写内部方法
			whtmlConverter.setPicturesManager(new PicturesManager() {
				@Override
				public String savePicture(byte[] arg0, PictureType arg1, String arg2, float arg3, float arg4) {
					return "image\\"+arg2;
				}
			});
			//转化器开始转化接收到的dom对象
			whtmlConverter.processDocument(doc);
			//获取文档中的所有图片
			List pics=doc.getPicturesTable().getAllPictures();
			for(int i=0;i<pics.size();i++) {
				Picture pic=(Picture) pics.get(i);
				try {
					pic.writeImageContent(new FileOutputStream(path+pic.suggestFullFileName()));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			//从加载了输入文件的转换器中提取DOM节点
			org.w3c.dom.Document  htmlDocument=whtmlConverter.getDocument();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			//从提取的DOM节点中获得内容
			DOMSource domSource = new DOMSource(htmlDocument);
			StreamResult streamResult = new StreamResult(outStream);
			//转化工厂生成序列转化器
			TransformerFactory tf = TransformerFactory.newInstance();
			//设置序列化内容格式
			Transformer serializer = tf.newTransformer();
			serializer.setOutputProperty(OutputKeys.ENCODING, "utf-8");
			serializer.setOutputProperty(OutputKeys.INDENT, "yes");
			serializer.setOutputProperty(OutputKeys.METHOD, "html");
			serializer.transform(domSource, streamResult);
			outStream.close();
			//生成文件
			String content = new String(outStream.toString("UTF-8"));
			FileUtils.writeStringToFile(new File(file), content, "utf-8");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    /**
     * TODO 读取word07的表格
     * @param docx
     * @return
     */
    public static String getWord07Info(XWPFDocument docx) {
    	StringBuffer sb = new StringBuffer();
    	Iterator<XWPFTable> it=docx.getTablesIterator();
    	
    	//迭代文档中的表格
    	while(it.hasNext()) {
    		XWPFTable tb=(XWPFTable)it.next();
    		XWPFTableRow row = null;
    		XWPFTableCell cell = null;
    		sb.append("<table style='border-collapse:collapse;' width='100%'>");//
    		//行数不为空,这里的行数包括嵌套的总行数
    		for(int rowNum=0;rowNum<tb.getNumberOfRows();rowNum++) {
    			row=tb.getRow(rowNum);
    			//System.out.println("第"+(rowNum+1)+"行共:"+row.getTableCells().size()+"列");
    			if(null==row) {
    				sb.append("<tr><td style='border: 1px solid #000000;'><nobr> </nobr></td></tr>");
                    continue;
    			}
    			sb.append("<tr>");
    			for(int colNum=0;colNum<row.getTableCells().size();colNum++) {
    				cell=row.getCell(colNum);
    				if (cell == null) {    //特殊情况 空白的单元格会返回null
    					//System.out.print("|==单元格为空==|");
                        sb.append("<td style='border: 1px solid #000000;'> </td>");
                        continue;
                    }
    				//System.out.print("|==单元格不为空==|");
    				String value=cell.getText();
    				sb.append("<td style='border: 1px solid #000000;'");
    				sb.append(getMergeCellMap(tb,rowNum,colNum));
    				sb.append("><nobr>");
                    if (value == null || "".equals(value.trim())) {
                        sb.append("   ");
                    } else {
                        // 将ascii码为160的空格转换为html下的空格()
                        sb.append(value.replace(String.valueOf((char) 160)," "));
                    }
                    sb.append("</nobr></td>");
    			}//  colNum end
    			//System.out.println();
    			sb.append("</tr>");
    		}//  rowNum end
    		 sb.append("</table>");
    	}
    	
		return sb.toString();
    	
    }
    /**
     * TODO 处理03单元格样式
     */
    public static void dealWord03CellStyle(TableCell cell) {
    	 BorderCode btop=cell.getBrcTop();
    	 BorderCode bbtm=cell.getBrcBottom();
    	 BorderCode blft=cell.getBrcLeft();
    	 BorderCode brit=cell.getBrcRight();
    }
    
    /**
     * TODO 处理07单元格样式
     * @param cell
     */
    public static void dealWord07CellStyle(XWPFTable tb,XWPFTableCell cell,StringBuffer sb) {
    	
    	//null
    	CTTblBorders borders=tb.getCTTbl().getTblPr().getTblBorders();
    	
    	if(null!=borders) {
    		CTBorder tBorder=(CTBorder) borders.getTop();
    		if(null!=tBorder) {
    			System.out.println(getBorderStyle(tBorder));
    		}else {
    			System.out.println("tBorder is null");
    		}
        	
    	}else {
    		System.out.println("borders is null");
    	}
    	
    }
    
    static String[] bordesr={"border-top:","border-right:","border-bottom:","border-left:"};
    static String[] borderStyles={"solid ","solid ","solid ","solid ","solid ","solid ","solid ","solid ","solid ","solid","solid","solid","solid","solid"};
      
    /**
     * TODO 获取边框样式  07
     * @param ctBorder
     * @return
     */
    public static String getBorderStyle(CTBorder ctBorder) {
    	
    	String brtype="";//ctBorder.getType().toString();
    	String brwidth=ctBorder.getWidth().toString();
    	
		return brwidth+brtype;
    	
    }
    
    /**
     * TODO 获取单元格的合并信息
     * @param tb
     * @return
     */
    public static String getMergeCellMap(XWPFTable table,int row,int col){
    	String content="";
    	XWPFTableCell cell=table.getRow(row).getCell(col);
    	CTTcPr tcpr=cell.getCTTc().getTcPr();//每个单元格都有的属性
//    	//列合并
//    	tcpr.getGridSpan().getVal().toString();//列合并数
//    	//行合并
//    	tcpr.getVMerge().getVal().toString();//restart:行合并开始的单元格
//    	tcpr.getVMerge().getVal();//null  被行合并的单元格
//    	//没有行列合并的单元格
//    	tcpr.getGridSpan();//null
//    	tcpr.getVMerge();//null
    	
    	//先处理列合并信息
    	if(null!=tcpr.getGridSpan()) {
    		content="colspan='"+tcpr.getGridSpan().getVal().toString()+"' ";
    	}
    	//再处理行合并信息
    	if(null!=tcpr.getVMerge()) {
    		//判断是不是行合并开始的单元格
    		if(null!=tcpr.getVMerge().getVal()) {
    			//content+=" rowspan='"+getRowspan(table,row,col)+"' ";
    		}
    	}
		return content; 	
    } 
    
    /**
     * TODO 获取单元格行合并数
     * @param table
     * @param row
     * @param col
     * @return
     */
    public static int getRowspan(XWPFTable table,int row,int col) {
    	XWPFTableCell cell = table.getRow(row).getCell(col);
    	List<Boolean> list=new ArrayList<>();//记录当前单元格合并的单元格数
    	getRowspan(table,row,col,list);
		return list.size()+1;
    	
    }
    public static void getRowspan(XWPFTable table,int row,int col,List<Boolean> list) {
    	// 已达到最后一行
        if (row + 1 >= table.getNumberOfRows()) {
            return;
        }
        int next_row= row + 1;
        int colsNum=table.getRow(row).getTableCells().size();//获取这一行的总列数
        // 因为列合并单元格可能导致行合并的单元格并不在同一列，所以从头遍历列，通过属性、宽度以及距离左边框间距来判断是否是行合并
        for(int i=0;i<colsNum;i++) {
        	XWPFTableCell cell=table.getRow(row).getCell(i);
        	//是否是行合并的中间行
        	if(isMergeRow(cell)) {
        		//是否是上一行单元格合并的单元格
        		XWPFTableCell cell2=table.getRow(row-1).getCell(i);
        		CTTcPr tcPr=cell2.getCTTc().getTcPr();
        		if(tcPr.getVMerge()!=null) {
        			list.add(true);
        			getRowspan(table, next_row, i, list);
                    break;
        		}
        	}
        }
        
    }
    
    /**
     * TODO 判断是否为行合并中间单元格
     * @param cell
     * @return
     */
    public static boolean isMergeRow(XWPFTableCell cell) {
    	CTTcPr tcpr=cell.getCTTc().getTcPr();
    	if(null==tcpr.getVMerge()) {
    		return false;
    	}else if (null==tcpr.getVMerge().getVal()) {
			return true;
		}
		return false; 	
    }
       
    /**
     * TODO 创建html文件
     * @param content 生成的excel表格标签
     * @param htmlPath 生成的html文件地址
     */
    private static void writeFile(String content,String htmlPath){
        File file2 = new File(htmlPath);
         StringBuilder sb = new StringBuilder();
         try {
          file2.createNewFile();//创建文件
        
          sb.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>"+file2.getName()+"</title></head><body>");
          sb.append("<div>");
          sb.append(content);
          sb.append("</div>");
          sb.append("</body></html>");
         
          PrintStream printStream = new PrintStream(new FileOutputStream(file2));
          printStream.println(sb.toString());//将字符串写入文件
         } catch (IOException e) {
          e.printStackTrace();
         } 
    }
    
    
}