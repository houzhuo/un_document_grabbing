package com.dream.common.officetable2html;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
/**excel转HTML表格
 * @author w.g
 * @date 2018-09-26
 * 
 */
@SuppressWarnings("all")
public class ExcelToHtmlTable {
	
	public static void main(String[] args) {
		String filePath="E:/testFile/问题报告表单模板.xlsx";
		String htmlPath="E:/testFile/问题报告表单模板.html";
		String content=readExcelToHtml(filePath,htmlPath,true);
		//String content=readExcelToHtml(filePath,htmlPath,3,true);
		//System.out.println(content);
	}
	
	/**
	 * TODO excel转html key:value形式输出
     * @param filePath excel源文件文件的路径
     * @param htmlPositon 生成的html文件的路径
     * @param kvNum 每一行有几对key:value
     * @param isStyle 是否需要表格样式 包含 字体 颜色 边框 对齐方式
     */
    public static String readExcelToHtml(String filePath ,String htmlPositon,int kvNum, boolean isStyle){
        InputStream is = null;
        String htmlExcel = null;
        try {
            File sourcefile = new File(filePath);
            is = new FileInputStream(sourcefile);
            Workbook wb = WorkbookFactory.create(is);
            if (wb instanceof XSSFWorkbook) {   //07版及10以后的excel处理方法
            	System.out.println("文件为07版本");
                XSSFWorkbook xWb = (XSSFWorkbook) wb;
                htmlExcel = getExcelKVInfo(xWb,kvNum,isStyle);
            }else if(wb instanceof HSSFWorkbook){  //03版excel处理方法
            	System.out.println("文件为03版本");
                HSSFWorkbook hWb = (HSSFWorkbook) wb;
                htmlExcel = getExcelKVInfo(hWb,kvNum,isStyle);
            }
            //写入文件
            writeFile(htmlExcel,htmlPositon);
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return htmlExcel;
    }
	
    public static String readExcelToHtml(File file) throws IOException {
    	InputStream is = null;
        String htmlExcel = null;
        try {
            is = new FileInputStream(file);
            Workbook wb = WorkbookFactory.create(is);
            if (wb instanceof XSSFWorkbook) {   //07版及10以后的excel处理方法
            	System.out.println("文件为07版本");
                XSSFWorkbook xWb = (XSSFWorkbook) wb;
                htmlExcel = getExcelInfo(xWb,true);
                //htmlExcel = getExcelKVInfo(xWb,3,isStyle);
            }else if(wb instanceof HSSFWorkbook){  //03版excel处理方法
            	System.out.println("文件为03版本");
                HSSFWorkbook hWb = (HSSFWorkbook) wb;
                htmlExcel = getExcelInfo(hWb,true);
                //htmlExcel = getExcelKVInfo(hWb,4,isStyle);
            }
            //写入文件
            //writeFile(htmlExcel,htmlPositon);
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return htmlExcel;
    }
	/**
	 * TODO excel转html 原样输出
     * @param filePath excel源文件文件的路径
     * @param htmlPositon 生成的html文件的路径
     * @param isStyle 是否需要表格样式 包含 字体 颜色 边框 对齐方式
     */
    public static String readExcelToHtml(String filePath ,String htmlPositon, boolean isStyle){
        
        InputStream is = null;
        String htmlExcel = null;
        try {
            File sourcefile = new File(filePath);
            is = new FileInputStream(sourcefile);
            Workbook wb = WorkbookFactory.create(is);
            if (wb instanceof XSSFWorkbook) {   //07版及10以后的excel处理方法
            	System.out.println("文件为07版本");
                XSSFWorkbook xWb = (XSSFWorkbook) wb;
                htmlExcel = getExcelInfo(xWb,isStyle);
                //htmlExcel = getExcelKVInfo(xWb,3,isStyle);
            }else if(wb instanceof HSSFWorkbook){  //03版excel处理方法
            	System.out.println("文件为03版本");
                HSSFWorkbook hWb = (HSSFWorkbook) wb;
                htmlExcel = getExcelInfo(hWb,isStyle);
                //htmlExcel = getExcelKVInfo(hWb,4,isStyle);
            }
            //写入文件
            writeFile(htmlExcel,htmlPositon);
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return htmlExcel;
    }
    
    /**
     * TODO 获取excel信息 转化为key:value形式的布局
     * @param wb
     * @param kvNum 每行key:value的个数
     * @param isStyle
     * @return
     */
    private static String getExcelKVInfo(Workbook wb,int kvNum,boolean isStyle) {
    	StringBuffer sb = new StringBuffer();
        Sheet sheet = wb.getSheetAt(0);//获取第一个Sheet的内容
        int lastRowNum = sheet.getLastRowNum();//获取行数
        Map<String, String> map[] = getRowSpanColSpanMap(sheet);
        sb.append("<table style='border-collapse:collapse;' width='100%'>");
        Row row = null;        //行
        Cell cell = null;    //单元格
        List<String> titles= new ArrayList<String>(); //存放标题信息
        for (int rowNum = sheet.getFirstRowNum(); rowNum <= lastRowNum; rowNum++) {
        	 row = sheet.getRow(rowNum);
        	 int lastColNum = row.getLastCellNum();
        	 if(rowNum == sheet.getFirstRowNum()) {
        		 for(int colNum = 0; colNum < lastColNum; colNum++) {
        			 cell = row.getCell(colNum);
        			 //第一行为标题行
                	 titles.add(getCellValue(cell));
        		 }
        	 }else {
        		 if (row == null) {
                     sb.append("<tr><td ><nobr> </nobr></td></tr>");
                     continue;
                 }
                 sb.append("<tr>");
                 
                 for (int colNum = 0; colNum < lastColNum; colNum++) {
                	 cell = row.getCell(colNum);
                	
    	            //内容行的列
    	            if (cell == null) { //特殊情况 空白的单元格会返回null
    	            	 sb.append("<td ");
        	             //判断是否需要样式
        	             if(isStyle){
        	            	int a= sheet.getFirstRowNum();
        	            	Row b=sheet.getRow(a);
        	            	Cell c=b.getCell(colNum);
        	                dealExcelStyle(wb, sheet, c, sb);//获取标题行相应单元格样式
        	             }
        	             sb.append("><nobr>");
        	             sb.append(titles.get(colNum));
        	             sb.append("</nobr></td> "); 
        	             
    	                 sb.append("<td> </td>");
    	                 continue;
    	             }
    	            
    	             sb.append("<td ");
    	             //判断是否需要样式
    	             if(isStyle){
    	            	int a= sheet.getFirstRowNum();
    	            	Row b=sheet.getRow(a);
    	            	Cell c=b.getCell(colNum);
    	                dealExcelStyle(wb, sheet, c, sb);//获取标题行相应单元格样式
    	             }
    	             sb.append("><nobr>");
    	             sb.append(titles.get(colNum));
    	             sb.append("</nobr></td> ");   
    	             
    	             String stringValue = getCellValue(cell);
    	             sb.append("<td ");
    	             
    	             //判断是否需要样式
    	             if(isStyle){
    	                 dealExcelStyle(wb, sheet, cell, sb);//处理单元格样式
    	             }
    	             
    	             sb.append("><nobr>");
    	             if (stringValue == null || "".equals(stringValue.trim())) {
    	                 sb.append("   ");
    	             } else {
    	                 // 将ascii码为160的空格转换为html下的空格（ ）
    	                 sb.append(stringValue.replace(String.valueOf((char) 160)," "));
    	             }
    	             sb.append("</nobr></td>");
    	           //每kvNum列换行
    	             if((colNum+1)%kvNum==0) {
    	             	 sb.append("</tr><tr>"); 
    	             }
                 }
                 sb.append("</tr>"); 
        	 }
        }
        sb.append("</table>");
        return sb.toString();
    	
    }
   
    /**
     * TODO 获取excel信息 原样输出
     * @param wb
     * @param isStyle
     * @return
     */
    private static String getExcelInfo(Workbook wb,boolean isStyle){
        
        StringBuffer sb = new StringBuffer();
        Sheet sheet = wb.getSheetAt(0);//获取第一个Sheet的内容
        int lastRowNum = sheet.getLastRowNum();//获取行数
        Map<String, String> map[] = getRowSpanColSpanMap(sheet);
        sb.append("<table style='border-collapse:collapse;' width='100%'>");
        Row row = null;        //行
        Cell cell = null;    //单元格
        
        for (int rowNum = sheet.getFirstRowNum(); rowNum <= lastRowNum; rowNum++) {
            row = sheet.getRow(rowNum);
            if (row == null) {
                sb.append("<tr><td ><nobr> </nobr></td></tr>");
                continue;
            }
            sb.append("<tr>");
            int lastColNum = row.getLastCellNum();
            for (int colNum = 0; colNum < lastColNum; colNum++) {
                cell = row.getCell(colNum);
                if (cell == null) {    //特殊情况 空白的单元格会返回null
                    sb.append("<td> </td>");
                    continue;
                }
                String stringValue = getCellValue(cell);
                int rowSpan=1,colSpan=1;
                if (map[0].containsKey(rowNum + "," + colNum)) {
                    String pointString = map[0].get(rowNum + "," + colNum);
                    map[0].remove(rowNum + "," + colNum);
                    int bottomeRow = Integer.valueOf(pointString.split(",")[0]);
                    int bottomeCol = Integer.valueOf(pointString.split(",")[1]);
                    rowSpan = bottomeRow - rowNum + 1;
                    colSpan = bottomeCol - colNum + 1;
                    sb.append("<td rowspan= '" + rowSpan + "' colspan= '"+ colSpan + "' ");
                } else if (map[1].containsKey(rowNum + "," + colNum)) {
                    map[1].remove(rowNum + "," + colNum);
                    continue;
                } else {
                    sb.append("<td ");
                }
                //sb.append(" align='right' ");
                //判断是否需要样式
                if(isStyle){
                    dealExcelStyle(wb, sheet, cell, sb);//处理单元格样式
                }
                
                sb.append("><nobr>");
                if (stringValue == null || "".equals(stringValue.trim())) {
                    //sb.append("   ");
                	if(rowSpan>1) {
                		sb.append("<textarea type='text' rows='"+rowSpan+"' style='width:100%; resize: vertical;'></textarea>");
                	}else {
                		sb.append("<input type='text' style='width:100%;'>");
                	}
                	
                } else {
                    // 将ascii码为160的空格转换为html下的空格()
                    sb.append(stringValue.replace(String.valueOf((char) 160)," "));
                }
                sb.append("</nobr></td>");
            }
            sb.append("</tr>");
        }
        sb.append("</table>");
        return sb.toString();
    }
    /**
     * TODO 获取合并单元格的信息
     * @param sheet
     * @return
     */
    private static Map<String, String>[] getRowSpanColSpanMap(Sheet sheet) {
        Map<String, String> map0 = new HashMap<String, String>();
        Map<String, String> map1 = new HashMap<String, String>();
        int mergedNum = sheet.getNumMergedRegions();//获取合并单元格的数量
        CellRangeAddress range = null;
        for (int i = 0; i < mergedNum; i++) {
            range = sheet.getMergedRegion(i);//获取合并的单元格
            int topRow = range.getFirstRow();//起始行
            int topCol = range.getFirstColumn();//起始列
            int bottomRow = range.getLastRow();//结束行
            int bottomCol = range.getLastColumn();//结束列
            map0.put(topRow + "," + topCol, bottomRow + "," + bottomCol);//位置信息存入map中
            // System.out.println(topRow + "," + topCol + "," + bottomRow + "," + bottomCol);
            int tempRow = topRow;
            while (tempRow <= bottomRow) {
                int tempCol = topCol;
                while (tempCol <= bottomCol) {
                    map1.put(tempRow + "," + tempCol, "");
                    tempCol++;
                }
                tempRow++;
            }
            map1.remove(topRow + "," + topCol);
        }
        Map[] map = { map0, map1 };
        return map;
    }
    
    
    /**
     * TODO 获取表格单元格Cell内容
     * @param cell
     * @return
     */
    private static String getCellValue(Cell cell) {
        String result = new String();  
        switch (cell.getCellType()) {  
        case Cell.CELL_TYPE_NUMERIC:// 数字类型  
            if (HSSFDateUtil.isCellDateFormatted(cell)) {// 处理日期格式、时间格式  
                SimpleDateFormat sdf = null;  
                if (cell.getCellStyle().getDataFormat() == HSSFDataFormat.getBuiltinFormat("h:mm")) {  
                    sdf = new SimpleDateFormat("HH:mm");  
                } else {// 日期  
                    sdf = new SimpleDateFormat("yyyy-MM-dd");  
                }  
                Date date = cell.getDateCellValue();  
                result = sdf.format(date);  
            } else if (cell.getCellStyle().getDataFormat() == 58) {  
                // 处理自定义日期格式：m月d日(通过判断单元格的格式id解决，id的值是58)  
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
                double value = cell.getNumericCellValue();  
                Date date = org.apache.poi.ss.usermodel.DateUtil  
                        .getJavaDate(value);  
                result = sdf.format(date);  
            } else {  
                double value = cell.getNumericCellValue();  
                CellStyle style = cell.getCellStyle();  
                DecimalFormat format = new DecimalFormat();  
                String temp = style.getDataFormatString();  
                // 单元格设置成常规  
                if (temp.equals("General")) {  
                    format.applyPattern("#");  
                }  
                result = format.format(value);  
            }  
            break;  
        case Cell.CELL_TYPE_STRING:// String类型  
            result = cell.getRichStringCellValue().toString();  
            break;  
        case Cell.CELL_TYPE_BLANK:  
            result = "";  
            break; 
        default:  
            result = "";  
            break;  
        }  
        return result;  
    }
    
    /**
     * TODO 处理表格样式
     * @param wb
     * @param sheet
     * @param sb
     */
    private static void dealExcelStyle(Workbook wb,Sheet sheet,Cell cell,StringBuffer sb){
        
        CellStyle cellStyle = cell.getCellStyle();
        if (cellStyle != null) {
            short alignment = cellStyle.getAlignment();
            sb.append("align='" + convertAlignToHtml(alignment) + "' ");//单元格内容的水平对齐方式
            short verticalAlignment = cellStyle.getVerticalAlignment();
            sb.append("valign='"+ convertVerticalAlignToHtml(verticalAlignment)+ "' ");//单元格中内容的垂直排列方式
            
            if (wb instanceof XSSFWorkbook) {
                            
                XSSFFont xf = ((XSSFCellStyle) cellStyle).getFont(); 
                boolean bold = xf.getBold();
                String  align = convertAlignToHtml(alignment);
                sb.append("style='");
                if(bold) {
                	sb.append("font-weight:bold;"); // 字体加粗
                }
                sb.append("font-size: " + xf.getFontHeight() / 2 + "%;"); // 字体大小
                int columnWidth = sheet.getColumnWidth(cell.getColumnIndex()) ;
                sb.append("width:" + columnWidth + "px;");
                sb.append("text-align:" + align + ";");//表头排版样式
                XSSFColor xc = xf.getXSSFColor();
                if (xc != null && !"".equals(xc)) {
                    sb.append("color:#" + xc.getARGBHex().substring(2) + ";"); // 字体颜色
                }
                
                XSSFColor bgColor = (XSSFColor) cellStyle.getFillForegroundColorColor();
                if (bgColor != null && !"".equals(bgColor)) {
                    sb.append("background-color:#" + bgColor.getARGBHex().substring(2) + ";"); // 背景颜色
                }
                sb.append(getBorderStyle(0,cellStyle.getBorderTop(), ((XSSFCellStyle) cellStyle).getTopBorderXSSFColor()));
                sb.append(getBorderStyle(1,cellStyle.getBorderRight(), ((XSSFCellStyle) cellStyle).getRightBorderXSSFColor()));
                sb.append(getBorderStyle(2,cellStyle.getBorderBottom(), ((XSSFCellStyle) cellStyle).getBottomBorderXSSFColor()));
                sb.append(getBorderStyle(3,cellStyle.getBorderLeft(), ((XSSFCellStyle) cellStyle).getLeftBorderXSSFColor()));
                    
            }else if(wb instanceof HSSFWorkbook){
                
                HSSFFont hf = ((HSSFCellStyle) cellStyle).getFont(wb);
                boolean bold = hf.getBold();
                short fontColor = hf.getColor();
                sb.append("style='");
                HSSFPalette palette = ((HSSFWorkbook) wb).getCustomPalette(); // 类HSSFPalette用于求的颜色的国际标准形式
                HSSFColor hc = palette.getColor(fontColor);
                if(bold) {
                	sb.append("font-weight: bold ;"); // 字体加粗
                }
                sb.append("font-size: " + hf.getFontHeight() / 2 + "%;"); // 字体大小
                String  align = convertAlignToHtml(alignment);
                sb.append("text-align:" + align + ";");//表头排版样式
                String fontColorStr = convertToStardColor(hc);
                if (fontColorStr != null && !"".equals(fontColorStr.trim())) {
                    sb.append("color:" + fontColorStr + ";"); // 字体颜色
                }
                int columnWidth = sheet.getColumnWidth(cell.getColumnIndex()) ;
                sb.append("width:" + columnWidth + "px;");
                short bgColor = cellStyle.getFillForegroundColor();
                hc = palette.getColor(bgColor);
                String bgColorStr = convertToStardColor(hc);
                if (bgColorStr != null && !"".equals(bgColorStr.trim())) {
                    sb.append("background-color:" + bgColorStr + ";"); // 背景颜色
                }
                sb.append( getBorderStyle(palette,0,cellStyle.getBorderTop(),cellStyle.getTopBorderColor()));
                sb.append( getBorderStyle(palette,1,cellStyle.getBorderRight(),cellStyle.getRightBorderColor()));
                sb.append( getBorderStyle(palette,3,cellStyle.getBorderLeft(),cellStyle.getLeftBorderColor()));
                sb.append( getBorderStyle(palette,2,cellStyle.getBorderBottom(),cellStyle.getBottomBorderColor()));
            }
            sb.append("' ");
        }
    }
    
    /**
     * TODO 单元格内容的水平对齐方式
     * @param alignment
     * @return
     */
    private static String convertAlignToHtml(short alignment) {
        String align = "center";
       /*switch (alignment) {//poi-3.17 无此方法
        case CellStyle.ALIGN_LEFT: 
            align = "left";
            break;
        case CellStyle.ALIGN_CENTER:
            align = "center";
            break;
        case CellStyle.ALIGN_RIGHT:
            align = "right";
            break;
        default:
            break;
        }*/
        return align;
    }
    /**
     * TODO 单元格中内容的垂直排列方式
     * @param verticalAlignment
     * @return
     */
    private static String convertVerticalAlignToHtml(short verticalAlignment) {
        String valign = "middle";
        /*switch (verticalAlignment) {//poi-3.17 无此方法
        case CellStyle.VERTICAL_BOTTOM:
            valign = "bottom";
            break;
        case CellStyle.VERTICAL_CENTER:
            valign = "center";
            break;
        case CellStyle.VERTICAL_TOP:
            valign = "top";
            break;
        default:
            break;
        }*/
        return valign;
    }
    
    private static String convertToStardColor(HSSFColor hc) {
        StringBuffer sb = new StringBuffer("");
        if (hc != null) {
            if (HSSFColor.AUTOMATIC.index == hc.getIndex()) {
                return null;
            }
            sb.append("#");
            for (int i = 0; i < hc.getTriplet().length; i++) {
               sb.append(fillWithZero(Integer.toHexString(hc.getTriplet()[i])));
            }
        }
        return sb.toString();
    }
    
    private static String fillWithZero(String str) {
        if (str != null && str.length() < 2) {
            return "0" + str;
        }
        return str;
    }
    
    static String[] bordesr={"border-top:","border-right:","border-bottom:","border-left:"};
    static String[] borderStyles={"solid ","solid ","solid ","solid ","solid ","solid ","solid ","solid ","solid ","solid","solid","solid","solid","solid"};
    /**
     * TODO 边框样式
     * @param palette
     * @param b
     * @param s
     * @param t
     * @return
     */
    private static  String getBorderStyle(  HSSFPalette palette ,int b,short s, short t){
         
        if(s==0)return  bordesr[b]+borderStyles[s]+"#d0d7e5 1px;";;
        String borderColorStr = convertToStardColor( palette.getColor(t));
        borderColorStr=borderColorStr==null|| borderColorStr.length()<1?"#000000":borderColorStr;
        return bordesr[b]+borderStyles[s]+borderColorStr+" 1px;";
    }
    
    private static  String getBorderStyle(int b,short s, XSSFColor xc){
         if(s==0)return  bordesr[b]+borderStyles[s]+"#d0d7e5 1px;";;
         if (xc != null && !"".equals(xc)) {
             String borderColorStr = xc.getARGBHex();//t.getARGBHex();
             borderColorStr=borderColorStr==null|| borderColorStr.length()<1?"#000000":borderColorStr.substring(2);
             return bordesr[b]+borderStyles[s]+borderColorStr+" 1px;";
         }
         return "";
    }
   
    /**
     * TODO 创建html文件
     * @param content 生成的excel表格标签
     * @param htmlPath 生成的html文件地址
     */
    private static void writeFile(String content,String htmlPath){
        File file2 = new File(htmlPath);
         StringBuilder sb = new StringBuilder();
         try {
          file2.createNewFile();//创建文件
        
          sb.append("<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><title>Html Test</title></head><body>");
          sb.append("<div>");
          sb.append(content);
          sb.append("</div>");
          sb.append("</body></html>");
         
          PrintStream printStream = new PrintStream(new FileOutputStream(file2));
          printStream.println(sb.toString());//将字符串写入文件
         } catch (IOException e) {
          e.printStackTrace();
         }
        
    }
	
}