package com.dream.common;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.dream.common.init.DataInit;
import com.dream.common.init.SpringInit;
import com.dream.model.File_Entity;
import com.dream.model.Operation_Log;
import com.dream.model.System_User;
import com.dream.system.service.FileService;
import com.dream.system.service.OperateLogService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

@Component
@SuppressWarnings("all")
public class SuperController {

	@Autowired
	private OperateLogService operateLogServiceImp;
	
	@Autowired
	private FileService fileServiceImp;
	
	public void respWriter(HttpServletResponse resp, String msg) throws IOException {
		resp.getWriter().write(msg);
		resp.getWriter().flush();
		resp.getWriter().close();
	}

	public void respReturn(HttpServletResponse resp, String errMsg) throws IOException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("success", false);
		jsonObject.put("errMsg", errMsg);
		respWriter(resp, jsonObject.toString());
	}

	public void respReturn(HttpServletResponse resp) throws IOException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("success", true);
		respWriter(resp, jsonObject.toString());
	}

	public String getUserTableId(HttpServletRequest req) throws IOException {
		JSONObject json = (JSONObject) req.getSession().getAttribute("systemUser");
		System.out.println(json.toString() + "----");
		if (json.isNullObject()) {
			return "";
		}
		return json.get("table_id").toString();
	}

	public String getUserName(HttpServletRequest req) throws IOException {
		JSONObject json = JSONObject.fromObject(req.getSession().getAttribute("systemUser"));
		if (json.isNullObject()) {
			return "";
		}
		return json.get("userName").toString();
	}

	/**
	 * 得到系统当前的sessionUser
	 * 
	 * @param req
	 * @return
	 * @throws IOException
	 */
	public System_User getSessionUser(HttpServletRequest req) throws IOException {
		JSONObject ju1 = (JSONObject) req.getSession().getAttribute("systemUser");
		if (ju1 == null) {
			return null;
		}
		System_User ju = (System_User) JSONObject.toBean(ju1, System_User.class);
		return ju;
	}

	/**
	 * 
	 * 验证是不是超级管理员
	 * 
	 * @param ju
	 * @return
	 */
	public boolean isAdmin1(System_User ju) {

		for (int i = 0; i < DataInit.listAdmin.size(); i++) {

			System_User ju2 = DataInit.listAdmin.get(i);

		}
		return false;

	}

	/**
	 * 
	 * 验证是不是超级管理员
	 * 
	 * @param ju
	 * @return
	 */
	public System_User isAdmin(System_User ju) {

		for (int i = 0; i < DataInit.listAdmin.size(); i++) {
			System_User ju2 = DataInit.listAdmin.get(i);
			if (ju2.getUserName().equals(ju.getUserName()) && ju2.getPassWord().equals(ju.getPassWord())) {
				return ju2;
			}
		}
		return null;

	}
	public void outData(HttpServletResponse response,Object object,JsonConfig jsonConfig) throws IOException{
		response.setContentType("text/html;charset=UTF-8");
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		response.getWriter().write(JSONObject.fromObject(object,jsonConfig).toString());
		response.getWriter().close();
		response.getWriter().flush();
	}

	public void outData(HttpServletResponse response, Object object) throws IOException {
		response.setContentType("text/html;charset=UTF-8");
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		JSONObject json = JSONObject.fromObject(object, jsonConfig);
		response.getWriter().write(json.toString());
		response.getWriter().close();
		response.getWriter().flush();
	}

	public void outData(HttpServletResponse response, JSONArray object) throws IOException {
		response.setContentType("text/html;charset=UTF-8");
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		JSONArray json = JSONArray.fromObject(object, jsonConfig);
		response.getWriter().write(json.toString());
		response.getWriter().close();
		response.getWriter().flush();
	}
	
	/**
	 * 新增操作日志，新增根据配置文件判断日志是否开启
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	public void insertOperationLog(Operation_Log model, HttpServletRequest request) throws Exception {
		//String path = "config/setting.xml";
		String path = "setting.properties";
		//读取配置文件，判断是否开启系统日志功能
		if(!SpringInit.IsLogOn(path)) return ;
		if(null == model) throw new Exception("CHECK Operation_Log IS NULL OR NOT!") ;
		if(StringUtils.isBlank(model.getIp())) model.setIp(request.getRemoteAddr());
		operateLogServiceImp.insertOperateLog(model);
		
	}
	
	
}
