package com.dream.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * 处理session超时的拦截器
 */
@Component
public class UTF8Fiter implements HandlerInterceptor, HandlerExceptionResolver {

	public String[] allowUrls;

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse arg1, Object arg2, Exception arg3) throws Exception {
		String requestUrl = request.getRequestURI().replace(request.getContextPath(), "");
		request.setCharacterEncoding("UTF-8");
		arg1.setCharacterEncoding("UTF-8");
		arg1.setContentType("text/html;charset=UTF-8");
		System.out.println("------------------" + requestUrl + "------------------");
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub
		arg0.setCharacterEncoding("UTF-8");
		arg1.setCharacterEncoding("UTF-8");
		arg1.setContentType("text/html;charset=UTF-8");
		System.out.println("------------------postHandle------------------");
	}

	@Override
	public boolean preHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2) throws Exception {
		// TODO Auto-generated method stub
		arg0.setCharacterEncoding("UTF-8");
		arg1.setCharacterEncoding("UTF-8");
		arg1.setContentType("text/html;charset=UTF-8");
		System.out.println("-------------" + arg0.getRequestURI() + "----------------");
		System.out.println("------------------preHandle------------------");
		 
		// System.out.println("------------------preHandle------------------");
		return true;
	}

	public String[] getAllowUrls() {
		return allowUrls;
	}

	public void setAllowUrls(String[] allowUrls) {
		this.allowUrls = allowUrls;
	}

	@Override
	public ModelAndView resolveException(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3) {
		// TODO Auto-generated method stub
		arg3.printStackTrace();
		System.out.println("------------------resolveException------------------");
		return new ModelAndView("/index.jsp");
	}

}