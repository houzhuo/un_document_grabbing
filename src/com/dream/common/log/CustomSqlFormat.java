package com.dream.common.log;

import org.apache.commons.lang3.StringUtils;

import com.p6spy.engine.spy.appender.MessageFormattingStrategy;
/**
 * 打印，处理SQL语句
 * @author zlj
 *
 */
public class CustomSqlFormat implements MessageFormattingStrategy {
	String sql = "";

	/**
	 * 处理sql语句
	 */
	@Override
	public String formatMessage(int connectionId, String now, long elapsed, String category, String arg4, String arg5) {

		//过滤select
		if (arg5.trim().toLowerCase().startsWith("select")) {
			// return arg5+";\r\n" ;
			return "";// 过滤select
		}
		if (sql.equals(arg5)) {
			return "";
		}

		//过滤用户角色菜单表的操作
		String[] partSql = arg5.trim().toLowerCase().split(" ");
		if ("insert".equals(partSql[0]) || "delete".equals(partSql[0])) {
			if (checkTableNames(partSql[2])) {
				return "";
			}
		}
		if ("update".equals(partSql[0])) {
			if (checkTableNames(partSql[1])) {
				return "";
			}
		}
		//如果arg为空，不返回数据
		if(StringUtils.isEmpty(arg5)){
			return "";
		}
		if(arg5.trim().endsWith(";")){
			return arg5+"\r\n";
		}
		sql = arg5;
		return arg5 + ";\r\n";
	}

	/**
	 * 判断表名是否是用户，角色，菜单及他们的中间表
	 * @param name表名
	 * @return
	 */
	private boolean checkTableNames(String name) {
		if ("system_menu".equals(name) || "system_role".equals(name) || "system_role_menu".equals(name)
				|| "system_user".equals(name) || "system_user_role".equals(name)) {
			return true;
		}
		return false;
	}

}
