package com.dream.common.log;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.HTMLLayout;
import org.apache.log4j.spi.LoggingEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.dream.common.date.DateTools;
import com.dream.model.System_Log;
import com.hxtt.b.co;

@Component
@SuppressWarnings("all")
public class Log4jFormatHTMLLayout extends HTMLLayout {

	private System_Log log;

	private static BasicDataSource dataSource;
	private static Connection conn = null;
	private static Statement stmt = null;

	public Log4jFormatHTMLLayout() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String format(LoggingEvent event) {
		// TODO Auto-generated method stub

		try {
			
			if (event.getMessage() != null && !"".equals(String.valueOf(event.getMessage()).trim())) {
				log = new System_Log();
				if (stmt == null) {
					setDataSource();
					if (stmt == null) {
						return "";
					}
				}
				String msg = String.valueOf(event.getMessage());
				String level = event.getLevel().toString();
				if(!"error".equals(level.toLowerCase())) {
					return super.format(event);
				}
				msg = msg.replaceAll("'", "''");
				String uuid = UUID.randomUUID().toString().replaceAll("-", "");
				String sql = "insert into system_log(table_id,createTime,showTime,className,message,level,lineNumber,methodName) values (" 
							+ " '" + uuid + "'," + DateTools.getCurrentDateLong() + ",'" + DateTools.getCurrentDateTime() + "','" + event.getLocationInformation().getClassName() + "', " 
							+ " '" + msg + "','" + event.getLevel().toString() + "','" + event.getLocationInformation().getLineNumber() + "', " 
							+ " '" + event.getLocationInformation().getMethodName() + "') ";
				System.out.println(sql);
				stmt.execute(sql);

				return super.format(event);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return "";
	}

	private void setDataSource() {
		try {
			WebApplicationContext wac = ContextLoader.getCurrentWebApplicationContext();
			if (wac == null) {
				return;
			}
			dataSource = wac.getBean(BasicDataSource.class);
			conn = dataSource.getConnection();
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
