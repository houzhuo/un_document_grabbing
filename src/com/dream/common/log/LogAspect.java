package com.dream.common.log;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.dream.common.annotation.AspectLogInfo;
import com.dream.common.annotation.OperationAssist;
import com.dream.common.date.DateTools;
import com.dream.model.Operation_Log;
import com.dream.model.System_User;
import com.dream.system.service.OperateLogService;
import com.sun.org.apache.xml.internal.utils.ObjectPool;

@Aspect
@Component
@SuppressWarnings("all")
public class LogAspect {

	@Autowired
	private OperateLogService operateLogServiceImp;

	@Pointcut("execution(public * com.dream.*.*.*.*(..))")
	private void controllerAspect() {
	}

	@Around(value = "controllerAspect()")
	public Object timeAround(ProceedingJoinPoint joinPoint) throws Exception {
		// 定义返回对象、得到方法需要的参数
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes();
		HttpServletRequest request = requestAttributes.getRequest();
		Object obj = null;
		Object[] args = joinPoint.getArgs();
		long startTime = System.currentTimeMillis();
		boolean mark = true;
		try {
			obj = joinPoint.proceed(args);
		} catch (Throwable e) {
			e.printStackTrace();
			mark = false;
		}

		long endTime = System.currentTimeMillis();
		String path = request.getServletContext().getRealPath("config") + File.separatorChar + "operationLog.xml";
		// 读取配置文件，判断是否开启系统日志功能

		MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();

		Method method = methodSignature.getMethod();
		AspectLogInfo log = method.getAnnotation(AspectLogInfo.class);
		System_User user = (System_User) request.getSession().getAttribute("userLoginInfo");

		if (log != null && user != null) {

			Operation_Log model = new Operation_Log();

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");

			model.setCreateTime(DateTools.getCurrentDateLong());
			model.setShowTime(DateTools.getCurrentDateTime());
			model.setOperationContent(log.content());
			model.setOperationType(log.type());
			model.setTableName(log.tableName());
			model.setUserName(user.getUserName());
			
			model.setTable_id(UUID.randomUUID().toString().replaceAll("-", ""));
			model.setStartTime(format.format(new Date(startTime)));
			model.setEndTime(format.format(new Date(endTime)));
			model.setProceedTime(String.valueOf(endTime - startTime));
			if (log.assist()) {
				String clazs = log.claz();
				List<String> list = new ArrayList<>();
				if (!log.isUseResult()) {
					if (!"".equals(clazs.trim())) {
						clazs = clazs.trim();
						List<String> clazList = new ArrayList<>();
						if (clazs.contains(";")) {
							clazList.addAll(Arrays.asList(clazs.split(";")));
						} else {
							clazList.add(clazs);
						}
						list = getAllAssistContents(args, clazList);
						String contents = dealtAssistList(list);
						model.setAssistContents(contents);
					}

				} else {
					getAssistContents(obj, list);
					String contents = dealtAssistList(list);
					model.setAssistContents(contents);
				}

			}
			if (!mark) {
				model.setOperationResult("失败");
			} else {
				model.setOperationResult("成功");
			}

			if ("1".equals(user.getTable_id())) {
				model.setRoleName("admin");
			} else {
				model.setRoleName(user.getString2());
			}

			if (StringUtils.isBlank(model.getIp()))
				model.setIp(request.getRemoteAddr());

			operateLogServiceImp.insertOperateLog(model);

		}

		return obj;
	}

	private String dealtAssistList(List<String> list) {
		String contents = "";
		if (list == null || list.size() == 0) {
			return contents;
		}

		for (int i = 0; i < list.size(); i++) {
			contents += list.get(i);
		}

		return contents;
	}

	private List<String> getAllAssistContents(Object[] objects, List<String> clazList) {
		List<String> res = new ArrayList<>();
		for (String claz : clazList) {
			for (Object ob : objects) {
				if (ob.getClass().getName().equals(claz)) {
					getAssistContents(ob, res);
					break;
				}
			}
		}
		return res;
	}

	private void getAssistContents(Object ob, List<String> res) {
		if (ob == null) {
			return;
		}
		List<Field> list = new ArrayList<>();
		Class<?> claz = ob.getClass();

		list.addAll(Arrays.asList(claz.getDeclaredFields()));

		for (Field f : list) {
			OperationAssist oa = f.getAnnotation(OperationAssist.class);
			if (oa != null && oa.objType() != null && !"".equals(oa.objType())) {
				if (oa.objType().equals(OperationAssistSystemContants.ARRAY)) {
					res.add(oa.desc() + "->{");
					dealListObject(ob, f, res);
					res.add("}");
				} else if (oa.objType().equals(OperationAssistSystemContants.ONE)) {
					res.add("[");
					getAssistContents(ClassUtils.invokeGet(ob, f.getName()), res);
					res.add("]");
				} else if (oa.objType().equals(OperationAssistSystemContants.NORMAL)) {
					res.add(oa.desc().trim() + "->" + ClassUtils.invokeGet(ob, f.getName()) + " ");
				}
			}
		}
	}

	private void dealListObject(Object ob, Field field, List<String> result) {
		List list = (List) ClassUtils.invokeGet(ob, field.getName());
		if (list == null) {
			return;
		}
		for (Object res : list) {
			getAssistContents(res, result);
		}
	}
}
