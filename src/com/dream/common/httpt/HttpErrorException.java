package com.dream.common.httpt;


public class HttpErrorException extends Exception {
	
	private static final long serialVersionUID = 3414898155785375151L;
	
	public HttpErrorException(String message){
		super(message);
	}
}
