package com.dream.system.action;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dream.common.JsonString;
import com.dream.common.SuperController;
import com.dream.model.System_Permission;
import com.dream.model.System_Role;
import com.dream.model.System_User;
import com.dream.system.service.PermissionService;
import com.dream.system.service.UserService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 用户Controller
 * @author zlj
 *
 */
@Controller
@RequestMapping("/user/userAction.do")
@SuppressWarnings("all")
public class UserAction extends SuperController{

	@Autowired
	private UserService userServiceImpl;
	
	@Autowired
	private PermissionService permissionService;
	/**
	 * 获取所有用户数据(分页)
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=getAllUser")
	public void getAllUser(System_User model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		Map<String, Object>  map = userServiceImpl.getAllUser(model);
		
		JsonConfig config = new JsonConfig();
		config.setExcludes(JsonString.sys_user);
		
		response.getWriter().write(JSONObject.fromObject(map, config).toString());
		response.getWriter().close();
		response.getWriter().flush();
		
	}
	
	/**
	 * 添加或修改用户
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=addOrUpdateUser")
	public void addOrUpdateUser(System_User model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONObject obj = new JSONObject();
		try{
			if(StringUtils.isEmpty(model.getTable_id())){
				userServiceImpl.addUser(model);
			}else{
				userServiceImpl.updateUser(model);
			}
			obj.put("status", 1);
		}catch (Exception e) {
			obj.put("status", 0);
			e.printStackTrace();
		}finally{
			response.getWriter().write(obj.toString());
			response.getWriter().close();
			response.getWriter().flush();			
		}
	}
	
	/**
	 * 删除用户
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=deleteUser")
	public void deleteUser(System_User model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONObject obj = new JSONObject();
		try {
			userServiceImpl.deleteUser(model);
			obj.put("status", 1);
		} catch (Exception e) {
			e.printStackTrace();
			obj.put("status", 0);
		}finally {
			response.getWriter().write(obj.toString());
			response.getWriter().close();
			response.getWriter().flush();	
		}
	}
	
	/**
	 * 获取所有用户
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=getAllUsers")
	public void getAllUsers(HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONArray json = userServiceImpl.getAllUsers();
		response.getWriter().write(json.toString());
		response.getWriter().close();
		response.getWriter().flush();
	}
	
	/**
	 * 获取用户权限数据
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=getUserMenuJsonArr")
	public void getUserMenuJsonArr(HttpServletRequest request,HttpServletResponse response) throws IOException{
		System_User user = (System_User) request.getSession().getAttribute("loginUser");
		//未登录
		if(user == null){
			response.getWriter().write(new JSONArray().toString());
			response.getWriter().close();
			response.getWriter().flush();
			return;
		}
		JSONArray jsonMenu = new JSONArray();
		//超级管理员
		if("1".equals(user.getTable_id())){
			jsonMenu = permissionService.getAdminMenuJsonArr();
		}else{//普通用户
			jsonMenu = permissionService.getUserMenuJsonArr(user);
		}
		JSONObject obj = new JSONObject();
		JSONArray powBtn = permissionService.getAdminBtnJsonArr();
		obj.put("initMenu", jsonMenu);
		obj.put("powBtn", powBtn);
		response.getWriter().write(obj.toString());
		response.getWriter().close();
		response.getWriter().flush();
		
	}
	
	@RequestMapping(params = "method=updatePassword")
	public void updatePassword(System_User model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONObject obj = new JSONObject();
		try {
			int i = userServiceImpl.updatePassword(model);
			obj.put("status", i);
		}catch (Exception e) {
			obj.put("status", 0);
			e.printStackTrace();
		}finally {
			response.getWriter().write(obj.toString());
			response.getWriter().close();
			response.getWriter().flush();
		}
	}
	
}
