package com.dream.system.action;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dream.common.SuperController;
import com.dream.model.System_Organization;
import com.dream.model.System_Permission;
import com.dream.system.service.OrganizationService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 机构 Controller
 * @author zlj
 *
 */
@Controller
@RequestMapping("/organization/organizationAction.do")
@SuppressWarnings("all")
public class OrganizatinAction extends SuperController{

	@Autowired
	private OrganizationService organizationServiceImpl;
	
	/**
	 * 获取机构所有数据
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=getAllOrganization")
	public void getAllOrganization(System_Organization model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		Map<String, Object>  map = organizationServiceImpl.getAllOrganization(model);
		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[]{"listop","listuo","sys_user","listFileOrga"});
		response.getWriter().write(JSONObject.fromObject(map, config).toString());
		response.getWriter().close();
		response.getWriter().flush();
	}
	
	/**
	 * 获取机构tree数据
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=getOrganizationJsonData")
	public void getOrganizationJsonData(String table_id,HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONArray json = organizationServiceImpl.getOrganizationJsonData(table_id);
		response.getWriter().write(json.toString());
		response.getWriter().close();
		response.getWriter().flush();
	}
	
	/**
	 * 添加或修改机构
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=addOrUpdateOrganization")
	public void addOrUpdateOrganization(System_Organization model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONObject obj = new JSONObject();
		try{
			if(StringUtils.isEmpty(model.getTable_id())){
				organizationServiceImpl.addOrganization(model);
			}else{
				organizationServiceImpl.updateOrganization(model);
			}
			obj.put("status", 1);
		}catch (Exception e) {
			obj.put("status", 0);
			e.printStackTrace();
		}finally{
			response.getWriter().write(obj.toString());
			response.getWriter().close();
			response.getWriter().flush();			
		}
	}
	
	/**
	 * 删除机构
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=deleteOrganization")
	public void deleteOrganization(String table_id,HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONObject obj = new JSONObject();
		try {
			organizationServiceImpl.deleteOrganization(table_id);
			obj.put("status", 1);
		}catch (Exception e) {
			e.printStackTrace();
			obj.put("status", 0);
		}finally {
			response.getWriter().write(obj.toString());
			response.getWriter().close();
			response.getWriter().flush();
		}
	}
	
	/**
	 * 获取机构负责人
	 * @param table_id 机构id
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=getOrgaPrincipal")
	public void getOrgaPrincipal(String table_id,HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONObject obj = organizationServiceImpl.getOrgaPrincipal(table_id);
		response.getWriter().write(obj.toString());
		response.getWriter().close();
		response.getWriter().flush();
	}
}
