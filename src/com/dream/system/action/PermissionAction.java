package com.dream.system.action;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dream.common.SuperController;
import com.dream.model.System_Permission;
import com.dream.system.service.PermissionService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 权限controller
 * @author zlj
 *
 */
@Controller
@RequestMapping("/permission/permissionAction.do")
@SuppressWarnings("all")
public class PermissionAction extends SuperController{

	@Autowired
	private PermissionService permissionServiceImpl;
	
	/**
	 * 获取所有权限数据
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=getAllPermission")
	public void getAllPermission(System_Permission model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		Map<String, Object>  map = permissionServiceImpl.getAllPermission(model);
		
		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[]{"listrp","listup","listop"});
		response.setCharacterEncoding("utf-8");
		response.getWriter().write(JSONObject.fromObject(map, config).toString());
		response.getWriter().close();
		response.getWriter().flush();
		
	}
	
	/**
	 * 获取权限tree数据
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=getMenuJsonData")
	public void getMenuJsonData(String type,String table_id,HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONArray json = permissionServiceImpl.getPermissionJsonData(type,table_id);
		response.getWriter().write(json.toString());
		response.getWriter().close();
		response.getWriter().flush();
	}
	
	/**
	 * 修改权限
	 * @throws IOException 
	 */
	@RequestMapping(params = "method=addOrUpdatePermission")
	public void addOrUpdatePermission(System_Permission model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONObject obj = new JSONObject();
		try{
			permissionServiceImpl.updatePermission(model);
			obj.put("status", 1);
		}catch (Exception e) {
			obj.put("status", 0);
			e.printStackTrace();
		}finally{
			response.getWriter().write(obj.toString());
			response.getWriter().close();
			response.getWriter().flush();			
		}
	}
}
