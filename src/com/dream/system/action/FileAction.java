package com.dream.system.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.FutureTask;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.dream.common.SuperController;
import com.dream.common.file2pdf.JacobUtils;
import com.dream.common.upload.BigFileUploadUtil;
import com.dream.file.PullMain;
import com.dream.model.File_Entity;
import com.dream.model.Reptile_Record;
import com.dream.system.service.FileService;
import com.itextpdf.text.pdf.PushbuttonField;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

@Controller
@SuppressWarnings("all")
@RequestMapping("file/fileAction.do")
public class FileAction extends SuperController {

	@Autowired
	private FileService fileServiceImp;
	
	/**
	 * 查询上传文件列表
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=getFilesList")
	public void getFilesList(File_Entity model, HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String,Object> map=new HashMap<String,Object>();
		try {
			map = fileServiceImp.getFileList(model);
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(new String[]{"listur", "listCompact", "listInstrument", 
				"listStandard", "listQuestionInfo","listGenre","listCase", "listClient", "listLiterature", 
				"listTemplateInfo", "listAskQuestion", "listProjectInfo"});
		super.outData(response, map, jsonConfig);
	}
	@RequestMapping(params = "method=getRRList")
	public void getRRList(Reptile_Record record ,HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String,Object> map=new HashMap<String,Object>();
		map = fileServiceImp.getReptileRecords(record);
		super.outData(response, map);
	}
	/**
	 * 更新文件状态，0显示，1不显示
	 * @param request
	 * @param response
	 */
	@RequestMapping(params = "method=updateFileStatus", method = RequestMethod.POST)
	public void updateFileStatus(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			JSONArray jsonArray = JSONArray.fromObject(request.getParameter("data"));
			List<File_Entity> list = new ArrayList<File_Entity>();
			if(null != jsonArray && jsonArray.size() > 0) {
				for(int i=0; i< jsonArray.size(); i++) {
					 JSONObject jsonObject2 = JSONObject.fromObject(jsonArray.get(i));
					 File_Entity model = (File_Entity)JSONObject.toBean(jsonObject2, File_Entity.class);
					 fileServiceImp.updateFileStatus(model);
				}
			}
				map.put("msg", "1");
		}catch (Exception e) {
			// TODO: handle exception
			map.put("msg", "0");
		}
		super.outData(response, map);
	}
	
	@Deprecated
	@RequestMapping(params = "method=deleteFiles")
	public void deleteFiles(File_Entity model,HttpServletRequest request, HttpServletResponse response) {
		try {
			fileServiceImp.deleteFile(model);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 下载文件
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params="method=downLoadFile")
	public void downLoadFile(File_Entity model,HttpServletRequest request, HttpServletResponse response)throws IOException {
		File_Entity fileEntity = fileServiceImp.getFileById(model);
		if(fileEntity == null){
			return;
		}
		String filePath = BigFileUploadUtil.getBasePath() + File.separator + fileEntity.getPath();
		System.out.println(filePath);
		try{
			String fileName = fileEntity.getFileName();
				
			InputStream in = new FileInputStream(new File(filePath));
			 // 设置输出的格式
		    response.reset();
		    response.setContentType("multipart/form-data");	
		    response.addHeader("Content-Disposition", "attachment;filename="
			    + URLEncoder.encode(fileName, "UTF-8"));
		    byte[] buffer = new byte[1024];
		    int len = 0;
		    while ((len = in.read(buffer)) != -1) {
		    	response.getOutputStream().write(buffer, 0, len);
			}
		    response.getOutputStream().flush();
		    response.getOutputStream().close();
		    in.close();
		}catch(FileNotFoundException e){
			e.printStackTrace();
			
			response.getWriter().write("文件不存在,请联系管理员！");
			response.getWriter().close();
			response.getWriter().flush();
            /*response.reset();
             try {
                OutputStreamWriter writer = new OutputStreamWriter(response.getOutputStream(), "UTF-8");  
                String data = "<script language='javascript'>alert(\"文件不存在,请联系管理员！\");</script>";
                writer.write(data); 
                writer.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }*/
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			
		}
	}
	
	@RequestMapping(params = "method=loadPdfFile")
	public void loadPdfFile(File_Entity model,HttpServletRequest request,HttpServletResponse response) throws Exception{
		File_Entity file = fileServiceImp.getFileById(model);
		if(file == null){
			outData(response, "没有查询到数据!");
			return;
		}
		String resourceFilePath = BigFileUploadUtil.getBasePath() + File.separator + file.getPath();
		if(!"pdf".equals(file.getFileType().toLowerCase())){
			resourceFilePath = JacobUtils.file2Pdf(resourceFilePath);
		}
		try{
			InputStream in = new FileInputStream(new File(resourceFilePath));
			 // 设置输出的格式
		    response.reset();
		    response.setContentType("multipart/form-data");	
		    response.addHeader("Content-Disposition", "attachment;filename="
			    + URLEncoder.encode(file.getFileName(), "UTF-8"));
		    byte[] buffer = new byte[1024];
		    int len = 0;
		    while ((len = in.read(buffer)) != -1) {
		    	response.getOutputStream().write(buffer, 0, len);
			}
		    response.getOutputStream().flush();
		    response.getOutputStream().close();
		    in.close();
		}catch (Exception e) {
			// TODO: handle exception
			outData(response, "没有查询到数据!");
		}
	}
	
	@RequestMapping(params = "method=getFilesByDocId")
	public void getFilesByDocId(@RequestParam("docId") String docId,HttpServletRequest request,HttpServletResponse response) throws IOException{
		List<File_Entity> list = fileServiceImp.getFilesByIds(docId);
		outData(response, JSONArray.fromObject(list));
	}
	
	@RequestMapping(params = "method=zipFiles")
	public void zipFiles(@RequestParam("ids") String ids,HttpServletRequest request,HttpServletResponse response){
		try{
			
			List<File_Entity> list = fileServiceImp.getFilesByIds(ids);
			
			if(list != null && list.size() > 0){
				byte[] buffer = new byte[1024];
				String zipFileName = UUID.randomUUID().toString().replaceAll("-", "") + ".zip";
				File zipFile = new File(BigFileUploadUtil.getBasePath() + File.separator + zipFileName );
				FileOutputStream fileOutputStream = new FileOutputStream(zipFile);
				ZipOutputStream zipops = new ZipOutputStream(fileOutputStream);
				for(File_Entity e:list){
					File f = new File(BigFileUploadUtil.getBasePath() + e.getPath());
					if (!f.exists()) {
						continue;
					}
					
					FileInputStream fis = new FileInputStream(f);
					// 创建ZIP实体，并添加进压缩包
					ZipEntry zipEntry = new ZipEntry(e.getFileName()+"/"+e.getFileJobID() + "_" + e.getFileLanguage() +"."+e.getFileType());
					zipops.putNextEntry(zipEntry);
					int len = 0;
					while ((len = fis.read(buffer)) != -1) {
						zipops.write(buffer, 0, len);
					}
					zipops.flush();
					zipops.closeEntry();
					fis.close();
				}
				fileOutputStream.close();
				
				InputStream in = new FileInputStream(zipFile);
				 // 设置输出的格式
			    response.reset();
			    response.setContentType("multipart/form-data");	
			    response.addHeader("Content-Disposition", "attachment;filename="
				    + URLEncoder.encode(zipFileName, "UTF-8"));
			    int len = 0;
			    while ((len = in.read(buffer)) != -1) {
			    	response.getOutputStream().write(buffer, 0, len);
				}
			    response.getOutputStream().flush();
			    response.getOutputStream().close();
			    in.close();
			    zipFile.delete();
			}
			
		    
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@RequestMapping(params="method=reptileRemoteData")
	public void reptileRemoteData(@RequestParam("startDate") String startDate,@RequestParam("endDate") String endDate,
					HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONObject json = new JSONObject();
		try{
			long d = 1000 * 60 * 60 * 24;
			
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			long startLong= format.parse(startDate).getTime();
			long endLong = format.parse(endDate).getTime();
			List<String> dateList = new ArrayList<>();
			long max = (endLong - startLong)/d;
			for(int i = 0;i <= max;i++){
				String tempDate = format.format(new Date(startLong + i * d));
				if(!PullMain.searchDateQueue.contains(tempDate)){
					PullMain.searchDateQueue.add(tempDate);
					
					Reptile_Record rr = new Reptile_Record();
					rr.setStartDate(tempDate);
					rr.setEndDate(tempDate);
					
					PullMain.insertQueue.add(rr);
				}
			}
			json.put("msg", "请耐心等待后台正在爬取数据!");
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			json.put("msg", "后台异常，请稍后再试!");
		}
		outData(response, json);
	}
}
