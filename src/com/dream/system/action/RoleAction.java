package com.dream.system.action;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dream.model.System_Organization;
import com.dream.model.System_Role;
import com.dream.model.System_User;
import com.dream.system.service.RoleService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 角色Controller
 * @author zlj
 *
 */
@Controller
@RequestMapping("/role/roleAction.do")
@SuppressWarnings("all")
public class RoleAction {

	@Autowired
	private RoleService roleServiceImpl;
	
	/**
	 * 获取所有角色数据
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=getAllRole")
	public void getAllRole(System_Role model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		Map<String, Object>  map = roleServiceImpl.getAllRole(model);
		
		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[]{"listur","listrp","listFileRole"});
		
		response.getWriter().write(JSONObject.fromObject(map, config).toString());
		response.getWriter().close();
		response.getWriter().flush();
		
	}
	
	/**
	 * 添加或修改角色
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=addOrUpdateRole")
	public void addOrUpdateRole(System_Role model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONObject obj = new JSONObject();
		try{
			if(StringUtils.isEmpty(model.getTable_id())){
				roleServiceImpl.addRole(model);
			}else{
				roleServiceImpl.updateRole(model);
			}
			obj.put("status", 1);
		}catch (Exception e) {
			obj.put("status", 0);
		}finally{
			response.getWriter().write(obj.toString());
			response.getWriter().close();
			response.getWriter().flush();			
		}
	}
	
	/**
	 * 获取角色tree数据
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=getRoleJsonData")
	public void getRoleJsonData(String table_id,HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONArray json = roleServiceImpl.getRoleJsonData(table_id);
		response.getWriter().write(json.toString());
		response.getWriter().close();
		response.getWriter().flush();
	}
	
	/**
	 * 删除角色
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=deleteRole")
	public void deleteRole(System_Role model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		JSONObject obj = new JSONObject();
		try {
			roleServiceImpl.deleteRole(model);
			obj.put("status", 1);
		}catch (Exception e) {
			e.printStackTrace();
			obj.put("status", 0);
		}finally {
			response.getWriter().write(obj.toString());
			response.getWriter().close();
			response.getWriter().flush();
		}
	}
}
