package com.dream.system.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dream.common.SuperController;
import com.dream.common.annotation.AspectLogInfo;
import com.dream.common.export.ExportExcel;
import com.dream.model.Operation_Log;
import com.dream.system.service.OperateLogService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 
* <p>Title: OperationLogAction</p>
* <p>Description: 操作日期控制层，负责转发调用给业务处理层，并且把结果返回给前台</p>
* @author z.q.k
* @date 上午10:35:18
 */
@SuppressWarnings("all")
@Controller
@RequestMapping("operationLog/operationLogAction.do")
public class OperationLogAction extends SuperController {

	@Autowired
	private OperateLogService operateLogServiceImp;
	
	/**
	 * 查询操作日志，封装分页
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=getOperationLogList")
	public void getOperationLogList(Operation_Log model, HttpServletRequest request, HttpServletResponse response) throws IOException{
		Map<String, Object> map = new HashMap<String, Object>(); 
		JsonConfig jsonConfig = new JsonConfig();
		String roleTag=request.getParameter("roleTag");
		try {
			model.setTemp3(roleTag);
			map = operateLogServiceImp.getOperateLists(model);
			jsonConfig.setExcludes(new String[]{"listur", "listCompact", "listInstrument", 
					"listStandard", "listQuestionInfo","listGenre","listCase", "listClient", "listLiterature", 
					"listTemplateInfo", "listAskQuestion", "listProjectInfo"});
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		super.outData(response, map, jsonConfig);
	}
	
	/**
	 * 删除操作日志
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=deleteOperationLog")
	@AspectLogInfo(tableName="Operation_Log",content="deleteOperationLog:删除日志",type="删除")
	public void deleteOperationLog(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		Map<String, Object> map=new HashMap<String, Object>();
		try {
			JSONArray jsonArray = JSONArray.fromObject(request.getParameter("data"));
			List<Operation_Log> list = new ArrayList<Operation_Log>();
			if(null != jsonArray && jsonArray.size() > 0) {
				for(int i=0; i< jsonArray.size(); i++) {
					 JSONObject jsonObject2 = JSONObject.fromObject(jsonArray.get(i));
					 Operation_Log model = (Operation_Log)JSONObject.toBean(jsonObject2, Operation_Log.class);
					 list.add(model);
				}
			}
			operateLogServiceImp.deleteOperateLogs((list));
			map.put("msg", "1");
		}catch (Exception e) {
			// TODO: handle exception
			map.put("msg", "0");
		}
		super.outData(response, map);
		
	}
	
	/**
	 * 导出日志Excel数据
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	@RequestMapping(params = "method=importExcelDatas")
	@AspectLogInfo(tableName="Operation_Log",content="importExcelDatas:导出日志",type="下载")
	public void importExcelDatas(Operation_Log model, HttpServletRequest request, HttpServletResponse response) throws IOException{
		List<String> listTitle = new ArrayList<String>();
		listTitle.add("操作人名称");
		listTitle.add("操作人工号");
		listTitle.add("操作角色");
		listTitle.add("操作类型");
		listTitle.add("操作表");
		listTitle.add("操作内容");
		listTitle.add("操作结果");
		listTitle.add("操作时间");
		listTitle.add("ip地址");
		List<String> listFields = new ArrayList<String>();
		listFields.add("userName");
		listFields.add("userEmployeeId");
		listFields.add("roleName");
		listFields.add("operationType");
		listFields.add("tableName");
		listFields.add("operationContent");
		listFields.add("operationResult");
		listFields.add("proceedTime");
		listFields.add("ip");
		
		List<Object> listDatas = operateLogServiceImp.getExportData(model);
		ExportExcel.exportFile(listTitle,listFields, listDatas,response);
	}
}
