package com.dream.system.action;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.dream.common.SuperController;
import com.dream.common.annotation.AspectLogInfo;
import com.dream.common.upload.BigFileUploadUtil;
import com.dream.model.System_Log;
import com.dream.system.service.SystemService;
import java.text.ParseException;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/systemAction/systemAction.do")
@SuppressWarnings("all")
public class SystemAction extends SuperController{

	@Autowired
	private SystemService systemService;
	
	@RequestMapping(params = "method=uploadPartFile")
	@ResponseBody
	public void uploadPartFile(@RequestParam("file") MultipartFile file ,HttpServletRequest request,
			HttpServletResponse response) throws IllegalStateException, IOException, ServletException{
		response.setContentType("text/html;charset=UTF-8");
		response.setHeader("Access-Control-Allow-Origin", "*");
		BigFileUploadUtil.uploadPartFile(request,file);
	}
	
	@RequestMapping(params = "method=meragePartFile")
	@ResponseBody
	@AspectLogInfo(tableName="",content="meragePartFile：上传文件",type="合并")
	public void meragePartFile(HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		response.setHeader("Access-Control-Allow-Origin", "*");
		List<JSONObject> list = BigFileUploadUtil.meragePartFile(request, false);
		outData(response, JSONArray.fromObject(list));
	}
	
	@RequestMapping(params = "method=getSystemLog")
	@AspectLogInfo(tableName="system_log",content="getSystemLog：获取系统运行日志",type="查询")
	public void getSystemLog(System_Log model,HttpServletRequest request,HttpServletResponse response) throws ParseException, IOException {
		Map<String, Object> map = systemService.getSystemLog(model);
		outData(response, map);
	}
	
	@RequestMapping(params = "method=targetMain")
	public String targetMain(HttpServletRequest request,HttpServletResponse response){
		return "web/index";
	}
}
