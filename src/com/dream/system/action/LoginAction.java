package com.dream.system.action;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.dream.common.Common;
import com.dream.common.JsonString;
import com.dream.common.MD5;
import com.dream.common.SuperController;
import com.dream.model.System_User;
import com.dream.system.service.PermissionService;
import com.dream.system.service.UserService;

/**
 * 登录Controller
 * @author zlj
 *
 */
@Controller
@RequestMapping("/login/loginAction.do")
@SuppressWarnings("all")
public class LoginAction extends SuperController {
	@Autowired
	private UserService userServiceImpl;
	
	@Autowired
	private PermissionService permissionServiceImpl;

	/**
	 * 登录
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=loginSystem")
	public void loginSystem(System_User model,HttpServletRequest request,HttpServletResponse response) throws IOException{
		model.setPassWord(MD5.getMD5(model.getPassWord().getBytes()));
		System_User user = userServiceImpl.checkUserExist(model);
		String i = "0";
		if(user!=null){
			request.getSession().setAttribute("loginUser", user);
			i = "1";
		}else{
			if("admin".equals(model.getUserName()) && MD5.getMD5("admin".getBytes()).equals(model.getPassWord())){
				System_User us = new System_User();
				us.setUserName("admin");
				us.setTable_id("1");
				request.getSession().setAttribute("loginUser", us);
				i = "1";
			}
		}
		JSONObject json = new JSONObject();
		json.put("status", i);
		response.getWriter().write(json.toString());
		response.getWriter().close();
		response.getWriter().flush();
	}
	
	/**
	 * 登出
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(params = "method=logoutSys")
	public void logoutSys(HttpServletRequest request ,HttpServletResponse response) throws IOException {
		JSONObject obj = new JSONObject();
		try {
			request.getSession().removeAttribute("loginUser");
			obj.put("status", "1");
		}catch (Exception e) {
			obj.put("status", "0");
			e.printStackTrace();
		}finally {
			response.getWriter().write(obj.toString());
			response.getWriter().close();
			response.getWriter().flush();
		}
	}
}
