package com.dream.system.service;

import java.util.Map;

import com.dream.model.System_Organization;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 机构service
 * @author zlj
 *
 */
public interface OrganizationService {

	/**
	 * 获取所有的机构数据
	 * @param model
	 * @return
	 */
	public Map<String,Object> getAllOrganization(System_Organization model);

	/**
	 * 获取机构tree数据
	 * @param table_id 用户id
	 * @return
	 */
	public JSONArray getOrganizationJsonData(String table_id);

	/**
	 * 添加机构
	 * @param model
	 */
	public void addOrganization(System_Organization model);

	/**
	 * 修改机构
	 * @param model
	 */
	public void updateOrganization(System_Organization model);

	/**
	 * 删除机构
	 * @param model
	 */
	public void deleteOrganization(String table_id);

	/**
	 * 根据机构ids获取tree数据
	 * @param orgas 需要选中的id
	 * @return
	 */
	public JSONArray getOrganizationTreeData(String orgas);

	/**
	 * 获取机构toolTip数据
	 * @param orgas 机构ids
	 * @return
	 */
	public String getOrgaToolTipData(String orgas);

	/**
	 * 获取机构负责人
	 * @param table_id 机构id
	 * @return
	 */
	public JSONObject getOrgaPrincipal(String table_id);
}
