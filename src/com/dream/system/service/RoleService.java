package com.dream.system.service;

import java.util.Map;

import com.dream.model.System_Role;

import net.sf.json.JSONArray;

/**
 * 角色Service
 * @author zlj
 *
 */
public interface RoleService {

	/**
	 * 获取所有角色
	 * @param model
	 * @return
	 */
	public Map<String,Object> getAllRole(System_Role model);

	/**
	 * 添加角色
	 * @param model
	 */
	public void addRole(System_Role model);

	/**
	 * 获取角色tree数据
	 * @param table_id 
	 * @return
	 */
	public JSONArray getRoleJsonData(String table_id);

	/**
	 * 修改角色
	 * @param model
	 */
	public void updateRole(System_Role model);

	/**
	 * 删除角色
	 * @param model
	 */
	public void deleteRole(System_Role model);

	/**
	 * 根据角色ids获取tree数据
	 * @param roles  需要选中的id
	 * @return
	 */
	public JSONArray getRoleTreeData(String roles);

	/**
	 * 获取角色toolTip数据
	 * @param roles 角色ids
	 * @return
	 */
	public String getRoleToolTipData(String roles);
}
