package com.dream.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.dream.common.SuperDao;
import com.dream.common.date.DateTools;
import com.dream.model.System_Organization;
import com.dream.model.System_Organization_Permission;
import com.dream.model.System_Permission;
import com.dream.model.System_User;
import com.dream.system.service.OrganizationService;
import com.hxtt.b.co;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 机构serviceImpl
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Component("organizationServiceImpl")
public class OrganizationServiceImpl extends SuperDao<Object> implements OrganizationService{

	/**
	 * 获取所有机构数据
	 */
	@Override
	public Map<String, Object> getAllOrganization(System_Organization model) {
		Map<String, Object> map = new HashMap<String, Object>();
		StringBuffer hql = new StringBuffer();
		hql.append(" from System_Organization t where 1 = 1 ");
		if(StringUtils.isNotEmpty(model.getOrganizationName())){
			hql.append(" and t.organizationName like '%"+model.getOrganizationName()+"%' ");
		}
		List<System_Organization> list = this.getList("select t "+hql.toString(), model.getStart(), model.getLimit(), "");
		int count = this.getListCount("select count(*) "+hql.toString(), "");
		int page = count % model.getLimit() == 0 ? count / model.getLimit() : count / model.getLimit() + 1;
		map.put("list", list);
		map.put("count", page);
		map.put("sum", count);
		return map;
	}
	
	/**
	 * 获取所有机构数据
	 */
	/*@Override
	public Map<String, Object> getAllOrganization(System_Organization model) {
		Map<String, Object> map = new HashMap<String, Object>();
		Criteria crit = this.sessionFactory.getCurrentSession().createCriteria(System_Organization.class);
		if (StringUtils.isNotEmpty(model.getOrganizationName())) {// 搜索
			crit.add(Restrictions.like("organizationName", "%" + model.getOrganizationName() + "%"));
		}
		
		int sum = crit.list().size();// 总数
		// 排序
		String order = model.getDesc().trim();
		if (order.endsWith("desc")) {
			crit.addOrder(Order.desc(order.substring(0, order.length() - 4).trim()));
		} else if (order.endsWith("asc")) {
			crit.addOrder(Order.asc(order.substring(0, order.length() - 3).trim()));
		} else {
			crit.addOrder(Order.desc("createTime"));
		}
		// 分页
		crit.setFirstResult(model.getStart());
		crit.setMaxResults(model.getLimit());
		List<Object> list = crit.list();
		int count = (int) Math.ceil((double) sum / (double) model.getLimit());// 页数
		map.put("list", list);
		map.put("count", count);
		map.put("sum", sum);
		return map;
	}*/


	/**
	 * 获取机构tree数据
	 */
	@Override
	public JSONArray getOrganizationJsonData(String table_id) {
		List<JSONObject> listObj = new ArrayList<JSONObject>();
		String hql = " from System_Organization t ";
		List<System_Organization> list = this.getList(hql, 0, 0, "");
		List<Object[]> listData = new ArrayList<Object[]>();
		if(StringUtils.isNotEmpty(table_id)){
			String hqlData = " from System_Organization t left join t.listuo t1 where t1.sys_user.table_id = '"+table_id+"'";
			listData = this.getList(hqlData, 0, 0, "");
		}
		if(list!=null&&list.size()>0){
			for (System_Organization orga : list) {
				JSONObject obj = new JSONObject();
				obj.put("table_id", orga.getTable_id());
				obj.put("cid",orga.getCid() );
				obj.put("name", orga.getOrganizationName());
				obj.put("pid", orga.getPid());
				obj.put("type", orga.getOrganizationType());
				if (listData != null && listData.size() > 0) {
					for (Object[] objs : listData) {
						System_Organization organization = (System_Organization) objs[0];
						if(organization.getTable_id().equals(orga.getTable_id())){
						obj.put("checked", true);
						}
					}	
				}
				listObj.add(obj);
			}
		}
			
		return JSONArray.fromObject(listObj);
	}

	/**
	 * 添加机构
	 */
	@Override
	public void addOrganization(System_Organization model) {
		String temp1 = model.getTemp1();
		String temp2 = model.getTemp2();
		model.setCid(UUID.randomUUID().toString().replaceAll("-", ""));
		model.setShowTime(DateTools.getCurrentDate());
		model.setCreateTime(DateTools.getCurrentDateLong());
		model.setTemp1("");
		model.setTemp2("");
		model.setSys_user((System_User)this.loadT(new System_User(), temp2));
		this.addT(model);
		
		//添加机构权限中间表
		if(StringUtils.isNotEmpty(temp1)){
			List<System_Permission> list =(List<System_Permission>) JSONArray.toCollection(JSONArray.fromObject(temp1), System_Permission.class);
			if(list!=null&&list.size()>0){
				for (System_Permission perm : list) {
					System_Organization_Permission sop = new System_Organization_Permission();
					sop.setSys_orga(model);
					sop.setSys_perm((System_Permission)this.loadT(perm, perm.getTable_id()));
					sop.setCreateTime(DateTools.getCurrentDateLong());
					sop.setShowTime(DateTools.getCurrentDate());
					this.addT(sop);
				}
			}
		}
		
	}

	/**
	 * 修改机构
	 */
	@Override
	public void updateOrganization(System_Organization model) {
		String temp1 = model.getTemp1();
		String temp2 = model.getTemp2();
		model.setTemp1("");
		model.setTemp2("");
		System_Organization organization = (System_Organization) this.loadT(model, model.getTable_id());
		BeanUtils.copyProperties(model, organization, new String[]{"table_id","cid","pid","createTime","listop","listuo","sys_user","listFileOrga"});
		organization.setShowTime(DateTools.getCurrentDate());
		organization.getListop().clear();
		organization.setSys_user((System_User)this.loadT(new System_User(), temp2));
		this.updateT(organization);
		//添加机构权限中间表
		if(StringUtils.isNotEmpty(temp1)){
			List<System_Permission> list =(List<System_Permission>) JSONArray.toCollection(JSONArray.fromObject(temp1), System_Permission.class);
			if(list!=null&&list.size()>0){
				for (System_Permission perm : list) {
					System_Organization_Permission sop = new System_Organization_Permission();
					sop.setSys_orga(model);
					sop.setSys_perm((System_Permission)this.loadT(perm, perm.getTable_id()));
					sop.setCreateTime(DateTools.getCurrentDateLong());
					sop.setShowTime(DateTools.getCurrentDate());
					this.addT(sop);
				}
			}
		}
	}

	/**
	 * 删除机构
	 */
	@Override
	public void deleteOrganization(String table_id) {
		System_Organization orga = (System_Organization) this.loadT(new System_Organization(), table_id);
		this.delete(orga);
	}

	/**
	 * 根据机构ids获取tree数据
	 */
	@Override
	public JSONArray getOrganizationTreeData(String orgas) {
		JSONArray listObj = new JSONArray();
		String[] orgaArr = null;
		if(StringUtils.isNotEmpty(orgas)){
			orgaArr = orgas.split(",");			
		}
		String hql = " from System_Organization t ";
		List<System_Organization> list = this.getList(hql, 0, 0, "");
		if(list!=null&&list.size()>0){
			for (System_Organization orga : list) {
				JSONObject obj = new JSONObject();
				obj.put("table_id", orga.getTable_id());
				obj.put("cid",orga.getCid() );
				obj.put("name", orga.getOrganizationName());
				obj.put("pid", orga.getPid());
				obj.put("type", orga.getOrganizationType());
				if (orgaArr != null && orgaArr.length > 0) {
					for (String orgaId : orgaArr) {
						if(orga.getTable_id().equals(orgaId)){
							obj.put("checked", true);
						}
					}	
				}
				listObj.add(obj);
			}
		}
		return listObj;
	}

	/**
	 * 获取机构toolTip数据
	 */
	@Override
	public String getOrgaToolTipData(String orgas) {
		String orgaTip = "";
		String[] orgaIds = null;
		if(StringUtils.isNotEmpty(orgas)){
			orgaIds = orgas.split(",");
		}
		if(orgaIds != null && orgaIds.length > 0){
			for(int i = 0; i<orgaIds.length;i++){
				System_Organization orga = (System_Organization) this.loadT(new System_Organization(), orgaIds[i]);
				orgaTip += orga.getOrganizationName();
				if(i != orgaIds.length -1){
					orgaTip += ",";
				}
			}
		}
		return orgaTip;
	}

	/**
	 * 获取机构负责人
	 */
	@Override
	public JSONObject getOrgaPrincipal(String table_id) {
		String hql = " from System_User t left join t.listOrga t1 where t1.table_id = ? ";
		System_User user = (System_User) this.getListOne("select t "+hql, new String[] {table_id}, "");
		JsonConfig config = new JsonConfig();
		config.setExcludes(new String[] {"listOrga","listur","listup","listuo","listFileUser"});
		return JSONObject.fromObject(user,config);
	}
	
}
