package com.dream.system.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.dream.common.SuperDao;
import com.dream.common.date.DateTools;
import com.dream.model.System_Permission;
import com.dream.model.System_User;
import com.dream.system.service.PermissionService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * permissionServiceImpl
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Component("permissionServiceImpl")
public class PermissionServiceImpl extends SuperDao<Object> implements PermissionService {

	/**
	 * 获取所有权限数据
	 */
	@Override
	public Map<String, Object> getAllPermission(System_Permission model) {
		Map<String, Object> map = new HashMap<String, Object>();
		StringBuffer hql = new StringBuffer();
		hql.append(" from System_Permission t where 1 = 1 ");
		if(StringUtils.isNotEmpty(model.getPermissionName())){
			hql.append(" and t.permissionName like '%"+model.getPermissionName()+"%' ");
		}
		List<System_Permission> list = this.getList(hql.toString(), model.getStart(), model.getLimit(), "");
		int count = this.getListCount("select count(*) "+hql.toString(), "");
		int page = count % model.getLimit() == 0 ? count / model.getLimit() : count / model.getLimit() + 1;
		map.put("list", list);
		map.put("count", page);
		map.put("sum", count);
		return map;
	}


	/**
	 * 初始化菜单
	 */
	@Override
	public void initMenuData(List<System_Permission> listMenu) {
		
		String hql =" from System_Permission t ";
		List<System_Permission> listData = this.getList(hql, 0, 0, "");
		boolean mark = true;
		
		//判断需要添加和修改的permission
		for (System_Permission permMenu : listMenu) {
			for (System_Permission permData : listData) {
				if(permMenu.getCid().equals(permData.getCid())){
					mark = false;
					BeanUtils.copyProperties(permMenu, permData, new String[]{"table_id","listup","listrp","listop","startUsing"});
					permData.setCreateTime(DateTools.getCurrentDateLong());
					permData.setShowTime(DateTools.getCurrentDate());
					this.updateT(permData);
					break;
				}
			}
			if(mark){
				permMenu.setCreateTime(DateTools.getCurrentDateLong());
				permMenu.setShowTime(DateTools.getCurrentDate());
				permMenu.setStartUsing("1");
				this.addT(permMenu);
			}
			mark = true;
		}
		
		//判断需要删除的permission
		for (System_Permission permData : listData) {
			for (System_Permission permMenu : listMenu) {
				if(permMenu.getCid().equals(permData.getCid())){
					mark = false;
					break;
				}
			}
			if(mark){
				this.delete(permData);
			}
			mark = true;
		}
	}

	/**
	 * 获取权限tree数据
	 */
	@Override
	public JSONArray getPermissionJsonData(String type, String table_id) {
		List<JSONObject> listObj = new ArrayList<JSONObject>();
		String hql = " from System_Permission t ";
		List<System_Permission> list = this.getList(hql, 0, 0, "");
		String hqlData = "";
		List<Object[]> listData = new ArrayList<Object[]>();
		if("orga".equals(type)){
			hqlData = " from System_Permission t left join t.listop t2 where t2.sys_orga.table_id = '"+table_id+"' and t.startUsing = '1' ";		
		}else if("role".equals(type)){
			hqlData = " from System_Permission t left join t.listrp t2 where t2.sys_role.table_id = '"+table_id+"' and t.startUsing = '1' ";		
		}else if("user".equals(type)){
			hqlData = " from System_Permission t left join t.listup t2 where t2.sys_user.table_id = '"+table_id+"' and t.startUsing = '1' ";		
		}
		if(StringUtils.isNotEmpty(hqlData)){
			listData = this.getList(hqlData, 0, 0, "");
		}
		if(list!=null&&list.size()>0){
			for (System_Permission perm : list) {
				JSONObject obj = new JSONObject();
				obj.put("table_id", perm.getTable_id());
				obj.put("cid",perm.getCid() );
				obj.put("name", perm.getPermissionName());
				obj.put("pid", perm.getPid());
				obj.put("type", perm.getType());
				obj.put("using", perm.getStartUsing());
				if(listData!=null&&listData.size()>0){
					for (Object[] objs : listData) {
						System_Permission permission = (System_Permission) objs[0];
						if(permission.getTable_id().equals(perm.getTable_id())){
						obj.put("checked", true);
						}
					}	
				}
				listObj.add(obj);
			}
		}
			
		return JSONArray.fromObject(listObj);
	}

	/**
	 * 修改权限
	 */
	@Override
	public void updatePermission(System_Permission model) {
		System_Permission perm = (System_Permission) this.loadT(model, model.getTable_id());
		perm.setPermissionName(model.getPermissionName());
		perm.setStartUsing(model.getStartUsing());
		perm.setShowTime(DateTools.getCurrentDate());
		this.updateT(perm);
		
		String hql = " from System_Permission t where t.pid = ? ";
		List<System_Permission> list = this.getList(hql,new String[]{perm.getCid()}, 0, 10000, "");
		if(list != null && list.size() > 0){
			for (System_Permission system_Permission : list) {
				system_Permission.setStartUsing(perm.getStartUsing());
				this.updatePermission(system_Permission);
			}
		}
	}


	/**
	 * 获取用户权限数据
	 */
	@Override
	public JSONArray getUserMenuJsonArr(System_User user) {
		JSONArray json =new JSONArray();
		String hql1 = "select t from System_Permission t left join t.listup t1 where t1.sys_user.table_id = '"+user.getTable_id()+"' and t.startUsing = '1'";
		String hql2 = "select t from System_Permission t left join t.listrp t1 left join t1.sys_role t2 left join t2.listur t3 where t3.sys_user.table_id = '"+user.getTable_id()+"' and t.startUsing = '1'";
		String hql3 = "select t from System_Permission t left join t.listop t1 left join t1.sys_orga t2 left join t2.listuo t3 where t3.sys_user.table_id = '"+user.getTable_id()+"' and t.startUsing = '1'";
		List<System_Permission> list1 = this.getList(hql1, 0, 0, "");
		List<System_Permission> list2 = this.getList(hql2, 0, 0, "");
		List<System_Permission> list3 = this.getList(hql3, 0, 0, "");
		Set<System_Permission> set = new HashSet<System_Permission>(); //去掉重复
		//合并list1，list2，结果为list1
		if(list1!=null&&list1.size()>0){
			for (System_Permission system_Permission : list1) {
				set.add(system_Permission);
			}
		}
		if(list2!=null&&list2.size()>0){
			for (System_Permission system_Permission : list2) {
				set.add(system_Permission);
			}
		}
		if(list3!=null&&list3.size()>0){
			for (System_Permission system_Permission : list3) {
				set.add(system_Permission);
			}
		}
		
		List<System_Permission> list = new ArrayList<System_Permission>(set);
		if(list!=null&&list.size()>0){
			Collections.sort(list, new Comparator<System_Permission>() {//根据sort排序
				@Override
				public int compare(System_Permission o1, System_Permission o2) {
					
					return o1.getSort() - o2.getSort();
				}
			});
			
			for (System_Permission perm : list) {
				JSONObject js = new JSONObject();
				js.put("id", perm.getCid());
				js.put("text",perm.getPermissionName());
				js.put("url",perm.getUrl());
				js.put("icon", perm.getIcon());
				js.put("pId",perm.getPid());
				js.put("type",perm.getType());
				json.add(js);
			}
		}
		
		return json;
	}

	/**
	 * 获取所有权限数据
	 */
	@Override
	public JSONArray getAdminMenuJsonArr() {
		JSONArray json = new JSONArray();
		String hql =" from System_Permission t ";
		List<System_Permission> list = this.getList(hql, 0, 0, "order by t.sort");
		if (list != null && list.size() > 0) {
			for (System_Permission perm : list) {
				JSONObject js = new JSONObject();
				js.put("id", perm.getCid());
				js.put("text",perm.getPermissionName());
				js.put("url",perm.getUrl());
				js.put("icon", perm.getIcon());
				js.put("pId",perm.getPid());
				js.put("type",perm.getType());
				json.add(js);
			}
		}
		return json;
	}

	/**
	 * 获取所有btn数据
	 */
	@Override
	public JSONArray getAdminBtnJsonArr() {
		JSONArray json = new JSONArray();
		String hql =" from System_Permission t where t.type = 'button'";
		List<System_Permission> list = this.getList(hql, 0, 0, "");
		if (list != null && list.size() > 0) {
			for (System_Permission perm : list) {
				JSONObject js = new JSONObject();
				js.put("id", perm.getCid());
				js.put("name",perm.getPermissionName());
				js.put("pId",perm.getPid());
				js.put("type",perm.getType());
				json.add(js);
			}
		}
		return json;
	}

	/**
	 * 根据权限ids获取tree数据
	 */
	@Override
	public JSONArray getPermissionTreeData(String perms) {
		JSONArray listObj = new JSONArray();
		String[]  permArr = null;
		if(StringUtils.isNotEmpty(perms)){
			permArr = perms.split(",");			
		}
		String hql = " from System_Permission t ";
		List<System_Permission> list = this.getList(hql, 0, 0, "");
		if (list != null && list.size() > 0) {
			for (System_Permission perm : list) {
				JSONObject obj = new JSONObject();
				obj.put("table_id", perm.getTable_id());
				obj.put("cid", perm.getCid());
				obj.put("name", perm.getPermissionName());
				obj.put("pid", perm.getPid());
				obj.put("type", perm.getType());
				obj.put("using", perm.getStartUsing());
				if (permArr != null && permArr.length > 0) {
					for (String permId : permArr) {
						if (perm.getTable_id().equals(permId)) {
							obj.put("checked", true);
						}
					}
				}
				listObj.add(obj);
			}
		}
		return listObj;
	}

	/**
	 * 获取权限toolTip数据
	 */
	@Override
	public String getPermToolTipData(String perms) {
		String permTip = "";
		String[] permIds = null;
		if(StringUtils.isNotEmpty(perms)){
			permIds = perms.split(",");
		}
		if(permIds != null && permIds.length > 0){
			for(int i = 0;i<permIds.length;i++){
				System_Permission perm = (System_Permission) this.loadT(new System_Permission(), permIds[i]);
				permTip +=perm.getPermissionName();
				if(i != permIds.length - 1){
					permTip +=",";
				}
			}
		}
		return permTip;
	} 
}
