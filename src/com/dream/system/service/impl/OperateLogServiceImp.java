package com.dream.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.dream.common.SuperDao;
import com.dream.common.date.DateTools;
import com.dream.model.Operation_Log;
import com.dream.model.System_User;
import com.dream.system.service.OperateLogService;

import jxl.format.Format;

/**
 * 
* <p>Title: OperateLogServiceImp</p>
* <p>Description: 操作日志服务层，负责业务处理和调用数据的持久层</p>
* @author z.q.k
* @date 上午9:49:29
 */
@SuppressWarnings("all")
@Component("operateLogServiceImp")
public class OperateLogServiceImp extends SuperDao<Object> implements OperateLogService {

	@Override
	public void insertOperateLog(Operation_Log model) throws Exception
	{
		// TODO Auto-generated method stub
		if(model.getIp().equals("0:0:0:0:0:0:0:1")) {
			model.setIp("127.0.0.1");
		}
		super.addT(model);
	}

	@Override
	public Map<String, Object> getOperateLists(Operation_Log model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		List<Object> params = new ArrayList<Object>();
		StringBuffer sb = new StringBuffer();
		
		long startTime = 0;
		long endTime = 0;
		if(StringUtils.isNotBlank(model.getStartTime())) 
			startTime = DateTools.convert2long(model.getStartTime(),DateTools.NORMAL_M_FORMAT);
		
		if(StringUtils.isNotBlank(model.getEndTime())) 
			endTime = DateTools.convert2long(model.getEndTime(), DateTools.NORMAL_M_FORMAT);
		
		sb.append("FROM Operation_Log t  WHERE 1=1 ");
		if(StringUtils.isNotEmpty(model.getTemp3())) {
			if(model.getTemp3().equals("aqgl")) {
				sb.append(" and FIND_IN_SET('安全管理员',t.roleName)>0 ");
			}
			if(model.getTemp3().equals("aqsj")) {
				sb.append(" and FIND_IN_SET('安全审计员',t.roleName)>0 ");
			}
		}
		if(StringUtils.isNotEmpty(model.getUserName())){
			sb.append(" AND t.userName LIKE ? ");
			params.add("%" + model.getUserName() + "%");
		}
		
		if(StringUtils.isNotBlank(model.getRoleName())) {
			sb.append(" AND t.roleName LIKE ? ");
			params.add("%" + model.getRoleName() + "%");
		}
		
		if(StringUtils.isNotBlank(model.getStartTime())) {
			sb.append(" AND t.createTime >= ? ");
			params.add(startTime);
		}
		
		if(StringUtils.isNotBlank(model.getEndTime())) {
			sb.append(" AND t.createTime <= ? ");
			params.add(endTime);
		}
//		List<Object[]> list = this.getList(sb.toString(), params.toArray(), model.getStart(), model.getLimit(), " order by t." + model.getDesc());
		List<Object> list = this.getList(sb.toString(), params.toArray(), model.getStart(), model.getLimit(), " order by t.createTime desc ");
		int sum = this.getListCount("SELECT count(*) " + sb.toString(), params.toArray(), "");
		int count = (int)Math.ceil(Double.valueOf(sum)/Double.valueOf(model.getLimit()));
		
		/**
		List<Operation_Log> nList = new ArrayList<Operation_Log>();
		
		for(Object[] obj : list) 
		{
			getOperationInfo(nList, (Operation_Log)obj[0], (System_User)obj[1]);
		}*/
		
		map.put("list", list);
		map.put("count", count);
		map.put("sum", list.size());
		
		return map;
	}

	
	public void getOperationInfo(List<Operation_Log> nList, Operation_Log oLog, System_User user) throws Exception {
		
		nList.add(oLog);
		for(Operation_Log model : nList) {
			if(model.getTable_id().equals(oLog.getTable_id())) {
				if(user == null) oLog.setTemp1("admin");
				else oLog.setTemp1(user.getUserName());
			}
		}
	}
	
	
	@Override
	public int deleteOperateLogs(List<Operation_Log> logs) {
		// TODO Auto-generated method stub
		try {
			Operation_Log entity;
			for(Operation_Log log : logs) {
				entity = (Operation_Log)super.getT(log, log.getTable_id());
				super.delete(entity);
			}
			return 1;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
		
		
	}

	@Override
	public List<Object> getExportData(Operation_Log model) {
		List<Object> params = new ArrayList<Object>();
		StringBuffer sb = new StringBuffer();
		
		long startTime = 0;
		long endTime = 0;
		if(StringUtils.isNotBlank(model.getStartTime())) 
			startTime = DateTools.convert2long(model.getStartTime(),DateTools.NORMAL_M_FORMAT);
		
		if(StringUtils.isNotBlank(model.getEndTime())) 
			endTime = DateTools.convert2long(model.getEndTime(), DateTools.NORMAL_M_FORMAT);
		
		sb.append("FROM Operation_Log t  WHERE 1=1 ");
		if(StringUtils.isNotEmpty(model.getUserName())){
			sb.append(" AND t.userName LIKE ? ");
			params.add("%" + model.getUserName() + "%");
		}
		
		if(StringUtils.isNotBlank(model.getRoleName())) {
			sb.append(" AND t.roleName LIKE ? ");
			params.add("%" + model.getRoleName() + "%");
		}
		
		if(StringUtils.isNotBlank(model.getStartTime())) {
			sb.append(" AND t.createTime >= ? ");
			params.add(startTime);
		}
		
		if(StringUtils.isNotBlank(model.getEndTime())) {
			sb.append(" AND t.createTime <= ? ");
			params.add(endTime);
		}
		List<Object> list = this.getList(sb.toString(), params.toArray(), 0, 2000, "");
		return list;
	}
	
}
