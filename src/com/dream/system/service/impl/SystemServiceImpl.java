package com.dream.system.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.dream.common.SuperDao;
import com.dream.model.System_Log;
import com.dream.system.service.SystemService;

@SuppressWarnings("all")
@Component("systemServiceImpl")
public class SystemServiceImpl extends SuperDao<Object> implements SystemService {
	@Override
	public Map<String, Object> getSystemLog(System_Log log) throws ParseException {
		// TODO Auto-generated method stub
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Map<String, Object> map = new HashMap<>();
		
		String sql = "from System_Log t where 1 = 1 ";
		List<Object> params = new ArrayList<>();
		
		if(!StringUtils.isEmpty(log.getLevel())) {
			sql += " and t.level = ? ";
			params.add(log.getLevel());
		}
		if(!StringUtils.isEmpty(log.getString1())) {
			sql += " and t.createTime >= ? ";
			params.add(format.parse(log.getString1()+":00").getTime());
		}
		if(!StringUtils.isEmpty(log.getString2())) {
			sql += " and t.createTime <= ? ";
			params.add(format.parse(log.getString2()+":00").getTime());
		}
		List<System_Log> list = this.getList(sql, params.toArray(), log.getStart(),log.getLimit(), " order by t.createTime desc");
		int sum = this.getListCount("select count(table_id) "+sql, params.toArray(), "");
		int count = (int)Math.ceil(Double.valueOf(sum)/Double.valueOf(log.getLimit()));
		
		map.put("list", list);
		map.put("count", count);
		map.put("sum", list.size());
		
		return map;
	}
}
