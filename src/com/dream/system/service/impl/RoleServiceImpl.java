package com.dream.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.dream.common.SuperDao;
import com.dream.common.date.DateTools;
import com.dream.model.System_Organization;
import com.dream.model.System_Permission;
import com.dream.model.System_Role;
import com.dream.model.System_Role_Permission;
import com.dream.model.System_User;
import com.dream.system.service.RoleService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 角色serviceImpl
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Component("roleServiceImpl")
public class RoleServiceImpl extends SuperDao<Object> implements RoleService{

	/**
	 * 获取所有角色
	 */
	@Override
	public Map<String, Object> getAllRole(System_Role model) {
		Map<String, Object> map = new HashMap<String, Object>();
		StringBuffer hql = new StringBuffer();
		hql.append(" from System_Role t where 1 = 1 ");
		if(StringUtils.isNotEmpty(model.getRoleName())){
			hql.append(" and t.roleName like '%"+model.getRoleName()+"%' ");
		}
		List<System_Role> list = this.getList(hql.toString(), model.getStart(),model.getLimit(), "");
		int count = this.getListCount("select count(*) "+hql.toString(), "");
		int page = count % model.getLimit() == 0 ? count / model.getLimit() : count / model.getLimit() + 1;
		map.put("list", list);
		map.put("count", page);
		map.put("sum", count);
		return map;
	}

	/**
	 * 添加角色
	 */
	@Override
	public void addRole(System_Role model) {
		String temp1 = model.getTemp1();
		model.setCreateTime(DateTools.getCurrentDateLong());
		model.setShowTime(DateTools.getCurrentDate());
		model.setTemp1("");
		this.addT(model);
		
		//添加角色权限中间表
		if(StringUtils.isNotEmpty(temp1)){
			List<System_Permission> list =(List<System_Permission>) JSONArray.toCollection(JSONArray.fromObject(temp1), System_Permission.class);
			if(list!=null&&list.size()>0){
				for (System_Permission permission : list) {
					System_Role_Permission srp = new System_Role_Permission();
					srp.setSys_role(model);
					srp.setSys_perm((System_Permission)this.loadT(permission, permission.getTable_id()));
					srp.setCreateTime(DateTools.getCurrentDateLong());
					srp.setShowTime(DateTools.getCurrentDate());
					this.addT(srp);
				}
			}
		}
	}

	/**
	 * 获取角色tree数据
	 */
	@Override
	public JSONArray getRoleJsonData(String table_id) {
		List<JSONObject> listObj = new ArrayList<JSONObject>();
		String hql = " from System_Role t ";
		List<System_Role> list = this.getList(hql, 0, 0, "");
		List<Object[]> listData = new ArrayList<Object[]>();
		if(StringUtils.isNotEmpty(table_id)){
			String hqlData = " from System_Role t left join t.listur t1 where t1.sys_user.table_id = '"+table_id+"'";
			listData = this.getList(hqlData, 0, 0, "");
		}
		if(list!=null&&list.size()>0){
			for (System_Role role : list) {
				JSONObject obj = new JSONObject();
				obj.put("table_id", role.getTable_id());
				obj.put("cid",role.getTable_id() );
				obj.put("name", role.getRoleName());
				obj.put("pid", "0");
				if(listData!=null&&listData.size()>0){
					for (Object[] objs : listData) {
						System_Role sys_role = (System_Role) objs[0];
						if(sys_role.getTable_id().equals(role.getTable_id())){
							obj.put("checked", true);
						}
					}	
				}
				listObj.add(obj);
			}
		}
			
		return JSONArray.fromObject(listObj);
	}

	/**
	 * 修改角色
	 */
	@Override
	public void updateRole(System_Role model) {
		String temp1 = model.getTemp1();
		System_Role role = (System_Role) this.loadT(model, model.getTable_id());
		BeanUtils.copyProperties(model, role, new String[]{"table_id","createTime","listur","listrp","temp1","listFileRole"});
		role.setShowTime(DateTools.getCurrentDate());
		role.getListrp().clear();
		this.updateT(role);
		//添加角色权限中间表
		if(StringUtils.isNotEmpty(temp1)){
			List<System_Permission> list =(List<System_Permission>) JSONArray.toCollection(JSONArray.fromObject(temp1), System_Permission.class);
			if(list!=null&&list.size()>0){
				for (System_Permission permission : list) {
					System_Role_Permission srp = new System_Role_Permission();
					srp.setSys_role(model);
					srp.setSys_perm((System_Permission)this.loadT(permission, permission.getTable_id()));
					srp.setCreateTime(DateTools.getCurrentDateLong());
					srp.setShowTime(DateTools.getCurrentDate());
					this.addT(srp);
				}
			}
		}
	}

	/**
	 * 删除角色
	 */
	@Override
	public void deleteRole(System_Role model) {
		System_Role role = (System_Role) this.loadT(model, model.getTable_id());
		this.delete(role);
		
	}

	/**
	 * 根据角色ids获取tree数据
	 */
	@Override
	public JSONArray getRoleTreeData(String roles) {
		JSONArray listObj = new JSONArray();
		String[] roleArr= null;
		if(StringUtils.isNotEmpty(roles)){
			roleArr = roles.split(",");			
		}
		String hql = "from System_Role t ";
		List<System_Role> list = this.getList(hql, 0, 0, "");
		if(list!=null&&list.size()>0){
			for (System_Role role : list) {
				JSONObject obj = new JSONObject();
				obj.put("table_id", role.getTable_id());
				obj.put("cid",role.getTable_id() );
				obj.put("name", role.getRoleName());
				obj.put("pid", "0");
				if (roleArr != null && roleArr.length > 0) {
					for (String roleId : roleArr) {
						if(role.getTable_id().equals(roleId)){
							obj.put("checked", true);
						}
					}	
				}
				listObj.add(obj);
			}
		}
		return listObj;
	}

	/**
	 * 获取角色toolTip数据
	 */
	@Override
	public String getRoleToolTipData(String roles) {
		String roleTip = "";
		String[] roleIds = null;
		if(StringUtils.isNotEmpty(roles)){
			roleIds = roles.split(",");
		}
		if(roleIds != null && roleIds.length > 0 ){
			for(int i = 0;i<roleIds.length;i++){
				System_Role role = (System_Role) this.loadT(new System_Role(), roleIds[i]);
				roleTip += role.getRoleName();
				if( i != roleIds.length - 1){
					roleTip += ",";
				}
			}
		}
		return roleTip;
	}

}
