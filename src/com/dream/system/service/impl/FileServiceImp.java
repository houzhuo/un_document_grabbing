package com.dream.system.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder.In;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Component;

import com.dream.common.SuperDao;
import com.dream.file.PullMain;
import com.dream.model.File_Entity;
import com.dream.model.Reptile_Record;
import com.dream.model.System_User;
import com.dream.system.service.FileService;
/**
 	* 
	* <p>Title: FileServiceImp</p>
	* <p>Description: 处理文件业务，然后调用持久化接口</p>
	* @author z.q.k
	* @date 下午6:00:25
 */
@SuppressWarnings("all")
@Component("fileServiceImp")
public class FileServiceImp extends SuperDao<Object> implements FileService {

	private SimpleDateFormat fotmat1 = new SimpleDateFormat("yyyy/MM/dd");
	
	private SimpleDateFormat fotmat2 = new SimpleDateFormat("yyyy-MM-dd");
	
	@Override
	public Map<String, Object> getFileList(File_Entity model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		List<Object> params = new ArrayList<Object>();
		StringBuffer sb = new StringBuffer();
		sb.append("select f.table_id as table_id,f.path as path,f.uuidFileName as uuidFileName,"
				+ "f.fileSize as fileSize,f.fileType as fileType,f.fileName as fileName,f.status as status,"
				+ "f.fileHttpUrl as fileHttpUrl,f.fileLanguage as fileLanguage,f.fileDocID as fileDocID,f.fileRange as fileRange,"
				+ "f.fileDomain as fileDomain,f.fileAnnual as fileAnnual,f.fileAgenda as fileAgenda,f.fileJobID as fileJobID,"
				+ "f.filePublishDate as filePublishDate,f.createTime as createTime,f.showTime as showTime,"
				+ " f.keyWords as keyWords,f.version as version,f.temp1 as temp1 ,f.temp2 as temp2,f.temp3 as temp3 "
				+ "from (SELECT table_id FROM file_entity t  WHERE  1 = 1  ");
		String sql = "";
		if(StringUtils.isNotEmpty(model.getFileName())){
			sb.append(" and t.fileName like ? ");
			sql += "and t.fileName like ?";
			params.add("%"+model.getFileName()+"%");
		}
		
		if(StringUtils.isNotEmpty(model.getFileDocID())){
			sb.append(" and t.fileDocID like ? ");
			sql += " and t.fileDocID like  ? ";
			params.add("%"+model.getFileDocID()+"%");
		}
		sb.append(" group by  t.fileDocID LIMIT "+model.getStart()+","+model.getLimit()+") "
				+ " temp  LEFT JOIN file_entity f on temp.table_id = f.table_id  ");
		
		
		
		Query sqlQuery =  this.sessionFactory.getCurrentSession().createSQLQuery(sb.toString()).addEntity(File_Entity.class);
		
		for(int i = 0;i < params.size();i++){
			sqlQuery.setParameter(i, params.get(i));
		}
		List<File_Entity> list = sqlQuery.list();
		
		/*if(StringUtils.isNotEmpty(sql)){
			sql = " and " + sql;
		}*/
		
		int sum = this.getListCountSql("select count(*) from ( select table_id FROM file_entity t  WHERE  1 = 1    "+sql + " group by  t.fileDocID ) temp ", params.toArray(), "");
		int count = (int)Math.ceil(Double.valueOf(sum)/Double.valueOf(model.getLimit()));
		
		List<File_Entity> nList = new ArrayList<File_Entity>();
		
	
		map.put("list", list);
		map.put("count", count);
		map.put("sum", list.size());
		
		return map;
	}

	@Deprecated
	@Override
	public Integer deleteFiles(List<File_Entity> models) {
		// TODO Auto-generated method stub
		try {
			if(models != null && models.size() > 0){
				for (File_Entity fileEntity : models) {
					File_Entity file = (File_Entity) this.loadT(fileEntity, fileEntity.getTable_id());
					this.delete(file);
				}
			}
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	
	}

	@Deprecated
	@Override
	public Integer deleteFile(File_Entity model) {
		// TODO Auto-generated method stub
		try {
			if(null != model) {
				File_Entity file = (File_Entity) this.loadT(model, model.getTable_id());
				this.delete(file);
			}
			return 1;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public Integer updateFileStatus(File_Entity model) {
		// TODO Auto-generated method stub
		try {
			File_Entity entity = (File_Entity)this.loadT(model, model.getTable_id().trim());
			
			this.updateT(entity);
			return 1;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public void insertFile(File_Entity model) throws Exception {
		// TODO Auto-generated method stub
		super.addT(model);
		String sql = "select count(fileDocID) from (select DISTINCT fileDocID from file_entity where filePublishDate = '"+fotmat1.format(fotmat2.parse(model.getString1()))+"')temp";
		String reNum = this.sessionFactory.getCurrentSession().createSQLQuery(sql).uniqueResult().toString();
		String updateSql = "  UPDATE reptile_record set allNum = '"+model.getString3()+"' , reptlieNum = '"+reNum+"' where startDate = '"+model.getString1()+"' and endDate = '"+model.getString2()+"'  ";
		this.sessionFactory.getCurrentSession().createSQLQuery(updateSql).executeUpdate();
	}

	@Override
	public void getCreateFileInfo(List<File_Entity> nList, File_Entity file, System_User user) throws Exception {
		// TODO Auto-generated method stub
		if(null != user) nList.add(file);
		for(File_Entity model : nList) {
			if(model.getTable_id().equals(file.getTable_id())) {
				file.setTemp1(user.getUserName());
			}
		}
	}

	@Override
	public List<File_Entity> getFilesByBusinessId(String businessId) {
		String hql = "from File_Entity t where t.business_id = ? and t.fileStatus = 0";
		List<File_Entity> list = this.getList(hql,new String[]{businessId}, 0, 100, "");
		return list;
	}

	public Map<String, Object> getFileByBusinessId(File_Entity model) throws Exception {
		Map<String,Object> map=new HashMap<String,Object>();
		/*String hql=" from File_Entity t  where t.business_id = ? and t.fileStatus = '0' ";
		List<File_Entity> list=this.getList("select t "+hql,new String[] {model.getBusiness_id()}, 0, 0, "");
		map.put("list", list);*/
		return map;
	}

	@Override
	public File_Entity getFileById(File_Entity mdoel) {
		return (File_Entity) this.getT(new File_Entity(),mdoel.getTable_id());
	}

	@Override
	public List<File_Entity> getAllFileInfo() {
		// TODO Auto-generated method stub
		String sql = "from File_Entity t ";
		List<File_Entity> list = this.getList(sql, 0, 0, "");
		
		return list;
	}

	/**
	 * 根据多个业务id查询文件
	 */
	@Override
	public List<File_Entity> getFilesByManyBusinessIds(String businessIds) {
		List<File_Entity> listFiles = new ArrayList<File_Entity>();
		String hql=" from File_Entity t  where t.business_id = ? and t.fileStatus = '0' ";
		String[] ids = businessIds.split(",");
		if (ids != null && ids.length > 0) {
			for(int i = 0;i<ids.length;i++){
				List<File_Entity> list=this.getList("select t "+hql,new String[] {ids[i]}, 0, 0, "");
				listFiles.addAll(list);
			}
		}
		return listFiles;
	}

	@Override
	public File_Entity getFilePathByTableIdAnTableName(String tableId, String tableName) {
		// TODO Auto-generated method stub
		String sql = "select filePath as filePath,fileName as fileName from "+ tableName + " where table_id = '"+tableId+"'";
		
		SQLQuery query = this.sessionFactory.getCurrentSession().createSQLQuery(sql);
		query.addScalar("filePath", StringType.INSTANCE).addScalar("fileName", StringType.INSTANCE)
			 .setResultTransformer(Transformers.aliasToBean(File_Entity.class));
		
		List<File_Entity> list = query.list();
		 if(list == null || list.size() <=0 ) {
			 return null;
		 }
		return list.get(0);
	}

	@Override
	public List<File_Entity> getFilesByIds(String ids) {
		// TODO Auto-generated method stub
		
		List<String> idList = Arrays.asList(ids.split(","));
		String params = "";
		for(String id:idList){
			params += "'" + id + "'" + ",";
		}
		
		if(params.contains(",")){
			params = params.substring(0, params.lastIndexOf(","));
		}
		
		String hql=" from File_Entity t  where t.fileDocID  in  ("+params+") ";
		List<File_Entity> list =this.getList(hql,new String[] {}, 0, 0, "");
		
		return list;
	}

	@Override
	public void getFileDocIds() {
		// TODO Auto-generated method stub
		String sql = "select fileHttpUrl from File_Entity t";
		List<String> list = this.sessionFactory.getCurrentSession().createSQLQuery(sql).list();
		PullMain.addAll(list);
	}

	@Override
	public Map<String, Object> getReptileRecords(Reptile_Record record) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		List<Object> params = new ArrayList<Object>();
		StringBuffer sb = new StringBuffer();
		sb.append(" FROM Reptile_Record t  ");
	
		List<Reptile_Record> list = this.getList(sb.toString(), params.toArray(), record.getStart(), record.getLimit(), " order by t.startDate  ");
		int sum = this.getListCount("select count(*) "+sb.toString(), params.toArray(), "");
		int count = (int)Math.ceil(Double.valueOf(sum)/Double.valueOf(record.getLimit()));
		
		map.put("list", list);
		map.put("count", count);
		map.put("sum", list.size());
		
		return map;
	}

	@Override
	public void insertObject(Reptile_Record object) {
		// TODO Auto-generated method stub
		
		String sql = "select count(*) from reptile_record where startDate = '"+object.getStartDate()+"' and endDate = '"+object.getEndDate()+"' ";
		
		Integer num = Integer.valueOf(this.sessionFactory.getCurrentSession().createSQLQuery(sql).uniqueResult().toString());
		
		if(num > 0){
			if(StringUtils.isNotEmpty(object.getAllNum())){
				 sql = "update reptile_record set allNum = '"+object.getAllNum()+"'    where startDate = '"+object.getStartDate()+"' and endDate = '"+object.getEndDate()+"' ";
				 this.sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			}
			 return;
		}
		
		this.addT(object);
	}

	@Override
	public List<Reptile_Record> getPullDate() {
		// TODO Auto-generated method stub
		String sql = "from Reptile_Record t where t.allNum is null or t.allNum = ''  or t.allNum != t.reptlieNum ";
		List<Reptile_Record> list = this.getList(sql, 0, 0, "");
		if(list == null){
			return new ArrayList<Reptile_Record>();
		}
		return list;
	}

	@Override
	public boolean checkRecord(String startDate, String endDate) {
		// TODO Auto-generated method stub
		
		String sql = "from Reptile_Record t where t.startDate  = '"+startDate+"' and t.endDate = '"+endDate+"' ";
		
		List<Reptile_Record> list = this.getList(sql, 0, 0, "");
		if(list.size() <= 0){
			return false;
		}
		
		Reptile_Record rr = list.get(0);
		
		if(StringUtils.isEmpty(rr.getAllNum())){
			return false;
		}
		
		if("0".equals(rr.getAllNum()) || rr.getAllNum().equals(rr.getReptlieNum())){
			return true;
		}
		
		return false;
	}

	@Override
	public List<Reptile_Record> getCompletDate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Reptile_Record> getAllSearchDate() {
		// TODO Auto-generated method stub
		String sql = "from Reptile_Record t  ";
		List<Reptile_Record> list = this.getList(sql, 0, 0, "");
		
		if(list == null){
			return new ArrayList<Reptile_Record>();
		}
		return list;
	}

	@Override
	public boolean deleteFiles(String tableId) {
		// TODO Auto-generated method stub
		String sql = "delete from File_Entity where table_id = '"+tableId+"'";
		this.sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
		return true;
	}

	@Override
	public List<Object[]> getFileIds() {
		// TODO Auto-generated method stub
		String sql = "select table_id,path from (select  count(*) as allNum,table_id,path from file_entity  GROUP BY fileHttpUrl)temp where allNum > 1"; 
		
		List<Object[]> list = this.sessionFactory.getCurrentSession().createSQLQuery(sql).list();
		
		return list;
	}

	@Override
	public List<String> getFileUUidName() {
		// TODO Auto-generated method stub
		String sql = "select uuidFileName from File_Entity ";
		
		return this.sessionFactory.getCurrentSession().createSQLQuery(sql).list();
	}

	@Override
	public boolean deleteFilesByUUIDName(String UUIDName) {
		// TODO Auto-generated method stub
		String sql = "delete from File_Entity where uuidFileName = '"+UUIDName+"'";
		this.sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
		return true;
	}
	public static void main(String[] args) throws ParseException {
		SimpleDateFormat fotmat1 = new SimpleDateFormat("yyyy/MM/dd");
		
		 SimpleDateFormat fotmat2 = new SimpleDateFormat("yyyy-MM-dd");
		
		String date = "2013-04-26";
		
		System.out.println(fotmat1.format(fotmat2.parse(date)));
	}
	
}
