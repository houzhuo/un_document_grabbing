package com.dream.system.service.impl;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.dream.common.JsonString;
import com.dream.common.MD5;
import com.dream.common.SuperDao;
import com.dream.common.date.DateTools;
import com.dream.model.System_Organization;
import com.dream.model.System_Permission;
import com.dream.model.System_Role;
import com.dream.model.System_Role_Permission;
import com.dream.model.System_User;
import com.dream.model.System_User_Organization;
import com.dream.model.System_User_Permission;
import com.dream.model.System_User_Role;
import com.dream.system.service.UserService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 用户serviceImpl
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Component("userServiceImpl")
public class UserServiceImpl extends SuperDao<Object> implements UserService {

	/**
	 * 获取所有用户
	 */
	@Override
	public Map<String, Object> getAllUser(System_User model) {
		Map<String, Object> map = new HashMap<String, Object>();
		StringBuffer hql = new StringBuffer();
		hql.append(" from System_User t where 1 = 1 ");
		if(StringUtils.isNotEmpty(model.getUserName())){
			hql.append(" and t.userName like '%"+model.getUserName()+"%' ");
		}
		List<System_User> list = this.getList(hql.toString(), model.getStart(),model.getLimit(), "");
		int count = this.getListCount("select count(*) "+hql.toString(), "");
		int page = count % model.getLimit() == 0 ? count / model.getLimit() : count / model.getLimit() + 1;
		map.put("list", list);
		map.put("count", page);
		map.put("sum", count);
		return map;
	}

	/**
	 * 添加用户
	 */
	@Override
	public void addUser(System_User model) {
		String temp1 = model.getTemp1();
		String temp2 = model.getTemp2();
		String temp3 = model.getTemp3();
		model.setPassWord(MD5.getMD5(model.getPassWord().getBytes()));
		model.setCreateTime(DateTools.getCurrentDateLong());
		model.setShowTime(DateTools.getCurrentDate());
		model.setTemp1("");
		model.setTemp2("");
		model.setTemp3("");
		this.addT(model);
		
		//添加角色权限中间表
		if(StringUtils.isNotEmpty(temp1)){
			List<System_Permission> list =(List<System_Permission>) JSONArray.toCollection(JSONArray.fromObject(temp1), System_Permission.class);
			if(list!=null&&list.size()>0){
				for (System_Permission permission : list) {
					System_User_Permission sup = new System_User_Permission();
					sup.setSys_user(model);
					sup.setSys_perm((System_Permission)this.loadT(permission, permission.getTable_id()));
					sup.setCreateTime(DateTools.getCurrentDateLong());
					sup.setShowTime(DateTools.getCurrentDate());
					this.addT(sup);
				}
			}
		}
		
		//添加用户机构中间表
		if(StringUtils.isNotEmpty(temp2)){
			List<System_Organization> list =(List<System_Organization>) JSONArray.toCollection(JSONArray.fromObject(temp2), System_Organization.class);
			if(list!=null&&list.size()>0){
				for (System_Organization organization : list) {
					System_User_Organization suo = new System_User_Organization();
					suo.setSys_user(model);
					suo.setSys_orga((System_Organization)this.loadT(organization, organization.getTable_id()));
					suo.setCreateTime(DateTools.getCurrentDateLong());
					suo.setShowTime(DateTools.getCurrentDate());
					this.addT(suo);
				}
			}
		}
		
		//添加用户角色中间表
		if(StringUtils.isNotEmpty(temp3)){
			List<System_Role> list =(List<System_Role>) JSONArray.toCollection(JSONArray.fromObject(temp3), System_Role.class);
			if(list!=null&&list.size()>0){
				for (System_Role role : list) {
					System_User_Role sur = new System_User_Role();
					sur.setSys_user(model);
					sur.setSys_role((System_Role)this.loadT(role, role.getTable_id()));
					sur.setCreateTime(DateTools.getCurrentDateLong());
					sur.setShowTime(DateTools.getCurrentDate());
					this.addT(sur);
				}
			}
		}
	}

	/**
	 * 修改用户
	 */
	@Override
	public void updateUser(System_User model) {
		try {
			
			System_User user = (System_User) this.loadT(model, model.getTable_id());
			BeanUtils.copyProperties(model, user, new String[]{"table_id","createTime","listOrga","listur","listup",
									"listuo","temp1","temp2","temp3","passWord","themeUserList","listFileUser"});
			user.setShowTime(DateTools.getCurrentDate());
			if(StringUtils.isNotEmpty(model.getPassWord()) && !model.getPassWord().equals(user.getPassWord())) {
				user.setPassWord(MD5.getMD5(model.getPassWord().getBytes()));
			}
			user.getListuo().clear();
			user.getListup().clear();
			user.getListur().clear();
			this.updateT(user);
			
			//添加用户权限中间表
			if(StringUtils.isNotEmpty(model.getTemp1())){
				List<System_Permission> list =(List<System_Permission>) JSONArray.toCollection(JSONArray.fromObject(model.getTemp1()), System_Permission.class);
				if(list!=null&&list.size()>0){
					for (System_Permission permission : list) {
						System_User_Permission sup = new System_User_Permission();
						sup.setSys_user(user);
						sup.setSys_perm((System_Permission)this.loadT(permission, permission.getTable_id()));
						sup.setCreateTime(DateTools.getCurrentDateLong());
						sup.setShowTime(DateTools.getCurrentDate());
						this.addT(sup);
					}
				}
			}
			
			//添加用户机构中间表
			if(StringUtils.isNotEmpty(model.getTemp2())){
				List<System_Organization> list =(List<System_Organization>) JSONArray.toCollection(JSONArray.fromObject(model.getTemp2()), System_Organization.class);
				if(list!=null&&list.size()>0){
					for (System_Organization organization : list) {
						System_User_Organization suo = new System_User_Organization();
						suo.setSys_user(user);
						suo.setSys_orga((System_Organization)this.loadT(organization, organization.getTable_id()));
						suo.setCreateTime(DateTools.getCurrentDateLong());
						suo.setShowTime(DateTools.getCurrentDate());
						this.addT(suo);
					}
				}
			}
			
			//添加用户角色中间表
			if(StringUtils.isNotEmpty(model.getTemp3())){
				List<System_Role> list =(List<System_Role>) JSONArray.toCollection(JSONArray.fromObject(model.getTemp3()), System_Role.class);
				if(list!=null&&list.size()>0){
					for (System_Role role : list) {
						System_User_Role sur = new System_User_Role();
						sur.setSys_user(user);
						sur.setSys_role((System_Role)this.loadT(role, role.getTable_id()));
						sur.setCreateTime(DateTools.getCurrentDateLong());
						sur.setShowTime(DateTools.getCurrentDate());
						this.addT(sur);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除用户
	 */
	@Override
	public void deleteUser(System_User model) {
		System_User user = (System_User) this.loadT(model, model.getTable_id());
		this.delete(user);
	}

	/**
	 * 判断用户是否存在
	 */
	@Override
	public System_User checkUserExist(System_User model) {
		String hql = " from System_User t where t.userName = ? and t.passWord = ? ";
		System_User user = (System_User) this.getListOne(hql, new String[]{model.getUserName(),model.getPassWord()}, "");
		return user;
	}

	/**
	 * 根据用户ids获取tree数据
	 */
	@Override
	public JSONArray getUserTreeData(String users) {
		JSONArray listObj = new JSONArray();
		String[] userArr = null;
		if(StringUtils.isNotEmpty(users)){
			userArr = users.split(",");			
		}
		String hql = "from System_User t ";
		List<System_User> list = this.getList(hql, 0, 0, "");
		if (list != null && list.size() > 0) {
			for (System_User user : list) {
				JSONObject obj = new JSONObject();
				obj.put("table_id", user.getTable_id());
				obj.put("cid", user.getTable_id());
				obj.put("name", user.getUserName());
				obj.put("pid", "0");
				if(userArr!=null && userArr.length >0){
					for(int i = 0;i<userArr.length;i++){
						if(user.getTable_id().equals(userArr[i])){
							obj.put("checked", true);
						}
					}
				}
				listObj.add(obj);
			}
		}
		return listObj;
	}

	/**
	 * 获取用户toolTip数据
	 */
	@Override
	public String getUserToolTipData(String users) {
		String userTip = "";
		String[] userIds = null;
		if(StringUtils.isNotEmpty(users)){
			userIds = users.split(",");
		}
		if(userIds != null && userIds.length > 0){
			for (int i=0;i<userIds.length;i++) {
				System_User user = (System_User) this.loadT(new System_User(), userIds[i]);
				userTip += user.getUserName();
				if(i != userIds.length - 1){
					userTip += ",";
				}
			}
		}
		return userTip;
	}

	/**
	 * 获取所有用户
	 */
	@Override
	public JSONArray getAllUsers() {
		String hql = "from System_User t";
		List<System_User> list = this.getList(hql, 0, 0, "");
		JsonConfig config = new JsonConfig();
		config.setExcludes(JsonString.sys_user);
		return JSONArray.fromObject(list, config);
	}

	/**
	 * 修改密码
	 * @param model 用户信息
	 * @return 0：修改失败；1：修改成功；2：原密码不正确
	 */
	@Override
	public int updatePassword(System_User model) {
		try {
			System_User user = (System_User) this.loadT(model, model.getTable_id());
			if(!MD5.getMD5(model.getPassWord().getBytes()).equals(user.getPassWord())){
				return 2;//原密码不正确
			}
			user.setPassWord(MD5.getMD5(model.getTemp1().getBytes()));
			this.updateT(user);
			return 1;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	
}
