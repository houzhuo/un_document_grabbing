package com.dream.system.service;

import java.util.List;
import java.util.Map;


import com.dream.model.File_Entity;
import com.dream.model.Reptile_Record;
import com.dream.model.System_User;

public interface FileService {

	/**
	 * 新增上传文件，并且抛出异常
	 * @param model
	 * @throws Exception
	 */
	public void insertFile(File_Entity model) throws Exception;
	
	
	/**
	 * 分页查询相关信息
	 * @param model
	 * @return
	 */
	public Map<String,Object> getFileList(File_Entity model) throws Exception;
	
	/**
	 * 根据table_id获取文件
	 * @param mdoel
	 * @return
	 */
	public File_Entity getFileById(File_Entity mdoel);
	
	/**
	 * 根据业务ID获取文件
	 * @param business_id
	 * @return
	 * @throws Exception
	 */
	public Map<String,Object> getFileByBusinessId(File_Entity model) throws Exception;
	
	/**
	 * 删除多条记录
	 * @param models
	 * 			文件列表
	 * @return
	 */
	public Integer deleteFiles(List<File_Entity> models) throws Exception;
	
	
	/**
	 * 删除单条记录
	 * @param model
	 * 			文件对象
	 * @return
	 */
	public Integer deleteFile(File_Entity model) throws Exception;
	
	/**
	 * 更新对应文件记录的状态
	 * @param model
	 * @return
	 */
	public Integer updateFileStatus(File_Entity model) throws Exception;
	
	/**
	 * 
	 * @param nList
	 * @param file
	 * @param user
	 * @throws Exception
	 */
	public void getCreateFileInfo(List<File_Entity> nList, File_Entity file, System_User user) throws Exception;
	
	/**
	 * 根据业务id获取文件
	 * @param businessId
	 * @return
	 */
	public List<File_Entity> getFilesByBusinessId(String businessId);
	
	/**
	 * 获取所有文件
	 * @return
	 */
	public List<File_Entity> getAllFileInfo();
	
	/**
	 * 根据多个业务id查询文件
	 * @param ids 多个业务id，以','隔开
	 * @return 结果集
	 */
	public List<File_Entity> getFilesByManyBusinessIds(String businessIds);
	
	public File_Entity getFilePathByTableIdAnTableName(String tableId,String tableName);
	
	public List<File_Entity> getFilesByIds(String ids);
	
	public void getFileDocIds();
	
	public List<Reptile_Record> getPullDate();
	
	public List<Reptile_Record> getCompletDate();
	
	public List<Reptile_Record> getAllSearchDate();
	
	public Map<String, Object> getReptileRecords(Reptile_Record record);
	
	public void insertObject(Reptile_Record record);
	
	public boolean checkRecord(String startData,String endDate);
	
	public boolean deleteFiles(String tableId);
	
	public List<Object[]> getFileIds();
	
	public List<String> getFileUUidName();
	
	public boolean deleteFilesByUUIDName(String UUIDName);
}
