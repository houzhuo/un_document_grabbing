package com.dream.system.service;

import java.util.List;
import java.util.Map;

import com.dream.model.Operation_Log;
import com.dream.model.System_User;

public interface OperateLogService {

	
	/**
	 * 新增操作日志，根据配置文件判断操作日志是否开启
	 * @param model
	 */
	public void insertOperateLog(Operation_Log model) throws Exception;
	
	/**
	 * 根据前台条件获取操作日志信息，并且返回封装的分页数据。
	 * @param model
	 * @return
	 */
	public Map<String, Object> getOperateLists(Operation_Log model) throws Exception;
	
	/**
	 * 根据前台选中的操作日志标识，删除操作日志
	 * @param logs
	 * @return
	 */
	public int deleteOperateLogs(List<Operation_Log> logs) throws Exception;
	
	/**
	 * 把查询结果重新解析成前台需要展示的数据
	 * @param nList
	 * @param oLog
	 * @param user
	 * @throws Exception
	 */
	public void getOperationInfo(List<Operation_Log> nList, Operation_Log oLog, System_User user) throws Exception;

	/**
	 * 获取导出日志数据
	 * @param model
	 * @return
	 */
	public List<Object> getExportData(Operation_Log model);
	
	
}
