package com.dream.system.service;

import java.util.List;
import java.util.Map;

import com.dream.model.System_Permission;
import com.dream.model.System_User;

import net.sf.json.JSONArray;

/**
 * permissionService
 * @author zlj
 *
 */
public interface PermissionService {
	
	/**
	 * 获取所有权限数据
	 * @param model
	 * @return
	 */
	public Map<String, Object> getAllPermission(System_Permission model);
	
	/**
	 * 初始化权限数据
	 * @param list
	 */
	public void initMenuData(List<System_Permission> list);

	/**
	 * 获取权限tree数据
	 * @param table_id 
	 * @param type 
	 * @return
	 */
	public JSONArray getPermissionJsonData(String type, String table_id);

	/**
	 * 修改权限
	 * @param model
	 */
	public void updatePermission(System_Permission model);

	/**
	 * 获取用户权限数据
	 * @param user 
	 * @return
	 */
	public JSONArray getUserMenuJsonArr(System_User user);

	/**
	 * 获取所有权限数据
	 * @return
	 */
	public JSONArray getAdminMenuJsonArr();

	/**
	 * 获取所有btn数据
	 * @return
	 */
	public JSONArray getAdminBtnJsonArr();

	/**
	 * 根据权限ids获取tree数据
	 * @param perms  需要选中的id
	 * @return
	 */
	public JSONArray getPermissionTreeData(String perms);

	/**
	 * 获取权限toolTip数据
	 * @param perms 权限ids
	 * @return
	 */
	public String getPermToolTipData(String perms);
}