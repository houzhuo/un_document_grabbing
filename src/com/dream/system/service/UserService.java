package com.dream.system.service;

import java.util.Map;

import com.dream.model.System_User;

import net.sf.json.JSONArray;

/**
 * 用户Service
 * @author zlj
 *
 */
public interface UserService {

	/**
	 * 获取所有用户
	 * @param model
	 * @return
	 */
	public Map<String,Object> getAllUser(System_User model);

	/**
	 * 添加用户
	 * @param model
	 */
	public void addUser(System_User model);

	/**
	 * 修改用户
	 * @param model
	 */
	public void updateUser(System_User model);

	/**
	 * 删除用户
	 * @param model
	 */
	public void deleteUser(System_User model);

	/**
	 * 判断用户是否存在
	 * @param model
	 * @return
	 */
	public System_User checkUserExist(System_User model);

	/**
	 * 根据用户ids获取tree数据
	 * @param users  需要选中的id
	 * @return
	 */
	public JSONArray getUserTreeData(String users);

	/**
	 * 获取用户toolTip数据
	 * @param users  用户ids
	 * @return
	 */
	public String getUserToolTipData(String users);

	/**
	 * 获取所有用户
	 * @return
	 */
	public JSONArray getAllUsers();

	/**
	 * 修改密码
	 * @param model 用户信息
	 * @return 0：修改失败；1：修改成功；2：原密码不正确
	 */
	public int updatePassword(System_User model);
}
