package com.dream.system.service;

import java.text.ParseException;
import java.util.Map;

import com.dream.model.System_Log;

@SuppressWarnings("all")
public interface SystemService {
	
	public Map<String, Object> getSystemLog(System_Log log) throws ParseException;
}
