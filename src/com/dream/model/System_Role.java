package com.dream.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 角色
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Entity
@Table(name = "system_role")
public class System_Role extends AllId {

	private static final long serialVersionUID = 1L;
	
	private String roleName;//角色名称
	
	private String description;//描述
	
	//预留字段
	private String temp1;
	
	private String temp2;
	
	private String temp3;
	
	public static String[] str = new String[]{"listur","listrp"};
	
	private List<System_User_Role> listur = new ArrayList<System_User_Role>();
	
	private List<System_Role_Permission> listrp = new ArrayList<System_Role_Permission>();
	

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTemp1() {
		return temp1;
	}

	public void setTemp1(String temp1) {
		this.temp1 = temp1;
	}

	public String getTemp2() {
		return temp2;
	}

	public void setTemp2(String temp2) {
		this.temp2 = temp2;
	}

	public String getTemp3() {
		return temp3;
	}

	public void setTemp3(String temp3) {
		this.temp3 = temp3;
	}

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval=true,mappedBy = "sys_role")
	public List<System_User_Role> getListur() {
		return listur;
	}

	public void setListur(List<System_User_Role> listur) {
		this.listur = listur;
	}

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval=true,mappedBy = "sys_role")
	public List<System_Role_Permission> getListrp() {
		return listrp;
	}

	public void setListrp(List<System_Role_Permission> listrp) {
		this.listrp = listrp;
	}

	
	
}
