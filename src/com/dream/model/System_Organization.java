package com.dream.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

/**
 * 机构
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Entity
@Table(name = "system_organization")
public class System_Organization extends AllId implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String cid;//机构id
	
	private String pid;//父级机构id
	
	private String organizationName;//机构名称
	
	private String organizationType;//机构类型
	
	private String discription;//描述
	
	private System_User sys_user;//机构负责人
	
	//预留字段
	private String temp1;
	
	private String temp2;
	
	private String temp3;
	
	private List<System_User_Organization> listuo = new ArrayList<System_User_Organization>();

	private List<System_Organization_Permission> listop = new ArrayList<System_Organization_Permission>();

	
	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(String organizationType) {
		this.organizationType = organizationType;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String discription) {
		this.discription = discription;
	}

	public String getTemp1() {
		return temp1;
	}

	public void setTemp1(String temp1) {
		this.temp1 = temp1;
	}

	public String getTemp2() {
		return temp2;
	}

	public void setTemp2(String temp2) {
		this.temp2 = temp2;
	}

	public String getTemp3() {
		return temp3;
	}

	public void setTemp3(String temp3) {
		this.temp3 = temp3;
	}

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval=true, mappedBy = "sys_orga")
	public List<System_User_Organization> getListuo() {
		return listuo;
	}

	public void setListuo(List<System_User_Organization> listuo) {
		this.listuo = listuo;
	}

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval=true, mappedBy = "sys_orga")
	public List<System_Organization_Permission> getListop() {
		return listop;
	}

	public void setListop(List<System_Organization_Permission> listop) {
		this.listop = listop;
	}

	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.PERSIST,optional=true)
	public System_User getSys_user() {
		return sys_user;
	}

	public void setSys_user(System_User sys_user) {
		this.sys_user = sys_user;
	}

	
	
}
