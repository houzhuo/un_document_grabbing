package com.dream.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 用户
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Entity
@Table(name = "system_user")
public class System_User extends AllId implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String userName;//用户名
	
	private String passWord;//密码
	
	//预留字段
	private String temp1;
	
	private String temp2;
	
	private String temp3;
	
	private List<System_Organization> listOrga = new ArrayList<System_Organization>();
	
	private List<System_User_Role> listur = new ArrayList<System_User_Role>();
	
	private List<System_User_Permission> listup = new ArrayList<System_User_Permission>();
	
	private List<System_User_Organization> listuo = new ArrayList<System_User_Organization>();
	


	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getTemp1() {
		return temp1;
	}

	public void setTemp1(String temp1) {
		this.temp1 = temp1;
	}

	public String getTemp2() {
		return temp2;
	}

	public void setTemp2(String temp2) {
		this.temp2 = temp2;
	}

	public String getTemp3() {
		return temp3;
	}

	public void setTemp3(String temp3) {
		this.temp3 = temp3;
	}

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval=true, mappedBy = "sys_user")
	public List<System_User_Role> getListur() {
		return listur;
	}

	public void setListur(List<System_User_Role> listur) {
		this.listur = listur;
	}

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval=true, mappedBy = "sys_user")
	public List<System_User_Permission> getListup() {
		return listup;
	}

	public void setListup(List<System_User_Permission> listup) {
		this.listup = listup;
	}

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval=true, mappedBy = "sys_user")
	public List<System_User_Organization> getListuo() {
		return listuo;
	}

	public void setListuo(List<System_User_Organization> listuo) {
		this.listuo = listuo;
	}

	@OneToMany(fetch=FetchType.LAZY,orphanRemoval=true, mappedBy = "sys_user")
	public List<System_Organization> getListOrga() {
		return listOrga;
	}

	public void setListOrga(List<System_Organization> listOrga) {
		this.listOrga = listOrga;
	}

	

}
