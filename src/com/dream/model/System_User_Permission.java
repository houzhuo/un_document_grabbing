package com.dream.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
/**
 * 用户权限中间表
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Entity
@Table(name="System_user_permission")
public class System_User_Permission extends AllId {

	private static final long serialVersionUID = 1L;
	
	private System_User sys_user;
	
	private System_Permission sys_perm;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade(CascadeType.PERSIST)
	public System_User getSys_user() {
		return sys_user;
	}

	public void setSys_user(System_User sys_user) {
		this.sys_user = sys_user;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade(CascadeType.PERSIST)
	public System_Permission getSys_perm() {
		return sys_perm;
	}

	public void setSys_perm(System_Permission sys_perm) {
		this.sys_perm = sys_perm;
	}
	
	

}
