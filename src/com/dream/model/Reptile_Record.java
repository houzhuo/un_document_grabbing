package com.dream.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="reptile_record")
@SuppressWarnings("all")
public class Reptile_Record extends AllId{

	private String startDate;
	
	private String endDate;
	
	private String allNum;
	
	private String reptlieNum;
	
	private String temp1;
	
	private String temp2;
	
	private String temp3;

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getAllNum() {
		return allNum;
	}

	public void setAllNum(String allNum) {
		this.allNum = allNum;
	}

	public String getReptlieNum() {
		return reptlieNum;
	}

	public void setReptlieNum(String reptlieNum) {
		this.reptlieNum = reptlieNum;
	}

	public String getTemp1() {
		return temp1;
	}

	public void setTemp1(String temp1) {
		this.temp1 = temp1;
	}

	public String getTemp2() {
		return temp2;
	}

	public void setTemp2(String temp2) {
		this.temp2 = temp2;
	}

	public String getTemp3() {
		return temp3;
	}

	public void setTemp3(String temp3) {
		this.temp3 = temp3;
	}
	
}
