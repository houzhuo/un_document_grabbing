package com.dream.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import net.sf.json.JsonConfig;

@SuppressWarnings("all")
@Entity
@Table(name = "file_entity")
public class File_Entity extends AllId  {
	
	private String path;

	private String uuidFileName;

	private String fileSize;

	private String fileType;

	private String fileName;

	private String status = "0";

	private String fileHttpUrl;

	private String fileLanguage;
	
	private String fileDocID;
	
	private String fileRange;
	
	private String fileDomain;
	
	private String fileAnnual;
	
	private String fileAgenda;
	
	private String fileJobID;
	
	private String filePublishDate;
	
	private String temp1;

	private String temp2;

	private String temp3;

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getUuidFileName() {
		return uuidFileName;
	}

	public void setUuidFileName(String uuidFileName) {
		this.uuidFileName = uuidFileName;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	@Type(type="text")
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFileHttpUrl() {
		return fileHttpUrl;
	}

	public void setFileHttpUrl(String fileHttpUrl) {
		this.fileHttpUrl = fileHttpUrl;
	}

	public String getFileLanguage() {
		return fileLanguage;
	}

	public void setFileLanguage(String fileLanguage) {
		this.fileLanguage = fileLanguage;
	}

	public String getFileDocID() {
		return fileDocID;
	}

	public void setFileDocID(String fileDocID) {
		this.fileDocID = fileDocID;
	}

	public String getFileRange() {
		return fileRange;
	}

	public void setFileRange(String fileRange) {
		this.fileRange = fileRange;
	}

	public String getFileDomain() {
		return fileDomain;
	}

	public void setFileDomain(String fileDomain) {
		this.fileDomain = fileDomain;
	}

	public String getFileAnnual() {
		return fileAnnual;
	}

	public void setFileAnnual(String fileAnnual) {
		this.fileAnnual = fileAnnual;
	}

	public String getFileAgenda() {
		return fileAgenda;
	}

	public void setFileAgenda(String fileAgenda) {
		this.fileAgenda = fileAgenda;
	}

	public String getFileJobID() {
		return fileJobID;
	}

	public void setFileJobID(String fileJobID) {
		this.fileJobID = fileJobID;
	}

	public String getFilePublishDate() {
		return filePublishDate;
	}

	public void setFilePublishDate(String filePublishDate) {
		this.filePublishDate = filePublishDate;
	}

	public String getTemp1() {
		return temp1;
	}

	public void setTemp1(String temp1) {
		this.temp1 = temp1;
	}

	public String getTemp2() {
		return temp2;
	}

	public void setTemp2(String temp2) {
		this.temp2 = temp2;
	}

	public String getTemp3() {
		return temp3;
	}

	public void setTemp3(String temp3) {
		this.temp3 = temp3;
	}

	
	
}
