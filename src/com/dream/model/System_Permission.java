package com.dream.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 权限
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Entity
@Table(name = "system_permission")
public class System_Permission extends AllId implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String permissionName;//权限名称
	
	private String cid;//id
	
	private String pid;//父级id
	
	private String icon;//图标
	
	private String url;//地址
	
	private String type;//类型
	
	private String orderNum;
	
	private String startUsing;//0:禁用      1：启用
	
	private int sort;//排序
	
	//预留字段
	private String temp1;
	
	private String temp2;
	
	private String temp3;
	
	private List<System_User_Permission> listup = new ArrayList<System_User_Permission>();

	private List<System_Role_Permission> listrp = new ArrayList<System_Role_Permission>();

	private List<System_Organization_Permission> listop = new ArrayList<System_Organization_Permission>();

	public String getPermissionName() {
		return permissionName;
	}

	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(String orderNum) {
		this.orderNum = orderNum;
	}

	public String getTemp1() {
		return temp1;
	}

	public void setTemp1(String temp1) {
		this.temp1 = temp1;
	}

	public String getTemp2() {
		return temp2;
	}

	public void setTemp2(String temp2) {
		this.temp2 = temp2;
	}

	public String getTemp3() {
		return temp3;
	}

	public void setTemp3(String temp3) {
		this.temp3 = temp3;
	}

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval=true, mappedBy = "sys_perm")
	public List<System_User_Permission> getListup() {
		return listup;
	}

	public void setListup(List<System_User_Permission> listup) {
		this.listup = listup;
	}

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval=true, mappedBy = "sys_perm")
	public List<System_Role_Permission> getListrp() {
		return listrp;
	}

	public void setListrp(List<System_Role_Permission> listrp) {
		this.listrp = listrp;
	}

	@OneToMany(fetch = FetchType.LAZY,orphanRemoval=true, mappedBy = "sys_perm")
	public List<System_Organization_Permission> getListop() {
		return listop;
	}

	public void setListop(List<System_Organization_Permission> listop) {
		this.listop = listop;
	}

	public String getStartUsing() {
		return startUsing;
	}

	public void setStartUsing(String startUsing) {
		this.startUsing = startUsing;
	}

	/** 
	 * 获取sort 
	 * @return sort sort 
	 */
	public int getSort() {
		return sort;
	}
	

	/** 
	 * 设置sort 
	 * @param sort sort 
	 */
	public void setSort(int sort) {
		this.sort = sort;
	}
	
	
}
