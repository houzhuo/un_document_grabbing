package com.dream.model;

import java.beans.Transient;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * 
* <p>Title: Operation_Log</p>
* <p>Description: 操作日志模型</p>
* @author z.q.k
* @date 上午11:25:10
 */
@SuppressWarnings("all")
@Entity
@Table(name="Operation_Log")
public class Operation_Log extends AllId{

	private String userEmployeeId;
	
	private System_User operator; //操作人
	
	private String roleName;//角色名称
	
	private String userName;//用户名称
	
	private String ip;//IP地址
	
	private String operationType; //操作类型
	
	private String tableName; //操作表
	
	private String operationContent;//操作内容

	private String operationResult; //操作结果
	
	private String temp1;
	
	private String temp2;
	
	private String temp3;
	
	private String startTime; //开始时间(查询时用)
	
	private String endTime; //结束时间(查询时用)
	
	private String proceedTime;
	
	private String assistContents;//辅助信息
	
	@Type(type="text")
	public String getAssistContents() {
		return assistContents;
	}

	public void setAssistContents(String assistContents) {
		this.assistContents = assistContents;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getOperationContent() {
		return operationContent;
	}

	public void setOperationContent(String operationContent) {
		this.operationContent = operationContent;
	}

	public String getUserEmployeeId() {
		return userEmployeeId;
	}

	public void setUserEmployeeId(String userEmployeeId) {
		this.userEmployeeId = userEmployeeId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getOperationResult() {
		return operationResult;
	}

	public void setOperationResult(String operationResult) {
		this.operationResult = operationResult;
	}

	public String getTemp1() {
		return temp1;
	}

	public void setTemp1(String temp1) {
		this.temp1 = temp1;
	}

	public String getTemp2() {
		return temp2;
	}

	public void setTemp2(String temp2) {
		this.temp2 = temp2;
	}

	public String getTemp3() {
		return temp3;
	}

	public void setTemp3(String temp3) {
		this.temp3 = temp3;
	}

	@Transient
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@Transient
	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getProceedTime() {
		return proceedTime;
	}

	public void setProceedTime(String proceedTime) {
		this.proceedTime = proceedTime;
	}

	@ManyToOne(targetEntity = System_User.class)
	@JoinColumn(name = "operator_id")
	public System_User getOperator() {
		return operator;
	}

	public void setOperator(System_User operator) {
		this.operator = operator;
	}

	
	
	
}
