package com.dream.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * 角色机构中间表
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Entity
@Table(name="system_role_permission")
public class System_Role_Permission extends AllId{

	private static final long serialVersionUID = 1L;
	
	private System_Role sys_role;
	
	private System_Permission sys_perm;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade(CascadeType.PERSIST)
	public System_Role getSys_role() {
		return sys_role;
	}

	public void setSys_role(System_Role sys_role) {
		this.sys_role = sys_role;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade(CascadeType.PERSIST)
	public System_Permission getSys_perm() {
		return sys_perm;
	}

	public void setSys_perm(System_Permission sys_perm) {
		this.sys_perm = sys_perm;
	}	
	
	
}
