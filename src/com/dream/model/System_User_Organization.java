package com.dream.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * 用户机构中间表
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Entity
@Table(name="system_user_organization")
public class System_User_Organization extends AllId{

	private static final long serialVersionUID = 1L;
	
	private System_User sys_user;
	
	private System_Organization sys_orga;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade(CascadeType.PERSIST)
	public System_User getSys_user() {
		return sys_user;
	}

	public void setSys_user(System_User sys_user) {
		this.sys_user = sys_user;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade(CascadeType.PERSIST)
	public System_Organization getSys_orga() {
		return sys_orga;
	}

	public void setSys_orga(System_Organization sys_orga) {
		this.sys_orga = sys_orga;
	}
	
	
}
