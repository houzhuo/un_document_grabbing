package com.dream.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * 用户角色中间表
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Entity
@Table(name="system_user_role")
public class System_User_Role extends AllId{
	private static final long serialVersionUID = 1417150285513441412L;
	 

	private System_User sys_user;

	private System_Role sys_role;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade(CascadeType.PERSIST)
	public System_User getSys_user() {
		return sys_user;
	}

	public void setSys_user(System_User sys_user) {
		this.sys_user = sys_user;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade(CascadeType.PERSIST)
	public System_Role getSys_role() {
		return sys_role;
	}

	public void setSys_role(System_Role sys_role) {
		this.sys_role = sys_role;
	}
}
