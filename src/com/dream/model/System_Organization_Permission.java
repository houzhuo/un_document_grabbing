package com.dream.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * 机构权限中间表
 * @author zlj
 *
 */
@SuppressWarnings("all")
@Entity
@Table(name = "system_organization_permission")
public class System_Organization_Permission extends AllId{

	private static final long serialVersionUID = 1L;
	
	private System_Organization sys_orga;
	
	private System_Permission sys_perm;

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade(CascadeType.PERSIST)
	public System_Organization getSys_orga() {
		return sys_orga;
	}

	public void setSys_orga(System_Organization sys_orga) {
		this.sys_orga = sys_orga;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@Cascade(CascadeType.PERSIST)
	public System_Permission getSys_perm() {
		return sys_perm;
	}

	public void setSys_perm(System_Permission sys_perm) {
		this.sys_perm = sys_perm;
	}

	
}
